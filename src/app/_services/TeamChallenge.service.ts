import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,forkJoin} from 'rxjs';

const CHALLENGE_API = 'http://localhost:8203/TeamChallengeManager/';
const CHALLENGESOLUTION_API = 'http://localhost:8203/TeamSolution/';
const SOLUTIONLINK_API = 'http://localhost:8203/TeamSolutionLink/';
const SOLUTIONLINKKEYPOINT_API = 'http://localhost:8203/TeamSolutionKeypoints/';
const SOLUTIONSHARE_API = 'http://localhost:8203/ShareTeamSolution/';
const CHALLENGEAPPROACH_API = 'http://localhost:8203/TeamApproach/';
const APPROACHLINK_API = 'http://localhost:8203/TeamApproachLink/';
const APPROACHLINKKEYPOINT_API = 'http://localhost:8203/TeamApproachKeypoints/';
const APPROACHSHARE_API = 'http://localhost:8203/TeamApproachShare/';
const TEAMCHALLENGESHARE_API = 'http://localhost:8203/TeamChallengeShare/';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    
    };

    @Injectable({
        providedIn: 'root'
        })
        export class TeamChallengeService {
        
        constructor(private http: HttpClient) { }


        addTopic(teamchallenge) {

            return this.http.post(CHALLENGE_API + 'add', teamchallenge);
            
            }
            updateteamchallenge(teamchallengeId,teamchallenge: any) {
            return this.http.put(CHALLENGE_API + 'updateTeamChallenge/' 
            +teamchallengeId, teamchallenge);
            }
            getChallenge(userid,teamId) {
            return this.http.get(CHALLENGE_API + 'get/'+ userid+'/'+ teamId);
            
            }
            findByTeamChallengeName(teamchallengeName:String,UserId:number)
            {
            return this.http.get(CHALLENGE_API+'getTeamChallengeByName/'+teamchallengeName+'/'+UserId)
            }
            
            deleteTeamChallenge(teamchallengeId): Observable<any> {
            return this.http.delete(CHALLENGE_API + 'deleteTeamChallengeManager/'+ teamchallengeId ,{ responseType: 'text' });
            }
            
            AddTeamSolution(teamsolution) {
                return this.http.post(CHALLENGESOLUTION_API + 'add', teamsolution);
                }
                
            updateSolution(teamsolutionId,teamsolution: any) {
                return this.http.put(CHALLENGESOLUTION_API + 'updateTeamSolution/' 
                +teamsolutionId, teamsolution);
                }
            getTeamChallengeSolution(teamchallengeId) {
                return this.http.get(CHALLENGESOLUTION_API + 'getteamsolution/'+teamchallengeId);
                }
            // findBySolutionName(teamsolutionName:String,UserId:number){
            //     return this.http.get(CHALLENGESOLUTION_API+'getSolutionByName/'+teamsolutionName+'/'+UserId)
            //     }
            deleteSolution(teamsolutionId){
                return this.http.delete(CHALLENGESOLUTION_API + 'deleteTeamSolution/'+ teamsolutionId ,{ responseType: 'text' });
                }

            
  AddTeamSolutionLink(teamsolutionlink) {

    return this.http.post(SOLUTIONLINK_API + 'add', teamsolutionlink);
                    
     }
                   
     updatesolutionLink(teamsolutionLinkId,teamsolutionLink: any)
    {
    return this.http.put(SOLUTIONLINK_API + 'updateTeamSolutionLink/' 
    +teamsolutionLinkId, teamsolutionLink);
    }
    findBySolutionLink(link:String,UserId:number)
    {
    return this.http.get(SOLUTIONLINK_API+'getTeamSolutionLinkByName/'+link+'/'+UserId)
    }
    getteamsolutionlink(teamsolutionId) {
        return this.http.get(SOLUTIONLINK_API + 'get/'+teamsolutionId);
        
        }
       
        deleteTeamSolutionlink(teamsolutionlinkId): Observable<any> {
        return this.http.delete(SOLUTIONLINK_API + 'deleteTeamSolutionLink/'+ teamsolutionlinkId ,{ responseType: 'text' });
     }


     addteamsolutionkeypoints(teamsolutionkeypoints) {

        return this.http.post(SOLUTIONLINKKEYPOINT_API + 'add/', teamsolutionkeypoints);
        
        }
        getteamsolutionkeypoint(teamsolutionlinkId){
            return this.http.get(SOLUTIONLINKKEYPOINT_API + 'get/'+ teamsolutionlinkId);
        }
        deleteKeypoint(teamkeypointId) {
            return this.http.delete(SOLUTIONLINKKEYPOINT_API + 'deleteTeamKeypoint/'+ teamkeypointId,{ responseType: 'text' });
        }
        updateKeypoint(teamkeypointId,keypointDetails){
            return this.http.put(SOLUTIONLINKKEYPOINT_API + 'updateKeypoint/'+ teamkeypointId,keypointDetails);
        }
        addteamsharesolution(shareteamsolution) {

            return this.http.post(SOLUTIONSHARE_API + 'add', shareteamsolution);
            
            }
            addApproach(teamapproach) {
                return this.http.post(CHALLENGEAPPROACH_API + 'add', teamapproach);
                }
                
            updateApproach(teamapproachId,teamapproach: any) {
                return this.http.put(CHALLENGEAPPROACH_API + 'updateTeamApproach/' 
                +teamapproachId, teamapproach);
                }
            getApproach(teamchallengeId) {
                return this.http.get(CHALLENGEAPPROACH_API + 'getteamapproach/'+teamchallengeId);
                }
            findByApproachName(teamapproachName:String,UserId:number){
                return this.http.get(CHALLENGEAPPROACH_API+'getApproachByName/'+teamapproachName+'/'+UserId)
                }
        deleteApproach(teamapproachId): Observable<any>
        {
          return this.http.delete(CHALLENGEAPPROACH_API + 'deleteTeamApproach/'+ teamapproachId ,{ responseType: 'text' });
           }

           addapproachlink(teamapproachlink) {

            return this.http.post(APPROACHLINK_API + 'add', teamapproachlink);
                            
         }
         getApproachlink(teamapproachId) {
            return this.http.get(APPROACHLINK_API + 'get/'+ teamapproachId);
            
            }
            deleteApproachlink(teamapproachlinkId): Observable<any> {
                return this.http.delete(APPROACHLINK_API + 'deleteTeamApproachLink/'+ teamapproachlinkId ,{ responseType: 'text' });
             }
             addapproachkeypoints(teamapproachkeypoints) {

                return this.http.post(APPROACHLINKKEYPOINT_API + 'add/', teamapproachkeypoints);
                
                }
                getapproachkeypoint(teamapproachlinkId){
                    return this.http.get(APPROACHLINKKEYPOINT_API + 'get/'+ teamapproachlinkId);
                }
                deleteKeypoints(teamkeypointId) {
                    return this.http.delete(APPROACHLINKKEYPOINT_API + 'deleteTeamKeypoint/'+ teamkeypointId,{ responseType: 'text' });
                }
                updateKeypoints(teamKeypointId,keypointDetails){
                    return this.http.put(APPROACHLINKKEYPOINT_API + 'updateKeypoint/'+ teamKeypointId,keypointDetails);
                }
                addshareapproach(shareteamapproach) {

                    return this.http.post(APPROACHSHARE_API + 'add', shareteamapproach);
                    
                    }

                    addteamChallengeshare(teamChallengeShare) {

                        return this.http.post(TEAMCHALLENGESHARE_API + 'addteamchallengeshare/', teamChallengeShare);
                        
                        }

        }
    
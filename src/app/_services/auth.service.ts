import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../_model/user';
import { isNullOrUndefined } from 'util';
const USER_KEY = 'auth-user';
const AUTH_API = 'http://localhost:8200/api/auth/';
const baseUrl = 'http://localhost:8200/api/auth/findall';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })

};

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private baseUrl = 'http://localhost:8200/api/auth/';
 
  constructor(private http: HttpClient) { }

  login(credentials): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      email: credentials.email,
      password: credentials.password
    }, httpOptions);
  }

 

  register(user): Observable<any> {
    
    return this.http.post(AUTH_API + 'signup', {
      username: user.username,
      email: user.email,
      phoneNumber:user.phoneNumber,

      password: user.password
    }, httpOptions);
  }
  confirmEmail(model): Observable<any> {
    return this.http.get(AUTH_API + 'confirm/'+ model,
    {responseType:'text'});
  }
  loginsms(email): Observable<any> {
    return this.http.get(AUTH_API + 'logimSmsOTP/'+ email,
    {responseType:'text'});
  }
  verifyphoneOTP(phoneOTPcode) {
    return this.http.get(AUTH_API + 'getUserbyOTP/'+ phoneOTPcode,
    {responseType:'text'});
  }
  verifySessionphoneOTP(phoneOTP) {
    return this.http.get(AUTH_API + 'verifySessionphoneOTP/'+ phoneOTP,
    {responseType:'text'});
  }

smsphCode(model): Observable<any> {
    return this.http.post(AUTH_API + 'sms/',
    {phonenumber: model.phonenumber,message:model.message},httpOptions);
   
  }
  changePassword(resetToken,model: any) {
    return this.http.post(AUTH_API + 'updatepassword/'   
    +resetToken, model);
  }

  changeUser(id, model: any) {
    return this.http.put(AUTH_API + 'updateduser/'+id, model);
  }

//  ADDED BY KARTHIKA
  updateUserInfo(id, model: any) {
    return this.http.put(AUTH_API + 'findalluser/'+id, model);
  }


   requestresetEmail(email): Observable<any> {
    return this.http.get(AUTH_API + 'requestresetEmail/'
     +email ,
     httpOptions);
  }
  public getuserbytoken(resettoken){
    return this.http.get(AUTH_API + 'getuserbytoken/'
      +resettoken,
    { responseType: 'text' });
  }

  
  get(id): Observable<any> {
    return this.http.get(AUTH_API+ 'updateuser/'+id);
  }

  getuserList(id): Observable<any> {
    return this.http.get(AUTH_API+ 'getUsers/'+id);
  }
 
 
  public getUser() {
    return JSON.parse(sessionStorage.getItem(USER_KEY));
  }

 
  public getAllUserlist(friendslist:any=[]):Observable<any>{
    
    return this.http.post(AUTH_API + 'findallbyid/',
    friendslist);
 
  }

 /* ValidPasswordToken(body): Observable<any> {
    return this.http.post(AUTH_API+'valid-password-token', body);
  }*/

  setUser(user: User): void {
    let user_string = JSON.stringify(user);
    localStorage.setItem("currentUser", user_string);
  }

  getCurrentUser(): User {
    let user_string = localStorage.getItem("currentUser");
    if (!isNullOrUndefined(user_string)) {
      let user: User = JSON.parse(user_string);
      return user;
    } else {
      return null;
    }
  }

  // getEmployee(id: number): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${id}`);
  // }
}


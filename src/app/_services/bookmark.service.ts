import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, forkJoin, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Bookmarklink } from '../_model/bookmarklink';


const BOOKMARK_API = 'http://localhost:8201/Bookmark/';
const BOOKMARKLINK_API = 'http://localhost:8201/Bookmarklink/';
const KEYPOINT_API = 'http://localhost:8201/Keypoint/';

const CHAIN_API = 'http://localhost:8080/chainrequest/';
const AUTH_API = 'http://localhost:8200/api/auth/';
const BOOKMARKSHARE_API = 'http://localhost:8201/BookmarkShare/';
const STEPKEYPOINT_API = 'http://localhost:8201/StepLinkKeypoint/';
const GOALKEYPOINT_API = 'http://localhost:8201/GoalKeypoint/';
const SOLUTIONKEYPOINT_API = 'http://localhost:8201/SolutionKeyPoints/';
const APPROACHKEYPOINT_API = 'http://localhost:8201/ApproachKeyPoints/';
const SILLKEYPOINT_API = 'http://localhost:8201/SkillKeypoint/';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })

};

@Injectable({
    providedIn: 'root'
})
export class BookmarkService {


    constructor(private http: HttpClient) {
     
    }

    getBookmarklink(bookmarkCategoryId) {
        return this.http.get(BOOKMARKLINK_API + 'getBookmarkLink/' + bookmarkCategoryId).pipe(
            map(
                (data) => {
                    localStorage.setItem("bookmarkdata", JSON.stringify(data));
                    console.log("data", data)
                    return data
                }
            )
        )
    }
    addBookmark(bookmark) {

        return this.http.post(BOOKMARK_API + 'add', bookmark);

    }
    addKeypoint(keypoint) {

        return this.http.post(KEYPOINT_API + 'add', keypoint);

    }
    addBookmarklink(bookmarklink) {

        return this.http.post(BOOKMARKLINK_API + 'addbookmarklink', bookmarklink);

    }
    updateKeypoint(keypointId, Keypoint: any) {
        return this.http.put(KEYPOINT_API + 'updateKeypoint/'
            + keypointId, Keypoint);
    }
    updatebookmark(bookmarkId, bookmark: any) {
        return this.http.put(BOOKMARK_API + 'updateBookmark/'
            + bookmarkId, bookmark);
    }
    updatebookmarkLink(bookmarkLinkId, bookmarkLink: any) {
        return this.http.put(BOOKMARKLINK_API + 'updateBookmarklink/'
            + bookmarkLinkId, bookmarkLink);
    }
    getBookmark(userid) {
        return this.http.get(BOOKMARK_API + 'get/' + userid);

    }
    getfriendlist(userid) {
        return this.http.get(CHAIN_API + 'getfriendlist/' + userid);

    }

    findByBookmarkName(bookmarkName: String, UserId: number) {
        return this.http.get(BOOKMARK_API + 'getBookmarkByName/' + bookmarkName + '/' + UserId)
    }
    findByBookmarkLink(link: String, UserId: number) {
        return this.http.get(BOOKMARKLINK_API + 'findbyBookmarkLink/' + link + '/' + UserId)
    }
    // getAllUserlist(id: number): Observable<any> {
    // return Observable.forkjoin([
    // this.http.get('/api/authors/' + id).map(res => res.json()),
    // this.http.get('/api/authors/' + id + '/books').map(res => res.json())
    // ])
    // .map((data: any[]) => {
    // let author: any = data[0];
    // let books: any[] = data[1];
    // return author.books = books;
    // });
    //}

    getGoalKeypoint(skilllinkId) {
        return this.http.get(GOALKEYPOINT_API + 'getShareKeyPoints/' + skilllinkId);

    }

    addGoalKeypoint(goalKeypoint) {

        return this.http.post(GOALKEYPOINT_API + 'add', goalKeypoint);

    }


    addstepKeypoint(keypoint) {

        return this.http.post(STEPKEYPOINT_API + 'add', keypoint);

    }
    getStepKeypoint(steplinkId) {
        return this.http.get(STEPKEYPOINT_API + 'get/' + steplinkId);

    }



    getSolutionKeypoint(solutionlinkId) {
        return this.http.get(SOLUTIONKEYPOINT_API + 'get/' + solutionlinkId);

    }


    addsolutionKeypoint(solutionkeypoints) {

        return this.http.post(SOLUTIONKEYPOINT_API + 'add', solutionkeypoints);

    }




    getApproachKeypoint(approachlinkId) {
        return this.http.get(APPROACHKEYPOINT_API + 'get/' + approachlinkId);

    }


    addapproachKeypoint(approachkeypoints) {

        return this.http.post(APPROACHKEYPOINT_API + 'add', approachkeypoints);

    }




    getKeypoint(bookmarkLinkId) {
        return this.http.get(KEYPOINT_API + 'get/' + bookmarkLinkId);

    }



    getBookmarklink1(bookmarkCategoryId) {
        return this.http.get(BOOKMARKLINK_API + 'getBookmarkLink/' + bookmarkCategoryId);

    }
    deleteKeypoint(keypointId): Observable<any> {
        return this.http.delete(KEYPOINT_API + 'deleteKeypoint/' + keypointId, { responseType: 'text' });
    }
    deleteBookmarklink(bookmarklinkId): Observable<any> {
        return this.http.delete(BOOKMARKLINK_API + 'deleteBookmarklink/' + bookmarklinkId, { responseType: 'text' });
    }
    deleteBookmarklinkByCategory(bookmarkId): Observable<any> {
        return this.http.delete(BOOKMARKLINK_API + 'deleteBookmarklinkByCategory/' + bookmarkId, { responseType: 'text' });
    }
    deleteBookmark(bookmarkId): Observable<any> {
        return this.http.delete(BOOKMARK_API + 'deleteBookmark/' + bookmarkId, { responseType: 'text' });
    }
    deleteSharedBookmark(shareId): Observable<any> {
        return this.http.delete(BOOKMARKSHARE_API + 'deleteSharedBookmark/' + shareId, { responseType: 'text' });

    }

    addsharebookmark(sharebookmark) {

        return this.http.post(BOOKMARKSHARE_API + 'add', sharebookmark);

    }
    getSkillsKeypoint(skilllinkId) {
        return this.http.get(SILLKEYPOINT_API + 'get/' + skilllinkId);

    }

    addSkillKeypoint(skillkeypoint) {

        return this.http.post(SILLKEYPOINT_API + 'add', skillkeypoint);

    }
}

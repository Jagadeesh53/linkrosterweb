import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrl = 'http://localhost:8202/api/';


@Injectable({
  
  providedIn: 'root'
})
export class ChainService {

  
  constructor(private http: HttpClient) { }

  getAll(chainuserid) {
    return this.http.get(baseUrl + 'getchainlist/'+chainuserid);
  }
  showPendingList(chainuserid) :Observable<any>{
  
    return this.http.get(baseUrl + 'getchainlist/'+chainuserid);
    

  }
  get(id) {
    return this.http.get(`${baseUrl}/${id}`);
  }

  getFriend(userid) {
    return this.http.get(baseUrl + 'getMyfriendslist/'+userid);
  }

  

  create(data) {
    return this.http.post(baseUrl, data);
  }

  updated(id) {
    return this.http.get(baseUrl + 'updateChain/'+id);
  }
  reject(id) {
    return this.http.get(baseUrl + 'rejectChain/'+id);
  }


  deleteRequest(id): Observable<any> {
    return this.http.delete(baseUrl + 'deletechain/'+ id ,{ responseType: 'text' });
    }

  delete(id) {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll() {
    return this.http.delete(baseUrl);
  }

  findByUsername(username) {
    return this.http.get(`${baseUrl}?username=${username}`);
  }

  
}


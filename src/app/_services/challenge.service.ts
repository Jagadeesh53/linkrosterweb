import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,forkJoin} from 'rxjs';


const CHALLENGE_API = 'http://localhost:8201/ChallengeManager/';
const CHALLENGESOLUTION_API = 'http://localhost:8201/Solution/';
const CHALLENGEAPPROACH_API = 'http://localhost:8201/Approach/';
const SOLUTIONLINK_API = 'http://localhost:8201/SolutionLink/';
const APPROACHLINK_API = 'http://localhost:8201/ApproachLink/';
const SOLUTIONLINKKEYPOINT_API = 'http://localhost:8201/SolutionKeyPoints/';
const SOLUTIONSHARE_API = 'http://localhost:8201/SolutionShare/';
const APPROACHLINKKEYPOINT_API = 'http://localhost:8201/ApproachKeyPoints/';
const APPROACHSHARE_API = 'http://localhost:8201/ApproachShare/';
const CHALLENGESHARE_API = 'http://localhost:8201/Challengeshare/';
const httpOptions = {
headers: new HttpHeaders({ 'Content-Type': 'application/json' })

};

@Injectable({
providedIn: 'root'
})
export class ChallengeService {

constructor(private http: HttpClient) { }

addTopic(challenge) {

return this.http.post(CHALLENGE_API + 'add', challenge);

}
updatechallenge(challengeId,challenge: any) {
return this.http.put(CHALLENGE_API + 'updateChallenge/' 
+challengeId, challenge);
}
getChallenge(userid) {
return this.http.get(CHALLENGE_API + 'get/'+ userid);

}
findByChallengeName(challengeName:String,UserId:number)
{
return this.http.get(CHALLENGE_API+'getChallengeByName/'+challengeName+'/'+UserId)
}

deleteChallenge(challengeId): Observable<any> {
return this.http.delete(CHALLENGE_API + 'deleteChallengeManager/'+ challengeId ,{ responseType: 'text' });
}


addSolution(solution) {
    return this.http.post(CHALLENGESOLUTION_API + 'add', solution);
    }
    
updateSolution(solutionId,solution: any) {
    return this.http.put(CHALLENGESOLUTION_API + 'updateSolution/' 
    +solutionId, solution);
    }
getSolution(challengeId) {
    return this.http.get(CHALLENGESOLUTION_API + 'getsolution/'+challengeId);
    }
findBySolutionName(solutionName:String,UserId:number){
    return this.http.get(CHALLENGESOLUTION_API+'getSolutionByName/'+solutionName+'/'+UserId)
    }
deleteSolution(solutionId): Observable<any> {
    return this.http.delete(CHALLENGESOLUTION_API + 'deleteSolution/'+ solutionId ,{ responseType: 'text' });
    }


    addApproach(approach) {
        return this.http.post(CHALLENGEAPPROACH_API + 'add', approach);
        }
        
    updateApproach(approachId,approach: any) {
        return this.http.put(CHALLENGEAPPROACH_API + 'updateApproach/' 
        +approachId, approach);
        }
    getApproach(challengeId) {
        return this.http.get(CHALLENGEAPPROACH_API + 'getapproach/'+challengeId);
        }
    findByApproachName(approachName:String,UserId:number){
        return this.http.get(CHALLENGEAPPROACH_API+'getApproachByName/'+approachName+'/'+UserId)
        }
deleteApproach(approachId): Observable<any>
{
  return this.http.delete(CHALLENGEAPPROACH_API + 'deleteApproach/'+ approachId ,{ responseType: 'text' });
   }

       
 addsolutionlink(solutionlink) {

return this.http.post(SOLUTIONLINK_API + 'add', solutionlink);
                
 }
               
updatesolutionLink(solutionLinkId,solutionLink: any)
{
return this.http.put(SOLUTIONLINK_API + 'updateSolutionlink/' 
+solutionLinkId, solutionLink);
}
findBySolutionLink(link:String,UserId:number)
{
return this.http.get(SOLUTIONLINK_API+'findbySolutionLink/'+link+'/'+UserId)
}
 getSolutionlink(solutionId) {
    return this.http.get(SOLUTIONLINK_API + 'get/'+ solutionId);
    
    }
   
deleteSolutionLink(solutionlinkId): Observable<any> {
    return this.http.delete(SOLUTIONLINK_API + 'deleteSolutionLink/'+ solutionlinkId ,{ responseType: 'text' });
 }
deleteSolutionlinkByCategory(challengeId): Observable<any> {
    return this.http.delete(SOLUTIONLINK_API + 'deleteSolutionlinkByCategory/'+ challengeId ,{ responseType: 'text' });
 }
 addapproachlink(approachlink) {

    return this.http.post(APPROACHLINK_API + 'add', approachlink);
                    
 }
 getApproachlink(approachId) {
    return this.http.get(APPROACHLINK_API + 'get/'+ approachId);
    
    }
    deleteApproachlink(approachlinkId): Observable<any> {
        return this.http.delete(APPROACHLINK_API + 'deleteApproachLink/'+ approachlinkId ,{ responseType: 'text' });
     }
    addapproachkeypoints(approachkeypoints) {

        return this.http.post(APPROACHLINKKEYPOINT_API + 'add/', approachkeypoints);
        
        }
        getapproachkeypoint(approachlinkId){
            return this.http.get(APPROACHLINKKEYPOINT_API + 'get/'+ approachlinkId);
        }
        deleteKeypoints(keypointId) {
            return this.http.delete(APPROACHLINKKEYPOINT_API + 'deleteKeypoint/'+ keypointId,{ responseType: 'text' });
        }
        updateKeypoints(KeypointId,keypointDetails){
            return this.http.put(APPROACHLINKKEYPOINT_API + 'updateKeypoint/'+ KeypointId,keypointDetails);
        }
    addsolutionkeypoints(solutionkeypoints) {

        return this.http.post(SOLUTIONLINKKEYPOINT_API + 'add/', solutionkeypoints);
        
        }
        getsolutionkeypoint(solutionlinkId){
            return this.http.get(SOLUTIONLINKKEYPOINT_API + 'get/'+ solutionlinkId);
        }
        deleteKeypoint(keypointId) {
            return this.http.delete(SOLUTIONLINKKEYPOINT_API + 'deleteKeypoint/'+ keypointId,{ responseType: 'text' });
        }
        updateKeypoint(solutionKeypointId,keypointDetails){
            return this.http.put(SOLUTIONLINKKEYPOINT_API + 'updateKeypoint/'+ solutionKeypointId,keypointDetails);
        }
        addsharesolution(solutionShare) {

            return this.http.post(SOLUTIONSHARE_API + 'add', solutionShare);
            
            }

            addshareapproach(approachShare) {

                return this.http.post(APPROACHSHARE_API + 'add', approachShare);
                
                }


                addshareChallenge(challengeshare)
{
    return this.http.post(CHALLENGESHARE_API + 'addsharechallenge' , challengeshare);
}
        
}



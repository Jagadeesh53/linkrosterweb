import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';


const GOAL_API = 'http://localhost:8201/GoalManager/';
const GOALSTEP_API = 'http://localhost:8201/GoalStep/';
const GOALSKILL_API = 'http://localhost:8201/GoalSkill/';
const STEPLINK_API = 'http://localhost:8201/StepLink/';
const SKILLLINK_API = 'http://localhost:8201/SkillLink/';
const STEPLINKKEYPOINT_API = 'http://localhost:8201/StepLinkKeypoint/';
const SKILLKEYPOINT_API = 'http://localhost:8201/SkillKeypoint/';
const STEPLINKSHARE_API = 'http://localhost:8201/StepLinkShare/';
const SKILLLINKSHARE_API = 'http://localhost:8201/SkillLinkShare/';
const GOALSHARE_API = 'http://localhost:8201/Goalshare/';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })

};

@Injectable({
    providedIn: 'root'
})
export class GoalService {

    constructor(private http: HttpClient) { }

    addGoal(goal) {
        return this.http.post(GOAL_API + 'add', goal);
    }
    updateGoal(goalId, goal: any) {
        return this.http.put(GOAL_API + 'updateGoal/'
            + goalId, goal);
    }
    getGoal(userid) {
        return this.http.get(GOAL_API + 'get/' + userid);
    }
    findByGoalName(goalName: String, UserId: number) {
        return this.http.get(GOAL_API + 'getGoalByName/' + goalName + '/' + UserId)
    }
    deleteGoal(goalId): Observable<any> {
        return this.http.delete(GOAL_API + 'deleteGoal/' + goalId, { responseType: 'text' });
    }

    addgoalStep(goalstep) {
        return this.http.post(GOALSTEP_API + 'add', goalstep);
    }

    updategoalStep(stepId, goalstep: any) {
        return this.http.put(GOALSTEP_API + 'updateStep/' + stepId, goalstep);
    }
    getgoalStep(goalId) {
        return this.http.get(GOALSTEP_API + 'getstep/' + goalId);
    }
    findByStepName(stepName: String, UserId: number) {
        return this.http.get(GOALSTEP_API + 'getStepByName/' + stepName + '/' + UserId)
    }
    deleteStep(stepId): Observable<any> {
        return this.http.delete(GOALSTEP_API + 'deleteStep/' + stepId, { responseType: 'text' });
    }
    addstepLink(link) {
        return this.http.post(STEPLINK_API + 'add', link);
    }
    getstepLink(stepId) {
        return this.http.get(STEPLINK_API + 'get/' + stepId);
    }
    findBystepLink(steplink: String, UserId: number) {
        return this.http.get(STEPLINK_API + 'findbyStepLink/' + steplink + '/' + UserId)
    }
    deletestepLink(steplinkId): Observable<any> {
        return this.http.delete(STEPLINK_API + 'deleteSteplink/' + steplinkId, { responseType: 'text' });
    }
    deletesteplinkBystep(stepId) {
        return this.http.delete(STEPLINK_API + 'deletesteplinkBystep/' + stepId, { responseType: 'text' });
    }

    addgoalSkill(skill) {
        return this.http.post(GOALSKILL_API + 'add', skill);
    }
    getgoalSkill(goalId) {
        return this.http.get(GOALSKILL_API + 'getskill/' + goalId);
    }
    findBySkillName(skillName: String, UserId: number) {
        return this.http.get(GOALSKILL_API + 'getSkillByName/' + skillName + '/' + UserId)
    }
    updateskill(skillId, goalskill: any) {
        return this.http.put(GOALSKILL_API + 'updateSkill/' + skillId, goalskill);
    }
    deleteSkill(skillId): Observable<any> {
        return this.http.delete(GOALSKILL_API + 'deleteSkill/' + skillId, { responseType: 'text' });
    }

    addskillLink(skilllink) {
        return this.http.post(SKILLLINK_API + 'add', skilllink);
    }
    getskilllink(skillId) {
        return this.http.get(SKILLLINK_API + 'get/' + skillId);
    }
    findByskilllink(skilllink: String, UserId: number) {
        return this.http.get(SKILLLINK_API + 'findbySkillLink/' + skilllink + '/' + UserId)
    }
    deleteskilllink(skilllinkId): Observable<any> {
        return this.http.delete(SKILLLINK_API + 'deleteSkilllink/' + skilllinkId, { responseType: 'text' });
    }
    addstepKeypoint(keypoint) {
        return this.http.post(STEPLINKKEYPOINT_API + 'add', keypoint);
    }
    getskillKeypoint(steplinkId) {
        return this.http.get(SKILLKEYPOINT_API + 'get/' + steplinkId);
    }
    deleteskillKeypoint(keypointId): Observable<any> {
        return this.http.delete(SKILLKEYPOINT_API + 'deleteKeypoint/' + keypointId, { responseType: 'text' });
    }
    updateskillKeypoint(keypointId, Keypoint: any) {
        return this.http.put(SKILLKEYPOINT_API + 'updateKeypoint/' + keypointId, Keypoint);
    }

    addskillKeypoint(keypoint) {
        return this.http.post(SKILLKEYPOINT_API + 'add', keypoint);
    }
    getstepKeypoint(steplinkId) {
        return this.http.get(STEPLINKKEYPOINT_API + 'get/' + steplinkId);
    }
    deletestepKeypoint(skillkeypointId): Observable<any> {
        return this.http.delete(STEPLINKKEYPOINT_API + 'deleteKeypoint/' + skillkeypointId, { responseType: 'text' });
    }
    updatestepKeypoint(skillkeypointId, Keypoint: any) {
        return this.http.put(STEPLINKKEYPOINT_API + 'updateKeypoint/' + skillkeypointId, Keypoint);
    }
    addsharestep(sharestep) {
        return this.http.post(STEPLINKSHARE_API + 'add', sharestep);
    }
    addshareskill(shareskill) {
        return this.http.post(SKILLLINKSHARE_API + 'add', shareskill);
    }

    addshareGoal(goalshare) {
        return this.http.post(GOALSHARE_API + 'addsharegoal', goalshare);
    }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,forkJoin} from 'rxjs';


const LEARN_API = 'http://localhost:8201/LearnManager/';
const CONCEPT_API = 'http://localhost:8201/Concept/';
const CONCEPTLINK_API = 'http://localhost:8201/ConceptLink/';
const KEYPOINT_CONCEPT = 'http://localhost:8201/ConceptKeypoint/';
const SHARECONCEPT_API = 'http://localhost:8201/ConceptShare/';
const TOPIC_API= 'http://localhost:8201/Topicshare/';
const httpOptions = {
headers: new HttpHeaders({ 'Content-Type': 'application/json' })

};

@Injectable({
providedIn: 'root'
})
export class LearnService {

constructor(private http: HttpClient) { }

addTopic(learn) {

return this.http.post(LEARN_API + 'add', learn);

}
updatelearn(learnId,learn: any) {
return this.http.put(LEARN_API + 'updateLearn/' 
+learnId, learn);
}
getLearn(userid) {
return this.http.get(LEARN_API + 'get/'+ userid);

}
findByLearnName(learnName:String,UserId:number)
{
return this.http.get(LEARN_API+'getLearnByName/'+learnName+'/'+UserId)
}

deleteLearn(learnId): Observable<any> {
return this.http.delete(LEARN_API + 'deleteLearn/'+ learnId ,{ responseType: 'text' });
}

addConcept(concept) {
    return this.http.post(CONCEPT_API + 'addConcept/', concept);
    }
    
updateConcept(conceptId,concept: any) {
    return this.http.put(CONCEPT_API + 'updateConcept/' +conceptId, concept);
    }
   


getconcept(conceptCategoryId) {
    return this.http.get(CONCEPT_API + 'getconcepts/'+ conceptCategoryId);
    }


deleteConcept(conceptId): Observable<any> {
    return this.http.delete(CONCEPT_API + 'deleteConcept/'+ conceptId ,{ responseType: 'text' });
    }

    addconceptLink(conceptlink){
        return this.http.post(CONCEPTLINK_API + 'addConceptLink/',conceptlink);
    }
    getConceptLink(conceptId) {
        return this.http.get(CONCEPTLINK_API + 'getConceptLink/'+ conceptId);
    }
    deleteconceptlink(conceptlinkId){
        return this.http.delete(CONCEPTLINK_API + 'deleteConceptLink/'+ conceptlinkId,{ responseType: 'text' });
    }

    addconceptkeypoints(keypoint) {
        return this.http.post(KEYPOINT_CONCEPT + 'add/', keypoint);
    }
    addshareconcept(conceptShare){
        return this.http.post(SHARECONCEPT_API + 'add/',conceptShare);
    }
    updateKeypoint(conceptKeypointId,keypointDetails){
        return this.http.put(KEYPOINT_CONCEPT + 'updateKeypoint/'+ conceptKeypointId,keypointDetails);
    }
    getConceptkeypoint(conceptlinkId) {
        return this.http.get(KEYPOINT_CONCEPT + 'get/'+ conceptlinkId);
    }
    deleteKeypoint(conceptKeypointId) {
        return this.http.delete(KEYPOINT_CONCEPT + 'deleteKeypoint/'+ conceptKeypointId,{ responseType: 'text' });
    }

    addshareTopic(learnShare){
        return this.http.post(TOPIC_API + 'addshareTopic/', learnShare)
    }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable,forkJoin} from 'rxjs';


// const IMAGE_API = 'http://localhost:8200/Image/'
// const AUTH_API = 'http://localhost:8200/api/auth/';
const TEST_API = 'http://localhost:8200/api/test/';


const IMAGE_API = 'http://localhost:8200/Image/'
const AUTH_API = 'http://localhost:8200/api/auth/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })

};
@Injectable({
  providedIn: 'root'
})
export class  ProfileService {

  constructor(private http: HttpClient) { }
  saveImage(uploadImageData,userId:number): Observable<any>
  
  //  userId
  {
    return this.http.put(IMAGE_API + 'uploadImageData/' + userId ,uploadImageData); 
   
  }
  saveuserImage(file:File,userId:any): Observable<any>
  
  //  userId
  {const data: FormData = new FormData();
    data.append('file', file);

    return this.http.put(TEST_API + 'uploadimageFile/' + userId ,file); 
   
  }
  getUserImage(userId): Observable<any>
  {
    return this.http.get(TEST_API + 'getUserImage/' + userId);
  }


  
  getImage(userId): Observable<any>
  {
    return this.http.get(IMAGE_API + 'getImageData/' + userId);
  }
  updateImage(uploadImageData,userId:number): Observable<any>
  
  //  userId
  {
    return this.http.put(IMAGE_API + 'updateImageData/' + userId ,uploadImageData); 
   
  }

  pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
   
    const data: FormData = new FormData();
    data.append('file', file);
    const newRequest = new HttpRequest('POST', 'http://localhost:8200/api/test/uploadFile',
     data, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(newRequest);
    
  }
  upload(file: File,userId:any): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', TEST_API+'uploadimageFile/'+userId, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }
 


}



import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,forkJoin} from 'rxjs';

const BOOKMARKSHARE_API = 'http://localhost:8201/BookmarkShare/';

const BOOKMARKCONCEPTSHARE_API = 'http://localhost:8201/ConceptShare/';
const KEYPOINT_API = 'http://localhost:8201/Keypoint/';
const AUTH_API = 'http://localhost:8200/api/auth/';
const BOOKMARKSTEPSHARE_API = 'http://localhost:8201/StepLinkShare/';
const BOOKMARKSOLUTIONSHARE_API = 'http://localhost:8201/SolutionShare/';
const KEYPOINT_CONCEPT = 'http://localhost:8201/ConceptKeypoint/';
const BOOKMARKAPPROACHSHARE_API = 'http://localhost:8201/ApproachShare/';
const BOOKMARSKILL_API = 'http://localhost:8201/SkillLinkShare/';
const TOPICNAMESHARE_API = 'http://localhost:8201/Topicshare/';
const GOALNAMESHARE_API = 'http://localhost:8201/Goalshare/';
const CHALLENGENAMESHARE_API = 'http://localhost:8201/Challengeshare/';
const BOOKMARKSKILLSHARE_API = 'http://localhost:8201/SkillLinkShare/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })

};
@Injectable({
  providedIn: 'root'
})
export class  ShareService {

  constructor(private http: HttpClient) { }
    getShareBookmark(userId): Observable<any>
    //  userId
    {
      return this.http.get(BOOKMARKSHARE_API + 'getpublicBookmarkShare/'+userId)//+'/'+bookmarklinkId)
     // +"/"+userId);
    }

    getStepShareBookmark(userId): Observable<any>
    //  userId
    {
      return this.http.get(BOOKMARKSTEPSHARE_API + 'getpublicstepShare/'+userId)//+'/'+bookmarklinkId)
     // +"/"+userId);
    }
    addsharestep(stepShare){
      return this.http.post(BOOKMARKSTEPSHARE_API + 'add/',stepShare);
    }


    getSolutionShareBookmark(userId): Observable<any>
    //  userId
    {
      return this.http.get(BOOKMARKSOLUTIONSHARE_API + 'getpublicSolutionShares/'+userId)//+'/'+bookmarklinkId)
     // +"/"+userId);
    }


    getApproachShareBookmark(userId): Observable<any>
    //  userId
    {
      return this.http.get(BOOKMARKAPPROACHSHARE_API + 'getpublicApproachShares/'+userId)//+'/'+bookmarklinkId)
     // +"/"+userId);
    }


    getShareBookmarkConcept(userId): Observable<any>
    {
      return this.http.get(BOOKMARKCONCEPTSHARE_API + 'getpublicConceptShares/'+userId);
    }



    getSkillBookmarkshare(userId): Observable<any>
    {
      return this.http.get(BOOKMARKSKILLSHARE_API + 'getpublicskilllinkShares/'+userId);
    }
    addsharesolution(solutionShare){
      return this.http.post(BOOKMARKSOLUTIONSHARE_API + 'add/',solutionShare);
}
addshareskill(skillShare){
  return this.http.post(BOOKMARKSKILLSHARE_API + 'add/',skillShare);
}
   
         addshareconcept(conceptShare){
          return this.http.post(BOOKMARKCONCEPTSHARE_API + 'add/',conceptShare);
    }
    
      addsharebookmark(sharebookmark) {

        return this.http.post(BOOKMARKSHARE_API + 'add',  sharebookmark);
    
      }
      addconceptkeypoints(keypoint) {
        return this.http.post(KEYPOINT_CONCEPT + 'add/', keypoint);
      }
      getConceptkeypoint(conceptlinkId) {
        return this.http.get(KEYPOINT_CONCEPT + 'get/'+ conceptlinkId);
    }
    getSharetopicname(userId): Observable<any> {
      return this.http.get(TOPICNAMESHARE_API + 'getpublicTopicShares/'+userId);
    }
    getShareGoalname(userId): Observable<any> {
      return this.http.get(GOALNAMESHARE_API + 'getpublicGoalnameShares/'+userId);
    }
    getSharechallengename(userId): Observable<any> {
      return this.http.get(CHALLENGENAMESHARE_API + 'allChallengeManagerNameShares/'+userId);
    }
    }

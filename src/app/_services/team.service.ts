import { User } from './../_model/user';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,forkJoin} from 'rxjs';


const TEAM_API = 'http://localhost:8203/Team/';
const TEAM_MEMBER_INVITE_API = 'http://localhost:8203/TeamMember/';
const TEAM_BOOKMARK = 'http://localhost:8203/TeamMember/';

const BOOKMARK_API = 'http://localhost:8203/TeamBookmark/';
const BOOKMARKLINK_API = 'http://localhost:8203/TeamBookmarklink/';
const KEYPOINT_API = 'http://localhost:8203/TeamKeyPoint/';
const TeamKeySuggestion = 'http://localhost:8203/TeamKeySuggestion/'; 
const SHARETEAMBOOKMARK_API = 'http://localhost:8203/Share/';
const SHARETEAMSTEP_API = 'http://localhost:8203/TeamStepShare/';
const SHARETEAMSKILL_API = 'http://localhost:8203/TeamSkillShare/';

const SHARETEAMGOALNAME_API = 'http://localhost:8203/TeamGoalshare/';
const SHARETEAMTOPICNAME_API = 'http://localhost:8203/TeamTopicshare/';
const AUTH_API = 'http://localhost:8203/api/auth/';
const SolutionTeamKeySuggestion = 'http://localhost:8203/SolutionTeamKeySuggestion/'; 
const SHARETEAMSOLUTION_API = 'http://localhost:8203/ShareTeamSolution/';

const SOLUTIONLINKKEYPOINT_API = 'http://localhost:8203/TeamSolutionKeypoints/';
const SHARETEAMCHALLENGENAME_API = 'http://localhost:8203/TeamChallengeShare/';
const SHARETEAMAPPROACH_API = 'http://localhost:8203/TeamApproachShare/';
const CONCEPTSHARE_API= 'http://localhost:8203/TeamConceptShare/';
const CONCEPTLINKKEYPOINT_API = 'http://localhost:8203/TeamConceptKeypoint/';
const ConceptTeamKeySuggestion = 'http://localhost:8203/ConceptTeamKeySuggestion/'; 
const APPROACHLINKKEYPOINT_API = 'http://localhost:8203/TeamApproachKeypoints/';
const ApproachTeamKeySuggestion = 'http://localhost:8203/ApproachTeamKeySuggestion/';
const STEPLINKKEYPOINT_API = 'http://localhost:8203/TeamStepKeypoint/';
const StepTeamKeySuggestion = 'http://localhost:8203/TeamStepKeySuggestion/';
const SKILLLINKKEYPOINT_API = 'http://localhost:8203/TeamSkillKeypoint/';
const SkillTeamKeySuggestion= 'http://localhost:8203/TeamSkillKeySuggestion/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }) 

};

@Injectable({
  providedIn: 'root'
})
export class  




TeamService {

  constructor(private http: HttpClient) { }
  showInviteList(emailid): Observable<any>{

    return this.http.get(TEAM_MEMBER_INVITE_API + 'showInviteList/'+emailid);

  }
  getpublicTeamSharelink() : Observable<any>
  {
    return this.http.get(SHARETEAMBOOKMARK_API + 'getpublicSharedBookmark');
  }

  getpublicTeamStepSharelink() : Observable<any>
  {
    return this.http.get(SHARETEAMSTEP_API + 'getpublicteamSharedstep');
  }



    getTeamList(userid) : Observable<any>{

      return this.http.get(TEAM_MEMBER_INVITE_API + 'AcceptedTeamList/'+userid);
  
    }
    getMemberTeamList(userid) : Observable<any>{

      return this.http.get(TEAM_MEMBER_INVITE_API + 'AcceptedTeamList/'+userid);
  
    }
    getteamShareDetail(teamId): Observable<any>
    //  userId
    {
      return this.http.get(SHARETEAMBOOKMARK_API + 'getpublicTeamSharedBookmark/'+teamId)
     // +"/"+userId);
    }
  addTeam(team): Observable<any> {

    return this.http.post(TEAM_API + 'addTeam',  team);


  }
  addTeamMember(teamMember) {

    return this.http.post(TEAM_MEMBER_INVITE_API + 'addTeamMember',  teamMember);
   
  } 
  addTeamAcceptedMember(teamAcceptedMember) {

    return this.http.post(TEAM_MEMBER_INVITE_API + 'addTeamAcceptedMember',  teamAcceptedMember);
   
  } 
  updateTeam(teamId,team: any) {
    return this.http.put(TEAM_API + 'updateTeam/' 
    +teamId, team);
    }
   
  getTeam(teamName:String) {

    return this.http.get(TEAM_API + 'getTeam'+teamName);

  }
  getTeamRoles() {

    return this.http.get(TEAM_API + 'getTeamRoles');

  } 
  getallTeamInviteMember(userid,teamId) : Observable<any>{
    return this.http.get(TEAM_MEMBER_INVITE_API + 'getallTeamInviteMember/'+ userid+"/"+teamId);
   
  }
  getTeamnameList(userid) : Observable<any>{
    return this.http.get(TEAM_MEMBER_INVITE_API + 'getTeamMemberRequestList/'+ userid);
   
  }
  getTeamdetail(userid,teamId) : Observable<any>{
    return this.http.get(TEAM_API + 'getTeamdetail/'+ userid+"/"+teamId);
   
  }
  
  getKeySuggestion(keypointId) : Observable<any>{
    return this.http.get(TeamKeySuggestion + 'getKeySuggestion/'+ keypointId);
   
  }
  showAcceptInvitePage(inviteId)
  {
    return this.http.get(TEAM_MEMBER_INVITE_API + 'accept/'+inviteId,
    { responseType: 'text' });

  }
  declineInvite(inviteId, teamId)
  {
    return this.http.delete(TEAM_MEMBER_INVITE_API + 'reject/'+inviteId+'/'+ teamId,
    { responseType: 'text' });

  }
  showPendingList(emailid) :Observable<any>{
  
    return this.http.get(TEAM_MEMBER_INVITE_API + 'showPendingList/'+emailid);
    

  }
  addBookmark(bookmark) {

    return this.http.post(BOOKMARK_API + 'addTeamBookmark',  bookmark);

  }
  addsharebookmark(sharebookmark) {

    return this.http.post(SHARETEAMBOOKMARK_API + 'add',  sharebookmark);

  }
  
  
  addKeypoint(keypoint) {

    return this.http.post(KEYPOINT_API + 'add',  keypoint);

  }

  addKeySuggestion(teamKeySuggestion) {

    return this.http.post(TeamKeySuggestion + 'add',  teamKeySuggestion);

  }



  addBookmarklink(bookmarklink) {

    return this.http.post(BOOKMARKLINK_API + 'addbookmarklink',  bookmarklink);

  }
  updateKeypoint(keypointId,Keypoint:any)
  {
    return this.http.put(KEYPOINT_API + 'updateKeypoint/'   
    +keypointId, Keypoint);
  }
  updatebookmark(bookmarkId,bookmark: any) {
    return this.http.put(BOOKMARK_API + 'updateBookmark/'   
    +bookmarkId, bookmark);
  }
  updatebookmarkLink(bookmarkLinkId,bookmarkLink: any)
  {
    return this.http.put(BOOKMARKLINK_API + 'updateBookmarklink/'   
    +bookmarkLinkId, bookmarkLink);
  }
  getBookmark(userid,teamId) {
    return this.http.get(BOOKMARK_API + 'get/'+ userid+"/"+teamId);
   
  }

  
  findByBookmarkName(bookmarkName:String,UserId:number)
  {
return this.http.get(BOOKMARK_API+'getBookmarkByName/'+bookmarkName+'/'+UserId)
  }
  findByBookmarkLink(link:String,UserId:number)
  {
return this.http.get(BOOKMARKLINK_API+'findbyBookmarkLink/'+link+'/'+UserId)
  }


  getKeypoint(bookmarkLinkId) {
    return this.http.get(KEYPOINT_API + 'get/'+ bookmarkLinkId);
   
  }
  getBookmarklink(bookmarkCategoryId) {
    return this.http.get(BOOKMARKLINK_API + 'getBookmarkLink/'+ bookmarkCategoryId);
   
  }
  deleteKeypoint(keypointId): Observable<any> {
    return this.http.delete(KEYPOINT_API + 'deleteKeypoint/'+ keypointId ,{ responseType: 'text' });
  }
  deleteBookmarklink(bookmarklinkId): Observable<any> {
    return this.http.delete(BOOKMARKLINK_API + 'deleteBookmarklink/'+ bookmarklinkId ,{ responseType: 'text' });
  }
  deleteBookmarklinkByCategory(bookmarkId): Observable<any> {
    return this.http.delete(BOOKMARKLINK_API + 'deleteBookmarklinkByCategory/'+ bookmarkId ,{ responseType: 'text' });
  }
  deleteBookmark(teambookmarkId): Observable<any> {
    return this.http.delete(BOOKMARK_API + 'deleteBookmark/'+ teambookmarkId ,{ responseType: 'text' });
  }
  deleteTeam(teamId): Observable<any> {
    return this.http.delete(TEAM_API + 'deleteTeam/'+ teamId ,{ responseType: 'text' });
  }
  getshareteamsolutionlink()
  //  userId
  {
    return this.http.get(SHARETEAMSOLUTION_API + 'getpublicTeamSolutionShare/');
   // +"/"+userId);
  }
  getTeamApproachShare(): Observable<any>
  //  userId
  {
    return this.http.get(SHARETEAMAPPROACH_API + 'getpublicTeamapproachShares/');
   // +"/"+userId);
  }

  addsharesolution(shareteamsolution) {

    return this.http.post(SHARETEAMSOLUTION_API + 'add',  shareteamsolution);

  }
  addsolutionKeySuggestion(teamKeySolutionSuggestion) {

    return this.http.post(SolutionTeamKeySuggestion + 'add',  teamKeySolutionSuggestion);

  }

  
 addteamsolutionkeypoints(teamsolutionkeypoints) {

  return this.http.post(SOLUTIONLINKKEYPOINT_API + 'add', teamsolutionkeypoints);
  
  }
  getteamsolutionkeypoint(teamsolutionlinkId){
      return this.http.get(SOLUTIONLINKKEYPOINT_API + 'get/'+ teamsolutionlinkId);
  }
  getKeysolutionSuggestion(teamkeypointId) : Observable<any>{
    return this.http.get(SolutionTeamKeySuggestion + 'getKeysolutionSuggestion/'+ teamkeypointId);
   
  }

  getpublicConceptShare(userid){
    return this.http.get(CONCEPTSHARE_API + 'getpublicConceptShares/' + userid );
  }
  getteamSharedstep(): Observable<any> {
    return this.http.get(SHARETEAMSTEP_API + 'getpublicteamSharedstep/');
  }
  getteamSharedskill(): Observable<any> {
    return this.http.get(SHARETEAMSKILL_API + 'getpublicteamSharedskill/');
  }
  getteamSharedconcept(): Observable<any> {
    return this.http.get(CONCEPTSHARE_API + 'getpublicTeamConceptShare/');
  }
  getteamSharegoalname(): Observable<any> {
    return this.http.get(SHARETEAMGOALNAME_API + 'getpublicTeamGoalShareNames/');
  }
  getteamSharetopicname(): Observable<any> {
    return this.http.get(SHARETEAMTOPICNAME_API + 'getpublicTeamTopicShareNames/');
  }
 
  addshareconcept(teamconceptshare) {

    return this.http.post(CONCEPTSHARE_API + 'addShare',  teamconceptshare);

  }
  getteamconceptkeypoint(teamConceptLinkId){
    return this.http.get(CONCEPTLINKKEYPOINT_API + 'get/'+ teamConceptLinkId);
}
getKeyconceptSuggestion(teamConceptKeypointId) : Observable<any>{
  return this.http.get(ConceptTeamKeySuggestion + 'getKeySuggestion/'+ teamConceptKeypointId);
 
}
addconceptKeySuggestion(teamKeyconseptSuggestion) {

  return this.http.post(ConceptTeamKeySuggestion + 'add',  teamKeyconseptSuggestion);

}
addteamconceptkeypoints(keypoint) {

  return this.http.post(CONCEPTLINKKEYPOINT_API + 'add', keypoint);
  
  }
  addshareapproach(shareteamapproach) {

    return this.http.post(SHARETEAMAPPROACH_API + 'add', shareteamapproach);
    
    }
    getapproachkeypoints(teamapproachlinkId){
      return this.http.get(APPROACHLINKKEYPOINT_API + 'get/'+ teamapproachlinkId);
  }
  getKeyapproachSuggestion(teamkeypointId) : Observable<any>{
    return this.http.get(ApproachTeamKeySuggestion + 'getKeySuggestion/'+ teamkeypointId);
   
  }
  addapproachKeySuggestion(approachteamKeySuggestion) {

    return this.http.post(ApproachTeamKeySuggestion + 'add',  approachteamKeySuggestion);

  }
  addteamapproachkeypoints(teamapproachkeypoints) {

    return this.http.post(APPROACHLINKKEYPOINT_API + 'add',  teamapproachkeypoints);

  }
  getteamstepkeypoint(teamsteplinkId){
    return this.http.get(STEPLINKKEYPOINT_API + 'get/'+ teamsteplinkId);
}addteamstepkeypoints(keypoint) {

  return this.http.post(STEPLINKKEYPOINT_API + 'add',  keypoint);

}
getKeystepSuggestion(teamstepkeypointId) : Observable<any>{
  return this.http.get(StepTeamKeySuggestion + 'getStepKeySuggestion/'+ teamstepkeypointId);
 
}
addstepKeySuggestion(teamstepKeySuggestion) {

  return this.http.post(StepTeamKeySuggestion + 'addStepKeySuggestion',  teamstepKeySuggestion);

}
addsharestep(teamstepshare) {

  return this.http.post(SHARETEAMSTEP_API + 'add', teamstepshare);
  
  }
  getteamskillkeypoint(teamskilllinkId){
    return this.http.get(SKILLLINKKEYPOINT_API + 'get/'+ teamskilllinkId);
  }
  addteamskillkeypoints(keypoint) {

    return this.http.post(SKILLLINKKEYPOINT_API + 'add', keypoint);
    
    }
    addskillKeySuggestion(teamskillKeySuggestion) {

      return this.http.post(SkillTeamKeySuggestion + 'addSkillKeySuggestion',  teamskillKeySuggestion);
    
    }
    getKeyskillSuggestion(teamskillkeypointId) : Observable<any>{
      return this.http.get(SkillTeamKeySuggestion + 'getSkillKeySuggestion/'+ teamskillkeypointId);
     
    }
    addshareskill(teamskillshare) {

      return this.http.post(SHARETEAMSKILL_API + 'add', teamskillshare);
      
      }
      getteamSharechallengename(): Observable<any> {
        return this.http.get(SHARETEAMCHALLENGENAME_API + 'getpublicTeamChallengeShareNames/');
      }
}
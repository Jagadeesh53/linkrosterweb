import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,forkJoin} from 'rxjs';

const GOAL_API='http://localhost:8203/TeamGoalManager/'
const STEP_API='http://localhost:8203/TeamStep/'
const STEPLINK_API='http://localhost:8203/TeamStepLink/'
const KEYPOINT_API='http://localhost:8203/TeamStepKeypoint/'
const SHARE_API='http://localhost:8203/TeamStepShare/'
const SKILL_API='http://localhost:8203/TeamSkill/'
const SKILLLINK_API='http://localhost:8203/TeamSkillLink/'
const SKILLKEYPOINT_API='http://localhost:8203/TeamSkillKeypoint/'
const SKILLSHARE_API='http://localhost:8203/TeamSkillShare/'
const TEAMGOALSHARE_API='http://localhost:8203/TeamGoalshare/'

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    
    };
    
    @Injectable({
    providedIn: 'root'
    })
    export class TeamGoalService {
    
    
    
    constructor(private http: HttpClient) { }
    
    addGoal(goalmanager){
        return this.http.post(GOAL_API + 'addTeamGoal/',goalmanager);
    }

    getGoal(userid,teamId) {
        return this.http.get(GOAL_API + 'get/'+ userid+"/"+teamId);
    }
    
    findByGoalName(golaName:String,UserId:number)
    {
        return this.http.get(GOAL_API+'getGoalByName/'+golaName+'/'+UserId)
    }
    
    updateGoal(teamgoalId,goalDetails: any) {
        return this.http.put(GOAL_API + 'updateGoal/'+teamgoalId, goalDetails);
      }
      
      deleteGoal(teamgoalId){
        return this.http.delete(GOAL_API + 'deleteGoal/'+ teamgoalId,{ responseType: 'text' });
    }

    deleteStepByGoal(teamgoalId){
        return this.http.delete(STEP_API + 'deletestepByteamgoalId/'+ teamgoalId,{ responseType: 'text' });
    }

    

    addgoalStep(teamstep){
        return this.http.post(STEP_API + 'addTeamstep/',teamstep);
    }

    getgoalStep(teamgoalId){
        return this.http.get(STEP_API+'gettepss/'+teamgoalId) ;
    }

    addstepLink(teamstepLink){
        return this.http.post(STEPLINK_API + 'addStepLink/',teamstepLink);
    }  

    getstepLink(teamstepId){
        return this.http.get(STEPLINK_API + 'getTeamstepLink/'+teamstepId);
    }  
    
    deletestepLink(teamsteplinkId){
        return this.http.delete(STEPLINK_API + 'deleteTeamstepLink/'+ teamsteplinkId,{ responseType: 'text' });
    }

    addstepKeypoint(keypoint){
        return this.http.post(KEYPOINT_API + 'add/',keypoint);
    }

    getstepKeypoint(teamsteplinkId){
        return this.http.get(KEYPOINT_API + 'get/'+teamsteplinkId);
    }

    deletestepKeypoint(teamstepkeypointId){
        return this.http.delete(KEYPOINT_API + 'deleteKeypoint/'+ teamstepkeypointId,{ responseType: 'text' });   
    }

    deletesteplinkBystep(teamstepId){
        return this.http.delete(KEYPOINT_API + 'deleteKeypoint/'+ teamstepId,{ responseType: 'text' }); 
    }

    updatestepKeypoint(teamstepkeypointId,keypointDetails){
        console.log(keypointDetails);
        return this.http.put(KEYPOINT_API + 'updateKeypoint/'+teamstepkeypointId, keypointDetails);
    }

    addsharestep(shareSteps){
        return this.http.post(SHARE_API + 'add/',shareSteps);
    }
    addSkill(teamskill){
        return this.http.post(SKILL_API + 'addTeamskill/',teamskill);
    }

    getSkill(teamgoalId){
        return this.http.get(SKILL_API+'getteamskills/'+teamgoalId) ;
    }
    

    addskillLink(teamskillLink){
        return this.http.post(SKILLLINK_API + 'addSkillLink/',teamskillLink);
    }  

    getskillLink(teamskillId){
        return this.http.get(SKILLLINK_API + 'getTeamskillLink/'+teamskillId);
    }  
    
    deleteskillLink(teamskilllinkId){
        return this.http.delete(SKILLLINK_API + 'deleteTeamskillLink/'+ teamskilllinkId,{ responseType: 'text' });
    }

    addskillKeypoint(keypoint){
        return this.http.post(SKILLKEYPOINT_API + 'add/',keypoint);
    }

    getskillKeypoint(teamskilllinkId){
        return this.http.get(SKILLKEYPOINT_API + 'get/'+teamskilllinkId);
    }

    deleteskillKeypoint(teamskillkeypointId){
        return this.http.delete(SKILLKEYPOINT_API + 'deleteskillKeypoint/'+ teamskillkeypointId,{ responseType: 'text' });   
    }

    updateskillKeypoint(teamskillkeypointId,keypointDetails){
        console.log(keypointDetails);
        return this.http.put(SKILLKEYPOINT_API + 'updateskillKeypoint/'+teamskillkeypointId, keypointDetails);
    }
    addshareskill(shareSkills){
        return this.http.post(SKILLSHARE_API + 'add/',shareSkills);
    }
    addshareGoal(teamgoalShare){
        return this.http.post(TEAMGOALSHARE_API + 'addteamgoalshare/',teamgoalShare);
    }


    deleteteamstep(teamstepId){
        return this.http.delete(STEP_API + 'deleteTeamstep/'+teamstepId);
    }

    updateStep(stepId, stepDetails){
        return this.http.put(STEP_API+'updatestep/'+stepId, stepDetails);
    }
    updateskill(skillId: number, data: { teamskillName: String; userId: any; }) {
        return this.http.put(SKILL_API+'updateskill/'+skillId, data);
       
      }
      deleteSkill(teamskillId: number) {
        return this.http.delete(SKILL_API + 'deleteTeamskill/'+teamskillId);
      }
    }

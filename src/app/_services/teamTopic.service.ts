import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,forkJoin} from 'rxjs';

const TOPIC_API='http://localhost:8203/TeamTopic/'
const CONCEPT_API='http://localhost:8203/TeamConcept/'
const CONCEPTLINK_API='http://localhost:8203/TeamConceptLink/'
const KEYPOINT_API='http://localhost:8203/TeamConceptKeypoint/'
const CONCEPTSHARE_API='http://localhost:8203/TeamConceptShare/'
const TEAMTOPICSHARE_API='http://localhost:8203/TeamTopicshare/'


const httpOptions = {
    headers: new HttpHeaders(
        { 
            'Content-Type': 'application/json',
             'Access-Control-Allow-Origin': '*'
        }
    )
    
    };
    
    @Injectable({
    providedIn: 'root'
    })
    export class TeamTopicService {
     
    
    constructor(private http: HttpClient) { }

    deleteTopic(teamTopicId){
        return this.http.delete(TOPIC_API + 'deleteTopic/'+ teamTopicId,{ responseType: 'text' });
    }
    deleteConceptlinkByCategory(teamTopicId){
        return this.http.delete(TOPIC_API + 'deleteTopic/'+ teamTopicId,{ responseType: 'text' });
    }
    addTopic(topic){
        return this.http.post(TOPIC_API + 'addTeamTopic/',topic);
    }
    getTopic(userid,teamId) {
        return this.http.get(TOPIC_API + 'getTopic/'+ userid+"/"+teamId);
    }
    
    findByTopicName(teamTopicName:String,UserId:number)
    {
  return this.http.get(TOPIC_API+'getTopicByName/'+teamTopicName+'/'+UserId)
    }
        updateTopic(teamTopicId,topicDetails: any) {
        return this.http.put(TOPIC_API + 'updateTopic/'+teamTopicId, topicDetails);
      }
      addConcept(teamConcept){
          return this.http.post(CONCEPT_API + 'addTeamConcept/',teamConcept);
      }
      getconcept(teamTopicId) {
          return this.http.get(CONCEPT_API + 'getconcepts/'+ teamTopicId);
      }
      updateConcept(datastep: { teamConceptName: String; teamConceptId: number; userId: any; }) {
        return this.http.put(CONCEPT_API + 'updateConcept/'+datastep.teamConceptId,datastep);
      }
      deleteConcept(conceptId): Observable<any> {
        return this.http.delete(CONCEPT_API + 'deleteTeamConcept/'+ conceptId ,{ responseType: 'text' });
        }
      addconceptLink(teamConceptLink) {
          return this.http.post(CONCEPTLINK_API + 'addConceptLink/',teamConceptLink);
      }
      getconceptLink(teamConceptId) {
          return this.http.get(CONCEPTLINK_API + 'getTeamConceptLink/'+ teamConceptId);
      }
      addconceptkeypoints(keypoint) {
          return this.http.post(KEYPOINT_API + 'add/',keypoint);
      }
      addshareconcept(teamconceptshare) {
          return this.http.post(CONCEPTSHARE_API + 'addShare/',teamconceptshare);
      }
      deleteconceptlink(teamConceptLinkId) {
          return this.http.delete(CONCEPTLINK_API + 'deleteTeamConceptLink/'+teamConceptLinkId,{ responseType: 'text' });
      }
      getConceptkeypoint(teamConceptLinkId) {
          return this.http.get(KEYPOINT_API + 'get/' + teamConceptLinkId);
      }
      updateKeypoint(teamConceptKeypointId,keypointDetails) {
          return this.http.put(KEYPOINT_API + 'updateKeypoint/' + teamConceptKeypointId,keypointDetails);
      }
      deleteKeypoint(teamConceptKeypointId) {
          return this.http.delete(KEYPOINT_API + 'deleteKeypoint/' + teamConceptKeypointId,{ responseType: 'text' });
      }

      addshareTopic(teamTopicShare){
        return this.http.post(TEAMTOPICSHARE_API + 'addteamchallengeshare/' ,teamTopicShare);
      }
    }
    

import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { User } from '../_model/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const OTP_KEY='OTP';

const baseUrl = 'http://localhost:8200/api/auth/findall';
@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  save(newUser: User) {
    throw new Error("Method not implemented.");
  }

  
 
  constructor(private http: HttpClient) { }

  signOut() {
    window.sessionStorage.clear();
    localStorage.removeItem("auth-token");
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }
 
  public getToken(): string {
    //console.log(sessionStorage.getItem(TOKEN_KEY));
    return sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user) {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }
  

  public getUser() {
    return JSON.parse(localStorage.getItem(USER_KEY));
  }

  public putUser() {
    return JSON.parse(sessionStorage.putItem(USER_KEY));
  }
  public saveOTP(user) {
    window.sessionStorage.removeItem(OTP_KEY);
    window.sessionStorage.setItem(OTP_KEY, JSON.stringify(user));
  }

  public getOTP() {
    return JSON.parse(sessionStorage.getItem(OTP_KEY));
  }

 
  getAll() {
    return this.http.get(baseUrl);
  }

  get(id) {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data) {
    return this.http.post(baseUrl, data);
  }

  update(id, data) {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id) {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll() {
    return this.http.delete(baseUrl);
  }

  findByEmail(email) {
    return this.http.get(`${baseUrl}?email=${email}`);
  }







 
}




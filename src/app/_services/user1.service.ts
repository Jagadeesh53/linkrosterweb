import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
const Chain_API = 'http://localhost:8202/api/';
const User_API = 'http://localhost:8200/api/auth/';
@Injectable({
  providedIn: 'root'
})
export class User1Service {
  getchainrequest(id: any) {
    throw new Error("Method not implemented.");
  }

  private baseUrl = 'http://localhost:8200/api/auth/findall';

  constructor(private http: HttpClient) { }


  addChain(chainrequet) {

    return this.http.post(Chain_API + 'add',  chainrequet);

  }

  getUser(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  // getUsers(username) {
  //   return this.http.get(User_API + 'getUserListing/'+ username);
  // }

getUsers(username:String,id:number)
{
return this.http.get(User_API+'getUserListing/'+username+'/'+id)
}

  getList(id) {
    return this.http.get(User_API + 'getUserlist/'+ id);
  }

//   createUser(User: Object): Observable<Object> {
//     return this.http.post(`${this.baseUrl}`, User);
//   }

//   updateUser(id: number, value: any): Observable<Object> {
//     return this.http.put(`${this.baseUrl}/${id}`, value);
//   }

//   deleteUser(id: number): Observable<any> {
//     return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
//   }

  getUsersList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);

  }

  findByusername(username) {
    return this.http.get(`${this.baseUrl}?username=${username}`);
  }
}
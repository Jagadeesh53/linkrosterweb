import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private baseUrl = 'http://localhost:8200/api/auth/';

  constructor(private http: HttpClient) { }


  update(id) {
    return this.http.get(this.baseUrl + 'findalls/'+id);
  }

  getEmployee(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/`+'findall/'+id);
  }

 
  updateEmployee(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/+'findalls/'+${id}`, value);
  }

 
  
}
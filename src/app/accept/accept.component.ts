import { Component, OnInit } from '@angular/core';
import {TeamService} from '../_services/team.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-accept',
  templateUrl: './accept.component.html',
  styleUrls: ['./accept.component.css']
})
export class AcceptComponent implements OnInit {

  accept_emailConfirmed: boolean = false;
  urlParams: any = {};
  message:string;
  constructor( private teamService: TeamService,private route: ActivatedRoute,
  
    private tokenStorage:TokenStorageService
  
   ) {}
  ngOnInit(): void {
    this.urlParams.emailId = this.route.snapshot.queryParamMap.get('emailId');
    this.urlParams.inviteId =this.route.snapshot.queryParamMap.get('inviteId');
  
    this.acceptEmail();

  }
  
  acceptEmail() {
  
    this.teamService.showAcceptInvitePage(this.urlParams.inviteId)
    .subscribe((data) => {
   
     this.message=data;
      this.accept_emailConfirmed = true;
    }, error => {
     
      console.log(error);
    
      this.accept_emailConfirmed = false;
    })
  
  }
 
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { RequestResetComponent } from './request-reset/request-reset.component';
import{ConfirmationEmailComponent} from './confirmation-email/confirmation-email.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ConfirmemailotpComponent } from './confirmemailotp/confirmemailotp.component';
import { BookmarkComponent } from './bookmark/bookmark.component';
import { ChallengemanagerComponent } from './challengemanager/challengemanager.component';

import { ViewfriendsbookmarkComponent } from './viewfriendsbookmark/viewfriendsbookmark.component';
import { SearchviewpageComponent } from './searchviewpage/searchviewpage.component';
import { ChainuserComponent } from './chainuser/chainuser.component';
import { NewteamComponent } from './newteam/newteam.component';
import { AcceptComponent } from './accept/accept.component';
import { ShareteamComponent } from './shareteam/shareteam.component';
import { TeambookmarkComponent } from './teambookmark/teambookmark.component';
import { MyteamComponent } from './myteam/myteam.component';
import { TeamsettingComponent } from './teamsetting/teamsetting.component';
import { ChainListComponent } from './chain-list/chain-list.component';
 import { ShareComponent } from './share/share.component';
import { TestBed } from '@angular/core/testing';

 import { UsermoduleComponent } from './usermodule/usermodule.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { LearnManagerComponent } from './learn-manager/learn-manager.component';
import{ GoalComponent} from './goal/goal.component';
import { TeamLearnComponent } from './team-learn/team-learn.component';
import { TeamGoalComponent } from './team-goal/team-goal.component';
import { SolutionmanagerComponent } from './solutionmanager/solutionmanager.component';
import { TeamchallengeComponent } from './teamchallenge/teamchallenge.component';
import { DashboardComponent } from './dashboard/dashboard.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'login', component: HomeComponent },
  { path: 'register', component: RegisterComponent },
  {path:'request-reset',component:RequestResetComponent},
    { path: 'profile', component: ProfileComponent },
  { path: 'user', component: BoardUserComponent },
  { path: 'mod', component: BoardModeratorComponent },
  { path: 'admin', component: BoardAdminComponent },
 
  {path:'confirm',component:ConfirmationEmailComponent},
  {path:'changePassword',component:ChangePasswordComponent},
  {path:'changePassword/otp',component:ChangePasswordComponent},
  {path:'confirmemailotp',component:ConfirmemailotpComponent},
  {path:'bookmark',component:BookmarkComponent},
  {path:'challengemanager',component:ChallengemanagerComponent},

{path:'viewfriendsbookmark',component:ViewfriendsbookmarkComponent},
{path:'searchviewpage',component:SearchviewpageComponent},
{path:'chainuser',component:ChainuserComponent},
{path:'newteam',component:NewteamComponent},
{path:'accept',component:AcceptComponent},
{path:'shareteam',component:ShareteamComponent},
{path:'myteam',component:MyteamComponent},
{path:'teambookmark',component:TeambookmarkComponent},
{path:'teamsetting',component:TeamsettingComponent},
{path:'chain-list',component:ChainListComponent},

{path:'profile-edit',component:ProfileEditComponent},

{path:'edit-profile/:id',component: ProfileEditComponent},

{path:'usermodule',component:UsermoduleComponent},
{path:'share',component:ShareComponent},
{path:'goal',component:GoalComponent},
{path:'learnManager',component:LearnManagerComponent},
{path:'teamlearn',component:TeamLearnComponent},
{path:'teamgoal',component:TeamGoalComponent},
{path:'teamchallenge',component:TeamchallengeComponent},
{path:'solutionmanager',component:SolutionmanagerComponent},
{ path: '', redirectTo: 'home', pathMatch: 'full' },

];

@NgModule({
  
  imports: [RouterModule.forRoot(routes,{ useHash: false })],
  exports: [RouterModule],

})
export class AppRoutingModule { }

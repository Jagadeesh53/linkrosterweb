import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './_services/token-storage.service';
import { ProfileService } from './_services/profile.service';
import { ChainService } from './_services/chain.service';
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from './_services/auth.service';
import { Router, ActivatedRoute, Event, NavigationEnd, } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username: string;
  retrievedImage: any;
  base64Data: any;
  currentRoute: string;
  profilelist: any = [];
  retrieveResonse: any; InviteList: any = []; noticationstatus = false; notication = 0;
  constructor(private route: ActivatedRoute, private tokenStorageService: TokenStorageService, private authService: AuthService,
    private profileService: ProfileService, private chainService: ChainService, private SpinnerService: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.currentRoute = (<NavigationEnd>event).url;
        console.log("30", this.currentRoute)

      }
    })

    this.isLoggedIn = !!localStorage.getItem("auth-token");     //!!this.tokenStorageService.getToken();
    this.getuserImage();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

      this.username = user.username;
    }
    // else if (this.currentRoute && this.currentRoute.indexOf('changePassword?resetToken') >= 0) {
    //   console.log("inside change password")
    // }
    // else {
    //   this.router.navigate(["login"])
    // }
    this.showInviteList();
    this.getprofile();
  }

  logout() {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
  loggedinUserimage: string; userretrievedImage: string;
  getuserImage() {

    this.profileService.getUserImage(this.tokenStorageService.getUser().id).subscribe(
      res => {
        this.loggedinUserimage = res.name;
        console.log("log", res.name);
        this.userretrievedImage = res.name; console.log("log", this.userretrievedImage);
        if (!this.userretrievedImage) {
          this.userretrievedImage = "default-avatar.jpg";
        } else {
          this.userretrievedImage = this.loggedinUserimage;
        }
      }
    );
  }
  getImage() {
    this.SpinnerService.show();
    this.profileService.getImage(this.tokenStorageService.getUser().id).subscribe(
      res => {
        this.retrieveResonse = res;
        this.base64Data = this.retrieveResonse.picByte;
        this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        this.SpinnerService.hide();
      }

    );
  }
  showInviteList() {

    this.chainService.showPendingList(this.tokenStorageService.getUser().id)

      .subscribe(data => {
        this.InviteList = data

        console.log("invitelist", this.InviteList.length)

        console.log("len", this.InviteList.length.valueOf());
        if (this.InviteList.length > 0) {

          this.noticationstatus = true;
          console.log("stat", this.noticationstatus)
        }

        else { this.noticationstatus = false }
        this.notication = this.InviteList.length.valueOf()

        console.log("lnoticationen", this.notication);
      }

      );
  }


  getprofile() {

    this.authService.getuserList(this.tokenStorageService.getUser().id).subscribe(
      response => {

        this.profilelist = response;
        console.log("data", response);


      },
      error => {

        console.log(error);
      });
    //  this.gotoList();

  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {HttpModule} from "@angular/http";
import { AppRoutingModule } from './app-routing.module';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { ProfileComponent } from './profile/profile.component';

import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { RequestResetComponent } from './request-reset/request-reset.component';
import { ConfirmationEmailComponent } from './confirmation-email/confirmation-email.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { PathLocationStrategy} from '@angular/common';
import { ConfirmemailotpComponent } from './confirmemailotp/confirmemailotp.component';
import { BookmarkComponent } from './bookmark/bookmark.component';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ViewfriendsbookmarkComponent } from './viewfriendsbookmark/viewfriendsbookmark.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchviewpageComponent } from './searchviewpage/searchviewpage.component';
import { ChainuserComponent } from './chainuser/chainuser.component';
import { NewteamComponent } from './newteam/newteam.component';
import { AcceptComponent } from './accept/accept.component';

import { ShareteamComponent } from './shareteam/shareteam.component';
import { TeambookmarkComponent } from './teambookmark/teambookmark.component';
import { MyteamComponent } from './myteam/myteam.component';

import { TeamsettingComponent } from './teamsetting/teamsetting.component';

//  import { MatLinkPreviewModule } from '@angular-material-extensions/link-preview';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatBadgeModule} from '@angular/material/badge';

import { ShareComponent } from './share/share.component';
import { ChainListComponent } from './chain-list/chain-list.component';

///import { ShareTeamhomeComponent } from './share-teamhome/share-teamhome.component';
import { UsermoduleComponent } from './usermodule/usermodule.component';

import {NgxPaginationModule} from 'ngx-pagination';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SortnamePipe } from './sortname.pipe';
import { GoalComponent } from './goal/goal.component';
import { ChallengemanagerComponent } from './challengemanager/challengemanager.component';
import { LearnManagerComponent } from './learn-manager/learn-manager.component';
import { TeamLearnComponent } from './team-learn/team-learn.component';
import { TeamGoalComponent } from './team-goal/team-goal.component';
import { SolutionmanagerComponent } from './solutionmanager/solutionmanager.component';
import { TeamchallengeComponent } from './teamchallenge/teamchallenge.component';
import { DashboardComponent } from './dashboard/dashboard.component';



@NgModule({
  declarations: [
    AppComponent,
     LoginComponent,
    RegisterComponent,
  
    HomeComponent,
    BoardAdminComponent,
    BoardUserComponent,
    BoardModeratorComponent,
    ProfileComponent,
    RequestResetComponent,
   
    ConfirmationEmailComponent,
    ChangePasswordComponent,
   
    ConfirmemailotpComponent,
   
    BookmarkComponent,
   
   
   
    ViewfriendsbookmarkComponent,
   
    SearchviewpageComponent,
   
    ChainuserComponent,
   
   
    NewteamComponent,
   
    AcceptComponent,
     
    ShareteamComponent,
   
    TeambookmarkComponent,
    
    MyteamComponent,
   
    TeamsettingComponent,
  
    TeamsettingComponent,
    ChainListComponent,

    //ShareTeamhomeComponent,
 
    ShareComponent,
    
    UsermoduleComponent,ProfileEditComponent, SortnamePipe, 
    LearnManagerComponent,ChallengemanagerComponent,GoalComponent,
    TeamLearnComponent, TeamGoalComponent,SolutionmanagerComponent, TeamchallengeComponent, DashboardComponent

      
   
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,NgbModule,
   
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatBadgeModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    
  ],
  exports: [
    BrowserModule, 
    BrowserAnimationsModule,
    FormsModule,
  
    HttpClientModule,
  ], schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ],
  providers: [authInterceptorProviders,Location, {provide: LocationStrategy, useClass:PathLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { 

}

import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { BookmarkService } from '../_services/bookmark.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { Bookmark } from '../_model/Bookmark';
import { Router } from '@angular/router';
import { Lexer } from '@angular/compiler';
import { NgxSpinnerService } from "ngx-spinner";
import { NgForm } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.css']
})
export class BookmarkComponent implements OnInit {
  username: string;
  p: number = 1;
  p1: number = 1;
  public currentbookmarkid;
  public currentbookmarkname;
  sharedTime: number = Date.now();
  searchlink: '';
  bookmarkNamese
  bookmarkNameselected: any = {};
  bookmarkNameselectedId: any = {};
  bookmarkLinkselected: any = {};
  bookmarkLinkselectedId: any = {};
  bookmarkId: number;
  bookmarkLinkShareId: number;
  bookmarkLinkId: number;
  bookmarkname: string;
  link: string;
  bookmarklist: any = [];
  bookmarklinklist: any = []; KeypointList: any = [];
  message: String;
  isdelete = false;
  enableEdit = false;
  iskeySuccessful = false;
  enableEditIndex = null;
  hrefvisible = true;
  bookmarklinklistVisible = true;
  bookmarkinkeditMode = false;
  limitedKeypointList: any = [];
  limitedKeypointList2: any = [];
  form: any;
  errorMessage = '';
  errorMessage1 = '';
  sucessmessage: any;
  isLoggedIn = false;
  contenteditable = false;
  editField: string;
  bookmark = {

    bookmarkName: '',
    description: '',
    userId: 0
  };
  keypoint = {

    keyComments: '',
    bookmarkLinkId: 0,
    bookmarkCategoryId: 0
  };
  bookmarklink1 = '';
  bookmarklink = {

    link: '',
    bookmark_id: 0,
    userId: 0
  };

  sharebookmarklink = {
    bookmarkName: '',
    link: '',
    bookmarkLinkId: 0,
    shareId: 0,
    accessType: '',
    sharedTime: Date()
  };
  content = '';
  isSuccessful = false;
  showCollasal = true;
  showCollasalIndex = null;
  isgetBookmark = false;
  isgetKeyPoints = false;
  NoRecorderrorMessage = "";
  isSelected = true;
  i = 0;
  tableParse_vals: any = [];
  editKeyComments: any;
  constructor(private bookmarkService: BookmarkService,
    private router: Router,
    private tokenStorage: TokenStorageService,
    private SpinnerService: NgxSpinnerService
  ) { }


  showKeyModal: boolean;
  BookmarkLinkId: number;
  Link: string;
  keyComments: string;
  keypointId: number;
  bookmarkCategoryId: number; shareId: any;


  buttonCol(selectedItem: any) {
    this.SpinnerService.show();
    this.BookmarkLinkId = selectedItem.bookmarkLinkId;// You get the Id of the selected item here

    this.getkeypoint(this.BookmarkLinkId);
  }
  opentesttoggle(selectedItem: any, i) {

    this.showCollasalIndex = i;
    this.BookmarkLinkId = selectedItem.bookmarkLinkId;

    this.getkeypoint(this.BookmarkLinkId);
  }
  closetesttoggle(selectedItem: any, i) {

    this.showCollasalIndex = null;
    this.BookmarkLinkId = selectedItem.bookmarkLinkId;

    //this.getkeypoint(this.BookmarkLinkId);
  }
  openNav(selectedItem: any) {

    //  document.getElementById("collapseExample").style.width = "250px";
    this.BookmarkLinkId = selectedItem.bookmarkLinkId;// You get the Id of the selected item here

    this.getkeypoint(this.BookmarkLinkId);
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }


  hide() {
    this.showKeyModal = false;
  }


  opentoggle(id: number, property: string, event: any) {


    $("#slideme").slideToggle();
  }

  onSelectKeypoint(selectedItem: any) {
    $("#myModal").modal('show');
    this.keyComments = selectedItem.keyComments;
    console.log("Selected item Id: ", selectedItem.Id);
    this.BookmarkLinkId = selectedItem.bookmarkLinkId;// You get the Id of the selected item here
    this.showKeyModal = true;
    //this.getkeypoint(this.BookmarkLinkId);
  }


  showdelBookmarkModal(selectedItem: any): void {

    $("#mydelBookmarkModal").modal('show');

    console.log("Selected item Id: ", selectedItem.Id);

    this.bookmarkId = selectedItem.bookmarkId;// You get the Id of the selected item here

  }
  showModal(selectedItem: any): void {

    $("#mydelModal").modal('show');
    this.Link = selectedItem.link;
    console.log("Selected item Id: ", selectedItem.Id);
    this.BookmarkLinkId = selectedItem.bookmarkLinkId;// You get the Id of the selected item here

  }
  showEditKeyModal(selectedItem: any): void {
    $("#myEditKeyPointModal").modal('show');
    this.BookmarkLinkId = selectedItem.bookmarkLinkId;
    this.editKeyComments = selectedItem.keyComments;
    this.keypointId = selectedItem.keypointId;
    this.bookmarkCategoryId = selectedItem.bookmarkCategoryId;
    this.showKeyModal = true;
  }
  showdelKeyModal(selectedItem: any): void {

    $("#mydelKeyModal").modal('show');
    this.keyComments = selectedItem.keyComments;
    this.keypointId = selectedItem.keypointId;

    this.BookmarkLinkId = selectedItem.bookmarkLinkId;// You get the Id of the selected item here

  }
  sendModal(): void {
    //do something here
    this.hideModal();

  }
  hideModal(): void {

    document.getElementById('close-modal').click();
  }

  ngOnInit() {

    this.bookmarkNameselected = "Add CategoryLink";

    this.getbookmark();
    this.username = this.tokenStorage.getUser().username;
    this.bookmarklinklistVisible = false;
    //this.getkeypoint(this.BookmarkLinkId);
    if (this.bookmarklist == []) {

      this.isgetBookmark = true;
      this.errorMessage = "No Bookmarks"
    }



  }


  getBookmarkid(bookmarklist: any, i: any) {
    this.bookmarkNameselectedId = bookmarklist.bookmarkId;
    this.bookmarkNameselected = bookmarklist.bookmarkName;
    this.getbookmarklink();
  }
  deleteKeypoint() {


    this.bookmarkService.deleteKeypoint(this.keypointId)

      .subscribe(
        data => {
          console.log(data);
          this.getkeypoint(this.BookmarkLinkId);

        },
        error => console.log(error));

    this.getkeypoint(this.BookmarkLinkId);


  }

  deleteBookmark() {

    this.bookmarkService.deleteBookmark(this.bookmarkId)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
    this.hideModal();
    // this.deleteBookmarklinkByCategory(this.bookmarkId);

    $('.modal-backdrop').remove();
  }
  reloadData() {
    this.getbookmark();

  }
  deleteBookmarklinkByCategory(bookmarkId) {

    this.bookmarkService.deleteBookmarklinkByCategory(bookmarkId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";

        },

        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
    this.reloadData();
  }
  deleteBookmarklink() {

    this.bookmarkService.deleteBookmarklink(this.BookmarkLinkId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";
          this.bookmarklinklist = this.getbookmarklink();

        },

        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
    // this.getbookmarklink();
    this.hideModal();
  }

  getbookmark() {

    this.bookmarkService
      .getBookmark(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.bookmarklist = data


          if (this.bookmarklist.length > 0) {
            this.getBookmarkid(this.bookmarklist[0], 0);
          }
          else {
            this.bookmarklinklist = []
            this.bookmarkNameselectedId = null;
            this.isSuccessful = false;
            this.errorMessage = "";
            this.errorMessage1 = "";
            this.KeypointList = [];
            this.limitedKeypointList = null;

            this.bookmarkNameselected = "Add CategoryLink";
          }

          setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.SpinnerService.hide();
          }, 2000);
          //this.getBookmarkid(this.bookmarklist[0],(itemsPerPage*(p-1)));
        },
        err => {
          this.bookmarklist = JSON.parse(err.error).message;
        }
      );


  }
  getkeypoint(BookmarkLinkId: number) {
    this.KeypointList = null;
    this.limitedKeypointList = null;
    this.bookmarkService
      .getKeypoint(BookmarkLinkId)
      .subscribe(

        data => {
          this.KeypointList = data; console.log("data keypoint", data);
          this.limitedKeypointList = this.KeypointList.slice(0, 3)
          console.log("sliced data", this.limitedKeypointList);
        },
        err => {
          return console.error(err);
        }
        // () => console.log('getKeypointList completed') 
      );
  }

  onKeyPointsViewMore(): void {
    this.limitedKeypointList2 = this.KeypointList.slice(3, this.KeypointList.length);

    this.limitedKeypointList = this.limitedKeypointList.concat(this.limitedKeypointList2);
    console.log(this.limitedKeypointList);
    console.log(this.KeypointList.length, this.limitedKeypointList.length);
    $('#view-more').hide();
    $('#view-less').show();
  }

  onkeyPointsviewless(): void {
    this.limitedKeypointList = this.KeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }


  getbookmarklink() {
    this.bookmarklinklist = [];
    this.KeypointList = [];
    this.limitedKeypointList = [];
    this.bookmarkService.getBookmarklink(this.bookmarkNameselectedId)
      .subscribe(
        data => {
          this.bookmarklinklist = data
          console.log(data);
          this.showCollasalIndex=null;
          localStorage.setItem("bookmarkCount", this.bookmarklinklist.length)
          this.bookmarklinklistVisible = true;
        },
        err => console.error(err),
        () => console.log('getBookarklink completed')
      );
  }
  AddBookmark(f: NgForm, bookmarklink) {
    // console.log("FORM",this.bookmark.bookmarkName);
    // alert(bookmarklink)
    this.SpinnerService.show();
    const data = {
      bookmarkName: this.bookmark.bookmarkName,
      description: this.bookmark.description,
      userId: this.tokenStorage.getUser().id
    };

    this.bookmarkService.addBookmark(data).subscribe(
      data => {
      
        if (bookmarklink != '') {
          this.bookmarkNameselected = '';
          this.bookmarkNameselectedId = (<any>data).bookmarkId;
          this.bookmarklink.link = bookmarklink;
          //this.AddBookmarkLink(f)

        }

        //  console.log(data.bookmarkId);
        this.sucessmessage = "BookMark Added";
        this.isLoggedIn = true;
        this.isSuccessful = true;
        this.getbookmark();
        f.resetForm();
        this.errorMessage1 = '';
        console.log("added");

      },
      err => {
        this.errorMessage1 = err.error.message;
        this.isSuccessful = false;
      }
    );


  }
  linkisSuccessful = false; linkerrorMessage = '';
  AddBookmarkLink(fl: NgForm) {

    if (this.bookmarkNameselected == "Add CategoryLink") {

      this.errorMessage = "*Please Select Bookmark Category";
    }
    else {
      const datalink = {
        link: this.bookmarklink.link,
        bookmarkCategoryId: this.bookmarkNameselectedId,
        userId: this.tokenStorage.getUser().id
      };
      this.bookmarkService.addBookmarklink(datalink).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.bookmarklink.link = ''
          fl.resetForm();
          this.linkisSuccessful = true;
          this.getbookmarklink();
          //location.reload();
        },
        err => {
          this.errorMessage = '';
          this.linkerrorMessage = err.error.message;
          this.linkisSuccessful = false;
        }
      );
    }
  }


  AddKeypoint(bookmarkLinkId: number, fAddKey: NgForm) {


    const dataKey = {
      keyComments: this.keypoint.keyComments,
      bookmarkLinkId: this.BookmarkLinkId,
      bookmarkCategoryId: this.bookmarkNameselectedId,
      userId: this.tokenStorage.getUser().id
    };

    this.bookmarkService.addKeypoint(dataKey).subscribe(
      data => {
        this.sucessmessage = "Comments  Added";
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        fAddKey.resetForm();


        let $parent = this;

        setTimeout(function () {
          $("#myModal").modal('hide');
          $parent.sucessmessage = "";

          $parent.iskeySuccessful = false;
          $parent.showKeyModal = false;
          $('.modal-backdrop').remove();
        }, 1000);


        this.getkeypoint(this.BookmarkLinkId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );

    //this.getkeypoint(this.BookmarkLinkId);
    this.hideModal();
  }

  EditKeypoint(keyComments: String) {
    const dataKey = {
      keyComments: keyComments,
      bookmarkLinkId: this.BookmarkLinkId,
      bookmarkCategoryId: this.bookmarkNameselectedId

    };
    this.bookmarkService.updateKeypoint(this.keypointId, dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Edited";
        this.iskeySuccessful = true;
        this.getkeypoint(this.BookmarkLinkId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      });
    // this.getkeypoint(this.BookmarkLinkId);
  }
  prevent(keyComments: string) {
    event.preventDefault();
    this.EditKeypoint(keyComments);


  }
  findByBookmarkName() {
    this.isSuccessful = true;
    this.enableEdit = false;

    this.bookmarkService.findByBookmarkName(this.bookmark.bookmarkName,
      this.tokenStorage.getUser().id)

      .subscribe(
        data => {
          this.bookmarklist = data

        },
        err => {
          this.bookmarklist = JSON.parse(err.error).message;
        }
      );
  }
  findByBookmarkLink() {

    this.router.navigate(['searchviewpage'], { queryParams: { link: this.searchlink } });
  }
  editBookmark(bookmarkId: number, bookmarkName: String) {
    if (bookmarkName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableEdit = true;
      const data = {
        bookmarkName: bookmarkName,

        userId: this.tokenStorage.getUser().id
      };

      this.bookmarkService.updatebookmark(bookmarkId, data).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getbookmark();
          this.enableEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );

    }
  }
  editBookmarkLink(bookmarkLinkId: number, Link: String) {

    const datalink = {

      link: this.bookmarklink.link,
      bookmarkCategoryId: this.bookmarkNameselectedId,

      userId: this.tokenStorage.getUser().id
    };
    this.bookmarkService.updatebookmarkLink(bookmarkLinkId, datalink).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        this.getbookmarklink();
        this.enableEdit = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      }
    );
  }

  changeValue(id: number, property: string, event: any) {

    this.editField = event.target.textContent;

  }
  changetestValue(event: any) {

    this.editField = event.target.value;
    if (this.editField == "") {
      this.getbookmark();
    }
    else {
      this.findByBookmarkNameSearch(this.editField);
    }
  }

  findByBookmarkNameSearch(key) {
    this.isSuccessful = true;
    this.enableEdit = false;

    this.bookmarkService.findByBookmarkName(key,
      this.tokenStorage.getUser().id)

      .subscribe(
        data => {
          this.bookmarklist = data
          console.log("list", data);

        },
        err => {
          this.bookmarklist = JSON.parse(err.error).message;
        }
      );
  }

  enableEditMethod(e, i) {

    this.currentbookmarkid = e.bookmarkId;
    this.currentbookmarkname = e.bookmarkName;
    this.isSuccessful = true;
    this.enableEdit = true;
    this.enableEditIndex = i;
    console.log(i, e);
    this.hrefvisible = false;
    this.hrefvisible = i;

  }

  updateList(id: number, property: string, event: any) {
    const editField = event.target.textContent;
    this.bookmarklinklist[id][property] = editField;
    const datalink = {
      link: this.editField,
      bookmarkCategoryId: this.bookmarklinklist[id].bookmarkCategoryId,
      userId: this.tokenStorage.getUser().id
    };
    this.bookmarkService.updatebookmarkLink(this.bookmarklinklist[id].bookmarkLinkId, datalink).subscribe(
      data => {
        console.log(data);
        this.getbookmarklink();
      },
      err => {

      });
  }


  showShareModal(selectedItem: any): void {
    this.isSuccessful = false;
    $("#shareModal").modal('show');
    this.Link = selectedItem.link;
    this.bookmarkLinkShareId = selectedItem.bookmarkLinkId;// You get the Id of the selected item here
  }

  onchangeAccessType(val: any) {
    this.sharebookmarklink.accessType = val;
  }

  shareBookmark(bookmarkLinkshareId: number,fshare:NgForm) {
    this.sucessmessage = '';
    this.bookmarkLinkId = bookmarkLinkshareId;
    if(this.sharebookmarklink.accessType==''){
      this.isSuccessful = false;
    }
    else{
    const datalink = {
      bookmarkCategoryId: this.bookmarkNameselectedId,
      bookmarklinkId: bookmarkLinkshareId,
      bookmarkname: this.bookmarkname,
      link: this.Link,
      userId: this.tokenStorage.getUser().id,
      accesstype: this.sharebookmarklink.accessType,
      username: this.username,
      sharedTime: new Date().toDateString()
    };
    this.bookmarkService.addsharebookmark(datalink).subscribe(
      (data: any) => {
        console.log(data);
        document.getElementById('shareclose').click();
        fshare.resetForm();
        this.isSuccessful = true;
        this.sharebookmarklink.accessType='';
        this.getbookmarklink();
      },
      (err: any) => {
        this.isSuccessful = false;
      });
    }
  }

  update(id: number, event: any) {
    const datalink = {
      link: this.editField,
      bookmark_id: this.bookmarklinklist[id].bookmarkCategoryId,
      userId: this.tokenStorage.getUser().id
    };
    this.bookmarkService.updatebookmarkLink(this.bookmarklinklist[id].bookmarkLinkId, datalink).subscribe(
      data => {
        console.log(data);
        this.getbookmarklink();
      },
      err => {

      });
  }
}

import { Component, OnInit } from '@angular/core';
import { ChainService } from '../_services/chain.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenStorageService } from '../_services/token-storage.service';
import { Observable } from 'rxjs';
//import { Chainrequest } from '../_model/chainrequest';

@Component({
  selector: 'app-chain-list',
  templateUrl: './chain-list.component.html',
  styleUrls: ['./chain-list.component.css']
})
export class ChainListComponent implements OnInit {
  chainlists: any;
  currentChainlist = null;
  friendChainlist = null;
  requestdeletelist:any=[];
  messages:String;
  isdelete=false;
  enableEdit=false;
  errorMessage = '';
  sucessmessage:any;
  isSuccessful=false;
 currentIndex= -1;
 chainuserid='';
  username = '';
   userid='';

  message = '';
  status='';
  // chainuserid='';
  id='';

  inboundClick = true;
  outboundClick = true;


  constructor(private chainService: ChainService,private route: ActivatedRoute,
    private router: Router,private tokenStorage:TokenStorageService) { }

  ngOnInit() {
    this.retrieveChainlist();
    this.message = '';
    this.getChainlist();
   this.getChainlists();
  // this.deleteRequest();

  }


  ViewfriendsBookmark(chainuserid:number)
  {
  //  alert(chainuserid);
  this.router.navigate(['viewfriendsbookmark'], { queryParams: { chainuserid:chainuserid } });

}

  retrieveChainlist() {
    this.chainlists = [];
    this.chainService.getAll(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.chainlists = data;
          console.log("chainlists",data);
        },
        error => {
          console.log("retrive",error);
        });
  }

  refreshList() {
    this.retrieveChainlist();
    this.currentChainlist = null;
    this.currentIndex = -1;
    this.friendChainlist = null;

  }

  setActiveChainlist(chainlist, index) {

    this.currentChainlist = chainlist;
    // console.log(chainlist);

    this.currentIndex = index;
  }


  // removeAllChainlists() {
  //   this.chainService.deleteAll()
  //     .subscribe(
  //       response => {
  //         console.log(response);
  //         this.retrieveChainlist();
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }

  // searchEmail() {
  //   this.chainService.findByUsername(this.username)
  //     .subscribe(
  //       data => {
  //         this.chainlists = data;
  //         console.log(data);
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }



  getChainlist() {

    this.chainService.getAll(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.currentChainlist = data;
          console.log("chain",data);
        },
        error => {
          console.log("request ",error);
        });
  }
  getChainlists() {

    this.chainService.getFriend(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.friendChainlist = data;
          console.log("friend",data);
        },
        error => {
          console.log(error);
        });
  }

  updatePublished(id) {

    this.chainService.updated(id)
      .subscribe(
        response => {
          // this.currentChainlist.status = status;
          console.log("update ",response);
          this.message = 'The Chain-List was updated successfully!';
          this.retrieveChainlist();
          this.getChainlist();
          this.getChainlists();
          // this. refreshPage();
        },
        error => {
          console.log("update",error);
        });
        //  this.gotoList();
  }







  RequestDelate(id) {

    this.chainService.deleteRequest(id)
      .subscribe(
        response => {
          this.message = 'The request was deleted!';
          this.sucessmessage="request is deleted";
          this.retrieveChainlist();
          this.getChainlist();
        },
        error => {

          console.log(error);
        });
        // this. refreshPage();
  }


  updateChainlist() {

    this.chainService.updated(this.currentChainlist.id)

      .subscribe(
        response => {

          this.message = 'The Chainlist was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  // deleteChainlist() {
  //   this.chainService.delete(this.currentChainlist.id)
  //     .subscribe(
  //       response => {

  //         this.router.navigate(['/chainlists']);
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }


  deleteRequest() {

    this.chainService.deleteRequest(this.id)
      .subscribe(
        data => {
          console.log(data);
         this. isdelete=true;
          this.sucessmessage="request is deleted";
         this.requestdeletelist= this.getChainlists();

        },

      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful=false;
      });
  }



  gotoList() {
    this.router.navigate(['/chain-list']);
  }

  refreshPage() {
    console.log("234 ", "refresh");
    window.location.reload();
    console.log("236 ", "refresh");
   }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChainuserComponent } from './chainuser.component';

describe('ChainuserComponent', () => {
  let component: ChainuserComponent;
  let fixture: ComponentFixture<ChainuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChainuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChainuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

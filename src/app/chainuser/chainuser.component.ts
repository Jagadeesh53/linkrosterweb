import { Component, OnInit ,Input} from '@angular/core';
import { BookmarkService } from '../_services/bookmark.service';
import { TokenStorageService } from '../_services/token-storage.service';
import {bookmarkuser} from '../_model/bookmarkuser';   
import { forkJoin } from 'rxjs';
import { AuthService } from '../_services/auth.service';
import {Observable} from 'rxjs';
import * as Rx from 'rxjs';import { Router } from '@angular/router';

@Component({
  selector: 'app-chainuser',
  templateUrl: './chainuser.component.html',
  styleUrls: ['./chainuser.component.css']
})
export class ChainuserComponent implements OnInit {
  bookmarkNameselected: any = {};
  bookmarkNameselectedId: any = {};
  bookmarkLinkselected: any = {};
  bookmarkLinkselectedId: any = {};
  bookmarklinklistVisible=true;
  bookmarkUserList: any = [];
  bookmarklist:any=[];
  friendslist:any=[];
  friendsListRequestModel:any = {};
  bookmarklinklist:any=[];
  bookmarkUserlist:any=[];
  KeypointList:any=[];
  userlist:any=[];
  isSuccessful=false;
showCollasal=true;
errorMessage=false;
showCollasalIndex=null;
showKeyModal : boolean;
BookmarkLinkId    : number;
Link : string;
keyComments:string;
keypointId:number;
bookmarkCategoryId:number;

friend = {

  chainuserid:0
 
};

constructor(private bookmarkService: BookmarkService,  private router: Router,
  private tokenStorage:TokenStorageService,private authservice:AuthService
) { }

  ngOnInit(): void {
    
    this. bookmarkNameselected="Add CategoryLink";
   
    this.getfriendlist();
    let friendlist =  this.bookmarkService
    .getfriendlist(this.tokenStorage.getUser().id);
    const friendlist$ = this.bookmarkService
    .getfriendlist(this.tokenStorage.getUser().id);
    this.friendsListRequestModel=this.friendslist;
    console.log("userid",this.friendsListRequestModel)
    this.getAlluser();
  }
  getAlluser()
  {
  
    
   
    this.authservice.getAllUserlist(this.friendslist)
    .subscribe(
          data => { 
            this.userlist= data  
            console.log("userlsit",this.friendslist)  ;   
           },
           err => console.error(err), 
           () => console.log('getBookarklink completed')     
          );
  
  }
  getfriendlist()
  {
  
  
    this.bookmarkService
    .getfriendlist(this.tokenStorage.getUser().id)
    .subscribe(
          data => { 
           this.friendslist=data  
            console.log("friends",this.friendslist)  ;  
            this.authservice.getAllUserlist(this. friendslist)  
              .subscribe(
              userdata => { 
                this.userlist= userdata  
                console.log("userlsit",userdata)  ;   
               },
               err => console.error(err), 
               () => console.log('getBookarklink completed')     
              );
           
           },
           err => console.error(err), 
           () => console.log('getBookarklink completed')     
          );
  
  }
  getbookmarkbyUser(chainuserid:number)
{
 
 
   this.router.navigate(['viewfriendsbookmark'], 
   { queryParams: { chainuserid: chainuserid } });


 
 }
}

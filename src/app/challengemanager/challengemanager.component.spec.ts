import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengemanagerComponent } from './challengemanager.component';

describe('ChallengemanagerComponent', () => {
  let component: ChallengemanagerComponent;
  let fixture: ComponentFixture<ChallengemanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengemanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengemanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { ChallengeService } from '../_services/challenge.service';
declare var $:any;

@Component({
  selector: 'app-challengemanager',
  templateUrl: './challengemanager.component.html',
  styleUrls: ['./challengemanager.component.css']
})
export class ChallengemanagerComponent implements OnInit {

  constructor( private ChallengeService: ChallengeService,
      private router: Router,
     private tokenStorage:TokenStorageService,  
     private SpinnerService: NgxSpinnerService   
    ) { }
    showKeyModal : boolean;
    SolutionLinkId    : number;
    Link : string;
    keyComments:string;
    keypointId:number;
    solutionCategoryId:number;shareId:any;
    challenge = {

      challengeName: '',
      description:'',
      userId:0
    };
    solution={
      solutionName:'',
      challengeId:0,
      userId:0
    };
    approach={
      approachName:'',
      challengeId:0,
      userId:0
    };
    keypoint = {
  
      keypoints: '',
      solutionLinkId:0,
      solutionId:0,
      userId:0
    };
   solutionlinks = {
    
      solutionlink: '',
      solution_id:0,
      userId:0
    };

    approachlinks = {
    
      approachlink: '',
      approach_id:0,
      userId:0
    };
    
    sharesolutionlink = {
      challengeName: '',
      link: '',
      solutionLinkId:0,
      shareId:0,
      accessType:'',
      sharedTime:Date()
    };
    
    p: number = 1; 
    username: string;  
    errorMessage = '';
    searchlink:'';
    isdelete=false;
    sharedTime: number=Date.now();
    sucessmessage:any;
    isLoggedIn=false;
    isSuccessful=false;
    isgetGoal=false;
    challengelist:any=[];
    challengeNameselected: any = {};
    challengeNameselectedId: any = {};
    isgetChallenge=false;
    editField: string;
    enableEdit=false; 
    public currentchallengeid;
    public currentchallengename;
    enableEditIndex = null;
    hrefvisible=true;
    challengeId:number;
    solutionlist:any=[];
    approachlist:any=[];
    approacherrorMessage='';approachlistVisible=true;ApproachId:number;
    approachNameselectedId: any = {};approachisSuccessful=false;
    challengeisSuccessful=false;solutionerrorMessage='';
    solutionisSuccessful=false;challengeerrorMessage='';
    solutionlistVisible=true;showCollasalIndex=null;
    
    
    solutionId:number;
    solutionNameselectedId: any = {};
    solutionlinkShareId:number;
    solutionlinkId:number;
    
    solutionLinkselected: any = {};
    solutionLinkselectedId: any = {};
    solutionlinkisSuccessful=true;
    solutionlinklist:any=[];KeypointList:any=[];
    solutionlinklistVisible=true;solutionlinkerrorMessage ='';
    solutionlinkeditMode=false;
    limitedKeypointList:any=[];
    limitedKeypointList2:any=[];
    approachLinkId:number;
    approachLinkselected: any = {};
    approachLinkselectedId: any = {};
    approachlinkisSuccessful=true;
    approachlinklist:any=[];
    approachlinklistVisible=true;approachlinkerrorMessage ='';
    approachlinkeditMode=false;
    iskeySuccessful=false;
    solutionKeypointId:number;
    showKeypointIndex:null;

    buttonCol(selectedItem: any)
    {
      this.SpinnerService.show();
    this.SolutionLinkId =selectedItem.solutionLinkId;// You get the Id of the selected item here
   
    
    }
    changeValue(id: number, property: string, event: any) {
   
      this.editField = event.target.textContent;
      
    }
    
    openNav(selectedItem: any) {
    
      //  document.getElementById("collapseExample").style.width = "250px";
        this.SolutionLinkId =selectedItem.solutionLinkId;// You get the Id of the selected item here
  
        
    }
    closeNav() {
      document.getElementById("mySidenav").style.width = "0";
  }
   
  
    hide()
    {
      this.showKeyModal = false;
    }  
    

opentoggle(id: number, property: string, event: any)
{

   
    $("#slideme").slideToggle();
}

 /*opentesttoggle(selectedItem:any,i)
  {
     this.showCollasalIndex=i;
     this.challengeId =selectedItem.challengeId;
     this.ApproachId =selectedItem.approachId;
     this.SolutionLinkId =selectedItem.solutionLinkId;
    
     
  }*/
 /* showdelChallengeModal(selectedItem:any):void {
    
    $("#mydelChallengeModal").modal('show');
   
    console.log("Selected item Id: ", selectedItem.Id); 
    
  this.challengeId =selectedItem.challengeId;// You get the Id of the selected item here
   
  } */
      showModal(selectedItem:any):void {
      
        $("#mydelModal").modal('show');
        this.Link =selectedItem.link;  
        console.log("Selected item Id: ", selectedItem.Id); 
        this.SolutionLinkId =selectedItem.solutionLinkId;// You get the Id of the selected item here
        this.approachLinkId =selectedItem.approachLinkId;
        
      }
      // showEditKeyModal(selectedItem:any):void{
       
      //   $("#myEditKeyPointModal").modal('show');
      //   this.SolutionLinkId =selectedItem.solutionLinkId;
       
        
      //  this.solutionCategoryId=selectedItem.solutionCategoryId;
      //  this.showKeyModal=true;
       
        
      // }
      // showdelKeyModal(selectedItem:any):void {
      
      //   $("#mydelKeyModal").modal('show');
        
      //   this.SolutionLinkId =selectedItem.solutionLinkId;// You get the Id of the selected item here
        
      // }
      // sendModal(): void {
      //   //do something here
      //   this.hideModal();
        
      // }
      

  ngOnInit(): void {  
    
    this. challengeNameselected="Add CategoryLink";
    this.getChallenge();
    
    this.solutionlistVisible=false;
    this.approachlistVisible=false;
    this.solutionlinklistVisible=false;
    this.approachlinklistVisible=false;
    if(this.challengelist==[])
    {
     
      this.isgetChallenge=true;
      this.errorMessage="No Topics"
    }
    
        
      }
      getChallengeid(challengelist: any, i: any) {
  
        this.challengeNameselectedId=challengelist.challengeId; 
        this.challengeNameselected = challengelist.challengeName;
        this.getChallengeSolution();
        this.getChallengeApproach();
       // this.getsolutionlink(); 
      }
   
  
      AddTopic() {
            this.SpinnerService.show();
           const data = {
            challengeName: this.challenge.challengeName,
            description: this.challenge.description,
            userId:this.tokenStorage.getUser().id
          
          };
         
            this.ChallengeService.addTopic(data).subscribe(
              data => {
                console.log(data);     
                this.sucessmessage=data;
                this.isLoggedIn = true;
                this.isSuccessful=true;
                this.getChallenge();
              
                      console.log("added");
              },
              err => {
                this.errorMessage = err.error.message;
                this.isSuccessful=false;
              }
            );
            }

            AddSolution() {
              if(this.challengeNameselected=="Add solution") 
              {
                
               this.errorMessage="*Please Select Challenge";
              }
              else{
                const datalink = {
                  solutionName: this.solution.solutionName,
                  challengeId:this.challengeNameselectedId,
              
                  userId:this.tokenStorage.getUser().id
                    };
                    this.ChallengeService.addSolution(datalink).subscribe(
                      data => {
                        console.log(data);     
                        this.sucessmessage=data;
                        this.isLoggedIn = true;
                        this.isSuccessful=true;
                        this.getChallengeSolution();
                      
                              console.log("added");
                      },
                      err => {
                        this.errorMessage = '';
                        this.challengeerrorMessage =err.error.message;
                        this.challengeisSuccessful=false;
                      }
                    );
                    }  
           } 

           AddApproach() {
            if(this.challengeNameselected=="Add approach") 
            {
              
             this.errorMessage="*Please Select Challenge";
            }
            else{
              const datalink = {
                approachName: this.approach.approachName,
                challengeId:this.challengeNameselectedId,
            
                userId:this.tokenStorage.getUser().id
                  };
                  this.ChallengeService.addApproach(datalink).subscribe(
                    data => {
                      console.log(data);     
                      this.sucessmessage=data;
                      this.isLoggedIn = true;
                      this.isSuccessful=true;
                      this.getChallengeApproach();
                    
                            console.log("added");
                    },
                    err => {
                      this.errorMessage = '';
                      this.challengeerrorMessage =err.error.message;
                      this.challengeisSuccessful=false;
                    }
                  );
                  }  
         }
  
            getChallenge()
            {
            
              this.ChallengeService
              .getChallenge(this.tokenStorage.getUser().id)
              .subscribe(
                    data => { 
                               this.challengelist= data  
                      
                      console.log("challengelist",this.challengelist) 
                     
                    this.getChallengeid(this.challengelist[0],0);     
                    setTimeout(() => {
                      /** spinner ends after 5 seconds */
                      this.SpinnerService.hide();
                    },2000);
                    //this.getBookmarkid(this.bookmarklist[0],(itemsPerPage*(p-1)));
                     },
                     err => {
                      this.challengelist = JSON.parse(err.error).message;
                    }       
                    ); 
                   
                    
            }

            getChallengeSolution(){
          
              this.ChallengeService.getSolution(this.challengeNameselectedId)
                    .subscribe(
                          data => { 
                            this.solutionlist= data 
                            console.log(data); 
                            this.opentesttoggle(data[0],0); 
                            this.solutionlistVisible=true;  
                           },
                           err => console.error(err), 
                           () => console.log('getsolution completed') 
                          );           
            }
            getChallengeApproach(){
          
              this.ChallengeService.getApproach(this.challengeNameselectedId)
                    .subscribe(
                          data => { 
                            this.approachlist= data 
                            console.log(data); 
                            this.opentesttoggle(data[0],0); 
                            this.approachlistVisible=true;  
                           },
                           err => console.error(err), 
                           () => console.log('getapproach completed') 
                          );           
            }
            changetestValue( event: any)
            {
              
              this.editField =  event.target.value ;
              this.findByChallengeName(this.editField );
            }
            findByChallengeName(key)
            {
              this.isSuccessful=true; 
              this.enableEdit = false;
              
              this.ChallengeService.findByChallengeName(key,
                this.tokenStorage.getUser().id)
             
              .subscribe(
                    data => { 
                      this.challengelist= data  
                      console.log("list",data);
            
                     },
                     err => {
                      this.challengelist = JSON.parse(err.error).message;
                    }       
                    );
            }

 editChallenge(challengeId:number,challengeName:String)
{
 if(challengeName=="")
 {
this.isSuccessful=false;
 }
 else
 {
  this.isSuccessful=true;
  this.enableEdit=true;
  const data = {
    challengeName: challengeName,
    userId:this.tokenStorage.getUser().id
  };

    this.ChallengeService.updatechallenge(challengeId,data).subscribe(
      data => {
        console.log(data);     
        this.sucessmessage=data;
        this.isLoggedIn = true;
        this.isSuccessful=true;
        this.getChallenge();
        this.enableEdit=false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful=false;
      }
    );
    
    }
}
enableEditMethod(e, i) 
{
  
  this.currentchallengeid = e.challengeId; 
  this.currentchallengename = e.challengeName; 
 this.isSuccessful=true; 
  this.enableEdit = true;
  this.enableEditIndex = i;
  console.log(i, e);
  this.hrefvisible=false;
  this.hrefvisible = i;
 
}

showdelChallengeModal(selectedItem:any):void {
    
  $("#mydelChallengeModal").modal('show');
 
  console.log("Selected item Id: ", selectedItem.Id); 
  
this.challengeId =selectedItem.challengeId;// You get the Id of the selected item here
 
}
deleteChallenge() {

  this.ChallengeService.deleteChallenge(this.challengeId)
    .subscribe(
      data => {
        console.log(data);
        this.reloadData();
      },
      error => console.log(error));
      this.hideModal();
      this.deleteSolutionlinkByCategory(this.challengeId);
     // this.getsolutionlink();
     this.getChallenge();
}
reloadData() {
  this.challengelist = this.getChallenge();
}
deleteSolutionlinkByCategory(challengeId)
{
 
  this.ChallengeService.deleteSolutionlinkByCategory(challengeId)
  .subscribe(
    data => {
      console.log(data);
     this. isdelete=true;
      this.sucessmessage="Link is deleted";
    
    },
    err => {
      this.errorMessage = err.error.message;
      this.isSuccessful=false;
    }
  );
  }
  deleteSolutionlink() {

    this.ChallengeService.deleteSolutionLink(this.SolutionLinkId)
      .subscribe(
        data => {
          console.log(data);
         this. isdelete=true;
          this.sucessmessage="Link is deleted";
        // this.solutionlinklist= this.getsolutionlink();
        
        },
     
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful=false;
      }
    );
   // this.getsolutionlink();
    this.hideModal();
  }
hideModal():void {
    
  document.getElementById('close-modal').click();
}


  
getsolutionlink(solutionId:number)
{

 
  this.ChallengeService.getSolutionlink(solutionId)
  
  .subscribe(
        data => { 
          this.solutionlinklist= data 
          console.log(data); 
          //this.opentesttoggle(data[0],0); 
          this.solutionlinklistVisible=true;     
         },
         
        err => console.error(err), 
        () => console.log('getSolutionlink completed') 
         );    
}
linkisSuccessful=false;linkerrorMessage='';
AddSolutionLink(solutionId,solutionlink,i)
{
 
  const datalink = {
    solutionlink: solutionlink,
   solutionId:solutionId,

    userId:this.tokenStorage.getUser().id
  };
    this.ChallengeService.addsolutionlink(datalink).subscribe(
      data => {
        this.showCollasalIndex=i;
       this.solutionId =solutionId;
    
       this.getsolutionlink(this.solutionId );
     
        console.log(data);     
        this.sucessmessage=data;
        this.isLoggedIn = true;
        this.solutionlinkisSuccessful=true;
        //this. getsolutionlink();
      },
      err => {
       this.errorMessage='';
        this.solutionlinkerrorMessage =err.error.message;
        this.solutionlinkisSuccessful=false;
      }
    );
   this.getsolutionlink(solutionId);
}
opentesttoggle(selectedItem:any,i)
    {
    
      
       this.showCollasalIndex=i;
       this.solutionId =selectedItem.solutionId;
    
       this.getsolutionlink(this.solutionId );
     
   
       
    }
    onSelectKeypoint(selectedItem: any) {
      $("#myModal").modal('show');
      this.keyComments =selectedItem.keypoints; 
      console.log("Selected item Id: ", selectedItem.Id); 
      this.solutionlinkId =selectedItem.solutionlinkId;// You get the Id of the selected item here
      this.showKeyModal=true;
      //this.getkeypoint(this.BookmarkLinkId);
    }

    Addsolutionkeypoints(solutionlinkId,solutionId)
   {
 
 
     const dataKey = {
      keypoints: this.keypoint.keypoints,
      solutionlinkId:solutionlinkId,
      solutionId:solutionId,
      userId:this.tokenStorage.getUser().id
    };
  
      this.ChallengeService.addsolutionkeypoints(dataKey).subscribe(
        data => {
          console.log(data);     
          this.sucessmessage="Comments  Added";
          this.isLoggedIn = true;
          this.iskeySuccessful=true;
          this.getsolutionkeypoint(this.solutionlinkId);
        },
        err => {
          this.errorMessage = err.error.message;
          this.iskeySuccessful=false;
        }
      );
     
    this.getsolutionkeypoint(this.solutionlinkId);
    this.hideModal();
  }
  getsolutionkeypoint(solutionlinkId:number)
  {
   
    this.ChallengeService.getsolutionkeypoint(solutionlinkId)
    .subscribe(
          data => { 
            this.KeypointList= data;
          //   this.limitedKeypointList = this.KeypointList.slice(0,3)
          //  console.log("sliced data", this.limitedKeypointList);       
           },
          err => {
            return console.error(err);
          } 
          // () => console.log('getKeypointList completed') 
           );
  }
  opencommentstoggle(selectedItem:any,i)
  {
  
    
     this.showKeypointIndex=i;
     this.solutionlinkId =selectedItem.solutionlinkId;
  
     this.getsolutionkeypoint(this.solutionlinkId );
  
     
  }
  showEditKeyModal(selectedItem:any):void{
     
    $("#myEditKeyPointModal").modal('show');
    this.solutionlinkId =selectedItem.solutionlinkId;
   
    this.keypoint =selectedItem.keypoint; 
    this.solutionKeypointId=selectedItem.solutionKeypointId;
   this.solutionId=selectedItem.solutionId;
   this.showKeyModal=true;
   
    
  }showdelKeyModal(selectedItem:any):void {
    
    $("#mydelKeyModal").modal('show');
    this.keypoint =selectedItem.keypoint;  
    this.solutionKeypointId=selectedItem.solutionKeypointId;
  
    this.solutionlinkId =selectedItem.solutionlinkId;// You get the Id of the selected item here
    
  }
  deleteKeypoint() {
  

    this.ChallengeService.deleteKeypoint(this.solutionKeypointId)
    
      .subscribe(
        data => {
          console.log(data);
          this.getsolutionkeypoint(this.solutionlinkId);
         
        },
        error => console.log(error));
        
        this.getsolutionkeypoint(this.solutionlinkId);
      
       
       }
       EditKeypoint(keyComments:String)
                         
       {
     
        
        
         const dataKey = {
          keypoints: keyComments,
          solutionlinkId:this.solutionlinkId,
          solutionId:this.solutionId,
          userId:this.tokenStorage.getUser().id     
        };
     
     
          this.ChallengeService.updateKeypoint(this.solutionKeypointId,dataKey).subscribe(
            data => {
              console.log(data);     
              this.sucessmessage="Comments  Edited";
              
              this.iskeySuccessful=true;
              this.getsolutionkeypoint(this.solutionlinkId);
             
            },
            err => {
              this.errorMessage = err.error.message;
              this.iskeySuccessful=false;
            }
          );
        
         this.getsolutionkeypoint(this.solutionlinkId);
        
         
      }
         prevent(keypoints:string)
     {
      event.preventDefault();
      this.EditKeypoint(keypoints);
      
      
     }


  showShareModal(selectedItem:any):void {
   
    $("#shareModal").modal('show');
    this.Link =selectedItem.link;   
    console.log("Selected item Id: ", selectedItem.Id); 
    this.solutionlinkShareId =selectedItem.solutionlinkId;// You get the Id of the selected item here
   
    }
    shareSolution(solutionlinkShareId)
  {
  
    
    this.solutionlinkId=solutionlinkShareId;
   
const datalink = {
  solutionId:this.solutionNameselectedId,
 solutionlinkId:solutionlinkShareId,
  userId:this.tokenStorage.getUser().id,
 accesstype:this.sharesolutionlink.accessType,
 username:this.username,
 sharedTime:new Date().toDateString()

};
this.ChallengeService.addsharesolution(datalink).subscribe(
  (  data: any) => {
    console.log(data);
  // this.getsolutionlink()
},
  (err: any) => {

}
);

  }
  onchangeAccessType(val: any) {
 
    this.sharesolutionlink.accessType=val;
    
    
  }

getapproachlink()
{

  this.ChallengeService.getApproachlink(this.challengeNameselectedId)
  
  .subscribe(
        data => { 
          this.approachlinklist= data 
          console.log(data); 
          this.opentesttoggle(data[0],0); 
          this.approachlinklistVisible=true;     
         },
         
        err => console.error(err), 
        () => console.log('getApproachlink completed') 
         );    
}

AddApproachLink(approachId,approachlink)
{
 
  const datalink = {
    approachlink:approachlink,
   approachId:approachId,

    userId:this.tokenStorage.getUser().id
  };
    this.ChallengeService.addapproachlink(datalink).subscribe(
      data => {
        console.log(data);     
        this.sucessmessage=data;
        this.isLoggedIn = true;
        this.approachlinkisSuccessful=true;
        this. getapproachlink();
      },
      err => {
       this.errorMessage='';
        this.approachlinkerrorMessage =err.error.message;
        this.approachlinkisSuccessful=false;
      }
    );
    
}


findBySolutionLink()
{
  
 
   this.router.navigate(['searchviewpage'], { queryParams: { link: this.searchlink } });


 
 }
 editSolutionLink(solutionLinkId:number,Link:String)
{
 
  const datalink = {

    link: this.solutionlinks.solutionlink,
    solutionCategoryId:this.challengeNameselectedId,

    userId:this.tokenStorage.getUser().id
  };
    this.ChallengeService.updatesolutionLink(solutionLinkId,datalink).subscribe(
      data => {
        console.log(data);     
        this.sucessmessage=data;
        this.isLoggedIn = true;
        this.isSuccessful=true;
        //this.getsolutionlink();
        this.enableEdit=false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful=false;
      }
    );
}
updateList(id: number, property: string, event: any) {

  const editField = event.target.textContent;
  this.solutionlinklist[id][property] = editField;
  const datalink = {

    link: this. editField,
    solutionCategoryId:this.solutionlinklist[id].solutionCategoryId,
    userId:this.tokenStorage.getUser().id
  };
    this.ChallengeService.updatesolutionLink(this.solutionlinklist[id].solutionLinkId,datalink).subscribe(
      data => {
        console.log(data);

   //this.getsolutionlink();
  
 },
 err => {
  
 }
);

 
}





  }



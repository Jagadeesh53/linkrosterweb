import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = false;
   sucessmessage=false;
   paramSubscription:any;
   otp:number;

  constructor(private route: ActivatedRoute, private authService: AuthService)
   { }

  ngOnInit() {
  this.form.resetToken = this.route.snapshot.queryParamMap.get('resetToken');

 
 
  }
  changePassword() {
   
   this.authService.changePassword(this.form.resetToken,this.form)
   
    .subscribe((data) => {
      console.log(data);
      this.isSuccessful = true;
        this.sucessmessage=true;
        
        this.isSignUpFailed = false;
      console.log("success");
      
    }, error => {
      this.errorMessage = true;
        this.isSignUpFailed = true;

      console.log(error);
    
    })
  }

}

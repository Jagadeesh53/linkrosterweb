import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../_services/auth.service';


@Component({
  selector: 'app-confirmation-email',
  templateUrl: './confirmation-email.component.html',
  styleUrls: ['./confirmation-email.component.css']
})
export class ConfirmationEmailComponent implements OnInit {
  emailConfirmed: boolean = false;
  urlParams: any = {};
  message:string;
  constructor(private route: ActivatedRoute, private authService: AuthService)
    { }

  ngOnInit() {
    this.urlParams.resetToken = this.route.snapshot.queryParamMap.get('resetToken');
 
    this.confirmEmail();

  }
  confirmEmail() {
  
    this.authService.confirmEmail(this.urlParams.resetToken).subscribe(() => {
   
      console.log("success");
      this.emailConfirmed = true;
    }, error => {
     
      console.log(error);
     
      this.emailConfirmed = false;
    })
  }

}




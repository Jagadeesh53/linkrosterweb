import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmemailotpComponent } from './confirmemailotp.component';

describe('ConfirmemailotpComponent', () => {
  let component: ConfirmemailotpComponent;
  let fixture: ComponentFixture<ConfirmemailotpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmemailotpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmemailotpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirmemailotp',
  templateUrl: './confirmemailotp.component.html',
  styleUrls: ['./confirmemailotp.component.css']
})
export class ConfirmemailotpComponent implements OnInit {
  confirmationtoken :string;
  isOTPSuccessful =false;
  isLoginFailed=false;
  isLoggedIn=false;
  errmessage=false;
  constructor(private authService: AuthService,private router: Router, 
    private tokenStorage: TokenStorageService) { }

  ngOnInit() {
  
  }
  verifyconfirmation_token()
  {
     
    if (this.tokenStorage.getUser().otp==this.confirmationtoken) {
        this.isOTPSuccessful=true;
        
        this.authService.loginsms(this.tokenStorage.getUser().phonenumber).
        subscribe(() => {
   
          console.log("success");
          
        }, error => {
         
          console.log(error);
         
        
        })
      
    }
    else
    {
     
      this.errmessage=true;
      this.isLoginFailed = true;
    }
      
  }
}

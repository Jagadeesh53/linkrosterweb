import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookmarkService } from '../_services/bookmark.service';
import { LearnService } from '../_services/learn.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { ChallengeService } from '../_services/challenge.service';
import { GoalService } from '../_services/goal.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  bookmarklist: Object;
  bookmarkCount: any;
  challengeList: Object;
  challengeCount: any;
  goalList: Object;
  goalCount: any;
  learnList: any;
  learnCount: any;
  bookmarklinklist: Object;
  bookmarklinkcount: any;
  
  constructor(
    private bookmarkservice: BookmarkService,
    private router: Router,
    private tokenStorage: TokenStorageService,
    private learnService: LearnService,
    private ChallengeService: ChallengeService,
    private GoalService:GoalService
  ) { }

  ngOnInit(): void {
    this.bookmarklinkcount = 0;
    this.getBookamrks();
    this.getChallenges();
    this.getLearn();
    this.getGoal();
  }

  getBookamrks(){
    this.bookmarkservice
        .getBookmark(this.tokenStorage.getUser().id)
        .subscribe(
          data =>{
            this.bookmarklist = data;
            if((<any>this.bookmarklist)
            .length>0){
              this.bookmarkCount = (<any>this.bookmarklist).length;
              this.getbookmarklink(this.bookmarklist);
            }
            else{
              this.bookmarkCount = 0;
            }
          }
        )
  }
  getChallenges() {
    this.ChallengeService.getChallenge(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.challengeList = data;
          if((<any>this.challengeList).length>0){
            this.challengeCount = (<any>this.challengeList).length;
          }
          else{
            this.challengeCount = 0;
          }
        }
      );
  }
  getGoal() {
    this.GoalService
      .getGoal(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.goalList = data;
          if((<any>this.goalList).length>0){
            this.goalCount = (<any>this.goalList).length;
          }
          else{
            this.goalCount = 0;
          }
        })
  }
  go() {
    this.router.navigate(["bookmark"])
  }

  getLearn() {
    this.learnService
      .getLearn(this.tokenStorage.getUser().id)
      .subscribe(
      data => {
          this.learnList = data;
          if ((<any>this.learnList).length > 0) {
            this.learnCount = (<any>this.learnList).length;
          }
          else {
            this.learnCount = 0;
          }
        },
      err => {});
  }


  getbookmarklink(bookmark) {
    this.bookmarkservice.getBookmarklink(bookmark[0].bookmarkId)
      .subscribe(
        data => {
          console.log(data);
          this.bookmarklinklist = data
          console.log(this.bookmarklinklist);
          this.bookmarklinkcount = (<any>this.bookmarklinklist).length
        }
      )
  }
}

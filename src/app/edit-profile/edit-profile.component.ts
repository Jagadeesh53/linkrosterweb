import { Component, OnInit , Input} from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';


import { TokenStorageService } from '../_services/token-storage.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { User } from '../_model/user';

import {first} from "rxjs/operators";
import { AuthService } from '../_services/auth.service';
import { identifierName } from '@angular/compiler';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
 
 
  
 
  
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = false;
   sucessmessage=false;
   paramSubscription:any;
   id: '';
   username:'';
   email:'';
   phoneNumber:'';
   password:'';
   message = '';
 
  constructor(private route: ActivatedRoute,private router: Router,private authService: AuthService) { }

  ngOnInit() {


  
    
    
    console.log("form.id",this.form);
    this.id = this.route.snapshot.params['id'];
    

     this.form.id = this.route.snapshot.queryParamMap.get('id');
   
        // this.form=this.route.snapshot.queryParamMap.getUser();
        this.form= this.authService.getUser();
   
  }


  getUser(id) {
    this.authService.get(id)
      .subscribe(
        data => {
          this.form= data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
 
  changePassword() {
    
    const data = {
      username: this.form.username,
      email:this.form.email,
    password:this.form.password,
  phoneNumber:this.form.phoneNumber
  
    };
   
    this.authService.changeUser(this.form.id,data)
    
     .subscribe((data) => {
       console.log(data);
       this.isSuccessful = true;
         this.sucessmessage=true;
         
         this.isSignUpFailed = false;
       console.log("success");
       
     }, error => {
       this.errorMessage = true;
         this.isSignUpFailed = true;
 
       console.log(error);
     
     })
    //  alert('hello');
   }
 
 
}

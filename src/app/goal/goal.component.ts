import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { TokenStorageService } from '../_services/token-storage.service';
import { GoalService } from '../_services/goal.service';
import { NgForm } from '@angular/forms';
declare var $: any;
@Component({
  selector: 'app-goal',
  templateUrl: './goal.component.html',
  styleUrls: ['./goal.component.css']
})
export class GoalComponent implements OnInit {
  SkillKeypointList:any =[];
  editSkillkeypoints: any;

  constructor(private goalService: GoalService,
    private router: Router,
    private tokenStorage: TokenStorageService,
    private SpinnerService: NgxSpinnerService) { }
  goal = {

    goalName: '',
    description: '',
    userId: 0
  };
  step = {
    stepName: '',
    goal_id: 0,
    userId: 0
  };
  skill = {
    skillName: '',
    goal_id: 0,
    userId: 0
  };
  link = {
    steplink: '',
    step_id: 0,
    userId: 0
  };
  skilllinks = {
    skilllink: '',
    skill_id: 0,
    userId: 0
  };
  stepkeypoint = {
    keypoints: '',
    steplinkId: 0,
    stepId: 0
  };
  skillkeypoint = {
    keypoints: '',
    skilllinkId: 0,
    skillId: 0
  };
  sharesteplink = {
    steplink: '',
    stepId: 0,
    shareId: 0,
    accessType: null,
    userId: 0,
    sharedTime: Date()
  };
  shareskilllink = {
    skilllink: '',
    skillId: 0,
    shareId: 0,
    accessType: '',
    userId: 0,
    sharedTime: Date()
  };
  shareGoal = {
    goalName: '',
    goalId: 0,
    shareId: 0,
    accessType: '',
    userId: 0,
    sharedTime: Date()
  };
  editComment: string;
  goalName: string;
  p: number = 1;
  errorMessage = '';
  sucessmessage: any;
  isLoggedIn = false;
  isSuccessful = false;
  goallist: any = [];
  goalNameselected: any = {};
  goalNameselectedId: any = {};
  isgetGoal = false; Link: string;
  editField: string; enableEdit = false; enablestepEdit = false; enableskillEdit = false;
  keypoints: string; showKeyModal: boolean; enableskillEditIndex = null;
  public currentgoalid; public currentstepid; public currentskillid;
  public currentgoalname; public currentstepname; public currentskillname;
  enableEditIndex = null; iskeySuccessful = false;
  hrefvisible = true;; showCollasalIndex = null;
  goalId: number; skilllinklist: any = []; linklist: any = [];
  steplist: any = []; skilllist: any = []; skilllistVisible = true;
  goalisSuccessful = false; goalerrorMessage = '';
  stepisSuccessful = false; skillisSuccessful = false;
  steplistVisible = true;
  stepId: number; skillId: number; enablestepEditIndex = null;
  stepNameselected: any = {}; stepNameselectedId: any = {};
  StepLinkId: number; steplinkId: number; skilllinkId: number;
  KeypointList: any = []; keypointId: number; skillKeypointId: number;
  limitedKeypointList: any = []; limitedKeypointList2: any = [];
  showKeypointIndex = null; username: string; steplink: string;
  skilllink: string; skillNameselectedId: any = {}; skillNameselected: any = {};
  showskillKeypointIndex = null;
  opentesttoggle(selectedItem: any, i) {
    this.showCollasalIndex = i;
    this.stepId = selectedItem.stepId;
    this.getstepLink(this.stepId);
    this.skillId = selectedItem.skillId;
  }
  opentesttoggles(selectedItem: any, i) {
    this.showCollasalIndex = i;
    // this.stepId = selectedItem.stepId;
    this.skillId = selectedItem.skillId;
    this.getskillLink(this.skillId);
  }
  buttonCol(selectedItem: any) {
    this.SpinnerService.show();
    this.steplinkId = selectedItem.steplinkId;// You get the Id of the selected item here
  }
  openNav(selectedItem: any) {
    this.steplinkId = selectedItem.steplinkId;// You get the Id of the selected item here      
  }
  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
  hide() {
    this.showKeyModal = false;
  }
  onchangeAccessType(val: any) {
    this.sharesteplink.accessType = val;
    this.shareskilllink.accessType = val;
  }
  prevent(keypoints: string) {
    event.preventDefault();
    this.EditstepKeypoint(keypoints);
  }
  skillprevent(keypoints: string) {
    event.preventDefault();
    this.EditskillKeypoint(keypoints);
  }
  showstepsShareModal(selectedItem: any): void {

    $("#stepsshareModal").modal('show');
    //this.stepLink =selectedItem.stepLink;   
    console.log("Selected item Id: ", selectedItem.Id);
    this.steplink = selectedItem.steplink;
    this.steplinkId = selectedItem.steplinkId;// You get the Id of the selected item here
    this.stepId = selectedItem.stepId;
  }
  showskillShareModal(selectedItem: any): void {
    $("#skillshareModal").modal('show');
    console.log("selected item Id:", selectedItem.Id);
    this.skilllink = selectedItem.skilllink;
    this.skilllinkId = selectedItem.skilllinkId;
    this.skillId = selectedItem.skillId;
  }
  showstepModal(selectedItem: any): void {

    $("#mystepdelModal").modal('show');
    this.steplink = selectedItem.steplink;
    console.log("Selected item Id: ", selectedItem.Id);
    this.steplinkId = selectedItem.steplinkId;// You get the Id of the selected item here
    this.stepId = selectedItem.stepId;

  }
  showskillModal(selectedItem: any): void {
    $("#myskilldelModal").modal('show');
    this.skilllink = selectedItem.skilllink;
    console.log("Selected item Id: ", selectedItem.Id);
    this.skilllinkId = selectedItem.skilllinkId;
    this.skillId = selectedItem.skillId;
  }
  opencommentstoggle(selectedItem: any, i) {


    this.showKeypointIndex = i;
    this.steplinkId = selectedItem.steplinkId;
    this.getstepkeypoint(this.steplinkId);
  }
  closecommentstoggle(selectedItem: any, i) {


    this.showKeypointIndex = null;
    this.steplinkId = selectedItem.steplinkId;
    //this.getstepkeypoint(this.steplinkId );    
  }
  openskillcommentstoggle(selectedItem: any, i) {
    this.showskillKeypointIndex = i;
    this.skilllinkId = selectedItem.skilllinkId;
    //alert(this.skilllinkId)
    this.getskillkeypoint(this.skilllinkId);
  }
  closeskillcommentstoggle(selectedItem: any, i) {
    this.showskillKeypointIndex = null;
    this.skilllinkId = selectedItem.skilllinkId;
    //alert(this.skilllinkId)
    //this.getskillkeypoint(this.skilllinkId);
  }
  showEditKeyModal(selectedItem: any): void {

    $("#myEditKeyPointModal").modal('show');
    this.StepLinkId = selectedItem.steplinkId;
    this.editComment = selectedItem.keypoints;
    this.keypoints = selectedItem.keypoints;
    this.keypointId = selectedItem.keypointId;
    this.stepId = selectedItem.stepId;
    this.showKeyModal = true;
  }
  showskillEditKeyModal(selectedItem: any): void {
    $("#myskillEditKeyPointModal").modal('show');
    this.skilllinkId = selectedItem.skilllinkId;
    this.keypoints = selectedItem.keypoints;
    this.editSkillkeypoints = selectedItem.keypoints;
    this.skillKeypointId = selectedItem.skillKeypointId;

    this.skillId = selectedItem.skillId;
    this.showKeyModal = true;


  }

  showdelKeyModal(selectedItem: any): void {

    $("#mydelKeyModal").modal('show');
    this.keypointId = selectedItem.keypointId;
    this.StepLinkId = selectedItem.steplinkId;// You get the Id of the selected item here

  }
  showskilldelKeyModal(selectedItem: any): void {
    $("#myskilldelKeypointModal").modal('show');
    this.skillKeypointId = selectedItem.skillKeypointId;
  }
  sendModal(): void {
    //do something here
    this.hideModal();

  }
  opentoggle(id: number, property: string, event: any) {


    $("#slideme").slideToggle();
  }
  onKeyPointsViewMore(): void {
    this.limitedKeypointList2 = this.KeypointList.slice(3, this.KeypointList.length);

    this.limitedKeypointList = this.limitedKeypointList.concat(this.limitedKeypointList2);
    console.log(this.limitedKeypointList);
    console.log(this.KeypointList.length, this.limitedKeypointList.length);
    $('#view-more').hide();
    $('#view-less').show();
  }

  onkeyPointsviewless(): void {
    this.limitedKeypointList = this.KeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }
  ngOnInit(): void {
    this.goalNameselected = "Goal Name";
    this.getGoal();
    // this.getGoalStep();
    // this.getSkill();
    this.steplistVisible = false;
    this.skilllistVisible = false;
    if (this.goallist == []) {

      this.isgetGoal = true;
      this.errorMessage = "No Topics"
    }
  }

  getGoalid(goallist: any, i: any) {

    this.goalNameselectedId = goallist.goalId;
    this.goalNameselected = goallist.goalName;
    this.getGoalStep();
    this.getSkill();
    // this.getstepLink(this.stepId);
    // this.getskillLink(this.skillId);
    // this.getstepkeypoint(this.steplinkId);
  }
  getStepid(steplist: any, i: any) {

    this.stepNameselectedId = steplist.stepId;
    this.stepNameselected = steplist.stepName;
  }
  getSkillid(skilllist: any, i: any) {
    this.skillNameselectedId = skilllist.stepId;
    this.skillNameselected = skilllist.stepName;

  }
  AddGoal(f: NgForm) {

    // this.SpinnerService.show();
    const data = {
      goalName: this.goal.goalName,
      description: this.goal.description,
      userId: this.tokenStorage.getUser().id
    };

    this.goalService.addGoal(data).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        f.resetForm();
        this.getGoal();

        console.log("added");
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      }
    );
    // $('.goalName').val('');
    // $('.description').val('');
  }

  AddStep(fl: NgForm) {
    if (this.goalNameselected == "Goal Name") {

      this.errorMessage = "*Please Select Goal";
    }
    else {
      const datastep = {
        stepName: this.step.stepName,
        goalId: this.goalNameselectedId,

        userId: this.tokenStorage.getUser().id
      };
      this.goalService.addgoalStep(datastep).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getGoalStep();
          fl.resetForm();

          console.log("added");
        },
        err => {
          this.errorMessage = '';
          this.goalerrorMessage = err.error.message;
          this.stepisSuccessful = false;
        }
      );
    }
  }
  AddSkill(f2: NgForm) {
    if (this.goalNameselected == "Goal Name") {

      this.errorMessage = "*Please Select Goal";
    }
    else {
      const dataskill = {
        skillName: this.skill.skillName,
        goalId: this.goalNameselectedId,

        userId: this.tokenStorage.getUser().id
      };
      this.goalService.addgoalSkill(dataskill).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          f2.resetForm();
          this.getSkill();

          console.log("added");
        },
        err => {
          this.errorMessage = '';
          this.goalerrorMessage = err.error.message;
          this.skillisSuccessful = false;
        }
      );
    }
  }
  linkisSuccessful = false;
  AddstepLink(stepId, steplink, id, goalId) {

    const data = {
      steplink: steplink,
      stepId: stepId,
      goalId: goalId,
      userId: this.tokenStorage.getUser().id
    };

    this.goalService.addstepLink(data).subscribe(
      data => {
        this.showCollasalIndex = id;
        this.stepId = stepId;
        this.getGoalStep();
        //this.getstepLink(this.stepId);
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.linkisSuccessful = true;
      },
      err => {
        this.errorMessage = err.error.message;
        this.linkisSuccessful = false;
      }
    );
    this.getstepLink(stepId);
  }


  AddskillLink(skillId, skilllink, id, goalId) {

    const data = {
      skilllink: skilllink,
      skillId: skillId,
      goalId: goalId,
      userId: this.tokenStorage.getUser().id
    };
    this.goalService.addskillLink(data).subscribe(
      data => {
        this.showCollasalIndex = id;
        this.skillId = skillId;
        this.getSkill();
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        console.log("added");
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      }
    );
    this.getskillLink(skillId);
  }

  getGoal() {

    this.goalService
      .getGoal(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.goallist = data
          //this.getGoalid(this.goallist[0],(itemsPerPage*(p-1)));
          console.log("goallist", this.goallist)
          if (this.goallist.length > 0) {
            this.getGoalid(this.goallist[0], 0)
          }
          else {
            this.goalNameselected = "Goal Name"
          }
          setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.SpinnerService.hide();
          }, 1000);
        },
        err => {

          this.goallist = JSON.parse(err.error).message;
        }
      );
  }

  getGoalStep() {
    this.steplist = [];
    this.goalService.getgoalStep(this.goalNameselectedId)
      .subscribe(
        data => {
          this.steplist = data
          console.log(data);
          if(this.steplist.length>0){
            this.opentesttoggle(data[0], 0);
          }
          this.steplistVisible = true;
        },
        err => console.error(err),
        () => console.log('getstep completed')
      );
  }

  showdelStepModal(step){
    $("mydelStepModal").modal('show');
    this.stepId = step.stepId;
    console.log("484", step);
  }

  deletestep() {
    this.goalService.deleteStep(this.stepId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.isSuccessful = true;
          this.sucessmessage = "step is deleted";
          this.getGoalStep();
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
    // this.getGoalStep();
    this.hideModal();
  }
  getSkill() {
    this.skilllist = [];
    this.goalService.getgoalSkill(this.goalNameselectedId)
      .subscribe(
        data => {
          this.skilllist = data
          console.log(data);
          if(this.skilllist.length>0){
            this.opentesttoggles(data[0], 0);
          }
          this.skilllistVisible = true;
        },
        err => console.error(err),
        () => console.log('getskill completed')
      );
  }

  showdelSkillModal(skill){
    $("mydelSkillModal").modal('show');
    this.skillId = skill.skillId;
  }

  deleteskill(){
    this.goalService.deleteSkill(this.skillId)
        .subscribe(
          data =>{
            console.log(data);
            this.isSuccessful = true;
            this.sucessmessage = "skill is deleted";
            document.getElementById('skill-modal').click();
            this.getSkill();
          },
          err =>{
            this.errorMessage = err.error.message;
            this.isSuccessful = false;
          }
        )
  }


  steplinklistVisible = true; skilllinklistVisible = true;
  getstepLink(stepId: number) {

    this.goalService.getstepLink(stepId)
      .subscribe(
        data => {
          this.linklist = data
          console.log(data);
          localStorage.setItem("GoalsteplinkCount", this.linklist.length)
          this.steplinklistVisible = true;
        },
        err => console.error(err),
        () => console.log('getsteplink completed')
      );
  }
  getskillLink(skillId: number) {

    this.goalService.getskilllink(skillId)
      .subscribe(
        data => {

          this.skilllinklist = data
          console.log(data);
          this.skilllinklistVisible = true;
        },
        err => console.error(err),
        () => console.log('getskilllink completed')
      );
  }
  deleteskilllink() {

    this.goalService.deleteskilllink(this.skilllinkId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";
          this.getSkill();
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
    this.getSkill();
    this.hideModal();
  }
  AddstepKeypoint(steplinkId, stepId, goalId, fAddKey: NgForm) {
    this.iskeySuccessful = false;
    const dataKey = {
      keypoints: this.stepkeypoint.keypoints,
      steplinkId: this.steplinkId,
      stepId: stepId,
      goalId: goalId,
      userId: this.tokenStorage.getUser().id,
    };
    this.goalService.addstepKeypoint(dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Added";
        console.log(this.isLoggedIn);
        this.isLoggedIn = true;
        this.iskeySuccessful = true;

        fAddKey.resetForm();

        var $parent = this;
        setTimeout(function () {
          //$("#myModal").modal('hide');
          //$('#myModal').hide();
          $parent.sucessmessage = "";
          $parent.isLoggedIn = false;
          $parent.iskeySuccessful = false;
          $parent.showKeyModal = false;
          $parent.hideModal();
          // $('.modal-backdrop').remove();
        }, 1000);
        this.getstepkeypoint(this.steplinkId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );

    // this.getstepkeypoint(this.steplinkId);

  }

  getstepkeypoint(steplinkId: number) {

    this.KeypointList = [];
    this.goalService.getstepKeypoint(steplinkId)
      .subscribe(
        data => {
          this.KeypointList = data;
          console.log("keypoints", this.KeypointList);
          this.limitedKeypointList = this.KeypointList.slice(0, 3)
          console.log("sliced data", this.limitedKeypointList);
        },
        err => {
          return console.error(err);
        }
      );
  }
  deletestepKeypoint() {
    this.goalService.deletestepKeypoint(this.keypointId)
      .subscribe(
        data => {
          console.log(data);
          this.getstepkeypoint(this.steplinkId);
        },
        error => console.log(error));
    this.getstepkeypoint(this.steplinkId);
  }
  EditstepKeypoint(editComment: String) {
    this.iskeySuccessful = false;
    const dataKey = {
      keypoints: editComment,
      steplinkId: this.steplinkId,
      stepId: this.stepId,
      userId: this.tokenStorage.getUser().id
    };
    this.goalService.updatestepKeypoint(this.keypointId, dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Edited";
        this.iskeySuccessful = true;
        this.getstepkeypoint(this.steplinkId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
    // this.getstepkeypoint(this.steplinkId);

  }
  Addskillkeypoints(skilllinkId, skillId, goalId, f1AddKey: NgForm) {
    const dataKey = {
      keypoints: this.skillkeypoint.keypoints,
      skilllinkId: this.skilllinkId,
      skillId: this.skillId,
      goalId: goalId,
      userId: this.tokenStorage.getUser().id
    };

    this.goalService.addskillKeypoint(dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Added";
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        f1AddKey.resetForm();
        var $parent = this;
        setTimeout(function () {

          $parent.sucessmessage = "";
          $parent.isLoggedIn = false;
          $parent.iskeySuccessful = false;
          $parent.showKeyModal = false;

          document.getElementById('skillKeypoint-close-btn').click();

        }, 1000);
        this.getskillkeypoint(this.skilllinkId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
    //this.hideModal();
  }
  getskillkeypoint(skilllinkId: number) {
    this.SkillKeypointList = [];
    this.goalService.getskillKeypoint(skilllinkId)
      .subscribe(
        data => {
          this.SkillKeypointList = data;
        },
        err => {
          return console.error(err);
        }
        // () => console.log('getKeypointList completed') 
      );
  }
  deleteKeypoint() {
    this.goalService.deleteskillKeypoint(this.skillKeypointId)

      .subscribe(
        data => {
          console.log(data);
          this.getskillkeypoint(this.skilllinkId);

        },
        error => console.log(error));
    this.getskillkeypoint(this.skilllinkId);
  }
  EditskillKeypoint(keypoints: String) {
    const dataKey = {
      keypoints: keypoints,
      skilllinkId: this.skilllinkId,
      skillId: this.skillId,
      userId: this.tokenStorage.getUser().id
    };
    this.goalService.updateskillKeypoint(this.skillKeypointId, dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Edited";
        this.iskeySuccessful = true;
        this.getskillkeypoint(this.skilllinkId);

      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );

    // this.getskillkeypoint(this.skilllinkId);


  }

  isdelete = false;
  deletesteplink() {

    this.goalService.deletestepLink(this.steplinkId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";
          this.getGoalStep();
        },

        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
    this.getGoalStep();
    this.hideModal();
  }
  deletesteplinkBystep(stepId) {

    this.goalService.deletesteplinkBystep(stepId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";

        },

        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
  }

  changetestValue(event: any) {

    this.editField = event.target.value;
    this.findByGoalName(this.editField);
    this.getGoal();
  }
  findByGoalName(key) {
    this.isSuccessful = true;
    this.enableEdit = false;

    this.goalService.findByGoalName(key,
      this.tokenStorage.getUser().id)

      .subscribe(
        data => {
          this.goallist = data
          console.log("list", data);

        },
        err => {
          this.goallist = JSON.parse(err.error).message;
        }
      );
  }

  editGoal(goalId: number, goalName: String) {
    if (goalName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableEdit = true;
      const data = {
        goalName: goalName,
        userId: this.tokenStorage.getUser().id
      };

      this.goalService.updateGoal(goalId, data).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getGoal();
          this.enableEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );

    }
  }
  enableEditMethod(e, i) {

    this.currentgoalid = e.goalId;
    this.currentgoalname = e.goalName;
    this.isSuccessful = true;
    this.enableEdit = true;
    this.enableEditIndex = i;
    console.log(i, e);
    this.hrefvisible = false;
    this.hrefvisible = i;

  }
  changeValue(id: number, property: string, event: any) {

    this.editField = event.target.textContent;

  }
  onSelectKeypoint(selectedItem: any) {
    this.iskeySuccessful = false;
    $("#myModal").modal('show');
    this.keypoints = selectedItem.keypoints;
    console.log("Selected item Id: ", selectedItem.steplinkId);
    this.steplinkId = selectedItem.steplinkId;// You get the Id of the selected item here
    //this.getstepkeypoint(this.steplinkId);
    console.log(this.steplinkId)
    this.stepId = selectedItem.stepId;
    this.showKeyModal = true;
  }
  onSelectskillKeypoint(selectedItem: any) {
    // alert("hi")
    this.iskeySuccessful=false;
    $("#mykeyModal").modal('show');
    this.keypoints = selectedItem.keypoints;
    console.log("Selected item Id: ", selectedItem.Id);
    this.skilllinkId = selectedItem.skilllinkId;
    this.skillId = selectedItem.skillId;
    this.showKeyModal = true;

  }
  showdelGoalModal(selectedItem: any): void {

    $("#mydelGoalModal").modal('show');

    console.log("Selected item Id: ", selectedItem.Id);

    this.goalId = selectedItem.goalId;// You get the Id of the selected item here

  }
  showdelstepModal(selectedItem: any): void {
    $("#mydelstepModal").modal('show');
    console.log("Selected item Id: ", selectedItem.Id);
    this.stepId = selectedItem.stepId;
  }
  deleteGoal() {

    this.goalService.deleteGoal(this.goalId)
      .subscribe(
        data => {
          this.getGoal();
        },
        error => console.log(error));
    this.hideModal();

  }
  reloadData() {
    this.goallist = this.getGoal();
  }
  hideModal(): void {

    document.getElementById('close-modal').click();
  }
  editStep(stepId: number, stepName: String) {
    if (stepName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enablestepEdit = true;
      const data = {
        stepName: stepName,
        userId: this.tokenStorage.getUser().id
      };

      this.goalService.updategoalStep(stepId, data).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getGoalStep();
          this.enablestepEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );

    }
  }
  enablestepEditMethod(s, id) {

    this.currentstepid = s.stepId;
    this.currentstepname = s.stepName;
    this.isSuccessful = true;
    this.enablestepEdit = true;
    this.enablestepEditIndex = id;
    console.log(id, s);
    this.hrefvisible = false;
    this.hrefvisible = id;

  }
  sharestep(steplinkId: number,fshare: NgForm) {
    if (!this.sharesteplink.accessType) {
      this.isSuccessful = false;
    }
    else {
      this.steplinkId = steplinkId;
      const datalink = {
        stepId: this.stepId,
        steplinkId: steplinkId,
        steplink: this.steplink,
        userId: this.tokenStorage.getUser().id,
        accesstype: this.sharesteplink.accessType,
        //username:this.username,
        sharedTime: new Date().toDateString()
      };

      this.goalService.addsharestep(datalink).subscribe(
        (data: any) => {
          console.log(data);
          document.getElementById("shareclose").click();
          this.isSuccessful = true;
          this.sharesteplink.accessType ='';
          fshare.resetForm()
          this.getstepLink(this.stepId);
        },
        (err: any) => {
          this.isSuccessful = false;
        });
      }
}
  shareskill(skilllinkId: number,fshare:NgForm) {
    this.skilllinkId = skilllinkId;
    if(!this.shareskilllink.accessType){
      this.isSuccessful=false;
    }
    else{
      const datalink = {
        skillId: this.skillId,
        skilllinkId: skilllinkId,
        skilllink: this.skilllink,
        userId: this.tokenStorage.getUser().id,
        accesstype: this.shareskilllink.accessType,
        //username:this.username,
        sharedTime: new Date().toDateString()
      };
      this.goalService.addshareskill(datalink).subscribe(
        (data: any) => {
          console.log(data);
          document.getElementById("sharedclose").click();
          fshare.resetForm()
          this.isSuccessful=true;
          this.shareskilllink.accessType='';
          this.getskillLink(this.skillId);
        },
        (err: any) => {
          this.isSuccessful=false;
        });
    }

  }

  showGoalShareModal(selectedItem: any): void {


    this.goalName = selectedItem.goalName;
    console.log("Selected item Id: ", selectedItem.goalId);
    this.goalId = selectedItem.goalId;// You get the Id of the selected item here

  }

  onchangeGoalAccessType(val: any) {

    this.shareGoal.accessType = val;


  }

  shareGoalName(goalId: number,) {


    this.goalId = goalId;

    const datalink = {
      goalId: goalId,
      goalName: this.goalName,
      userId: this.tokenStorage.getUser().id,
      accesstype: this.shareGoal.accessType,
      sharedTime: new Date().toDateString()

    };
    this.goalService.addshareGoal(datalink).subscribe(
      (data: any) => {
        console.log(data);
        this.getGoal();
      },
      (err: any) => {

      }
    );

  }
  editSkill(skillId: number, skillName: String) {
    if (skillName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableskillEdit = true;
      const data = {
        skillName: skillName,
        userId: this.tokenStorage.getUser().id
      };

      this.goalService.updateskill(skillId, data).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getSkill();
          this.enableskillEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );

    }
  }
  enableskillEditMethod(s, id) {

    this.currentskillid = s.skillId;
    this.currentskillname = s.skillName;
    this.isSuccessful = true;
    this.enableskillEdit = true;
    this.enableskillEditIndex = id;
    console.log(id, s);
    this.hrefvisible = false;
    this.hrefvisible = id;

  }



}

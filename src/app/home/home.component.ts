import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
import {TeamService} from '../_services/team.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import {FormControl,FormGroup,Validators} from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  loginForm = new FormGroup({
    email : new FormControl('',[Validators.required,Validators.email,Validators.pattern('^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$')]),
    password : new FormControl('',[Validators.required,Validators.minLength(6)]),
    })  
    get email(){return this.loginForm.get('email')}
    get password(){return this.loginForm.get('password')}

  content: string;
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  isOTPSuccessful=false;
  phoneOTPcode:String;
  isPhonenotconfirmed=false;
  isSuccessful=false;
  accept_emailConfirmed: boolean = false;
    urlParams: any = {};
    message:string;

constructor(private userService: UserService,private authService: AuthService,private router: Router, 
    private tokenStorage: TokenStorageService,private teamService: TeamService,private route: ActivatedRoute,private SpinnerService: NgxSpinnerService) { }
    

  ngOnInit() {
    // this.SpinnerService.show();
    this.userService.getPublicContent().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );

    if (localStorage.getItem("auth-token")) {
    this.isLoggedIn = true;
    this.roles = this.tokenStorage.getUser().roles;
  }
  this.urlParams.emailId = this.route.snapshot.queryParamMap.get('emailId');
  this.route.queryParams.subscribe((params)=> {
  
    if(params['emailId']){
   
      this.acceptEmail();
      console.log(params['emailId']);
    } else {
     
      console.log('id not found in params')
    }
  });

}

  acceptEmail() {
  
    this.teamService.showAcceptInvitePage(this.urlParams.emailId)
    .subscribe(() => {
   
      console.log("success");
      this.accept_emailConfirmed = true;
    }, error => {
     
      console.log(error);
     
      this.accept_emailConfirmed = false;
    })
   
  }

  onSubmit() {
   //alert("hi")
    this.authService.login(this.form).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);
        localStorage.setItem("auth-user", JSON.stringify(data));
       
       if(this.tokenStorage.getUser().isEmailVerified=="0" )
       {
    
        this.isLoginFailed = true;
        this.isLoggedIn = false;
       
        this.errorMessage="Please Confirm your Email ID";
      
        window.sessionStorage.clear();
      

       }
     
      else
      {
        this.route.queryParams.subscribe((params)=> {
          
          if(params['emailId']){
          
            this.router.navigate(['shareteam'])
          } 
          else {
            
          this.isLoginFailed = false;
          this.isLoggedIn = true;
         
        localStorage.setItem("auth-token", data.accessToken);
          this.roles = this.tokenStorage.getUser().roles;
          this.reloadPage();      
          }
        });
      }
        err => {
         
  
          this.errorMessage = err.error.message;
          console.log(this.errorMessage);
          this.isLoginFailed = true;
        }
       
      },
      err => {
         
        this.isLoginFailed = true;
        //this.errorMessage = err.error.message;
        this.errorMessage = "Invalid username/password"
       // console.log("hhhhhhhhhhhhhhh",this.errorMessage);
       
      }
      
    );
    //this.router.navigate(['home'])  
      
     
   
 
  }


  reloadPage() {
    window.location.reload();
  }

  SignUp()
  {

  }
  verifyOTPCode()
  {
    
    if (this.tokenStorage.getUser().smscode==this.phoneOTPcode) {
       this.isOTPSuccessful=true;
       
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        
        this.roles = this.tokenStorage.getUser().roles;
        this.reloadPage()
    }
    else
    {
      
    }
  }
}

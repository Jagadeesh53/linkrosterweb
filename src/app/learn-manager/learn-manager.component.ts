import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { LearnService } from '../_services/learn.service';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-learn-manager',
  templateUrl: './learn-manager.component.html',
  styleUrls: ['./learn-manager.component.css']
})
export class LearnManagerComponent implements OnInit {
  alreadyTaken: boolean;
  editComment: any;

  constructor(private learnService: LearnService,
    private router: Router,
    private tokenStorage: TokenStorageService,
    private SpinnerService: NgxSpinnerService
  ) { }
  learn = {

    learnName: '',
    learnDescription: '',
    userId: 0
  };
  concept = {
    conceptName: '',
    conceptCategoryId: 0,
    userId: 0
  };

  conceptlink = {
    conceptlink: '',
    conceptId: 0,
    userId: 0
  };
  keypoint = {
    keypoints: '',
    conceptlinkId: 0,
    conceptLinkCategoryId: 0,
    userId: 0
  };
  shareconceptlink = {
    conceptlinkId: 0,
    accessType: '',
    sharedTime: Date(),
    userId: 0
  };

  shareTopic = {
    learnName: '',
    learnId: 0,
    shareId: 0,
    accessType: '',
    userId: 0,
    sharedTime: Date()
  };
  conceptName: string;
  p: number = 1;
  errorMessage = '';
  sucessmessage: any;
  isLoggedIn = false;
  isSuccessful = false;
  learnlist: any = [];
  learnNameselected: any = {};
  learnNameselectedId: any = {};
  isgetLearn = false;
  editField: string;
  enableEdit = false;
  public currentlearnid;
  public currentlearnname;
  enableEditIndex = null;
  hrefvisible = true;
  learnId: number; conceptlinklist: any = [];
  conceptlist: any = [];
  KeypointList: any = []; learnDescription: string;
  learnisSuccessful = false; learnerrorMessage = '';
  conceptisSuccessful = false;
  conceptlistVisible = true; showCollasalIndex = null;
  conceptId: number;
  conceptNameselected: any = {}; conceptNameselectedId: any = {};
  conceptLinkCategoryId: number;
  conceptlinkId: number;
  showKeyModal = false;
  keyComments: '';
  conceptCategoryId: any;
  iskeySuccessful = false;
  conceptLink: string;
  showKeypointIndex = null;
  keypoints: string;
  accessType: string;
  conceptKeypointId: number;
  learnName: string;
  isdelete = false;
  enableconceptEdit = false;
  enableconceptEditIndex = null;
  public currentconceptid;
  public currentconceptname;
  @ViewChild('conceptshareModal') conceptshareModal: ElementRef;
  // opentesttoggle(selectedItem:any,i)
  // {
  //    this.showCollasalIndex=i;
  //    this.conceptId =selectedItem.conceptId;

  //   }
  ngOnInit(): void {
    this.learnNameselected = "addConcept";
    this.getLearn();
    this.conceptlistVisible = false;
    if (this.learnlist == []) {

      this.isgetLearn = true;
      this.errorMessage = "No Topics"
    }
  }

  getLearnid(learnlist: any, i: any) {

    this.learnNameselectedId = learnlist.learnId;
    this.learnNameselected = learnlist.learnName;
    this.getconcept();
    //this.getconceptLink(this.conceptLinkCategoryId);
  }


  AddLearn(f) {


    const data = {
      learnName: this.learn.learnName,
      learnDescription: this.learn.learnDescription,
      userId: this.tokenStorage.getUser().id
    };

    this.learnService.addTopic(data).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        this.getLearn();
        f.resetForm();
        console.log("added");

      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      }
    );

  }

  AddConcept(fl) {
    if (this.learnNameselected == "Add concept") {

      this.errorMessage = "*Please Select Topic";
    }
    else {
      const datastep = {
        conceptName: this.concept.conceptName,
        conceptCategoryId: this.learnNameselectedId,

        userId: this.tokenStorage.getUser().id
      };
      this.learnService.addConcept(datastep).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          fl.resetForm();
          this.getconcept();

          console.log("added");
          this.concept.conceptName = '';
        },
        err => {
          this.errorMessage = '';
          //this.learnerrorMessage =err.error.message;
          this.learnerrorMessage = "Enter Topic First"
          this.conceptisSuccessful = false;
        }
      );
    }
  }

  AddconceptLink(conceptId, conceptlink, i, conceptCategoryId) {

    const data = {
      conceptLink: conceptlink,
      conceptLinkCategoryId: conceptId,
      conceptCategoryId: conceptCategoryId,
      userId: this.tokenStorage.getUser().id
    };

    this.learnService.addconceptLink(data).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        this.showCollasalIndex = i;
        //  this.conceptLinkCategoryId =selectedItem.conceptId;
        // this.conceptLinkCategoryId =conceptId;
        this.getconcept();
        this.getconceptLink(this.conceptLinkCategoryId);

        console.log("added");
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      }
    );
  }
  hide() {
    this.showKeyModal = false;
  }
  opentesttoggle(selectedItem: any, i) {


    this.showCollasalIndex = i;
    this.conceptLinkCategoryId = selectedItem.conceptId;
    this.conceptCategoryId = selectedItem.conceptCategoryId;
    this.getconceptLink(this.conceptLinkCategoryId);


  }
  opencommentstoggle(selectedItem: any, i) {
    console.log(selectedItem, i);

    this.showKeypointIndex = i;
    this.conceptlinkId = selectedItem.conceptlinkId;
    this.getConceptkeypoint(this.conceptlinkId);
  }
  closecommenttoggle() {
    this.showKeypointIndex = "";
  }
  closetesttoggle(selectedItem: any, i) {
    this.showKeypointIndex = null;
    this.conceptlinkId = selectedItem.conceptlinkId;
  }

  showEditKeyModal(selectedItem: any): void {
    this.isSuccessful = false;

    $("#myEditKeyPointModal").modal('show');
    this.conceptlinkId = selectedItem.conceptlinkId;

    this.editComment = selectedItem.keypoints;
    this.conceptKeypointId = selectedItem.conceptKeypointId;
    this.conceptLinkCategoryId = selectedItem.conceptLinkCategoryId;
    this.showKeyModal = true;

  }
  showdelKeyModal(selectedItem: any): void {

    $("#mydelKeyModal").modal('show');
    this.keypoints = selectedItem.keypoints;
    this.conceptKeypointId = selectedItem.conceptKeypointId;

    this.conceptlinkId = selectedItem.conceptlinkId;// You get the Id of the selected item here

  }
  deleteKeypoint() {


    this.learnService.deleteKeypoint(this.conceptKeypointId)

      .subscribe(
        data => {
          console.log(data);
          this.getConceptkeypoint(this.conceptlinkId);

        },
        error => console.log(error));

    this.getConceptkeypoint(this.conceptlinkId);


  }
  EditKeypoint(keyComments: String) {
    const dataKey = {
      keypoints: keyComments,
      conceptlinkId: this.conceptlinkId,
      conceptLinkCategoryId: this.conceptLinkCategoryId,
      userId: this.tokenStorage.getUser().id
    };


    this.learnService.updateKeypoint(this.conceptKeypointId, dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Edited";

        this.iskeySuccessful = true;
        this.getConceptkeypoint(this.conceptlinkId);

      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );

    this.getConceptkeypoint(this.conceptlinkId);


  }
  prevent(keypoints: string) {
    event.preventDefault();
    this.EditKeypoint(keypoints);


  }
  onSelectKeypoint(selectedItem: any) {
    this.iskeySuccessful = false
    $("#myModal").modal('show');
    this.keyComments = selectedItem.keypoints;
    console.log("Selected item Id: ", selectedItem.Id);
    this.conceptlinkId = selectedItem.conceptlinkId;// You get the Id of the selected item here
    this.showKeyModal = true;
    this.conceptLinkCategoryId = selectedItem.conceptLinkCategoryId
    //this.getkeypoint(this.BookmarkLinkId);
  }
  Addconceptkeypoints(form, conceptId: number, conceptCategoryId) {
    console.log(form);

    form.submitted = false
    const dataKey = {
      keypoints: this.keypoint.keypoints,
      conceptlinkId: this.conceptlinkId,
      conceptLinkCategoryId: conceptId,
      conceptCategoryId: conceptCategoryId,
      userId: this.tokenStorage.getUser().id
    };

    this.learnService.addconceptkeypoints(dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Added";
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        this.keypoint.keypoints = '';
        this.getConceptkeypoint(this.conceptlinkId);
        
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );

    //  this.getsolutionkeypoint(this.solutionlinkId);
    this.hideModal();
  }
  showModal(selectedItem: any): void {

    $("#mydelModal").modal('show');
    // this.Link =selectedItem.link;  
    this.conceptLinkCategoryId = selectedItem.conceptLinkCategoryId;
    this.conceptlinkId = selectedItem.conceptlinkId;// You get the Id of the selected item here

  }
  deleteconceptlink() {

    this.learnService.deleteconceptlink(this.conceptlinkId)
      .subscribe(
        data => {
          console.log(data);
          document.getElementById('close-modal').click();
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";
          this.getconcept();
          this.conceptlinklist = this.getconceptLink(this.conceptLinkCategoryId);

        },

        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
    // this.getconceptLink(this.conceptCategoryId);
    // this.hideModal();
  }

  showLearnShareModal(selectedItem: any): void {

    $("#shareModal").modal('show');
    this.learnName = selectedItem.learnName;
    console.log("Selected item Id: ", selectedItem.Id);
    this.learnId = selectedItem.learnId;// You get the Id of the selected item here

  }

  onchangeLearnAccessType(val: any) {

    this.shareTopic.accessType = val;


  }

  shareTopicName(learnId: number,) {


    this.learnId = learnId;

    const datalink = {
      learnId: this.learnId,
      learnName: this.learnName,
      userId: this.tokenStorage.getUser().id,
      accesstype: this.shareTopic.accessType,
      sharedTime: new Date().toDateString()

    };
    this.learnService.addshareTopic(datalink).subscribe(
      (data: any) => {
        console.log(data);
        this.getLearn();
      },
      (err: any) => {

      }
    );

  }





  getLearn() {
    this.learnlist = [];
    this.conceptlist = [];
    this.conceptlinklist = [];
    this.KeypointList = [];
    this.learnService
      .getLearn(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.learnlist = data
          //this.getGoalid(this.goallist[0],(itemsPerPage*(p-1)));
          if (this.learnlist.length == 0) {
            this.learnNameselected = "addConcept"
          }
          else {
            console.log("learnlist", this.learnlist)
            localStorage.setItem("learnconceptlinkCount", this.learnlist.length)
            console.log("hello", this.learnlist.length)

            this.getLearnid(this.learnlist[0], 0);

            setTimeout(() => {
              /** spinner ends after 5 seconds */
              this.SpinnerService.hide();
            }, 2000);

          }

        },
        err => {
          this.learnlist = JSON.parse(err.error).message;
        }
      );
  }

  getconcept() {

    this.learnService.getconcept(this.learnNameselectedId)
      .subscribe(
        data => {
          this.conceptlist = data
          console.log(data);
          this.opentesttoggle(data[0], 0);
          this.conceptlistVisible = true;
        },
        err => console.error(err),
        () => console.log('concept completed')
      );
  }

  getconceptLink(conceptLinkCategoryId: number) {

    this.learnService.getConceptLink(conceptLinkCategoryId)
      .subscribe(
        data => {
          this.conceptlinklist = data
          console.log("492",this.conceptlinklist);

          // this.opentesttoggle(data[0],0);   
        },
        err => console.error(err),
        () => console.log('getconceptlink completed')
      );
  }
  getConceptkeypoint(conceptlinkId: number) {
    this.KeypointList = null;
    this.learnService
      .getConceptkeypoint(conceptlinkId)
      .subscribe(
        data => {
          console.log("data", data);
          this.KeypointList = data;

        },
        err => {
          return console.error(err);
        }
        // () => console.log('getKeypointList completed') 
      );
  }
  changetestValue(event: any) {

    this.editField = event.target.value;
    this.findByLearnName(this.editField);
  }
  findByLearnName(key) {

    if (key) {

      this.isSuccessful = true;
      this.enableEdit = false;

      this.learnService.findByLearnName(key,
        this.tokenStorage.getUser().id)

        .subscribe(
          data => {
            this.learnlist = data
            console.log("list", data);

          },
          err => {
            // this.learnlist = JSON.parse(err.error).message;
          }
        );

    }
    else {
      this.getLearn()
    }
  }

  checkAlreadyExit(data): boolean {
    let sameData = this.learnlist.filter(item => item.learnName == data)
    console.log(sameData);
    if (sameData.length != 0) {
      return true
    }
    else {
      return false
    }
  }

  editLearn(learnId: number, learnName: String) {
    if (learnName == "") {
      this.isSuccessful = false;
    }
    else {
      if (this.checkAlreadyExit(learnName)) {
        this.alreadyTaken = true;
        return
      }
      else {
        this.alreadyTaken = false;
        this.isSuccessful = true;
        this.enableEdit = true;
        const data = {
          learnName: learnName,
          userId: this.tokenStorage.getUser().id
        };
        this.learnService.updatelearn(learnId, data).subscribe(
          data => {
            console.log(data);
            this.sucessmessage = data;
            this.isLoggedIn = true;
            this.isSuccessful = true;
            this.getLearn();
            this.enableEdit = false;
          },
          err => {
            this.errorMessage = err.error.message;
            this.isSuccessful = false;
          }
        );

      }
    }
  }
  enableEditMethod(e, i) {

    this.currentlearnid = e.learnId;
    this.currentlearnname = e.learnName;
    this.isSuccessful = true;
    this.enableEdit = true;
    this.enableEditIndex = i;
    console.log(i, e);
    this.hrefvisible = false;
    this.hrefvisible = i;

  }
  changeValue(id: number, property: string, event: any) {

    this.editField = event.target.textContent;

  }
  showdelTopicModal(selectedItem: any): void {

    $("#mydelTopicModal").modal('show');

    console.log("Selected item Id: ", selectedItem.Id);

    this.learnId = selectedItem.learnId;// You get the Id of the selected item here

  }
  showdelConceptModal(selectedItem: any): void {

    $("#mydelConceptModal").modal('show');

    console.log("Selected item Id: ", selectedItem.Id);

    this.conceptId = selectedItem.conceptId;
  }




  deleteLearn() {

    this.learnService.deleteLearn(this.learnId)
      .subscribe(
        data => {
          console.log(data);
          this.hideModal();
          this.reloadData();
        },
        error => console.log(error));

    //this.getLearn();
  }
  reloadData() {
    this.learnlist = this.getLearn();

  }
  hideModal(): void {

    document.getElementById('close-modal').click();
  }

  showconceptShareModal(selectedItem: any): void {
    $("#conceptshareModal").modal('show');
    this.conceptLink = selectedItem.conceptLink;
    console.log("Selected item Id: ", selectedItem.conceptlinkId);
    this.conceptlinkId = selectedItem.conceptlinkId;  // You get the Id of the selected item here
    this.conceptLinkCategoryId = selectedItem.conceptLinkCategoryId;
  }

  onchangeAccessType(val: any) {
    this.shareconceptlink.accessType = val;
  }

  shareConcept(conceptlinkId: number, fshare:NgForm) {
    console.log(this.shareconceptlink.accessType);
    this.conceptlinkId = conceptlinkId;
    if(this.shareconceptlink.accessType == ''){
      this.isSuccessful = false;
    }
    else{
    const datalink = {
      conceptLink: this.conceptLink,
      conceptLinkCategoryId: this.conceptLinkCategoryId,
      conceptlinkId: conceptlinkId,
      userId: this.tokenStorage.getUser().id,
      accesstype: this.shareconceptlink.accessType,
      sharedTime: new Date().toDateString()
    };
    this.learnService.addshareconcept(datalink).subscribe(
      (data: any) => {
        console.log(data);
        document.getElementById('conceptclose').click();
        fshare.resetForm();
        this.isSuccessful = true;
        this.shareconceptlink.accessType ='';
        this.getconceptLink(this.conceptLinkCategoryId);
        // this.conceptshareModal.nativeElement.hide();
      },
      (err: any) => {
        this.isSuccessful = false;
      });
    }
  }

  editConcept(conceptId: number, conceptName: String) {

    if (conceptName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableconceptEdit = true;
      const data = {
        conceptName: conceptName,
        userId: this.tokenStorage.getUser().id,
      };
      this.learnService.updateConcept(conceptId, data).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getconcept();
          this.enableconceptEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        });
    }
  }
  enableconceptEditMethod(s, id) {
    this.currentconceptid = s.conceptId;
    this.currentconceptname = s.conceptName;
    this.isSuccessful = true;
    this.enableconceptEdit = true;
    this.enableconceptEditIndex = id;
    console.log(id, s);
    this.hrefvisible = false;
    this.hrefvisible = id;

  }
  getConceptid(conceptlist: any, i: any) {
    this.conceptNameselectedId = conceptlist.conceptId;
    this.conceptNameselected = conceptlist.conceptName;
  }

  deleteConcept() {
    this.learnService.deleteConcept(this.conceptId)
      .subscribe(
        data => {
          console.log(data);
          document.getElementById('close-modal').click();
          // this.reloadData();
          this.getconcept();
        },
        error => console.log(error));
    // this.hideModal();
  }


}


import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
import {TeamService} from '../_services/team.service';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  isOTPSuccessful=false;
  phoneOTPcode:String;
  isPhonenotconfirmed=false;
  isSuccessful=false;
  accept_emailConfirmed: boolean = false;
    urlParams: any = {};
    message:string;
  constructor(private authService: AuthService,private router: Router, 
    private tokenStorage: TokenStorageService,
    private teamService: TeamService,private route: ActivatedRoute
    
    ) { }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
    this.urlParams.emailId = this.route.snapshot.queryParamMap.get('emailId');
    this.route.queryParams.subscribe((params)=> {
    
      if(params['emailId']){
     
        this.acceptEmail();
        console.log(params['emailId']);
      } else {
       
        console.log('id not found in params')
      }
    });
  
    
  }
  acceptEmail() {
  
    this.teamService.showAcceptInvitePage(this.urlParams.emailId)
    .subscribe(() => {
   
      console.log("success");
      this.accept_emailConfirmed = true;
    }, error => {
     
      console.log(error);
     
      this.accept_emailConfirmed = false;
    })
   
  }

  onSubmit() {
   
    this.authService.login(this.form).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        console.log(data.accessToken)
        this.tokenStorage.saveUser(data);
       
       if(this.tokenStorage.getUser().isEmailVerified=="0" )
       {
     
        this.isLoginFailed = true;
        this.isLoggedIn = false;
       
        this.errorMessage="Please Confirm your Email ID";
      
        window.sessionStorage.clear();
      

       }
   
      // else if(this.tokenStorage.getUser().isPhoneVerified =="0"  )
     
      //  {
        
      //  // this.router.navigate(['confirmemailotp']);
       
      //  // this.isPhonenotconfirmed=true;
      //  this.errorMessage="Phonenumber is not confirmed";
      //   this.isLoginFailed = true;
      //   this.isLoggedIn = false;
      //  this.isPhonenotconfirmed=true;
      //   window.sessionStorage.clear();
      //  }
     
      else
      {
        this.route.queryParams.subscribe((params)=> {
          
          if(params['emailId']){
          
            this.router.navigate(['shareteam'])
          } 
          else {
            
          this.isLoginFailed = false;
          this.isLoggedIn = true;
          
          this.roles = this.tokenStorage.getUser().roles;
          this.reloadPage();      
          }
        });
      }
        err => {
         
  
          this.errorMessage = err.error.message;
          this.isLoginFailed = true;
        }
       
      }
      
    );
this.router.navigate(['home'])     
   
 
  }


  reloadPage() {
    window.location.reload();
  }

  SignUp()
  {
    
    // this.route.queryParams.subscribe((params)=> {
    
    //   if(params['emailId']){
        
    //     this.router.navigate(['register'], 
    //     { queryParams: { emailId: this.urlParams.emailId } });

    //   } 
    // });
    
    
      
  }
  verifyOTPCode()
  {
    
    if (this.tokenStorage.getUser().smscode==this.phoneOTPcode) {
       this.isOTPSuccessful=true;
       
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        
        this.roles = this.tokenStorage.getUser().roles;
        this.reloadPage()
    }
    else
    {
      
    }
      
  }
}

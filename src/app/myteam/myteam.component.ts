import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { TeamService } from '../_services/team.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
declare var $: any;

@Component({
  selector: 'app-myteam',
  templateUrl: './myteam.component.html',
  styleUrls: ['./myteam.component.css']
})
export class MyteamComponent implements OnInit {
  teamList: any[]; teamMemberlist: any[]; InviteList: any[]; teamSharelist: any[];
  teamId = 0; status = false; Statusmessage: String; notication = 0; noticationstatus = false;
  p: number = 1;
  public currentteamid;
  public currentteamname;
  errorMessage = ''; isSuccessful = false; editField: string;
  sucessmessage: any;
  isLoggedIn = false;
  enableEdit = false;
  enableEditIndex = null;
  hrefvisible = true;

  teamMember = {
    status: ""
  };

  teamMember1 = {

    teamId: 0,
    userId: 0,
    accessroleid: 0,
    emailId: '',
    phoneNumber: String,
    message: '',
    status: 0,


  };

  team = {
    teamId:0,
    teamName: '',
    userId: 0,
    accessType: '',
    accessroleid: 0,
    emailId: String,
    message: String
  };

  teamrolelist: Object;
  isInviteSuccessful: boolean;
  isFailure: boolean;
  showdelTeamModal(selectedItem: any): void {

    $("#mydelTeamModal").modal('show');

    console.log("Selected item Id: ", selectedItem.Id);
    this.teamId = selectedItem.teamId;// You get the Id of the selected item here

  }
  constructor(private teamService: TeamService, private router: Router,
    private tokenStorage: TokenStorageService, private authservice: AuthService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.showInviteList();
    this.getMemberTeamList(); this.getteamMemberlist(this.teamId);
    this.getTeamRoles();

  }

  getteamShareDetail(teamId) {

    this.router.navigate(['teamsetting'], { queryParams: { teamId: teamId } });

  }

  editTeam() {
    if (this.team.teamName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableEdit = true;
      const data = {
        teamName: this.team.teamName
      };

      this.teamService.updateTeam(this.team.teamId, data).subscribe(
        data => {
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          document.getElementById("close-modal").click();
          this.getMemberTeamList();
          this.enableEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );

    }
  }

  enableEditMethod(e, id) {
    this.currentteamid = e.teamId;
    this.currentteamname = e.teamname;
    this.team.teamName = e.teamname;
    this.team.teamId = e.teamId;
    this.isSuccessful = true;
    this.enableEdit = true;
    this.enableEditIndex = id;
    this.hrefvisible = false;
    this.hrefvisible = id;
    this.errorMessage = '';
  }


  deleteTeam() {

    this.teamService.deleteTeam(this.teamId)
      .subscribe(data => {
        console.log(data);
        window.location.reload();

        this.getMemberTeamList();
      },
        error => console.log(error));

  }

  AcceptInvite(inviteId: Number) {

    this.teamService.showAcceptInvitePage(inviteId)
      .subscribe(() => {



      }, error => {



      })


  }

  showInviteList() {

    this.teamService.showPendingList(this.tokenStorage.getUser().email)

      .subscribe(data => {
        this.InviteList = data

        if (this.InviteList.length > 0) {
          this.noticationstatus = true;
          console.log("stat", this.noticationstatus)
        }

        else { this.noticationstatus = false }
        this.notication = this.InviteList.length.valueOf()

        console.log("lnoticationen", this.notication);
      }

      );
  }

  getMemberTeamList() {
    this.SpinnerService.show();
    this.teamService.getMemberTeamList(this.tokenStorage.getUser().id,
    )
      .subscribe(data => {
        this.teamList = data
        console.log("team", this.teamList)
      });

    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.SpinnerService.hide();
    }, 2000);
  }
  getteamMemberlist(teamId: number) {

    this.teamService.getallTeamInviteMember(this.tokenStorage.getUser().id, teamId)
      .subscribe(data => {
        this.teamMemberlist = data

        for (var i = 0; i < this.teamMemberlist.length; i++) {
          if (this.teamMemberlist[i].status == 0) {

            this.Statusmessage = "Pending"
          }

          console.log(this.teamMemberlist[i].status);
        }


      });
  }

  hideModal() {
    this.enableEdit = false;
  }

  getTeamRoles() {
    this.teamService
      .getTeamRoles()
      .subscribe(
        data => {
          this.teamrolelist = data
        },
        err => console.error(err),
        () => console.log('TeamRoleList completed')
      );
  }

  onchange(val: any) {

    this.team.accessroleid = val;
  }

  showInviteModal(team) {
    this.teamMember1.teamId = team.teamId;
    this.isFailure = false;
    this.isSuccessful = false;
    this.isInviteSuccessful = false;
    this.errorMessage="";
  }

  AddTeamMember(fAddTeamInvite) {
    if(this.team.accessroleid <=0){
      this.isSuccessful = false;
    }
    else{
      const TeamMember_data = {
        userId: this.tokenStorage.getUser().id,
        accessroleId: this.team.accessroleid,
        emailId: this.teamMember1.emailId,
        phoneNumber: "",
        status: 0,
        teamId: this.teamMember1.teamId
      }

      this.teamService.addTeamMember(TeamMember_data).subscribe(
        (data) => {
          this.isFailure = false;
          this.isSuccessful = true;
          this.isInviteSuccessful = true;
          this.team.accessroleid = 0;
          fAddTeamInvite.resetForm();
          this.sucessmessage = "Invite is sent";
        },
        err => {
          this.isSuccessful = false;
          this.isInviteSuccessful = false;
          this.errorMessage = err.error.message;
          this.isFailure = true;
        }
      )
    }
  }

  hideInviteModal(fAddTeamInvite){
    this.team.accessroleid = 0;
    fAddTeamInvite.resetForm();
  }

}

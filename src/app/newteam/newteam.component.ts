
import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { TeamService } from '../_services/team.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidationErrors, NgForm } from '@angular/forms';
@Component({
  selector: 'app-newteam',
  templateUrl: './newteam.component.html',
  styleUrls: ['./newteam.component.css']
})
export class NewteamComponent implements OnInit {
  errorMessage = ''; isTeamSuccessful = false; isInviteSuccessful = false;isAlert=false;
  sucessmessage: any; isSuccessful = false; form: any = {}; accessType: any;
  isLoggedIn = false; message: any;
  inviteteamId: 0; inviteteam: 0; registerForm: FormGroup;submitted:boolean;teamsubmited:boolean;accessTypesubmited:boolean;isTeamexist=false;
  teamrolelist: any = [];
  team = {

    teamName: '',
    userId: 0,
    accessType: '',
    accessroleid: 0,
    emailId: String,
    message: String

  };


  teamMember = {

    teamId: 0,
    userId: 0,
    accessroleid: 0,
    emailId: '',
    phoneNumber: String,
    message: '',
    status: 0,


  };
  isFailure: boolean;
  constructor(private teamService: TeamService, private route: ActivatedRoute,

    private tokenStorage: TokenStorageService, private formBuilder: FormBuilder

  ) { }

  get f() { return this.registerForm.controls; }
  
  
  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      teamname: ['', Validators.required],
      accesstype:  ['', Validators.required]
    });
    console.log("hello",  this.f.teamname);
    this.isInviteSuccessful = false;
    this.form.teamId = this.route.snapshot.queryParamMap.get('teamId');

    if (this.form.teamId) {

      this.isTeamSuccessful = true;
      //this.isInviteSuccessful=false;
    }
    else {
      this.isTeamSuccessful = false;
      // this.isInviteSuccessful=true;
    }
    this.getTeamRoles();

  }

  onchange(val: any) {

    this.team.accessroleid = val;
  }
  onchangeAccessType(val: any) {
    this.team.accessType = val;

  }
  invite() {

    this.isTeamSuccessful = true;
  }
  getTeamRoles() {
    this.teamService
      .getTeamRoles()
      .subscribe(
        data => {
          this.teamrolelist = data
          console.log(data)
        },
        err => console.error(err),
        () => console.log('TeamRoleList completed')
      );
  }
  AddTeam() {
    this.submitted = true;
    if(this.team.teamName== '' && this.team.accessType ==''){
      this.teamsubmited=true
      this.accessTypesubmited=true
      return
    }else if(this.team.teamName == '' ){
        this.teamsubmited=true
        this.accessTypesubmited=false
        return
      }else if(this.team.accessType == ''){
        this.accessTypesubmited=true
        this.teamsubmited=false
        return
      }
     
  
    var datas = {
      teamName: this.team.teamName,
      userId: this.tokenStorage.getUser().id,
      accesstype: this.team.accessType,
      time: Date().slice(0,24)
    };
    console.log(datas);
    this.teamService.addTeam(datas).subscribe(
      (data) => {
        this.inviteteam = data.message;
        this.sucessmessage = "Team Added";
        this.isLoggedIn = true; this.isSuccessful = true;this.isAlert=false;this.isTeamexist=false;
  
        

      },
      err => {
        this.errorMessage = err.error.message;
        if(err.error.message){
          this.sucessmessage="Team Name already exists.Try another name."
          this.isLoggedIn = true; this.isSuccessful = true;this.isInviteSuccessful=false;this.isTeamexist=true;this.isAlert=true;
        }

      }
    );
  if(this.submitted && this.team.accessType!="" && this.team.teamName!=null){
        this.isTeamSuccessful = true;

    }
   
  }

  AddTeamMember(fAddTeamInvite:NgForm) {
    if (this.form.teamId) {
      this.teamMember.teamId = this.form.teamId;
    }
    else {
       this.teamMember.teamId = this.inviteteam;
    }
    const TeamMember_data = {
      userId: this.tokenStorage.getUser().id,
      accessroleId: this.team.accessroleid,
      emailId: this.teamMember.emailId,
      phoneNumber: "",
      status: 0,
      teamId: this.teamMember.teamId
    };
    this.teamService.addTeamMember(TeamMember_data).subscribe(
      (data) => {
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.team.accessroleid = 0;
        fAddTeamInvite.resetForm();
        this.sucessmessage = "Invite is sent";
        this.isSuccessful = true;
        this.isInviteSuccessful = true;
        this.isFailure = false;
      },
      err => {
        this.isFailure = true;
        this.isInviteSuccessful = false;
        this.isSuccessful = false;
        this.errorMessage = err.error.message;
      });
  }
}




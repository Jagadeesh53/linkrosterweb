import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileService } from '../_services/profile.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { Location } from '@angular/common';




@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit {

  currentUser: any;
  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
 
  imageName: any;
  successmessage: any;
  
  showimage = false;
  editField: string;
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = false;
   sucessmessage=false;
   paramSubscription:any;
   id: '';
   username:'';
   email:'';
   phoneNumber:'';
   password:'';
   message = '';
 
  constructor(private location: Location ,private tokenStorage:TokenStorageService,private route: ActivatedRoute,private router: Router,
    private authService: AuthService,private profileService:ProfileService) { }

  ngOnInit() {
 
    console.log("form.id",this.form);
    this.id = this.route.snapshot.params['id'];
    

     this.form.id = this.route.snapshot.queryParamMap.get('id');
   
        // this.form=this.route.snapshot.queryParamMap.getUser();
        this.form= this.authService.getUser();
        this.getuserImage(); 
        this.showimage = false;
        
        
  }
  ngOnLoad()
  {
    window.location.reload();
  }
  public onFileChanged(event) {
    //Select File
    this.selectedFile = event.target.files[0];
    this.onUpload();
    this.getUser(this.id);
  }
  changeValue(id: number, property: string, event: any) {
     
    this.editField = event.target.textContent;
    
  }
  changetestValue( event: any)
  {
    
    this.editField =  event.target.value ;
    this.getUser(this.editField );
  }

  onUpload() {
    console.log(this.selectedFile);console.log(this.selectedFile);
    
    this .showimage=false;
      userid:this.tokenStorage.getUser().id;
      const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile,this.selectedFile.name);
   
  
    // this.profileService.saveImage(uploadImageData,this.tokenStorage.getUser().id).subscribe(
    //   Response =>{
    // console.log(Response);

    
    // this.getImage();
    // });  
    this.profileService.saveuserImage(this.selectedFile,this.tokenStorage.getUser().id).subscribe(
      Response =>{
    console.log(Response);

    
    this.getuserImage();
    });  
  }
  loggedinUserimage:string;userretrievedImage:any;
  getuserImage() {
 
    this.profileService.getUserImage(this.tokenStorage.getUser().id).subscribe(
      res => {
                this.loggedinUserimage = res.name;
              //  console.log(this.loggedinUserimage, res)
                console.log("log",this.loggedinUserimage, res);
                this.userretrievedImage = this.loggedinUserimage;
              }  
                  );
  }
  getImage() {
   
    this.profileService.getImage(this.tokenStorage.getUser().id).subscribe(
      res => {
                this.retrieveResonse = res;
                this.base64Data = this.retrieveResonse.picByte;
                
                this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
                console.log("image",this.retrievedImage);
              }  
              
    );
  }
  selectedFiles: FileList;currentFileUpload: File;
  onUpdate() {
    console.log(this.selectedFile);
    this.showimage=false;
    
    this.currentFileUpload = this.selectedFiles.item(0);
      this.profileService.saveuserImage(this.currentFileUpload ,this.tokenStorage.getUser().id).subscribe(
      Response =>{
    console.log(Response);
    this.successmessage=Response;
    this.isSuccessful=true;
    this.getuserImage();
    }); 
  

  }
  
  


  getUser(id) {
    console.log(this.selectedFile);
    this.authService.get(id)
      .subscribe(
        data => {
          this.form = data = this.selectedFiles.item(0);
          console.log(data);
          location.reload();

        },
        error => {
          console.log(error);
        });
  }
 
  changePassword() {
    
    const data = {
      username: this.form.username,
      email:this.form.email,
    password:this.form.password,
  phoneNumber:this.form.phoneNumber
  
    };
   
    this.authService.changeUser(this.form.id,data)
    
     .subscribe((data) => {
       console.log(data);
       this.isSuccessful = true;
         this.sucessmessage=true;
         
         this.isSignUpFailed = false;
       console.log("success");
       
     }, error => {
       this.errorMessage = true;
         this.isSignUpFailed = true;
 
       console.log(error);
     
     })
    
   }
 
 



  
  changeImage = false;
  change(event) {
    this.changeImage = true;
   
  }
  changedImage(event) {
    this.selectedFile = event.target.files[0];
    
  }
      

  upload() {
    this.currentFileUpload = this.selectedFiles.item(0);
    console.log("imagelog",this.currentFileUpload);
    this.profileService.upload(this.currentFileUpload,this.tokenStorage.getUser().id).subscribe(
  event => {
    this.successmessage=event;
    this.isSuccessful=true;
    this.getuserImage();
  },
  err => {
    this.message = 'Could not upload the file!';
  });
      this.selectedFiles = undefined;
      console.log("updated",this.message);
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
    this.upload();
  }

 
  updateUserInfo() {
    console.log(this.selectedFile);
    const data = {
      username: this.form.username,
      email:this.form.email,
      phoneNumber:this.form.phoneNumber,
  
  
  
    };
  
    this.authService.updateUserInfo(this.tokenStorage.getUser().id,data)
    
     .subscribe((data) => {
       console.log(data);
       this.isSuccessful = true;this.errorMessage=false;
       this.sucessmessage=true;
       window.location.reload();
       this.location.back(); 
       console.log("success",data);
     }, error => {
       this.errorMessage = true;
       this.sucessmessage=false;
       console.log("265",error);
     })
     console.log("success",data);
   }
}
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from '../_services/token-storage.service';
import {ProfileService} from '../_services/profile.service';
import { AuthService } from '../_services/auth.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']

})
export class ProfileComponent implements OnInit {
  title(title: any) {
    throw new Error('Method not implemented.');
  }
 
  
 
  currentUser: any;
  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  profilelist: any=[];
  message: string;
  imageName: any;
  successmessage: any;
  isSuccessful= false;
  showimage = false;
  // userid:any;
  
 id:number;
  
  constructor(private token: TokenStorageService,private router: Router,
    private tokenStorage:TokenStorageService,private profileService:ProfileService
    ,private authService:AuthService) { }


  ngOnInit() {
    this.currentUser = this.token.getUser();
    this.getuserImage(); 
    this.showimage = false;
    this. getprofile();

  }
  createInititals() {
    throw new Error('Method not implemented.');
  }
  ngOnLoad()
  {
    window.location.reload();
  }
  

  //Gets called when the user selects an image
  public onFileChanged(event) {
    //Select File
    this.selectedFile = event.target.files[0];
    this.onUpload();
   
    this. getuserImage();
   
  }

  // called when the user clicks on submit to upload the image
  onUpload() {
    console.log(this.selectedFile);
    
    this .showimage=false;
      userid:this.tokenStorage.getUser().id;
      const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile,this.selectedFile.name);
   
  
    this.profileService.saveImage(uploadImageData,this.tokenStorage.getUser().id).subscribe(
      Response =>{
    console.log(Response);
    this.getImage();
    });  
  }
  loggedinUserimage:string;userretrievedImage:any;
  getuserImage() {

    console.log(this.selectedFile);
    
    this.profileService.getUserImage(this.tokenStorage.getUser().id).subscribe(
      res => {
        this.loggedinUserimage = res.name;
               
        console.log("log",this.loggedinUserimage);
        this.userretrievedImage = this.loggedinUserimage;
        if(!this.userretrievedImage)
{this.userretrievedImage="default-avatar.jpg";
}else
{
  this.userretrievedImage=this.loggedinUserimage;
}

              }  
                  );
  }    
  getImage() {
    console.log(this.selectedFile);
    this.profileService.getImage(this.tokenStorage.getUser().id).subscribe(
      res => {
                this.retrieveResonse = res;
                this.base64Data = this.retrieveResonse.picByte;
                this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
              }  
    );
  }




  onUpdate() {
    console.log(this.selectedFile);
    this.showimage=false;
    
    
      userid:this.tokenStorage.getUser().id;
      const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile,this.selectedFile.name);
   
  
    this.profileService.updateImage(uploadImageData,this.tokenStorage.getUser().id).subscribe(
      Response =>{
    console.log(Response);
    this.successmessage=Response;
    this.isSuccessful=true;
    this.getImage();
    });  
  }


  getprofile() {
    
    this.authService.getuserList(this.tokenStorage.getUser().id).subscribe(
      response => {
       
        this.profilelist= response;
        console.log("data",response);
        this.message = 'The Chain-List was updated successfully!';
      
      },
      error => {
      
        console.log(error);
      });
      //  this.gotoList();
      
}



  }

   
   
   
  
import { Component, OnInit } from '@angular/core';

import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
import {TeamService} from '../_services/team.service';
import {FormControl,FormGroup,Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CustomValidators } from './custom-validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
signupForm = new FormGroup({
  email : new FormControl('',[Validators.required,Validators.email]),
  phoneNumber : new FormControl('',[Validators.required,Validators.pattern('^[7-9][0-9]{9}$')]),
  password : new FormControl(  null,
    Validators.compose([
      Validators.required,
      // check whether the entered password has a number
      CustomValidators.patternValidator(/\d/, {
        hasNumber: true
      }),
      // check whether the entered password has upper case letter
      CustomValidators.patternValidator(/[A-Z]/, {
        hasCapitalCase: true
      }),
      // check whether the entered password has a lower case letter
      CustomValidators.patternValidator(/[a-z]/, {
        hasSmallCase: true
      }),
      // check whether the entered password has a special character
      CustomValidators.patternValidator(
        /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
        {
          hasSpecialCharacters: true
        }
      ),
      Validators.minLength(6)
    ])),
  username : new FormControl('',Validators.required)
  })  
  get email(){return this.signupForm.get('email')}
  get phoneNumber(){return this.signupForm.get('phoneNumber')}
  get password(){return this.signupForm.get('password')}
  get username(){return this.signupForm.get('username')}
  form: any = {};
  isLoggedIn =false;
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage ="";
   sucessmessage:string;
   isOTPSuccessful=false;
   phoneOTPcode:String;
   SpecificerrorMessage:String;
   urlParams: any = {};
  constructor(private authService: AuthService,
    private tokenStorage:TokenStorageService,
    private router: Router, 
    
    private teamService: TeamService,private route: ActivatedRoute
    ) { }

  ngOnInit() {

  }

  onSubmit() {
    this.authService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.sucessmessage=data.message;
        console.log("Message",this.sucessmessage);
        this.isLoggedIn = true;
       
        this.isSignUpFailed = false;
        this.route.queryParams.subscribe((params)=> {
          //check lead Id here
          if(params['emailId']){
           this.acceptEmail();
            this.router.navigate(['shareteam'], 
            { queryParams: { emailId: this.urlParams.emailId } });
    
          } 

        });
      },
      err => {
        this.errorMessage = err.error.message;
        console.log(this.errorMessage)
        this.isSignUpFailed = true;
      }
    );
   
    //this.router.navigate(['home']);    
  }
  acceptEmail() {
  
    this.teamService.showAcceptInvitePage(this.urlParams.emailId)
    .subscribe(() => {
   
      console.log("success");
     // this.accept_emailConfirmed = true;
    }, error => {
     
      console.log(error);
     
      //this.accept_emailConfirmed = false;
    })
  
  }

  verifyOTPCode()
  {
    
   
    this.authService.verifyphoneOTP(this.phoneOTPcode).subscribe(
      data=>{
        console.log(data);
        this.isOTPSuccessful=true;
        this.isSuccessful=true;
        this.sucessmessage=data;
        this.isSignUpFailed=false;
        this.isLoggedIn = true;
        
       
      },
      err=>{
        
      //  this.errorMessage=err.error.message;
        this.isSignUpFailed=true;
      }


    );
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators,FormControl } from '@angular/forms';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';
import { TokenStorageService } from '../_services/token-storage.service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-request-reset',
  templateUrl: './request-reset.component.html',
  styleUrls: ['./request-reset.component.css']
})
export class RequestResetComponent implements OnInit {
  form: any = {};
  isSuccessful = false;
  isSendEmailFailed = false;
  errorMessage = false;
   sucessmessage:string;
   message:string;
  email:string;
  isOTPSuccessful=false;
  isEmailSuccessful=false;
  phoneOTPcode:String;
  smscode:String;
  isPhonenumber=false;
  constructor(
    private authService: AuthService,
    private router: Router,private tokenStorage:TokenStorageService
   ) {

  }

    ngOnInit() {
    
    }
  
  public findUserByEmailId(){
    //let resp= this.authService.getUsers(this.email);
    //resp.subscribe((data)=>this.sucessmessage=data);
     
   }
   forgotPassword() {

    var username=this.form.email;
    var emailregex = new RegExp("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}");
  
    var phoneregex = new RegExp("^[0][1-9]\\d{9}$|^[1-9]\\d{9}$");

   if(emailregex.test(username)==true)
   {
     
    this.authService.requestresetEmail(this.form.email)
    
    .subscribe((data) => {
      console.log(data);
      this.isSuccessful = true;
        this.sucessmessage=data.message;
        this.isOTPSuccessful=false;
        this.isEmailSuccessful=true;
        this.errorMessage = false;
        this.isSendEmailFailed = false;
        this.isPhonenumber=false;

      console.log("success");
      
    }, err => {
      this.errorMessage=true;
      this.errorMessage =err.error.message;
        this.isSendEmailFailed = true;
        this.isSuccessful = false;
      console.log(err);
    
    })

  }
  else if(phoneregex.test(username)==true)
  {
   
    this.authService.requestresetEmail(this.form.email)
    
    .subscribe((data) => {
      console.log(data);
      this.isSuccessful = true;
        this.sucessmessage=data.message;
       
       
        this.errorMessage = false;
       
        this.isOTPSuccessful=true;
    this.isPhonenumber=true;
    this.isEmailSuccessful=false;

      console.log("success");
      
    }, err => {
      this.errorMessage=true;
      this.errorMessage =err.error.message;
     
        this.isSuccessful = false;
      console.log(err);
    
    })
    

  }
   

   }

   verifyOTPCode()
  {
    
    if (this.sucessmessage.toString()==this.phoneOTPcode) {
       this.isOTPSuccessful=true;
       this.isEmailSuccessful=false;
     
      this.router.navigate(['changePassword'], { queryParams: { resetToken: this.form.email } });



    }
      
  }
}

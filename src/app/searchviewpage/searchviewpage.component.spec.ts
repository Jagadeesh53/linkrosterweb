import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchviewpageComponent } from './searchviewpage.component';

describe('SearchviewpageComponent', () => {
  let component: SearchviewpageComponent;
  let fixture: ComponentFixture<SearchviewpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchviewpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchviewpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

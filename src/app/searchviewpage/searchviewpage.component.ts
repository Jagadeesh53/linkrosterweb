import { Component, OnInit } from '@angular/core';
import { BookmarkService } from '../_services/bookmark.service';
import { TokenStorageService } from '../_services/token-storage.service';

import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';declare var $:any;

@Component({
  selector: 'app-searchviewpage',
  templateUrl: './searchviewpage.component.html',
  styleUrls: ['./searchviewpage.component.css']
})
export class SearchviewpageComponent implements OnInit {
  bookmarklinklist:any=[];sucessmessage:any;isSuccessful=false;errorMessage = '';isdelete=false;
  searchkeyword:String; KeypointList:any=[];showCollasalIndex=null;Link : string;  isLoggedIn=false;
   keypointId:number;   keyComments:string;bookmarkCategoryId:0; bookmarkNameselectedId: any = {};
  BookmarkLinkId:Number;editField: string;    showKeyModal : boolean;show=true;searchlink:String;
  bookmarklink = {

    link: '',
    bookmark_id:0,
    userId:0,bookmarkCategoryId:0
  };
  keypoint = {
  
    keyComments: '',
    bookmarkLinkId:0,
    bookmarkCategoryId:0
  };
  constructor(private bookmarkService: BookmarkService, 
    private tokenStorage:TokenStorageService,private route: ActivatedRoute
  ) { }
  test1()
  {
    $(document).ready(function() {
     
      var api_url = 'https://api.linkpreview.net'
      var key = 'c77e42d6e17c43e60b3111a5e2c1d133' // not real
    
      // $("textarea[name*='user_status']").blur(function () {
      //   var target = $(this).val();
     
  $( ".menu_link" ).each(function( index, element ) {
    var target =$( this ).text();
    $.ajax({
      url: "https://api.linkpreview.net",
      dataType: 'jsonp',
      data: {q: target, key: '5a2e292e7d25bb63a2d3b4c63524cd10abe39420dc68c'},
      
        success: function(result){
            // $( element ).after(
            // '<a href="' + result.url + '"> \n ' +
            //   '<div class="link-preview"> \n ' +
            //     '<div class="preview-image" style="background-image:url(' + result.image + ');"></div> \n ' +
            //     '<div style="width:70%;" class="link-info"> \n ' +
            //       '<h4>' + result.title +'</h4> \n ' +
            //       '<p>' + result.description +'</p> \n ' +
            //     '</div><br> \n ' +
            //       '<a href="' + result.url + '" class="url-info"><i class="far fa-link"></i>' + result.url + '</a> \n ' +
            //     '</div></a>'); $( element ).remove();
                $( element ).after(
'<div class="row"><div class="col-2"><img style="width:80%;" src="'+result.image+'"></div><div class="col"><h3>'

                  
               
                +result.title+'</h3><a href="'
                +result.url+'">'+result.url+'</a></div></div>'
             );
            $( element ).remove();
        }
    })
  });
});

  }
 
test()
{
  
  $(document).ready(function() {
   
$( ".menu_link" ).each(function( index, element ) {
  
  var target =$( this ).text();
  $.ajax({
    url: "https://api.linkpreview.net",
    dataType: 'jsonp',
    data: {q: target, key: '5a2e292e7d25bb63a2d3b4c63524cd10abe39420dc68c'},
    
      success: function(result){
         
              $( element ).after(
'<div class="row"><div class="col-4"><img style="width:80%;" src="'+result.image+'"></div><div class="col"><h3>'

                
             
              +result.title+'</h3><a href="'
              +result.url+'">'+result.url+'</a></div></div>'
           );
          $( element ).remove();
      }
  })
});
});
}
  ngOnInit(): void {
    this.test();
    
    this.searchkeyword = this.route.snapshot.queryParamMap.get('link');
   this.getSearchresult();
    
  }


  onSelectKeypoint(selectedItem: any) {
    
    $("#myModal").modal('show');
    this.keyComments =selectedItem.keyComments; 
    console.log("Selected item Id: ", selectedItem.Id); 
    this.BookmarkLinkId =selectedItem.bookmarkLinkId;
    this.bookmarkCategoryId=selectedItem.bookmarkCategoryId;// You get the Id of the selected item here
    this.showKeyModal=true;
    this.getkeypoint(this.BookmarkLinkId);
  }
  prevent(keyComments:string)
{
 event.preventDefault();
 this.EditKeypoint(keyComments);
 
 
}
  hide()
  {
    this.showKeyModal = false;
  } 
  hideModal():void {
    
    document.getElementById('close-modal').click();
  }
  showModal(selectedItem:any):void {
    
    $("#mydelModal").modal('show');
    this.Link =selectedItem.link;  
    console.log("Selected item Id: ", selectedItem.Id); 
    this.BookmarkLinkId =selectedItem.bookmarkLinkId;// You get the Id of the selected item here
    
  }
  opentesttoggle(selectedItem:any,i)
    {
    
      
       this.showCollasalIndex=i;
       this.BookmarkLinkId =selectedItem.bookmarkLinkId;
       
       this.getkeypoint(this.BookmarkLinkId );
     
   
       
    }

  buttonCol(selectedItem: any)
  {
   
  this.BookmarkLinkId =selectedItem.bookmarkLinkId;// You get the Id of the selected item here
 
  this.getkeypoint(this.BookmarkLinkId );
  }
  findByBookmarkLink()
  {
      this.bookmarkService
    .findByBookmarkLink(this.searchlink,this.tokenStorage.getUser().id)
    .subscribe(
          data => { 
            this.bookmarklinklist= data        
           },
           err => {
            this.bookmarklinklist = JSON.parse(err.error).message;
          }       
          );
  }
  getSearchresult()
  {
  
   
    this.bookmarkService
    .findByBookmarkLink(this.searchkeyword,this.tokenStorage.getUser().id)
    .subscribe(
          data => { 
            this.bookmarklinklist= data        
           },
           err => {
            this.bookmarklinklist = JSON.parse(err.error).message;
          }       
          );
  }
  getkeypoint(BookmarkLinkId)
{
 
  this.bookmarkService
  .getKeypoint(BookmarkLinkId)
  .subscribe(
        data => { 
          this.KeypointList= data        
         },
        err => console.error(err), 
        () => console.log('getKeypointList completed') 
        );
}
deleteBookmarklink() {

  this.bookmarkService.deleteBookmarklink(this.BookmarkLinkId)
    .subscribe(
      data => {
        console.log(data);
       this. isdelete=true;
        this.sucessmessage="Link is deleted";
       this.bookmarklinklist= this.getSearchresult();
      
      },
   
    err => {
      this.errorMessage = err.error.message;
      this.isSuccessful=false;
    }
  );
  this.getSearchresult();
  this.hideModal();
}
updateList(id: number, property: string, event: any) {

  const editField = event.target.textContent;
  this.bookmarklinklist[id][property] = editField;
  const datalink = {

    link: this. editField,
    bookmarkCategoryId:this.bookmarklinklist[id].bookmarkCategoryId,
    userId:this.tokenStorage.getUser().id
  };
    this.bookmarkService.updatebookmarkLink(this.bookmarklinklist[id].bookmarkLinkId,datalink).subscribe(
      data => {
        console.log(data);

   this.getSearchresult();
  
 },
 err => {
  
 }
);
alert("updated");
 
}
deleteKeypoint() {
  
  this.bookmarkService.deleteKeypoint(this.keypointId)
  
    .subscribe(
      data => {
        console.log(data);
        this.getkeypoint(this.BookmarkLinkId);
       
      },
      error => console.log(error));
      
      this.getkeypoint(this.BookmarkLinkId);
    
     
     }
     showdelKeyModal(selectedItem:any):void {
    
      $("#mydelKeyModal").modal('show');
      this.keyComments =selectedItem.keyComments;  
      this.keypointId=selectedItem.keypointId;
    
      this.BookmarkLinkId =selectedItem.bookmarkLinkId;// You get the Id of the selected item here
      
    }
    EditKeypoint(keyComments:String)
                         
    {
  
     
     
      const dataKey = {
       keyComments: keyComments,
       bookmarkLinkId:this.BookmarkLinkId,
       bookmarkCategoryId:this.bookmarklink.bookmarkCategoryId
       
     };
  
  
       this.bookmarkService.updateKeypoint(this.keypointId,dataKey).subscribe(
         data => {
           console.log(data);     
           this.sucessmessage="Comments  Edited";
           
           this.isSuccessful=true;
           this.getkeypoint(this.BookmarkLinkId);
          
         },
         err => {
           this.errorMessage = err.error.message;
           this.isSuccessful=false;
         }
       );
     
      this.getkeypoint(this.BookmarkLinkId);
      alert("Updated");
      
   }
   showEditKeyModal(selectedItem:any):void{
     
    $("#myEditKeyPointModal").modal('show');
    this.BookmarkLinkId =selectedItem.bookmarkLinkId;
   
    this.keyComments =selectedItem.keyComments; 
    this.keypointId=selectedItem.keypointId;
   this.bookmarkCategoryId=selectedItem.bookmarkCategoryId;
   this.showKeyModal=true;
    
  }
  changeValue(id: number, property: string, event: any) {
   
    this.editField = event.target.textContent;
    
  }
  AddKeypoint(bookmarkLinkId:number)
   {

 
     const dataKey = {
      keyComments: this.keypoint.keyComments,
      bookmarkLinkId:this.BookmarkLinkId,
      bookmarkCategoryId:this.bookmarklink.bookmarkCategoryId
      
    };
  
      this.bookmarkService.addKeypoint(dataKey).subscribe(
        data => {
          console.log(data);     
          this.sucessmessage="Comments  Added";
          this.isLoggedIn = true;
          this.isSuccessful=true;
          this.getkeypoint(this.BookmarkLinkId);
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful=false;
        }
      );
     
     this.getkeypoint(this.BookmarkLinkId);
    this.hideModal();
  }
}

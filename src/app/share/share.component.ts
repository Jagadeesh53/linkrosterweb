import { Component, OnInit,  ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import {ShareService} from '../_services/share.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { BookmarkService } from '../_services/bookmark.service';
import { ChainService } from '../_services/chain.service';
import { NgxSpinnerService } from "ngx-spinner";
import { NgForm } from '@angular/forms';
//  import { MatLinkPreviewService } from '@angular-material-extensions/link-preview';
//  import { MatLinkPreviewModule } from '@angular-material-extensions/link-preview';

declare var $:any;
// const getGoalKeypoint:string |undefined= undefined



@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.css']
})
export class ShareComponent implements OnInit {
goalkeypoint:any=[];
  teamlist:any=[];
  bookmarkSolutionlist:any=[];
  bookmarkSkilllist:any=[];
  bookmarkApproachlist:any=[];
  bookmarkSteplist:any=[];
  bookmarkskilllist:any=[];bookmarkchallengelist:any={};
  bookmarkconceptlist:any[];bookmarktopiclist:any={};bookmarkgoallist:any={};
  bookmarklist:any=[];userSharelist:any[];//sharedTime:'';link='';username='';
  userId:number;form: any = {};
  bookmarklinklistVisible=true;
  errorMessage = '';
  username: string;
  sucessmessage:any;
  isLoggedIn=false;
  iskeySuccessful=false;
  //  bookmarkNameselectedId: any ;
  bookmarkNameselectedId:number;
  bookmarkLinkShareId:number;
  Link : string;
  sharedTime:number=Date.now();
  searchkeyword:String;
  bookmarkCategoryId:number;
  notication=0;noticationstatus=false;
  InviteList:any[];invitationlistempty=false;
  p: number = 1;

  conceptlinkId:number;
  conceptLinkCategoryId:number;
  conceptlink:string;
  name:string;
  accesstype:string;
  conceptName:string;
  steplink:string;
  steplinkId:number;
  solutionId:number;
  solutionlinkId:number;
  solutionlink:string;
  approachlinkId:number;
  approachlink:string;
  approachId:number;
  skilllink:string;
  SkillKeypointList:any=[];
  //  bookmarklink = {

  //   link: '',
  //    bookmarklinkId:0,
  //    userId:0
  //  };
  
  // keyComments:string;
 
  skilllinkId:number;

  teamLinkId:number;
 
  bookmarkLinkId:number;
  bookmarklinkId    : number;
  showCollasalIndex=null;
  KeypointList:any=[];
  StepKeypointList:any=[];conceptKeypointList:any=[];conceptkeyComments:string;
  limitedconceptkeypointlist:any=[];limitedconceptkeypointlist2:any=[];
  SolutionKeypointList:any=[];
  ApproachKeypointList:any=[];
  
  showKeyModal : boolean;
  keyComments:String;

  skillskeypoint={
    skilllinkId:0,
    keypoints:'',
    skillKeypointId:0,
    skillId:0
  };

goalskeypoint = {
  
    keyComments: '',
    skilllinkId:0,
   
  };
  solutionkeypoint={
    solutionlinkId:0,
    keypoints:'',
    keypointId:0,
    solutionId:0
  }

 approachkeypoint={
  keypointId:0,
    keypoints:'',
    approachlinkId:0,
    approachId:0
  }
  sharesteplink = {
    steplink: '',
    stepId:0,
    shareId:0,
    accessType:'',
    userId:0,
    sharedTime:Date()
  };
  stepkeypoint = {
  
    keypoints: '',
    steplinkId:0,
    keypointId:0
  };
  keypoint = {
  
    keyComments: '',
    bookmarkLinkId:0,
    bookmarkCategoryId:0
  };
  sharebookmarklink = {
    bookmarkName: '',
    link: '',
    bookmarkLinkId:0,
    shareId:0,
    accessType:'',
    sharedTime:Date()
  };
  shareskilllink = {
    skilllink: '',
    skillId:0,
    shareId:0,
    accessType:'',
    userId:0,
    sharedTime:Date()
  };
  shareconceptlink = {
    conceptlinkId:0,
     accessType:'',
     sharedTime:Date(),
     userId:0,
     conceptlink:''
   };
   conceptkeypoint = {
    keypoints: '',
    conceptlinkId:0,
    conceptLinkCategoryId:0,
    userId:0
   };
   sharesolutionlink = {
    solutionId: 0,
    solutionlink: '',
    solutionLinkId:0,
    shareId:0,
    accesstype:'',
    sharedTime:Date(),
    userId:0
  };

  shareapproachlink = {
    approachId: 0,
    approachlink: '',
    approachLinkId:0,
    shareId:0,
    accesstype:'',
    sharedTime:Date(),
    userId:0
  };
  shareteamlink = {
    teamName: '',
    teamconceptlink: '',
    teamconceptlinkId:0,
    shareId:0,
    accessType:'',
    sharedTime:Date()
  };
  
    constructor( private shareService: ShareService,private bookmarkService:BookmarkService,  private router: Router,
    private route: ActivatedRoute,private chainService: ChainService,
    private tokenStorage:TokenStorageService,private authservice:AuthService,private tokenStorageService: TokenStorageService,private SpinnerService: NgxSpinnerService
  ) {}
  // private matLinkPreviewService:MatLinkPreviewService,
  
test()
{
  
  $(document).ready(function() {
   
$( ".menu_link" ).each(function( index, element ) {
  
  var target =$( this ).text();
  $.ajax({
    url: "https://api.linkpreview.net",
    dataType: 'jsonp',
    data: {q: target, key: '5a2e292e7d25bb63a2d3b4c63524cd10abe39420dc68c'},
    
      success: function(result){
         
              $( element ).after(
'<div class="row"><div class="col-4"><img style="width:80%;" src="'+result.image+'"></div><div class="col"><h3>'

                
             
              +result.title+'</h3><a href="'
              +result.url+'">'+result.url+'</a></div></div>'
           );
          $( element ).remove();
      }
  })
});
});
}
test1()
  {
    $(document).ready(function() {
      
      var api_url = 'https://api.linkpreview.net'
      var key = 'c77e42d6e17c43e60b3111a5e2c1d133' // not real
    
      // $("textarea[name*='user_status']").blur(function () {
      //   var target = $(this).val();
     
  $( ".menu_link" ).each(function( index, element ) {
    var target =$( this ).text();alert($( this ).text());
    $.ajax({
      url: "https://api.linkpreview.net",
      dataType: 'jsonp',
      data: {q: target, key: '5a2e292e7d25bb63a2d3b4c63524cd10abe39420dc68c'},
      
        success: function(result){
            // $( element ).after(
            // '<a href="' + result.url + '"> \n ' +
            //   '<div class="link-preview"> \n ' +
            //     '<div class="preview-image" style="background-image:url(' + result.image + ');"></div> \n ' +
            //     '<div style="width:70%;" class="link-info"> \n ' +
            //       '<h4>' + result.title +'</h4> \n ' +
            //       '<p>' + result.description +'</p> \n ' +
            //     '</div><br> \n ' +
            //       '<a href="' + result.url + '" class="url-info"><i class="far fa-link"></i>' + result.url + '</a> \n ' +
            //     '</div></a>'); $( element ).remove();
                $( element ).after('<div class="row"><div class="col-2"><img style="width:80%;" src="'+result.image+'"></div><div class="col"><h3>'+result.title+'</h3><a href="'+result.url+'">'+result.url+'</a></div></div>');
                $( element ).remove();
        }
    })
  });
});

  }
 
buttonCol(selectedItem: any)
    {
      this.skilllinkId =selectedItem.skilllinkId;
      this.getSkillKeypoint(this.skilllinkId);
    this.bookmarklinkId =selectedItem.bookmarkLinkId;// You get the Id of the selected item here
   
    this.getkeypoint(this.bookmarklinkId);

    this.steplinkId =selectedItem.steplinkId;

    this.getStepKeypoint(this.steplinkId);
// this.getGoalKeypoint(this.skilllinkId);

this.solutionlinkId =selectedItem.solutionlinkId;

this.getSolutionKeypoint(this.solutionlinkId);
this.approachlinkId =selectedItem.approachlinkId;

this.getApproachKeypoint(this.approachlinkId );


    }
        openNav(selectedItem: any) {
          this.skilllinkId =selectedItem.skilllinkId;
          this.getSkillKeypoint(this.skilllinkId);
    //  document.getElementById("collapseExample").style.width = "250px";
      this.bookmarklinkId =selectedItem.bookmarkLinkId;// You get the Id of the selected item here

      this.getkeypoint(this.bookmarklinkId );
      this.steplinkId =selectedItem.steplinkId;

    this.getStepKeypoint(this.steplinkId);

    this.solutionlinkId =selectedItem.solutionlinkId;

    this.getSolutionKeypoint(this.solutionlinkId);
    this.approachlinkId =selectedItem.approachlinkId;

    this.getApproachKeypoint(this.approachlinkId );
    this.conceptlinkId=selectedItem.conceptlinkId;

  }
  
   closeNav() {
      document.getElementById("mySidenav").style.width = "0";
  }
   
  
    hide()
    {
      this.showKeyModal = false;
      
    
    }  
    

// opentoggle(id: number, property: string, event: any)
// {

   
//     $("#slideme").slideToggle();
// }
  
 
  ngOnInit() { 
//  this.SpinnerService.show();
    this.searchkeyword = this.route.snapshot.queryParamMap.get('link');
    this.test1();
    this.showInviteList();
    const user = this.tokenStorageService.getUser().id;
    this.username = this.tokenStorageService.getUser().username;
    this.getShareBookmark();
    this.getShareBookmarkConceptLink();
    this.getStepkeypointShareBookmark();
    this.getSkillLinkShareBookmark();
    this.getSolutionkeypointShareBookmark();
    this.getApproachLinkShareBookmark();
  //   this.getTopicnameshare();
  //   this.getgoalnameshare();
  //   this.getchallengenameshare();
  }

  showInviteList()
  {
   
    this.chainService.showPendingList(this.tokenStorage.getUser().id)
   
    .subscribe(data=>{this.InviteList=data
   
   console.log("invitelist",this.InviteList.length)
 
   console.log("len",this.InviteList.length.valueOf());
   if(this.InviteList.length>0)
   {
     
     this.noticationstatus=true;
     console.log("stat",this.noticationstatus)}
 
   else{this.noticationstatus=false}
   this.notication =this.InviteList.length.valueOf()
  
   console.log("lnoticationen",this.notication);
  }
  
  );
 }
 
    getShareBookmark(){ 
      
      this.shareService
      .getShareBookmark(this.tokenStorage.getUser().id)//,this.bookmarklink.bookmarklinkId)
      .subscribe(
            data => { 
             
              this.bookmarklist= data;
              // this.test1();
              if(data.length>0){
                this.opentesttoggle(data[0],0);
              }
              // this.SpinnerService.hide();    
            
             },
             err => {
              console.log(err.error.message);
            }       
            );
            
    }

    opentesttoggle(selectedItem:any,i){
      console.log("395", selectedItem);
      // $("#mykeycommentsModal").modal('show');
       this.showCollasalIndex=i;
       this.bookmarklinkId =selectedItem.bookmarklinkId;
       this.getkeypoint(this.bookmarklinkId );
    }

    closetesttoggle(selectedItem:any,i){
      // $("#mykeycommentsModal").modal('show');
       this.showCollasalIndex=null;
       this.bookmarklinkId =selectedItem.bookmarklinkId;
      //  this.getkeypoint(this.bookmarklinkId );
    }

    limitedkeypointlist:any = [];
    limitedKeypointList2:any = [];
    getkeypoint(BookmarklinkId:number)
{
  this.KeypointList= null;
  this.limitedkeypointlist = null;

 
  this.bookmarkService
  .getKeypoint(BookmarklinkId)
  .subscribe(
    
        data => { 
          this.KeypointList= data;
          this.limitedkeypointlist = this.KeypointList.slice(0,3);     
          console.log("slice data",this.limitedkeypointlist);
         },
         err => {
          return console.error(err);
        } 
        // err => console.error(err)
        // () => console.log('getKeypointList completed') 
        );
}
onKeyPointsViewMore():void{
  this.limitedKeypointList2 = this.KeypointList.slice(3,this.KeypointList.length);
  this.limitedkeypointlist = this.limitedkeypointlist.concat(this.limitedKeypointList2);
  // console.log(this.KeypointList.length , this.limitedkeypointlist.length);
  // this.limitedgoalKeypointList2 = this.GoalKeypointList.slice(3,this.GoalKeypointList.length);
  // this.limitedgoalkeypointlist = this.limitedgoalkeypointlist.concat(this.limitedgoalKeypointList2);
 
  $('#view-more').hide();
  $('#view-less').show();     
}

onkeyPointsviewless():void{
  this.limitedkeypointlist = this.KeypointList.slice(0,3);
  $('#view-more').show();
      $('#view-less').hide();  
 
}


    onSelectKeypoint(selectedItem: any) {
      $("#myModal").modal('show');
      this.keyComments =this.keypoint.keyComments; 
      console.log("Selected item Id: ", +this.keyComments); 
      this.bookmarkLinkId =selectedItem.bookmarklinkId;// You get the Id of the selected item here
      console.log("bookmarkLinkId:"+this.bookmarkLinkId)
      this.bookmarkNameselectedId=selectedItem.bookmarkCategoryId;
      this.showKeyModal=true;

      this.getkeypoint(this.bookmarklinkId);
    }
    isSuccessful=false;
    AddKeypoint(bookmarklinkId,bookmarkCategoryId,keyComments,ff:NgForm)
    {
      
     
      
      const dataKey = {
       keyComments: keyComments,
       bookmarkLinkId:bookmarklinkId,
       bookmarkCategoryId:bookmarkCategoryId,
       userId:this.tokenStorage.getUser().id
     };
     console.log('datakey',dataKey)
       this.bookmarkService.addKeypoint(dataKey).subscribe(
        data => {
           console.log(data);     
           this.sucessmessage="Comments  Added";
           this.isLoggedIn = true;
           this.iskeySuccessful=true;
           ff.resetForm();
           this.getkeypoint(this.bookmarklinkId);
         },
         err => {
           this.errorMessage = err.error.message;
           this.iskeySuccessful=false;
         }
       );
      
      // this.getkeypoint(this.bookmarklinkId);
    //  this.hideModal();

this.gotoList();

   }
 



  // sendModal(): void {
  //   //do something here
  //   this.hideModal();
    
  // }
  // hideModal():void {
    
  //   document.getElementById('close-modal').click();
  // }
  
  showShareModal(selectedItem:any):void {
   
    $("#shareModal").modal('show');
    this.Link =selectedItem.link;   
    console.log("Selected item Id: ", selectedItem.Id); 
    this.bookmarkLinkShareId =selectedItem.bookmarklinkId;// You get the Id of the selected item here
    
  }

  showShareModal1(selectedItem:any):void {
   
   
    $("#shareModal1").modal('show');  
    console.log("Selected item Id: ", selectedItem.Id);
    this.steplink =selectedItem.steplink;  
    this.steplinkId =selectedItem.steplinkId;// Y
   
  }


  showShareModal2(selectedItem:any):void {
   
   
    $("#shareModal2").modal('show');
    this.approachlink =selectedItem.approachlink;   
    console.log("Selected item Id: ", selectedItem.Id); 
    this.approachlinkId =selectedItem.approachlinkId;// 
   


  }

  deleteBookmarkLink(shareId){
  this.bookmarkService.deleteSharedBookmark(shareId).subscribe(data=>{
    this.getShareBookmark();
    console.log(data)
  },
  err=>{
console.log(err)
  })
  }

  showShareModal3(selectedItem:any):void {
   
   
    $("#shareModal3").modal('show');
    this.solutionlink =selectedItem.solutionlink;   
    console.log("Selected item Id: ", selectedItem.Id); 
    this.solutionlinkId =selectedItem.solutionlinkId;


  }
  showShareModal4(selectedItem:any):void {
   
   
    $("#shareModal4").modal('show');
    this.skilllink =selectedItem.skilllink;   
    console.log("Selected item Id: ", selectedItem.Id); 
    this.skilllinkId =selectedItem.skilllinkId;


  }
  onchangeconceptAccessType(val:any)
  {
    this.shareconceptlink.accessType=val;
  } 
  onchangestepAccessType(val:any)
  {
    this.sharesteplink.accessType=val;
    
  } 
  
  onchangeAccessType(val: any) {
    this.sharesolutionlink.accesstype=val;
    this.sharebookmarklink.accessType=val;
  }
      
  shareBookmark(bookmarkLinkshareId:number,)
    {
     
     this.username=this.username;//currentUser.username
    
      this.bookmarkLinkId=bookmarkLinkshareId;
      this.bookmarkCategoryId=this.bookmarkNameselectedId;
     
  const datalink = {
    
    bookmarkCategoryId:this.bookmarkNameselectedId,
  
     bookmarklinkId:bookmarkLinkshareId,
    userId:this.tokenStorage.getUser().id,
    accesstype
  :this.sharebookmarklink.accessType,
  
  sharedTime:new Date().toDateString(),
  };
  this.shareService.addsharebookmark(datalink).subscribe(
    (  data: any) => {
      console.log(data);
      this.getShareBookmark();
  
  },
    (err: any) => {
  
  }
  );
  
    }

    gotoList() {
      this.router.navigate(['/share']);
    }
    goToLink(url:string){
      window.open(url,"_blank");
    }   
getTopicnameshare(){
  this.shareService.getSharetopicname(this.tokenStorage.getUser().id).subscribe(
    data => {
      this.bookmarktopiclist=data;
    },
    err => {
      console.log(err.error.message);
    }
  );
}
getgoalnameshare(){
  this.shareService.getShareGoalname(this.tokenStorage.getUser().id).subscribe(
    data => {
      this.bookmarkgoallist=data;
    },
    err => {
      console.log(err.error.message); 
    }
  );
}
getchallengenameshare(){
  this.shareService.getSharechallengename(this.tokenStorage.getUser().id).subscribe(
    data => {
      this.bookmarkchallengelist=data;
    },
    err => {
      this.bookmarkchallengelist = err.error.message; 
    }
  );
}
    getShareBookmarkConceptLink(){ 
     
      this.shareService
      .getShareBookmarkConcept(this.tokenStorage.getUser().id)
      .subscribe(
            data => { 
              this.bookmarkconceptlist= data
             },
             err => {
              console.log(err.error.message);
            }       
            );
            
    }

    shareBookmarkconcept(conceptlinkId)
{
const datalink = {

conceptlinkId:this.conceptlinkId,
userId:this.tokenStorage.getUser().id,
accesstype:this.shareconceptlink.accessType,
sharedTime:new Date()
};
this.shareService.addshareconcept(datalink).subscribe(
data => {
  console.log(data);

console.log(conceptlinkId);

},
err => {

}
);

}
showconceptShareModal(conceptlinkId:number):void {
  
  $("#shareconceptModal").modal('show'); 
  this.conceptlinkId =conceptlinkId;// You get the Id of the selected item here
 
}
onconceptKeyPointViewMore():void{
  this.limitedconceptkeypointlist2 = this.conceptKeypointList.slice(3,this.conceptKeypointList.length);
  this.limitedconceptkeypointlist = this.limitedconceptkeypointlist.concat(this.limitedconceptkeypointlist2);
  console.log(this.conceptKeypointList.length , this.limitedconceptkeypointlist.length);
  $('#conceptview-more').hide();
$('#conceptview-less').show();   
}

onconceptkeyPointviewless():void{
  this.limitedconceptkeypointlist = this.conceptKeypointList.slice(0,3);
  $('#conceptview-more').show();
  $('#conceptview-less').hide(); 
}

onSelectconceptKeypoint(selectedItem: any) {
  $("#myconceptModal").modal('show');
  this.conceptkeyComments =this.conceptkeypoint.keypoints; 
  console.log("Selected item Id: ", +this.conceptlinkId); 
  this.conceptlinkId =selectedItem.conceptlinkId;// You get the Id of the selected item here
  console.log("conceptlinkId:"+this.conceptlinkId)
  this.showKeyModal=true;
  this.getconceptKeypoint(this.conceptlinkId);
}
AddconceptKeypoints(conceptlinkId,  conceptKeypointId,keypoints,ff1:NgForm)
{
  

  
  const dataKey = {
    keypoints: keypoints,
    conceptlinkId:conceptlinkId,
    conceptKeypointId:conceptKeypointId,
   userId:this.tokenStorage.getUser().id
 };

   this.shareService.addconceptkeypoints(dataKey).subscribe(
    data => {
       console.log(data);     
       this.sucessmessage="Comments  Added";
       this.isLoggedIn = true;
       this.iskeySuccessful=true;
       ff1.resetForm();
       this.getconceptKeypoint(this.conceptlinkId);
       
     },
     err => {
       this.errorMessage = err.error.message;
       this.iskeySuccessful=false;
     }
   );
  
  this.getconceptKeypoint(this.conceptlinkId);

this.gotoList();

}
getconceptKeypoint(conceptlinkId:number)
{
  this.conceptKeypointList= null;
  this.limitedconceptkeypointlist = null;
  this.shareService.getConceptkeypoint(conceptlinkId).subscribe(
    
        data => { 
          this.conceptKeypointList= data;
          this.limitedconceptkeypointlist = this.conceptKeypointList.slice(0,3);     
          console.log("slice data",this.limitedconceptkeypointlist);
         },
         err => {
          return console.error(err);
        } 
        );
}
openconceptoggle(selectedItem:any,i)
    {
       this.showCollasalIndex=i;
       this.conceptlinkId =selectedItem.conceptlinkId;
       this.getconceptKeypoint(this.conceptlinkId );
    }
closeconceptoggle(selectedItem:any,i)
    {
       this.showCollasalIndex=null;
       this.conceptlinkId =selectedItem.conceptlinkId;
      //  this.getconceptKeypoint(this.conceptlinkId );
    }
    // opendemotoggle(selectedItem:any,i)
    // {
   
    //   // $("#mykeycommentsModal").modal('show');
    //    this.showCollasalIndex=i;
    //    this.skilllinkId =selectedItem.skilllinkId;
    
    //    this.getGoalKeypoint(this.skilllinkId );
     
      
       
    // }
    
    // limitedGoalkeypointlist:any = [];
    // limitedGoalKeypointList2:any = [];
    
    // getGoalKeypoint(skilllinkId:number)
    // {
     
    //   this.GoalKeypointList= null;
    //   this.limitedGoalkeypointlist = null;
    
   
    //   this.bookmarkService
    //   .getGoalKeypoint(skilllinkId)
    //   .subscribe(
        
    //         data => { 
              
    //           this.GoalKeypointList= data;
    //           this.limitedGoalkeypointlist = this.GoalKeypointList.slice(0,3);     
    //           console.log("slice data",this.limitedGoalkeypointlist);
          
    //          },
    //          err => {
    //           return console.error(err);
    //         } 
    //         // err => console.error(err)
    //         // () => console.log('getKeypointList completed') 
    //         );
    // }
    // onKeyPointViewMore():void{
    //   this.limitedGoalKeypointList2 = this.GoalKeypointList.slice(3,this.GoalKeypointList.length);
    //   this.limitedGoalkeypointlist = this.limitedGoalkeypointlist.concat(this.limitedGoalKeypointList2);
    //   console.log(this.GoalKeypointList.length , this.limitedGoalkeypointlist.length);
     
    //   $('#view-more').hide();
    // $('#view-less').show();   
    // }
    
    // onkeyPointviewless():void{
    //   this.limitedGoalkeypointlist = this.GoalKeypointList.slice(0,3);
    //   $('#view-more').show();
    //   $('#view-less').hide(); 
    // }

    // onSelectedKeypoint(selectedItem: any) {
    //   $("#myModal").modal('show');
    //   this.keyComments =this.goalskeypoint.keyComments; 
    //   console.log("Selected item Id: ", +this.keyComments); 
    //   this.skilllinkId =selectedItem.skilllinkId;// You get the Id of the selected item here
    //   console.log("skilllinkId:"+this.skilllinkId)
     
    //   this.showKeyModal=true;

    //   this.getGoalKeypoint(this.skilllinkId);
    // }
    // isSuccessfull=false;

    getStepkeypointShareBookmark(){ 
      
      this.shareService.getStepShareBookmark
      (this.tokenStorage.getUser().id)//,this.bookmarklink.bookmarklinkId)
      .subscribe(
            data => { 
             
              this.bookmarkSteplist= data;
              // this.test1();
              // this.opentesttoggle(data[0],0);
              // this.SpinnerService.hide();
              if(data.length>0){
                this.opensteptoggle(data[0],0);
              }
             
             },
             err => {
              console.log(err.error.message);
            }       
            );
            
    }
    shareBookmarkStep(steplinkId)
  {
    const datalink = {
      steplinkId:this.steplinkId,
      steplink:this.steplink,
      userId:this.tokenStorage.getUser().id,
      accesstype:this.sharesteplink.accessType,
      sharedTime:new Date()
  };
  this.shareService.addsharestep(datalink).subscribe(
    data => {
    console.log(data);
    console.log(steplinkId);
  },
  err => {
  }
  );

  }
    opensteptoggle(selectedItem:any,i)
    {
      // $("#mykeycommentsModal").modal('show');
       this.showCollasalIndex=i;
       this.steplinkId =selectedItem.steplinkId;
       this.getStepKeypoint(this.steplinkId );
    }
    closesteptoggle(selectedItem:any,i)
    {
      // $("#mykeycommentsModal").modal('show');
       this.showCollasalIndex=null;
       this.steplinkId =selectedItem.steplinkId;
      //  this.getStepKeypoint(this.steplinkId );
    }
    limitedstepkeypointlist:any = [];
    limitedstepKeypointList2:any = [];
    getStepKeypoint(steplinkId:number)
{
  this.StepKeypointList= null;
  this.limitedstepkeypointlist = null;

  

  this.bookmarkService
 .getStepKeypoint(steplinkId)
  .subscribe(
    
        data => { 
          this.StepKeypointList= data;
          this.limitedstepkeypointlist = this.StepKeypointList.slice(0,3);     
          console.log("slice data",this.limitedstepkeypointlist);
         
         },
         err => {
          return console.error(err);
        } 
        // err => console.error(err)
        // () => console.log('getKeypointList completed') 
        );
}
onKeyPointstepViewMore():void{
  this.limitedstepKeypointList2 = this.StepKeypointList.slice(3,this.StepKeypointList.length);
  this.limitedstepkeypointlist = this.limitedstepkeypointlist.concat(this.limitedstepKeypointList2);
  console.log(this.StepKeypointList.length , this.limitedstepkeypointlist.length);
  // this.limitedgoalKeypointList2 = this.GoalKeypointList.slice(3,this.GoalKeypointList.length);
  // this.limitedgoalkeypointlist = this.limitedgoalkeypointlist.concat(this.limitedgoalKeypointList2);
  $('#view-more').hide();
$('#view-less').show();   
}

onkeyPointstepviewless():void{
  this.limitedstepkeypointlist = this.StepKeypointList.slice(0,3);
  $('#view-more').show();
  $('#view-less').hide(); 
}



onSelectStepKeypoint(selectedItem: any) {
  $("#myModal").modal('show');
  this.keyComments =this.stepkeypoint.keypoints; 
  // this.keypoint =this.stepkeypoint.keypoints; 
  
  console.log("Selected item Id: ", +this.steplinkId); 
  this.steplinkId =selectedItem.steplinkId;// You get the Id of the selected item here
  console.log("steplinkId:"+this.steplinkId)
  // this.bookmarkNameselectedId=selectedItem.bookmarkCategoryId;
  this.showKeyModal=true;

  this.getStepKeypoint(this.steplinkId);
}
isSuccessfull=false;



AddStepKeypoints(steplinkId,keypointId,keypoints,ff:NgForm)
{
  
  
  
  const dataKey = {
    keypoints: keypoints,
   steplinkId:steplinkId,
   keypointId:keypointId,
   userId:this.tokenStorage.getUser().id
 };

   this.bookmarkService.addstepKeypoint(dataKey).subscribe(
    data => {
       console.log(data);     
       this.sucessmessage="Comments  Added";
       this.isLoggedIn = true;
       this.iskeySuccessful=true;
       ff.resetForm();
       this.getStepKeypoint(this.steplinkId);
       
     },
     err => {
       this.errorMessage = err.error.message;
       this.iskeySuccessful=false;
     }
   );
  
  this.getStepKeypoint(this.steplinkId);
//  this.hideModal();

this.gotoList();

}




getSolutionkeypointShareBookmark(){ 
      
  this.shareService.getSolutionShareBookmark
  (this.tokenStorage.getUser().id)//,this.bookmarklink.bookmarklinkId)
  .subscribe(
        data => { 
         
          this.bookmarkSolutionlist= data;
          console.log("1008", this.bookmarkSolutionlist);
          // this.test1();
          if(data.length>0){
            this.opensolutiontoggle(data[0],0);
          }
          // this.SpinnerService.hide();
          // this.opensteptoggle(data[0],0);
         
         },
         err => {
          // this.bookmarkSolutionlist = JSON.parse(err.error).message;
          console.log("err 1012 ", err)
        }       
        );
        
}
shareBookmarksolution(solutionlinkId)
{
const datalink = {

  solutionlinkId:this.solutionlinkId,
userId:this.tokenStorage.getUser().id,
accesstype:this.sharesolutionlink.accesstype,
sharedTime:new Date()
};
this.shareService.addsharesolution(datalink).subscribe(
data => {
  console.log(data);

console.log(solutionlinkId);

},
err => {

}
);

}
shareSkill(skilllinkId)
{
const datalink = {

  skilllinkId:this.skilllinkId,
userId:this.tokenStorage.getUser().id,
accesstype:this.shareskilllink.accessType,
sharedTime:new Date()
};
this.shareService.addshareskill(datalink).subscribe(
data => {
  console.log(data);

console.log(skilllinkId);

},
err => {

}
);

}

opensolutiontoggle(selectedItem:any,i)
{
  // $("#mykeycommentsModal").modal('show');
   this.showCollasalIndex=i;
   this.solutionlinkId =selectedItem.solutionlinkId;
   this.getSolutionKeypoint(this.solutionlinkId );
}
closesolutiontoggle(selectedItem:any,i)
{
  // $("#mykeycommentsModal").modal('show');
   this.showCollasalIndex=null;
   this.solutionlinkId =selectedItem.solutionlinkId;
  //  this.getSolutionKeypoint(this.solutionlinkId );
}
limitedsolutionkeypointlist:any = [];
limitedsolutionKeypointList2:any = [];
getSolutionKeypoint(solutionlinkId:number)
{
this.SolutionKeypointList= null;
this.limitedsolutionkeypointlist = null;



this.bookmarkService
.getSolutionKeypoint(solutionlinkId)
.subscribe(

    data => { 
      this.SolutionKeypointList= data;
      this.limitedsolutionkeypointlist = this.SolutionKeypointList.slice(0,3);     
      console.log("slice data",this.limitedsolutionkeypointlist);
      
     },
     err => {
      return console.error(err);
    } 
    // err => console.error(err)
    // () => console.log('getKeypointList completed') 
    );
}


onKeyPointsolutionViewMore():void{
  this.limitedsolutionKeypointList2 = this.SolutionKeypointList.slice(3,this.SolutionKeypointList.length);
  this.limitedsolutionkeypointlist = this.limitedsolutionkeypointlist.concat(this.limitedsolutionKeypointList2);
  console.log(this.SolutionKeypointList.length , this.limitedsolutionkeypointlist.length);
  // this.limitedgoalKeypointList2 = this.GoalKeypointList.slice(3,this.GoalKeypointList.length);
  // this.limitedgoalkeypointlist = this.limitedgoalkeypointlist.concat(this.limitedgoalKeypointList2);
  $('#view-more').hide();
$('#view-less').show();   
}

onkeyPointsolutionviewless():void{
  this.limitedsolutionkeypointlist = this.SolutionKeypointList.slice(0,3);
  $('#view-more').show();
      $('#view-less').hide();
}



onSelectSolutionKeypoint(selectedItem: any) {
  $("#myModal").modal('show');
  this.keyComments =this.solutionkeypoint.keypoints; 
  // this.keypoint =this.stepkeypoint.keypoints; 
  
  console.log("Selected item Id: ", +this.solutionlinkId); 
  this.solutionlinkId =selectedItem.solutionlinkId;// You get the Id of the selected item here
  console.log("solutionlinkId:"+this.solutionlinkId)
  // this.bookmarkNameselectedId=selectedItem.bookmarkCategoryId;
  this.showKeyModal=true;

  this.getSolutionKeypoint(this.solutionlinkId);
}
isSuccessfully=false;




AddSolutionKeypoints(solutionlinkId,keypointId,keypoints,ff:NgForm)
{
  
 
  
  const dataKey = {
    keypoints: keypoints,
    solutionlinkId:solutionlinkId,
   keypointId:keypointId,
   userId:this.tokenStorage.getUser().id
 };

   this.bookmarkService.addsolutionKeypoint(dataKey).subscribe(
    data => {
       console.log(data);     
       this.sucessmessage=" Solution Comments Added";
       this.isLoggedIn = true;
       this.iskeySuccessful=true;
       ff.resetForm();
       this.getSolutionKeypoint(this.solutionlinkId);
  
     },
     err => {
       this.errorMessage = err.error.message;
       this.iskeySuccessful=false;
     }
   );
  
  this.getSolutionKeypoint(this.solutionlinkId);
//  this.hideModal();

this.gotoList();

}



getApproachLinkShareBookmark(){ 
  this.shareService.getApproachShareBookmark
  (this.tokenStorage.getUser().id)//,this.bookmarklink.bookmarklinkId)
  .subscribe(
        data => { 
          this.bookmarkApproachlist= data
          // this.test1();
          if(data.length>0){
            this.openApproachtoggle(data[0],0);
          }
          // this.SpinnerService.hide();
          // this.opensteptoggle(data[0],0);
          
         },
         err => {
          console.log(err.error.message);
        }       
        );
        
}

openApproachtoggle(selectedItem:any,i)
{
  // $("#mykeycommentsModal").modal('show');
   this.showCollasalIndex=i;
   this.approachlinkId =selectedItem.approachlinkId;
   this.getApproachKeypoint(this.approachlinkId ); 
}
closeApproachtoggle(selectedItem:any,i)
{
  // $("#mykeycommentsModal").modal('show');
   this.showCollasalIndex=null;
   this.approachlinkId =selectedItem.approachlinkId;
  //  this.getApproachKeypoint(this.approachlinkId ); 
}
limitedApproachkeypointlist:any = [];
limitedApproachKeypointList2:any = [];
getApproachKeypoint(approachlinkId:number)
{
this.ApproachKeypointList= null;
this.limitedApproachkeypointlist = null;



this.bookmarkService
.getApproachKeypoint(approachlinkId)
.subscribe(

    data => { 
      this.ApproachKeypointList= data;
      this.limitedApproachkeypointlist = this.ApproachKeypointList.slice(0,3);     
      console.log("slice data",this.limitedApproachkeypointlist);
     
     },
     err => {
      return console.error(err);
    } 
    // err => console.error(err)
    // () => console.log('getKeypointList completed') 
    );
}
onKeyPointApproachViewMore():void{
this.limitedApproachKeypointList2 = this.ApproachKeypointList.slice(3,this.ApproachKeypointList.length);
this.limitedApproachkeypointlist = this.limitedApproachkeypointlist.concat(this.limitedApproachKeypointList2);
console.log(this.ApproachKeypointList.length , this.limitedApproachkeypointlist.length);
// this.limitedgoalKeypointList2 = this.GoalKeypointList.slice(3,this.GoalKeypointList.length);
// this.limitedgoalkeypointlist = this.limitedgoalkeypointlist.concat(this.limitedgoalKeypointList2);
$('#view-more').hide();
$('#view-less').show();   
}

onkeyPointApproachviewless():void{
this.limitedApproachkeypointlist = this.ApproachKeypointList.slice(0,3);
$('#view-more').show();
$('#view-less').hide(); 
}



onSelectApproachKeypoint(selectedItem: any) {
$("#myModal").modal('show');
this.keyComments =this.approachkeypoint.keypoints; 
// this.keypoint =this.stepkeypoint.keypoints; 

console.log("Selected item Id: ", +this.approachlinkId); 
this.approachlinkId =selectedItem.approachlinkId;// You get the Id of the selected item here
console.log("approachlinkId:"+this.approachlinkId)
// this.bookmarkNameselectedId=selectedItem.bookmarkCategoryId;
this.showKeyModal=true;

this.getApproachKeypoint(this.approachlinkId);
}
isSuccessfullly=false;


AddapproachKeypoints(approachlinkId,keypointId,keypoints,ff:NgForm)
{
  
  
  
  const dataKey = {
    keypoints: keypoints,
    approachlinkId:approachlinkId,
   keypointId:keypointId,
   userId:this.tokenStorage.getUser().id
 };

   this.bookmarkService.addapproachKeypoint(dataKey).subscribe(
    data => {
       console.log(data);     
       this.sucessmessage=" Approach Comments Added";
       this.isLoggedIn = true;
       this.iskeySuccessful=true;
       ff.resetForm();
       this.getApproachKeypoint(this.approachlinkId);
      
     },
     err => {
       this.errorMessage = err.error.message;
       this.iskeySuccessful=false;
     }
   );
  
  this.getApproachKeypoint(this.approachlinkId);
//  this.hideModal();

this.gotoList();

}




getSkillLinkShareBookmark(){ 
      
  this.shareService.getSkillBookmarkshare
  (this.tokenStorage.getUser().id)//,this.bookmarklink.bookmarklinkId)
  .subscribe(
        data => { 
         
          this.bookmarkSkilllist= data
          if(data.length>0){
            this.openSkilltoggle(data[0],0);
          }
         },
         err => {
          console.log(err.error.message);
        }       
        );
        
}
openSkilltoggle(selectedItem:any,i)
{
  // $("#mykeycommentsModal").modal('show');
   this.showCollasalIndex=i;
   this.skilllinkId =selectedItem.skilllinkId;
   this.getSkillKeypoint(this.skilllinkId );
}
closeSkilltoggle(selectedItem:any,i)
{
  // $("#mykeycommentsModal").modal('show');
   this.showCollasalIndex=null;
   this.skilllinkId =selectedItem.skilllinkId;
  //  this.getSkillKeypoint(this.skilllinkId );
}
limitedSkillkeypointlist:any = [];
limitedSkillKeypointList2:any = [];
getSkillKeypoint(skilllinkId:number)
{
this.SkillKeypointList= null;
this.limitedSkillkeypointlist = null;



this.bookmarkService
.getSkillsKeypoint(skilllinkId)
.subscribe(

    data => { 
      this.SkillKeypointList= data;
      this.limitedSkillkeypointlist = this.SkillKeypointList.slice(0,3);     
      console.log("slice data",this.limitedSkillkeypointlist);
      
     },
     err => {
      return console.error(err);
    } 
    // err => console.error(err)
    // () => console.log('getKeypointList completed') 
    );
}
onKeyPointSkillsViewMore():void{
  this.limitedSkillKeypointList2 = this.SkillKeypointList.slice(3,this.SkillKeypointList.length);
  this.limitedSkillkeypointlist = this.limitedSkillkeypointlist.concat(this.limitedSkillKeypointList2);
  console.log(this.SkillKeypointList.length , this.limitedSkillkeypointlist.length);
  // this.limitedgoalKeypointList2 = this.GoalKeypointList.slice(3,this.GoalKeypointList.length);
  // this.limitedgoalkeypointlist = this.limitedgoalkeypointlist.concat(this.limitedgoalKeypointList2);
  $('#view-more').hide();
  $('#view-less').show();   
  }
  onkeyPointSkillviewless():void{
    this.limitedSkillkeypointlist = this.SkillKeypointList.slice(0,3);
    $('#view-more').show();
      $('#view-less').hide();
    }
    

  onSelectSkillKeypoint(selectedItem: any) {
    $("#myModal").modal('show');
    this.keyComments =this.skillskeypoint.keypoints; 
    // this.keypoint =this.stepkeypoint.keypoints; 
    
    console.log("Selected item Id: ", +this.skilllinkId); 
    this.skilllinkId =selectedItem.skilllinkId;// You get the Id of the selected item here
    console.log("skilllinkId:"+this.skilllinkId)
    // this.bookmarkNameselectedId=selectedItem.bookmarkCategoryId;
    this.showKeyModal=true;
    
    this.getSkillKeypoint(this.skilllinkId);
    }
    isSuccessfulllly=false;
    AddSkillKeypoints(skilllinkId,keypointId,skillId,keypoints,ff4:NgForm)
  {
    
   
    
    const dataKey = {
      keypoints: keypoints,
      skilllinkId:skilllinkId,
     keypointId:keypointId,
     skillId:skillId,
     userId:this.tokenStorage.getUser().id
   };
  
     this.bookmarkService.addSkillKeypoint(dataKey).subscribe(
      data => {
         console.log(data);     
         this.sucessmessage=" Skill Comments Added";
         this.isLoggedIn = true;
         this.iskeySuccessful=true;
         ff4.resetForm();
         this.getSkillKeypoint(this.skilllinkId);
        
       },
       err => {
         this.errorMessage = err.error.message;
         this.iskeySuccessful=false;
       }
     );
    
    this.getSkillKeypoint(this.skilllinkId);
  //  this.hideModal();
  
  this.gotoList();
  
  }





}

    
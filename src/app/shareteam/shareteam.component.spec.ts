import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareteamComponent } from './shareteam.component';

describe('ShareteamComponent', () => {
  let component: ShareteamComponent;
  let fixture: ComponentFixture<ShareteamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareteamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareteamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

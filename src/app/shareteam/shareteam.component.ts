import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { TeamService } from '../_services/team.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router'; declare var $: any;
import { mergeMap } from 'rxjs/operators';
import { from, forkJoin, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProfileService } from '../_services/profile.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm } from '@angular/forms';

//import { defaultMaxListeners } from 'stream';
declare var $: any;


@Component({
  selector: 'app-shareteam',
  templateUrl: './shareteam.component.html',
  styleUrls: ['./shareteam.component.css']
})
export class ShareteamComponent implements OnInit {


  conceptlist: any = [];
  teamMemberList: any = []; userlist: any = []; teamList: any[]; teamSharelist: any[];
  teamId: number; form: any = {}; InviteList: any[]; invitationlistempty = false;
  notication = 0; noticationstatus = false; isLoggedIn
  iskeySuccessful = false;
  KeypointList: any = []; teamKeySuggestionlist: any[];
  errorMessage = false;
  showCollasal = true;
  teamsolutionkeypoints: string; teamsolutionlink: string;
  teamsolutionId: number;
  teamsolutionlinkId: number;
  bookmarkteamsolutionlist: any = [];
  bookmarkteamapproachlist: any = [];

  showCollasalIndex = null; conceptshowCollasalIndex = null;
  enableEdit = false;
  enableEditIndex = null; count: any; teamConceptLinkId: number; teamConceptId: number;
  p: number = 1;
  shareskilllist: any = []; shareconceptlist: any = []; sharegoallist: any = []; sharesteplist: any = [];
  teamsteplink: string; teamskilllink: string; teamConceptlink: string; teamskillId: number;
  username: string; userId: number;
  teamgoalName: string; teamstepName: string; teamskillName: string;
  sharetopiclist: any = []; teamTopicName: string; teamConceptName: string;
  skillKeypointList: any = []; ConceptSharelist: any = []; conceptKeypointList: any = [];
  limitedconceptKeypointLists: any = []; conceptKeypointLists: any = []; teamConceptKeySuggestionlist: any = [];
  keyConceptsuggestionimageName: string; limitedteamConceptKeySuggestion: any = []; limitedconceptKeypointLists2: any = [];
  limitedConceptteamKeySuggestion2: any = []; limitedKeypointList: any = [];

  limitedapproachKeypointLists: any = []; teamapproachlinkId: number; approachKeypointList: any = []; approachKeypointLists: any = [];
  limitedApproachteamKeySuggestion2: any = []; teamApproachKeySuggestionlist: any = [];
  keyApproachsuggestionimageName: any = []; limitedteamApproachKeySuggestion: any = [];
  limitedapproachKeypointLists2: any = []; teamsteplinkId: number; stepKeypointList: any = [];
  limitedstepKeypointLists: any = []; stepKeypointLists: any = []; keyStepsuggestionimageName: any = [];
  teamStepKeySuggestionlist: any = []; limitedteamStepKeySuggestion: any = [];
  limitedStepteamKeySuggestion2: any = []; limitedstepKeypointLists2: any = [];
  teamskilllinkId: number;
  limitedskillKeypointLists: any = []; skillKeypointLists: any = []; keySkillsuggestionimageName: any = [];
  teamSkillKeySuggestionlist: any = []; limitedteamSkillKeySuggestion: any = [];
  limitedSkillteamKeySuggestion2: any = []; limitedskillKeypointLists2: any = [];
  teamchallengeName: string; sharechallengelist: any = [];
  teamsolutionName: string;
  teamapproachName: string;
  // count:any;
  shareSkillLink = {

    teamskilllinkId: 0,
    teamskilllink: '',
    userId: 0, teamId: 0,
    accessType: '',
    sharedTime: Date()
  };
  shareStepLink = {

    teamsteplinkId: 0,
    teamstepId: 0,
    userId: 0, teamId: 0,
    accessType: '',
    sharedTime: Date()
  };
  shareapproachlink = {

    teamapproachlinkId: 0,
    userId: 0, teamId: 0,
    accessType: '',
    sharedTime: Date()
  };
  sharebookmarklink = {

    teambookmarkLinkId: 0,
    userId: 0, teamId: 0,
    accessType: '',
    sharedTime: Date()
  };
  team = {

    teamName: ''
  }
  teamsolutionkeypoint = {

    teamsolutionkeypoints: '',
    teamsolutionLinkId: 0, teamId: 0,
    teamsolutionId: 0,
    userId: 0
  };
  teamconceptkeypoint = {

    teamConceptKeypoints: '',
    teamConceptLinkId: 0,
    teamConceptId: 0, teamId: 0,
    userId: 0
  };
  sharesolutionlink = {
    solutionlink: '',
    teamsolutionlinkId: 0,
    userId: 0, teamId: 0,
    accessType: '',
    sharedTime: Date(),
    teamchallengeId: 0,
    teamsolutionId:0
  };

  shareConceptLink = {

    teamConceptLinkId: 0,
    teamConceptId: 0,
    teamconceptlink: '',
    userId: 0, teamId: 0,
    accessType: '',
    sharedTime: Date()
  };
  isSolSuccessful: boolean;
  isStepSuccessful: boolean;
  isConceptSuccessful: boolean;
  isAppSuccessful: boolean;
  isbokSuccessful: boolean;
  BookmarkKeypointList: any = [];
  BookmarkKeypointLists: any = [];
  teamSolKeySuggestionlist: any = [];
  keypointid: any;
  showstepkey: any;
  teamstepkeypointId: any;
  showconceptkey: any;
  teamConceptKeypointId: any;
  showskill: any;
  teamskillkeypointId: any;
  showsolution: any;
  teamsolutionkeypointId: any;
  showapproach: any;
  approachkeypointid: any;
  approachinvalid: boolean;
  bookkeyinvalid: boolean;
  solutioninvalid: boolean;
  stepinvalid: boolean;
  skillinvalid: boolean;
  conceptinvalid: boolean;


  ngOnInit(): void {
    this.SpinnerService.show();
    //this.toggle();
    this.test(); 
    this.showInviteList();
    this.form.teamId = this.route.snapshot.queryParamMap.get('teamId');
    this.showshareteamdetails(); 
    this.getImage();
    this.getTeamShareSolutionLink();
    this.getteamShareStepLink();
    this.getteamShareSkillLink();
     this.getpublicTeamConceptSharelink();
    this.getteamsharegoalname();
    this.getteamsharetopicname();
    this.getTeamApproachshareLink();
    this.getteamshareChallengename();
    if(this.form.teamId)
    {
      this.getteamShareDetail();
    }
    else{
      this.getpublicTeamSharelink();
    }
    //showShareModal(selectedItem:any):void {
    //   $("#shareModal").modal('show');
    //   this.Link =selectedItem.link;   
    //   console.log("Selected item Id: ", selectedItem.Id); 
    //   this.teambookmarkLinkId =selectedItem.teambookmarkLinkId;// You get the Id of the selected item here
    // }
    // this.getAlluser(1);this.getteamname();
  }

  //Solution List
  showsolutionShareModals(teamsolutionlinkId: number, teamId: number,teamsolution): void {
    $("#solutionshareModal").modal('show');
    this.sharesolutionlink.solutionlink = teamsolution.teamsolutionlink;
    this.sharesolutionlink.teamchallengeId = teamsolution.teamchallengeId;
    this.sharesolutionlink.teamsolutionId = teamsolution.teamsolutionId;
    this.teamsolutionlinkId = teamsolutionlinkId;// You get the Id of the selected item here
    console.log("174", teamsolution);
    console.log("174", this.sharesolutionlink);
  }

  //Solution List
  onchangeAccessTypes(val: any) {
    this.sharesolutionlink.accessType = val;
  }

  //Solution List
  shareSolution(teamsolutionlinkId, teamId, fshare) {
    if(this.sharesolutionlink.accessType == ''){
      this.isSolSuccessful = false;
    }
    else{
      const datalink = {
        teamsolutionlinkId: this.teamsolutionlinkId,
        teamsolutionlink: this.sharesolutionlink.solutionlink,
        userId: this.tokenStorage.getUser().id,
        teamId: teamId,
        accesstype: this.sharesolutionlink.accessType,
        sharedTime: new Date().toDateString(),
        teamchallengeId: this.sharesolutionlink.teamchallengeId,
        teamsolutionId: this.sharesolutionlink.teamsolutionId
      };
      this.teamService.addsharesolution(datalink).subscribe(
        data => {
          console.log(data);
          fshare.resetForm();
          document.getElementById('solclose').click();
          this.getTeamShareSolutionLink();
        },
        err => {
  
        });
    }
  }

  //Solution List
  getTeamShareSolutionLink() {
    this.bookmarkteamsolutionlist = [];
    this.teamService
      .getshareteamsolutionlink()
      .subscribe(
        data => {
          this.bookmarkteamsolutionlist = data;
          if(this.bookmarkteamsolutionlist.length>0){
            this.opensolutiontoggles(data[0], 0);
          }
        },
        err => {
          console.log("solution list error",err.error.message);
        });
  }

  //Solution List
  opensolutiontoggles(selectedItem: any, i) {
    this.showsolution=null;
    this.teamsolutionkeypointId=null;
    this.showCollasalIndex = i;
    this.getteamsolutionkeypoint(selectedItem.teamsolutionlinkId);
  }

  //Solution List
  getteamsolutionkeypoint(teamsolutionlinkId: number) {
    this.KeypointList = [];
    this.KeypointLists = [];
    this.teamService
      .getteamsolutionkeypoint(teamsolutionlinkId)
      .subscribe(
        data => {
          this.KeypointList = data
          console.log("KeypointList", data);
          var groups = new Set(this.KeypointList.map(t => t.name))
          // var groups =new Set(this.KeypointList.map(t => t.username))
          //console.log("group",groups);
          this.KeypointLists = [];
          groups.forEach(g =>
            this.KeypointLists.push({
              name: g,
              values: this.KeypointList.filter(i => i.name === g)
            }
            ))
          console.log("KeypointList", this.KeypointList.values);
        },
        err => console.error(err),
        () => console.log('getKeypointList completed')
      );
  }

  //Solution List
  enablecommentindex = null;;
  enableReplytwt(event, idval, id) {
    this.textPresent = true;
    this.enableEdit = true;
    this.enableEditIndex = id;
    this.enablecommentindex = idval;
    // this.hrefvisible=false;
    // this.hrefvisible = i;
  }

  opensolutionkeysuggestion($event,teamsolutionkeypointId,idval,id){
    this.getsolutionteamSuggestion(teamsolutionkeypointId);
    if(this.teamSolKeySuggestionlist){
      this.showsolution = id;
      this.teamsolutionkeypointId = teamsolutionkeypointId;
    }
  }
  closesolutionkeysuggestion(){
    // this.getsolutionteamSuggestion(teamsolutionkeypointId);
      this.showsolution = null;
      this.teamsolutionkeypointId = null;
  }

  //Solution List
  getsolutionteamSuggestion(teamkeypointId) {
    this.teamSolKeySuggestionlist = [];
    // this.show = id;
    this.teamService.getKeysolutionSuggestion(teamkeypointId)
        .subscribe(data => {
        this.teamSolKeySuggestionlist = data;
        for (var i = 0; i < this.teamSolKeySuggestionlist.length; i++) {
          this.keysuggestionimageName = "https://linkroster-userimage.s3.amazonaws.com/" + this.teamSolKeySuggestionlist[i].name;
          console.log("name", this.teamSolKeySuggestionlist[i].name);
        }
        //console.log("teamKeySuggestionlist",this.teamKeySuggestionlist)
        // this.textPresent = true;
        // this.enableEdit = true;
        // this.enableEditIndex = id;
        // this.enablecommentindex = idval;
      });
  }

  //Solution List
  public Comments(teamkeypointId, teamsolutionlinkId, keySuggestion, teamId,data,solform:NgForm) {
    this.solutioninvalid = false;
    if(keySuggestion == undefined){
      this.solutioninvalid = true;
    }
    else{
    const dataKey = {
      teamKeySuggestion: keySuggestion,
      teamsolutionlinkId: teamsolutionlinkId,
      // teamsolutionId: data.teamsolutionId,
      teamkeypointId: teamkeypointId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id
    };
    console.log("302", dataKey)
    this.teamService.addsolutionKeySuggestion(dataKey).subscribe(
      data => {
        console.log("comments",data);
        solform.resetForm();
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        this.enableEdit = false;
        this.getsolutionteamSuggestion(teamkeypointId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      });
    }
  }

  //Solution List
  Addteamsolutionkeypoints(teamsolutionkeypoint, teamsolutionlinkId, teamsolutionId, teamsolution,ff:NgForm) {
    const dataKey = {
      teamsolutionkeypoints: teamsolutionkeypoint,
      teamsolutionlinkId: teamsolutionlinkId,
      teamId: teamsolution.teamId,
      teamsolutionId: teamsolutionId,
      userId: this.tokenStorage.getUser().id,
      teamchallengeId: teamsolution.teamchallengeId

    };
    this.teamService.addteamsolutionkeypoints(dataKey).subscribe(
      data => {
        //console.log(data);     
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        ff.resetForm();
        this.getteamsolutionkeypoint(teamsolutionlinkId);
        // this.showshareteamdetails();
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
  } 

  //Solution List
  showshareteamdetails() {
    if (this.form.teamId) {
      this.getteamShareDetail();
    }
    else {
      this.getpublicTeamSharelink();
    }
  }

  //Step List
  showStepShareModal(teamsteplinkId: number, teamId: number): void {
    $("#stepShareModal").modal('show');
    this.teamsteplinkId = teamsteplinkId;
  }

  //Step List
  onchangeStepAccessType(val: any) {
    this.shareStepLink.accessType = val;
  }

  //Step List
  getStepkeypoint(teamsteplinkId: number) {
    this.stepKeypointList=[];
    this.stepKeypointLists = [];
    this.teamService
      .getteamstepkeypoint(teamsteplinkId)
      .subscribe(
        data => {
          this.stepKeypointList = data
          this.limitedstepKeypointLists = this.stepKeypointList.slice(0, 3);
          //console.log("slice data", this.limitedKeypointList);
          var groups = new Set(this.limitedstepKeypointLists.map(t => t.name))
          // var groups =new Set(this.KeypointList.map(t => t.username))
          //console.log("group",groups);
          this.stepKeypointLists = [];
          groups.forEach(g =>
            this.stepKeypointLists.push({
              name: g,
              values: this.limitedstepKeypointLists.filter(i => i.name === g)
            }
            ))
        },
        err => console.error(err),
        () => console.log('getStepKeypointList completed')
      );
  }

  //Step List
  shareStep(teamsteplinkId, teamId, teamsteplink, teamstepId,stepshare, fshare:NgForm) {
    if(this.shareStepLink.accessType == ''){
      this.isStepSuccessful = false;
    }
    else{
      const datalink = {
        teamsteplink: teamsteplink,
        teamstepId: teamstepId,
        teamsteplinkId: teamsteplinkId,
        userId: this.tokenStorage.getUser().id,
        teamId: teamId,
        accesstype: this.shareStepLink.accessType,
        sharedTime: new Date().toDateString(),
        teamgoalId: stepshare.teamgoalId
      };
      this.teamService.addsharestep(datalink).subscribe(
        data => {
          console.log("409",data);
          fshare.resetForm();
          document.getElementById('stpclose').click();
          this.getteamShareStepLink();
        },
        err => {
        });
    }
  }

  opensteplinkkeysuggestion($event,teamstepkeypointId,idval,id){
    this.getteamSteptSuggestion(teamstepkeypointId);
    if(this.teamStepKeySuggestionlist){
      this.showstepkey = id;
      this.teamstepkeypointId = teamstepkeypointId;
    }
  }
  closesteplinkkeysuggestion(){
    this.showstepkey = null;
    this.teamstepkeypointId = null;
    // this.getteamSteptSuggestion(teamstepkeypointId);
  }


  //Step List
  getteamSteptSuggestion(teamstepkeypointId) {
    // this.showstepkey = id;
    this.teamStepKeySuggestionlist = [];
    this.teamService.getKeystepSuggestion(teamstepkeypointId)
      .subscribe(data => {
        this.teamStepKeySuggestionlist = data;
        console.log("446 ", this.teamStepKeySuggestionlist);
        for (var i = 0; i < this.teamStepKeySuggestionlist.length; i++) {
          this.keyStepsuggestionimageName = "https://linkroster-userimage.s3.amazonaws.com/" + this.teamStepKeySuggestionlist[i].name;
          //console.log("name",this.teamKeySuggestionlist[i].name);
          this.limitedteamStepKeySuggestion = this.teamStepKeySuggestionlist.slice(0, 3)
        }

        //console.log("teamKeySuggestionlist",this.teamKeySuggestionlist)
        // this.textPresent = true;
        // this.enableEdit = true;
        // this.enableEditIndex = id; this.enablecommentindex = idval;
      });
  }

  //Step List
  getteamShareStepLink() {
    console.log(this.userId);
    this.teamService.getteamSharedstep().subscribe(
      data => {
        this.sharesteplist = data;
        console.log("933", this.sharesteplist);
        if(this.sharesteplist.length>0){
          this.opensteptesttoggle(data[0], 0);
        }
      },
      err => {
        console.log("getstep",err.error.message);
      }
    );
  }

  //Step List
  public StepComment(teamstepkeypointId, teamsteplinkId, keySuggestion, teamId, stpform:NgForm) {
    this.stepinvalid = false;
    if(keySuggestion == undefined){
      this.stepinvalid = true;
    }
    else{
    const dataKey = {
      teamstepKeySuggestion: keySuggestion,
      teamsteplinkId: teamsteplinkId,
      teamstepkeypointId: teamstepkeypointId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id
    };
    this.teamService.addstepKeySuggestion(dataKey).subscribe(
      data => {
        stpform.resetForm();
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        this.enableEdit = false;
        this.enablecommentindex = null;
        this.getteamSteptSuggestion(teamstepkeypointId);
        
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
    }
  }

  //Step List
  AddStepKeyComments(teamstepKeypoints, teamsteplinkId, teamId, teamstepId,stepshare,ff:NgForm) {
    console.log("1719",stepshare)
    const dataKey = {
      teamstepKeypoints: teamstepKeypoints,
      teamsteplinkId: teamsteplinkId,
      teamstepId: teamstepId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id,
      teamgoalId: stepshare.teamgoalId
    };

    console.log("datakey", dataKey);
    this.teamService.addteamstepkeypoints(dataKey).subscribe(
      data => {
        //console.log(data);     
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        ff.resetForm()
        this.getStepkeypoint(teamsteplinkId);
        // this.showshareteamdetails();
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
  }

  //Step List
  opensteptesttoggle(selectedItem: any, i) {
    this.showstepkey=null;
    this.teamstepkeypointId=null;
    this.showCollasalIndex = i;
    console.log("537");
    this.getStepkeypoint(selectedItem.teamsteplinkId);
  }

  //Step List
  onteamStepKeySuggestionViewMore(): void {
    this.limitedStepteamKeySuggestion2 = this.teamStepKeySuggestionlist.slice(3, this.teamStepKeySuggestionlist.length);
    this.limitedteamStepKeySuggestion = this.limitedteamStepKeySuggestion.concat(this.limitedStepteamKeySuggestion2);
    //console.log(this.limitedteamKeySuggestion);
    //console.log(this.teamKeySuggestionlist.length , this.limitedteamKeySuggestion2.length);
    $('#view-more').hide();
    $('#view-less').show();
  }

  //Step List
  onteamStepKeySuggestionviewless(): void {
    this.limitedteamStepKeySuggestion = this.teamStepKeySuggestionlist.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }

  //Step List
  onStepKeyPointsViewMore(): void {
    this.limitedstepKeypointLists2 = this.stepKeypointList.slice(3, this.stepKeypointList.length);
    // console.log("limitedKeypointList2",this.limitedKeypointList2);
    this.limitedstepKeypointLists = this.limitedKeypointList.concat(this.limitedstepKeypointLists2);
    $('#view-more').hide();
    $('#view-less').show();
  }

  //Step List
  onStepkeyPointsviewless(): void {
    this.limitedstepKeypointLists = this.stepKeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }




































  textPresent = false;

  constructor(private teamService: TeamService, private router: Router,
    private route: ActivatedRoute,
    private profileService: ProfileService,

    private tokenStorage: TokenStorageService, private authservice: AuthService,
    private SpinnerService: NgxSpinnerService
  ) { }

  toggle() {

    $('.list-group-item').on('click', function () {
      $('.glyphicon', this)
        .toggleClass('glyphicon-chevron-right')
        .toggleClass('glyphicon-chevron-down');
    });

  };
  test() {

    $(document).ready(function () {

      $(".menu_link").each(function (index, element) {

        var target = $(this).text();
        $.ajax({
          url: "https://api.linkpreview.net",
          dataType: 'jsonp',
          data: { q: target, key: '5a2e292e7d25bb63a2d3b4c63524cd10abe39420dc68c' },

          success: function (result) {

            $(element).after(
              '<div class="row"><div class="col-4 col-md-3 col-xl-2 col-lg-2"><img style="width: 100%;height: 100%;min-height: 50%!important;" class="img-responsive" src="' + result.image + '"></div><div class="col-8 col-md-9 col-xl-10 col-lg-10"><h3>'



              + result.title + '</h3><a href="'
              + result.url + '">' + result.url + '</a></div></div>'
            ); //console.log("RESULT", result);

            $(element).remove();
          }
        })
      });
    });
  }

  opentesttoggle(selectedItem: any, i) {
    this.show = null;
    this.keypointid=null;
    this.enableEdit=false;  
    console.log("selected item",selectedItem,i);
    this.showCollasalIndex = i;
    this.getkeypoint(selectedItem.teambookmarkLinkId);
  }

  openkeySuggestiontoggle(selectedItem: any, i) {

    console.log("645",selectedItem)
    this.showCollasalIndex = i;
    // this.getteamSuggestion(selectedItem.teamkeypointId);



  }
  openkeytoggle(selectedItem: any, i) {

    this.showCollasalIndex = i;
  }
  test1(teambookmarkLinkId) {
    this.getkeypoint(teambookmarkLinkId);
    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip()
    }
    );
  }


  

  public reply() {
    this.test(); this.textPresent = true;

  }
  public Comment(keypointid, teambookmarkLinkId, keySuggestion, teamId, bokform:NgForm) {
    this.bookkeyinvalid =false;
    if(keySuggestion==undefined){
      this.bookkeyinvalid =true;
    }
    else{
    const dataKey = {
      teamKeySuggestion: keySuggestion,
      teambookmarkLinkId: teambookmarkLinkId,
      teamkeypointId: keypointid,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id
    };
    this.teamService.addKeySuggestion(dataKey).subscribe(
      data => {
        console.log(data);
        bokform.resetForm();
        this.enablecommentindex = null;
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        this.enableEdit = false;
        this.getteamSuggestion(keypointid);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
    }
  }

  openbookmarkkeysuggestion(event, teamkeypointId, idval, id){
    this.show = id;
    this.keypointid = teamkeypointId;
    this.getteamSuggestion(teamkeypointId);
  }
  closebookmarkkeysuggestion(){
    this.show = null;
    this.keypointid = null;
    // this.getteamSuggestion(teamkeypointId);
  }
  show = null;
  getteamSuggestion(teamkeypointId) {
    this.teamKeySuggestionlist=[];
    this.teamService.getKeySuggestion(teamkeypointId)
        .subscribe(data => {
        this.teamKeySuggestionlist = data
        for (var i = 0; i < this.teamKeySuggestionlist.length; i++) {


          this.keysuggestionimageName = "https://linkroster-userimage.s3.amazonaws.com/" + this.teamKeySuggestionlist[i].name;

          console.log("name", this.teamKeySuggestionlist[i].name);
        }

        //console.log("teamKeySuggestionlist",this.teamKeySuggestionlist)
        // this.textPresent = true;
        // this.enableEdit = true;
        // this.enableEditIndex = id; this.enablecommentindex = idval;

      });
  }

  teamSettingclick() {

    this.router.navigate(['teamsetting'], { queryParams: { teamId: this.form.teamId } });
  }
  teambookmarkLinkId: number;
  showShareModal(teambookmarkLinkId: number, teamId: number): void {

    $("#shareModal").modal('show');

    //  /this.shareBookmark(teambookmarkLinkId,teamId); 
    // this.Link =selectedItem.link;   
    //  console.log("Selected item Id: ", selectedItem.Id); 
    this.teambookmarkLinkId = teambookmarkLinkId;// You get the Id of the selected item here
  }
  onchangeAccessType(val: any) {

    this.sharebookmarklink.accessType = val;

  }

  addHero(newHero: string) {


  }
  values = '';
  onKey(event: any) { // without type info

    this.values += event.target.value + ' | ';
  }



  Addcomments(keySuggestion, key) {

    this.enableEdit = true;
    const dataKey = {

      // teambookmarkLinkId:teambookmarkLinkId,
      // teamId:teamId,
      userId: this.tokenStorage.getUser().id

    };

    this.teamService.addKeypoint(dataKey).subscribe(
      data => {
        //console.log(data);     

        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        // this.getkeypoint(teambookmarkLinkId);

      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );


  }

  AddKeyComments(keyComments, teambookmarkLinkId, teamId,list,ff:NgForm) {
    console.log("387",list);
    const dataKey = {
      teamkeyComments: keyComments,
      teambookmarkLinkId: teambookmarkLinkId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id,
      teambookmarkCategoryId : list.teambookmarkcategoryId
    };
console.log("396",dataKey);
    this.teamService.addKeypoint(dataKey).subscribe(
      data => {
        //console.log(data);     

        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        ff.resetForm();
        this.getkeypoint(teambookmarkLinkId);
        this.showshareteamdetails();

      },



      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
    // of (true)
    // .pipe(mergeMap(()=>this.teamService.addKeypoint(dataKey).pipe(map(data=>console.log(data)))),

    // mergeMap(data=>forkJoin(...data.map(d => this.getcomment(d.teambookmarkLinkId,d)
    // ))
    // ))

    // .subscribe(data=>{
    //   console.log(data);  
    //   this.isLoggedIn = true; 
    //   this.iskeySuccessful=true;
    //   this.getkeypoint(teambookmarkLinkId);
    // });



    this.getkeypoint(teambookmarkLinkId);


  } KeypointLists = [];
  getkeypoint(BookmarkLinkId: number) {
    this.KeypointLists = [];
    this.BookmarkKeypointLists=[];
    this.teamService
      .getKeypoint(BookmarkLinkId)
      .subscribe(
        data => {
          this.BookmarkKeypointList = data
          console.log("KeypointList123", data);

          var groups = new Set(this.BookmarkKeypointList.map(t => t.name))
          // var groups =new Set(this.KeypointList.map(t => t.username))
          //console.log("group",groups);
          this.KeypointLists = [];
          groups.forEach(g =>
        
            
            this.BookmarkKeypointLists.push({
              name: g,
              values: this.BookmarkKeypointList.filter(i => i.name === g)

            }
       )
            )
          console.log("BookmarkKeypointList", this.BookmarkKeypointList.values);
        },

        err => console.error(err),
        () => console.log('getKeypointList completed')
      );

  }
  keypointSuggestion = []; keyimageName: string; keysuggestionimageName: string;
  getkeypointSuggestion(keypointid) {

    this.teamService
      .getKeySuggestion(keypointid)
      .subscribe(
        data => {
          this.teamKeySuggestionlist = data


          //console.log("KeypointsuggestionList",data);  


          var groups = new Set(this.teamKeySuggestionlist.map(t => t.keypointid))
          //console.log("group",groups);
          this.keypointSuggestion = [];
          groups.forEach(g =>
            this.keypointSuggestion.push({
              keypointid: g,
              values: this.teamKeySuggestionlist.filter(i => i.keypointid === g)

            }


            ))
          // console.log("KeypointList",this.KeypointList.values);   
        },
        err => console.error(err),
        () => console.log('getKeypointList completed')
      );

  }

  imageName: string;
  getpublicTeamSharelink() {

    //   this.teamService.getpublicTeamSharelink()

    //   .subscribe(data=>{this.teamSharelist=data

    //  console.log("teamSharelist",this.teamSharelist)
    // });
    // this.SpinnerService.show();
    // of(true)
    //   .pipe(mergeMap(() =>
       this.teamService.getpublicTeamSharelink()
        // .pipe(map(data => this.teamSharelist = data)
        // )),

        // mergeMap(data => forkJoin(...data.map(d => this.getcomment(d.teambookmarkLinkId, d)

        // ))


        // ))


      .subscribe(data => {
        console.log("data 524",data);
        
        this.teamSharelist = data;
        if(this.teamSharelist.length>0){
          this.opentesttoggle(data[0],0);
        }
        // this.SpinnerService.hide();

      });

  }
  getimage() {

  }
  getcomment(BookmarkLinkId: number, data) {



    return this.teamService
      .getKeypoint(BookmarkLinkId).pipe(map((keypoint: any) => {
        //console.log("keypoint:",keypoint)
        return {
          ...data,
          count: keypoint.length
        };
      }
      ))


  }

  getteamShareDetail() {


    this.teamService.getteamShareDetail(this.form.teamId)

      .subscribe(data => {
        this.teamSharelist = data
        console.log("teamSharelist",this.teamSharelist)

        this.SpinnerService.hide();

      });
  }
  getAlluser(teamId: number) {
    this.teamService.getallTeamInviteMember(this.tokenStorage.getUser().id, teamId)
      .subscribe(
        data => {
          this.userlist = data
          //console.log("userlsit",this.userlist)  ;   
        },
        err => console.error(err),
        () => console.log('not completed')
      );

  }

  shareBookmark(teambookmarkLinkIdparam, teamId,fshare:NgForm,list) {
    if(this.sharebookmarklink.accessType == ''){
      this.isbokSuccessful = false;
    }
    else{
      const datalink = {
        teambookmarklinkId: this.teambookmarkLinkId,
        userId: this.tokenStorage.getUser().id,
        teamId: teamId,
        accesstype: this.sharebookmarklink.accessType,
        sharedTime: new Date().toDateString(),
        teambookmarkcategoryId : list.teambookmarkcategoryId
      };
      this.teamService.addsharebookmark(datalink).subscribe(
        data => {
          console.log(data);
          fshare.resetForm();
          document.getElementById('bokclose').click();
          this.getpublicTeamSharelink();
        },
        err => {

        });
      }
  }


  hrefvisible = true;
  enableEditMethod(e, i) {

    this.enableEdit = true;
    this.enableEditIndex = i;
    //console.log(i, e);
    this.hrefvisible = false;
    this.hrefvisible = i;

  }

  enableReply(e, i) {

    this.textPresent = true;

    this.enableEdit = true;
    this.enableEditIndex = i;
    //console.log(i, e);
    this.hrefvisible = false;
    this.hrefvisible = i;

  }
  cancel() {
    this.enableEdit = false;
    this.approachinvalid = false;
    this.solutioninvalid = false;
    this.bookkeyinvalid = false;
    this.stepinvalid = false;
    this.skillinvalid = false;
    this.conceptinvalid = false;
    this.enablecommentindex = null;
    //window.location.reload();
  }

  showInviteList() {

    this.teamService.showPendingList(this.tokenStorage.getUser().email)

      .subscribe(data => {
        this.InviteList = data

        //console.log("invitelist",this.InviteList.length)

        //console.log("len",this.InviteList.length.valueOf());
        if (this.InviteList.length > 0) {

          this.noticationstatus = true;
          //console.log("stat",this.noticationstatus);
        }

        else { this.noticationstatus = false }
        this.notication = this.InviteList.length.valueOf()

        //console.log("lnoticationen",this.notication);
      }

      );
  }
  loggedinUserimage: string; retrievedImage: any;
  getImage() {

    this.profileService.getUserImage(this.tokenStorage.getUser().id).subscribe(
      res => {
        this.loggedinUserimage = res.name;

        //console.log("log",this.loggedinUserimage);
        this.retrievedImage = this.loggedinUserimage;
      }
    );
  }
  getteamname() {
    this.teamService.getTeamnameList(this.tokenStorage.getUser().id).subscribe(data => {
      this.teamList = data
      console.log("team", this.teamList)
    });
  }

  




  


  // KeypointLists = [];
  // getsolutionkeypoint(teamsolutionlinkId:number)
  // {

  // this.teamService
  // .getteamsolutionkeypoint(teamsolutionlinkId)
  // .subscribe(
  //   data => { 
  //     this.KeypointList= data 
  //     console.log("KeypointList",data);  

  //     var groups = new Set(this.KeypointList.map(t => t.name))
  //     // var groups =new Set(this.KeypointList.map(t => t.username))
  //     //console.log("group",groups);
  //       this.KeypointLists = [];
  //       groups.forEach(g => 
  //         this.KeypointLists.push({
  //           name: g, 
  //           values: this.KeypointList.filter(i => i.name === g)

  //         }


  //       ))  
  //       console.log("KeypointList",this.KeypointList.values);   
  //    },

  //   err => console.error(err), 
  //   () => console.log('getKeypointList completed') 
  //   );

  // }

  





  
  // {console.log("team",this.teamList)});}


  getteamShareSkillLink() {
    this.shareskilllist=[];
    this.teamService.getteamSharedskill().subscribe(
      data => {
        this.shareskilllist = data;
        this.openskilltesttoggle(data[0], 0);

      },
      err => {
        this.shareskilllist = JSON.parse(err.error).message;
      }
    );

  }
  //  getteamShareconceptLink(){ 
  //   // console.log(this.userId);
  //  this.teamService.getteamSharedconcept().subscribe(
  //        data => { 
  //          this.shareconceptlist= data;
  //          this.openconcepttoggles(data[0],0);

  //         },
  //         err => {
  //          this.shareconceptlist = JSON.parse(err.error).message;
  //        }       
  //        );

  // }

  getteamsharegoalname() {
    this.teamService.getteamSharegoalname().subscribe(
      data => {
        this.sharegoallist = data;
        console.log(this.username);
        console.log(this.teamgoalName);
        console.log(this.teamstepName);
        console.log(this.teamskillName);
      },
      err => {
        // this.sharegoallist = JSON.parse(err.console.error).message;
      }
    );
  }
  getteamsharetopicname() {
    this.teamService.getteamSharetopicname().subscribe(
      data => {
        this.sharetopiclist = data;
        console.log(this.username);
        console.log(this.teamTopicName);
        console.log(this.teamConceptName);
      },
      err => {
        // this.sharetopiclist = JSON.parse(err.console.error).message;
      }
    );
  }






  getpublicConceptShare() {
    this.teamService
      .getpublicConceptShare(this.tokenStorage.getUser().id)//,this.bookmarklink.bookmarklinkId)
      .subscribe(
        data => {

          this.conceptlist = data
          // this.test1();
          this.opentesttoggle(data[0], 0);
          this.SpinnerService.hide();
        },
        err => {
          this.conceptlist = JSON.parse(err.error).message;
        }
      );
  }
  getTeamApproachshareLink() {
    this.bookmarkteamapproachlist = [];
    this.teamService.getTeamApproachShare()
      .subscribe(data => {
        this.bookmarkteamapproachlist = data;
        if(this.bookmarkteamapproachlist.length>0){
          this.openapproachtesttoggle(data[0],0)
        }
        console.log("1043", this.bookmarkteamapproachlist);

      });

  }


  getpublicTeamConceptSharelink() {
    this.ConceptSharelist = [];
      this.teamService.getteamSharedconcept()
      .subscribe(data => {
        this.ConceptSharelist = data;
        console.log("1041", data);
        this.openconcepttesttoggle(data[0], 0);
      });

  }

  openconcepttesttoggle(selectedItem: any, i) {
    this.showconceptkey = null;
    this.teamConceptKeypointId=null;
    this.showCollasalIndex = i;
    this.getConceptkeypoint(selectedItem.teamConceptLinkId);
  }


  showConceptShareModal(teamConceptLinkId: number, teamId: number, da): void {

    $("#conceptShareModal").modal('show');
    console.log("1242", da);
    this.teamConceptLinkId = teamConceptLinkId;// You get the Id of the selected item here

  }
  onchangeConceptAccessType(val: any) {

    this.shareConceptLink.accessType = val;

  }


  getConceptkeypoint(teamConceptLinkId: number) {
    this.conceptKeypointList=[];
    this.conceptKeypointLists = [];
    this.teamService
      .getteamconceptkeypoint(teamConceptLinkId)
      .subscribe(
        data => {
          this.conceptKeypointList = data
          console.log("conkey", this.conceptKeypointList);
          this.limitedconceptKeypointLists = this.conceptKeypointList.slice(0, 3);
          //console.log("slice data", this.limitedKeypointList);
          var groups = new Set(this.limitedconceptKeypointLists.map(t => t.name))
          // var groups =new Set(this.KeypointList.map(t => t.username))
          //console.log("group",groups);
          this.conceptKeypointLists = [];


          groups.forEach(g =>
            this.conceptKeypointLists.push({
              name: g,
              values: this.conceptKeypointList.filter(i => i.name === g)

            }


            ))
          //console.log("count", this.limitedKeypointList.length);
          //console.log("KeypointList",this.KeypointList.values);   
        },

        err => console.error(err),
        () => console.log('getConceptKeypointList completed')
      );

  }


  shareConcept(teamConceptLinkId, teamId, teamconceptlink, teamConceptId, conceptshare) {
    console.log("1290", conceptshare)
    if(this.shareConceptLink.accessType == ''){
      this.isConceptSuccessful = false;
    }
    else{
      const datalink = {
        teamConceptId: teamConceptId,
        teamconceptlink: teamconceptlink,
        teamConceptLinkId: teamConceptLinkId,
        userId: this.tokenStorage.getUser().id,
        teamId: teamId,
        accesstype: this.shareConceptLink.accessType,
        sharedTime: new Date().toDateString(),
        teamTopicId: conceptshare.teamTopicId
      };
      console.log("datconcept", datalink);
      this.teamService.addshareconcept(datalink).subscribe(
        data => {
          console.log(data);
          document.getElementById("conclose").click();
          this.getpublicTeamConceptSharelink();
        },
        err => {

        });
    }
  }


  openconceptkeysuggestion($event,teamConceptKeypointId,idval,id){
    this.getteamConceptSuggestion(teamConceptKeypointId);
    if(this.teamConceptKeySuggestionlist){
      this.showconceptkey = id;
      this.teamConceptKeypointId = teamConceptKeypointId;
    }
  }
  closeconceptkeysuggestion(){
    // this.getteamConceptSuggestion(teamConceptKeypointId);
    this.showconceptkey = null;
    this.teamConceptKeypointId = null;
  }

  getteamConceptSuggestion(teamConceptKeypointId) {
    // this.show = id;
    this.teamConceptKeySuggestionlist=[];
    this.teamService.getKeyconceptSuggestion(teamConceptKeypointId)
      .subscribe(data => {
        this.teamConceptKeySuggestionlist = data;
        console.log("consug", this.teamConceptKeySuggestionlist)
        for (var i = 0; i < this.teamConceptKeySuggestionlist.length; i++) {
          this.keyConceptsuggestionimageName = "https://linkroster-userimage.s3.amazonaws.com/" + this.teamConceptKeySuggestionlist[i].name;
          //console.log("name",this.teamKeySuggestionlist[i].name);
          this.limitedteamConceptKeySuggestion = this.teamConceptKeySuggestionlist.slice(0, 3)
        }
        //console.log("teamKeySuggestionlist",this.teamKeySuggestionlist)
        // this.textPresent = true;
        // this.enableEdit = true;
        // this.enableEditIndex = id; this.enablecommentindex = idval;
      });
  }


  onteamConceptKeySuggestionViewMore(): void {
    this.limitedConceptteamKeySuggestion2 = this.teamConceptKeySuggestionlist.slice(3, this.teamConceptKeySuggestionlist.length);

    this.limitedteamConceptKeySuggestion = this.limitedteamConceptKeySuggestion.concat(this.limitedConceptteamKeySuggestion2);
    //console.log(this.limitedteamKeySuggestion);
    //console.log(this.teamKeySuggestionlist.length , this.limitedteamKeySuggestion2.length);
    $('#view-more').hide();
    $('#view-less').show();
  }

  onteamConceptKeySuggestionviewless(): void {
    this.limitedteamConceptKeySuggestion = this.teamConceptKeySuggestionlist.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }


  public ConceptComment(teamConceptKeypointId, teamConceptLinkId, keySuggestion, teamId, conform:NgForm) {
    this.conceptinvalid = false;
    if(keySuggestion == undefined){
      this.conceptinvalid =true;
    }
    else{
    const dataKey = {
      teamKeySuggestion: keySuggestion,
      teamConceptLinkId: teamConceptLinkId,
      teamConceptKeypointId: teamConceptKeypointId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id
    };

    this.teamService.addconceptKeySuggestion(dataKey).subscribe(
      data => {
        //  console.log(data);     
        //   this.keysuggestion = data;
        //   this.limitedkeysuggestion = this.limitedKeypointList.slice(0,3);
        //  console.log("limitedkeysuggestion",this.limitedkeysuggestion);
        conform.resetForm();
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        this.enableEdit = false;
        this.enablecommentindex=null;
        this.getteamConceptSuggestion(teamConceptKeypointId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
    }
  }



  onConceptKeyPointsViewMore(): void {
    this.limitedconceptKeypointLists2 = this.conceptKeypointList.slice(3, this.conceptKeypointList.length);

    // console.log("limitedKeypointList2",this.limitedKeypointList2);
    this.limitedconceptKeypointLists = this.limitedKeypointList.concat(this.limitedconceptKeypointLists2);
    //console.log("limitedKeypointList",this.limitedKeypointList);
    //console.log(this.KeypointList.length , this.limitedKeypointList.length);

    $('#view-more').hide();
    $('#view-less').show();
  }

  onConceptkeyPointsviewless(): void {
    this.limitedconceptKeypointLists = this.conceptKeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }


  AddConceptKeyComments(teamConceptKeypoints, teamConceptLinkId, teamId, teamConceptId, ConceptKeypoints, ff:NgForm) {

    console.log("1269", ConceptKeypoints)
    const dataKey = {
      teamConceptKeypoints: teamConceptKeypoints,
      teamConceptLinkId: teamConceptLinkId,
      teamConceptId: teamConceptId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id,
      teamTopicId: ConceptKeypoints.teamTopicId
    };

    this.teamService.addteamconceptkeypoints(dataKey).subscribe(
      data => {
        //console.log(data);     
        ff.resetForm();
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        this.getConceptkeypoint(teamConceptLinkId);
        // this.showshareteamdetails();
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );

    this.getConceptkeypoint(teamConceptLinkId);


  }
  openapproachtesttoggle(selectedItem: any, i) {
    this.showapproach=null;
    this.approachkeypointid=null;
    this.showCollasalIndex = i;
    // if (selectedItem == undefined) {
    //   this.getApproachkeypoint(1);

    // }
    // else {
      this.getApproachkeypoint(selectedItem.teamapproachlinkId);
    // }
  }
  showapproachShareModals(teamapproachlinkId: number, teamId: number): void {

    $("#approachshareModal").modal('show');
    this.teamapproachlinkId = teamapproachlinkId;// You get the Id of the selected item here

  }
  onchangeapproachAccessTypes(val: any) {

    this.shareapproachlink.accessType = val;

  }

  shareApproach(teamapproachlinkId, teamId,fshare:NgForm,approach) {
    if(this.shareapproachlink.accessType == ''){
      this.isAppSuccessful = false;
    }
    else{
      const datalink = {
        teamapproachlinkId: teamapproachlinkId,
        userId: this.tokenStorage.getUser().id,
        teamId: teamId,
        accesstype: this.shareapproachlink.accessType,
        sharedTime: new Date().toDateString(),
        teamchallengeId: approach.teamchallengeId,
        teamapproachlink: approach.teamapproachlink,
        teamapproachId: approach.teamapproachId
      };


      this.teamService.addshareapproach(datalink).subscribe(
        data => {
          console.log(data);
          fshare.resetForm();
          document.getElementById('appclose').click();
        },
        err => {

        });
      }
  }
  getApproachkeypoint(teamapproachlinkId: number) {
    this.approachKeypointList = [];
    this.approachKeypointLists = [];
    this.teamService
      .getapproachkeypoints(teamapproachlinkId)
      .subscribe(
        data => {
          this.approachKeypointList = data;
          this.limitedapproachKeypointLists = this.approachKeypointList.slice(0, 3);
          var groups = new Set(this.limitedapproachKeypointLists.map(t => t.name))
          // var groups =new Set(this.KeypointList.map(t => t.username))
          //console.log("group",groups);
          this.approachKeypointLists = [];
          groups.forEach(g =>
            this.approachKeypointLists.push({
              name: g,
              values: this.limitedapproachKeypointLists.filter(i => i.name === g)
            }
            ))
          //console.log("count", this.limitedKeypointList.length);
          //console.log("KeypointList",this.KeypointList.values);   
        },
        err => console.error(err),
        () => console.log('getApproachKeypointList completed')
      );
  }

  openapproachkeysuggestion($event,approachkeypointid,idval,id){
    this.getteamApproachSuggestion(approachkeypointid);
    if(this.teamApproachKeySuggestionlist){
      this.showapproach =id;
      this.approachkeypointid = approachkeypointid;
    }
  }
  closeapproachkeysuggestion(){
    // this.getteamApproachSuggestion(approachkeypointid);
      this.showapproach =null;
      this.approachkeypointid = null;
  }

  getteamApproachSuggestion(teamkeypointId) {
    this.teamApproachKeySuggestionlist = [];
    // this.show = id;
    this.teamService.getKeyapproachSuggestion(teamkeypointId)
        .subscribe(data => {
        this.teamApproachKeySuggestionlist = data;
        console.log("1641",this.teamApproachKeySuggestionlist)
        for (var i = 0; i < this.teamApproachKeySuggestionlist.length; i++) {
          this.keyApproachsuggestionimageName = "https://linkroster-userimage.s3.amazonaws.com/" + this.teamApproachKeySuggestionlist[i].name;
          //console.log("name",this.teamKeySuggestionlist[i].name);
          this.limitedteamApproachKeySuggestion = this.teamApproachKeySuggestionlist.slice(0, 3)
        }
        //console.log("teamKeySuggestionlist",this.teamKeySuggestionlist)
        // this.textPresent = true;
        // this.enableEdit = true;
        // this.enableEditIndex = id; this.enablecommentindex = idval;
      });
  }


  onteamApproachKeySuggestionViewMore(): void {
    this.limitedApproachteamKeySuggestion2 = this.teamApproachKeySuggestionlist.slice(3, this.teamApproachKeySuggestionlist.length);

    this.limitedteamApproachKeySuggestion = this.limitedteamApproachKeySuggestion.concat(this.limitedApproachteamKeySuggestion2);
    //console.log(this.limitedteamKeySuggestion);
    //console.log(this.teamKeySuggestionlist.length , this.limitedteamKeySuggestion2.length);
    $('#view-more').hide();
    $('#view-less').show();
  }

  onteamApproachKeySuggestionviewless(): void {
    this.limitedteamApproachKeySuggestion = this.teamApproachKeySuggestionlist.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }


  public ApproachComment(teamkeypointId, teamapproachlinkId, keySuggestion, teamId,appform:NgForm) {
    this.approachinvalid = false;
    if(keySuggestion == undefined){
      this.approachinvalid = true;
    }
    else{
    const dataKey = {
      teamKeySuggestion: keySuggestion,
      teamapproachlinkId: teamapproachlinkId,
      teamkeypointId: teamkeypointId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id
    };

    this.teamService.addapproachKeySuggestion(dataKey).subscribe(
      data => {
        //  console.log(data);     
        //   this.keysuggestion = data;
        //   this.limitedkeysuggestion = this.limitedKeypointList.slice(0,3);
        //  console.log("limitedkeysuggestion",this.limitedkeysuggestion);
        appform.resetForm();
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        this.enableEdit = false;
        this.getteamApproachSuggestion(teamkeypointId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
    }
  }



  onApproachKeyPointsViewMore(): void {
    this.limitedapproachKeypointLists2 = this.approachKeypointList.slice(3, this.approachKeypointList.length);

    // console.log("limitedKeypointList2",this.limitedKeypointList2);
    this.limitedapproachKeypointLists = this.limitedKeypointList.concat(this.limitedapproachKeypointLists2);

    $('#view-more').hide();
    $('#view-less').show();
  }

  onApproachkeyPointsviewless(): void {
    this.limitedapproachKeypointLists = this.approachKeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }


  AddApproachKeyComments(teamapproachkeypoints, teamapproachlinkId, teamId, teamapproachId,approach,ff:NgForm) {

    console.log("1488", approach)
    const dataKey = {
      teamapproachkeypoints: teamapproachkeypoints,
      teamapproachlinkId: teamapproachlinkId,
      teamapproachId: teamapproachId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id,
      teamchallengeId: approach.teamchallengeId
    };
    console.log("1497", dataKey);
    this.teamService.addteamapproachkeypoints(dataKey).subscribe(
      data => {
        //console.log(data);     
        ff.resetForm();
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        this.getApproachkeypoint(teamapproachlinkId);
        // this.showshareteamdetails();

      },



      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
    this.getApproachkeypoint(teamapproachlinkId);


  }













  openskilltesttoggle(selectedItem: any, i) {
    this.showskill=null;
    this.teamskillkeypointId=null;
    this.showCollasalIndex = i;
    this.getSkillkeypoint(selectedItem.teamskilllinkId);
  }


  showSkillShareModal(teamskilllinkId: number, teamId: number): void {

    $("#skillShareModal").modal('show');

    this.teamskilllinkId = teamskilllinkId;// You get the Id of the selected item here

  }
  onchangeSkillAccessType(val: any) {

    this.shareSkillLink.accessType = val;

  }


  getSkillkeypoint(teamskilllinkId: number) {
    this.skillKeypointLists = [];
    this.skillKeypointList = [];
    this.teamService
      .getteamskillkeypoint(teamskilllinkId)
      .subscribe(
        data => {
          this.skillKeypointList = data
          this.limitedskillKeypointLists = this.skillKeypointList.slice(0, 3);

          var groups = new Set(this.limitedskillKeypointLists.map(t => t.name))
          // var groups =new Set(this.KeypointList.map(t => t.username))
          //console.log("group",groups);
          this.skillKeypointLists = [];


          groups.forEach(g =>
            this.skillKeypointLists.push({
              name: g,
              values: this.limitedskillKeypointLists.filter(i => i.name === g)

            }


            ))

        },

        err => console.error(err),
        () => console.log('getSkillKeypointList completed')
      );

  }


  shareSkill(teamskilllinkId, teamId, teamskilllink) {

    const datalink = {


      teamskilllink: teamskilllink,

      teamskilllinkId: teamskilllinkId,

      userId: this.tokenStorage.getUser().id,
      teamId: teamId,
      accesstype: this.shareSkillLink.accessType,

      sharedTime: new Date()


    };


    this.teamService.addshareskill(datalink).subscribe(
      data => {
        console.log(data);



      },
      err => {

      }
    );
  }

  openskillkeysuggestion($event,teamskillkeypointId,idval,id){
    this.getteamSkillSuggestion(teamskillkeypointId);
    if(this.teamSkillKeySuggestionlist){
      this.showskill=id;
      this.teamskillkeypointId=teamskillkeypointId;
    }
  }
  closeskillkeysuggestion(){
    // this.getteamSkillSuggestion(teamskillkeypointId);
      this.showskill=null;
      this.teamskillkeypointId=null;
  }


  getteamSkillSuggestion(teamskillkeypointId) {
    this.teamSkillKeySuggestionlist=[];
    // this.show = id;
    this.teamService.getKeyskillSuggestion(teamskillkeypointId)
        .subscribe(data => {
        this.teamSkillKeySuggestionlist = data
        for (var i = 0; i < this.teamSkillKeySuggestionlist.length; i++) {
          this.keySkillsuggestionimageName = "https://linkroster-userimage.s3.amazonaws.com/" + this.teamSkillKeySuggestionlist[i].name;
          //console.log("name",this.teamKeySuggestionlist[i].name);
          this.limitedteamSkillKeySuggestion = this.teamSkillKeySuggestionlist.slice(0, 3)
        }
        //console.log("teamKeySuggestionlist",this.teamKeySuggestionlist)
        // this.textPresent = true;
        // this.enableEdit = true;
        // this.enableEditIndex = id; this.enablecommentindex = idval;
      });
  }


  onteamSkillKeySuggestionViewMore(): void {
    this.limitedSkillteamKeySuggestion2 = this.teamSkillKeySuggestionlist.slice(3, this.teamSkillKeySuggestionlist.length);

    this.limitedteamSkillKeySuggestion = this.limitedteamSkillKeySuggestion.concat(this.limitedSkillteamKeySuggestion2);
    //console.log(this.limitedteamKeySuggestion);
    //console.log(this.teamKeySuggestionlist.length , this.limitedteamKeySuggestion2.length);
    $('#view-more').hide();
    $('#view-less').show();
  }

  onteamSkillKeySuggestionviewless(): void {
    this.limitedteamSkillKeySuggestion = this.teamSkillKeySuggestionlist.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }


  public SkillComment(teamskillkeypointId, teamskilllinkId, keySuggestion, teamId,sklform:NgForm) {
    this.skillinvalid = false;
    if(keySuggestion == undefined){
      this.skillinvalid = true;
    }
    else{
    const dataKey = {
      teamskillKeySuggestion: keySuggestion,
      teamskilllinkId: teamskilllinkId,
      teamskillkeypointId: teamskillkeypointId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id
    };

    this.teamService.addskillKeySuggestion(dataKey).subscribe(
      data => {
        //  console.log(data);     
        //   this.keysuggestion = data;
        //   this.limitedkeysuggestion = this.limitedKeypointList.slice(0,3);
        //  console.log("limitedkeysuggestion",this.limitedkeysuggestion);
        sklform.resetForm();
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        this.enableEdit = false;
        this.getteamSkillSuggestion(teamskillkeypointId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
    }
  }



  onSkillKeyPointsViewMore(): void {
    this.limitedskillKeypointLists2 = this.skillKeypointList.slice(3, this.skillKeypointList.length);

    // console.log("limitedKeypointList2",this.limitedKeypointList2);
    this.limitedskillKeypointLists = this.limitedKeypointList.concat(this.limitedskillKeypointLists2);

    $('#view-more').hide();
    $('#view-less').show();
  }

  onSkillkeyPointsviewless(): void {
    this.limitedskillKeypointLists = this.skillKeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }


  AddSkillKeyComments(teamskillKeypoints, teamskilllinkId, teamId, teamskillId, skillshare,ff:NgForm) {
console.log("1937", skillshare);

    const dataKey = {
      teamskillKeypoints: teamskillKeypoints,
      teamskilllinkId: teamskilllinkId,
      teamskillId: teamskillId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id,
      teamgoalId: skillshare.teamgoalId
    };

    this.teamService.addteamskillkeypoints(dataKey).subscribe(
      data => {
        //console.log(data);     
        ff.resetForm();
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        this.getSkillkeypoint(teamskilllinkId);
        // this.showshareteamdetails();

      },



      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );

    this.getSkillkeypoint(teamskilllinkId);


  }
  getteamshareChallengename() {
    this.teamService.getteamSharechallengename().subscribe(
      data => {
        this.sharechallengelist = data;
        console.log(this.username);
        console.log(this.teamchallengeName);
        console.log(this.teamsolutionName);
        console.log(this.teamapproachName);
      },
      err => {
        // this.sharechallengelist = JSON.parse(err.console.error).message;
      }
    );
  }












}

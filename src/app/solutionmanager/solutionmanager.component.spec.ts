import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolutionmanagerComponent } from './solutionmanager.component';

describe('SolutionmanagerComponent', () => {
  let component: SolutionmanagerComponent;
  let fixture: ComponentFixture<SolutionmanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolutionmanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolutionmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

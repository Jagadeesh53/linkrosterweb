import { Component, OnInit, ViewChild } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { ChallengeService } from '../_services/challenge.service';
import { FormGroup, FormControl, Validators, NgForm} from '@angular/forms';
declare var $:any;

@Component({
  selector: 'app-solutionmanager',
  templateUrl: './solutionmanager.component.html',
  styleUrls: ['./solutionmanager.component.css']
})
export class SolutionmanagerComponent implements OnInit {
  solutionlistdata: boolean;
  approachlistdata: boolean;
  challengenames: any;
  challengenametaken: boolean;
  limitedSolutionKeypointList1: any;
  limitedKeypointList1: any;
  solutionNameSelected: any;
  currentsolutionid: any;
  currentsolutionname: any;
  enablesolutionEdit: boolean;
  enablesolutionEditIndex: any;
  soledit: boolean;
  approachNameSelected: any;
  currentapproachid: any;
  currentapproachname: any;
  appedit: boolean;
  enableapproachEdit: boolean;
  enableapproachEditIndex: any;

  constructor( private ChallengeService: ChallengeService,
      private router: Router,
     private tokenStorage:TokenStorageService,  
     private SpinnerService: NgxSpinnerService   
    ) { }

    showKeyModal : boolean;
    SolutionLinkId    : number;
    Link : string;
    keyComments:string;
    keypointId:number;
    solutionCategoryId:number;shareId:any;
    challenge = {

      challengeName: '',
      description:'',
      userId:0
    };
    solution={
      solutionName:'',
      challengeId:0,
      userId:0
    };
    approach={
      approachName:'',
      challengeId:0,
      userId:0
    };
    approachkeypoint = {
  
     keypoints: '',
     approachLinkId:0,
      approachId:0,
      userId:0
    };
    approachkeypoints = {
  
     keypoints: '',
     approachLinkId:0,
      approachId:0,
      userId:0
    };
    solutionkeypoint = {
      solutionkeypoint: '',
      solutionLinkId:0,
      solutionId:0,
      userId:0
    };
    keypoint = {
      solutionkeypoint: '',
      solutionLinkId:0,
      solutionId:0,
      userId:0
    };
   solutionlinks = {
    
      solutionlink: '',
      solution_id:0,
      userId:0
    };

    approachlinks = {
    
      approachlink: '',
      approach_id:0,
      userId:0
    };
    
    sharesolutionlink = {
      solutionId: 0,
      solutionlink: '',
      solutionLinkId:0,
      shareId:0,
      accesstype:'',
      sharedTime:Date(),
      userId:0
    };

    shareapproachlink = {
      approachId: 0,
      approachlink: '',
      approachLinkId:0,
      shareId:0,
      accesstype:'',
      sharedTime:Date(),
      userId:0
    };

    shareChallenge = {
      challengeName: '',
      challengeId:0,
      shareId:0,
      accessType:'',
      userId:0,
      sharedTime:Date()
    };
    challengeName:string;
    
    p: number = 1; 
    username: string;  
    errorMessage = '';
    searchlink:'';
    isdelete=false;
    sharedTime: number=Date.now();
    sucessmessage:any;
    isLoggedIn=false;
    isSuccessful=false;
    isgetGoal=false;
    challengelist:any=[];
    challengeNameselected: any = {};
    challengeNameselectedId: any = {};
    isgetChallenge=false;
    editField: string;
    enableEdit=false; 
    public currentchallengeid;
    public currentchallengename;
    enableEditIndex = null;
    hrefvisible=true;
    challengeId:number;
    solutionlist:any=[];
    approachlist:any=[];
    approacherrorMessage='';approachlistVisible=true;ApproachId:number;
    approachNameselectedId: any = {};approachisSuccessful=false;
    challengeisSuccessful=false;solutionerrorMessage='';
    solutionisSuccessful=false;challengeerrorMessage='';
    solutionlistVisible=true;showCollasalIndex=null;
    KeypointId:number;solutionkeypoints:string;
    solutionlink:string;
    solutionId:number;
    solutionNameselectedId: any = {};
    solutionlinkShareId:number;
    solutionlinkId:number;
    SolutionKeypointList:any=[];limitedSolutionKeypointList:any=[];
    solutionLinkselected: any = {};
    solutionLinkselectedId: any = {};
    solutionlinkisSuccessful=true;
    solutionlinklist:any=[];KeypointList:any=[];
    solutionlinklistVisible=true;solutionlinkerrorMessage ='';
    solutionlinkeditMode=false;
    limitedKeypointList:any=[];
    limitedKeypointList2:any=[];
    approachLinkId:number;
    approachlink:string;
    approachLinkselected: any = {};
    approachLinkselectedId: any = {};
    approachlinkisSuccessful=true;
    approachlinklist:any=[];
    approachlinklistVisible=true;approachlinkerrorMessage ='';
    approachlinkeditMode=false;
    iskeySuccessful=false;
    solutionKeypointId:number;
    showKeypointIndex:null;
    approachlinkId:0;showsolutionKeypointIndex:null;
    isgetKeyPoints=false;approachId:0;


    ngOnInit(): void {  
      this.challengeNameselected="Add CategoryLink";
      this.getChallenge();
      // this.getsolutionkeypoint(this.solutionlinkId);
      this.solutionlistVisible=false;
      this.approachlistVisible=false;
      this.solutionlinklistVisible=false;
      this.approachlinklistVisible=false;
      if(this.challengelist==[]){
        this.isgetChallenge=true;
        this.errorMessage="No Topics"
      } 
    }


  /**
   * 
   * 
   * Challenge Rel Functions
   * 
   */

  //Add Challenge
  AddTopic(f:NgForm){
    // this.SpinnerService.show();
    const data = {
        challengeName: this.challenge.challengeName,
        description: this.challenge.description,
        userId:this.tokenStorage.getUser().id
      };
    this.ChallengeService.addTopic(data).subscribe(
    data => {
        console.log(data);     
        this.sucessmessage=data;
        this.isLoggedIn = true;
        this.isSuccessful=true;
        f.resetForm();
        this.getChallenge();
        console.log("added");
      },
    err => {
          this.errorMessage = err.error.message;
          if(this.errorMessage.includes('Error:ChallengeManagername')){
            this.errorMessage = 'Challenge is already taken'
          }
          this.isSuccessful=false;
          });
  }

  //Get Challenge
  getChallenge(){
    this.ChallengeService
    .getChallenge(this.tokenStorage.getUser().id)
    .subscribe(
      data => { 
          this.challengelist= data;
          console.log("217", typeof this.challengelist);
          if(Object.keys(this.challengelist).length> 0){
            this.getChallengeid(this.challengelist[0],0);
          }
          else{
            this.challengeNameselected="Add CategoryLink";
          }
          setTimeout(() => {
              /** spinner ends after 5 seconds */
              this.SpinnerService.hide();
              },2000);
            },
        err => {
          this.challengelist = JSON.parse(err.error).message;
        });         
  }

  //Search Challenge OnChange event
  changetestValue( event: any){ 
      this.editField =  event.target.value ; 
      if(this.editField==""){
          this.getChallenge();
      }
      else{
          this.findByChallengeName(this.editField );
      }
  }

  //Get challenge Id onClick
  getChallengeid(challengelist: any, i: any){
    this.challengeNameselectedId=challengelist.challengeId;
    this.challengeNameselected = challengelist.challengeName;
    this.getChallengeSolution();
    this.getChallengeApproach();
  }

  //Enable challenge edit method  
  enableEditMethod(e, i){
   this.currentchallengeid = e.challengeId; 
   this.currentchallengename = e.challengeName; 
   this.isSuccessful=true; 
   this.enableEdit = true;
   this.enableEditIndex = i;
   this.hrefvisible=false;
   this.hrefvisible = i;
   this.challengenames = this.challengelist.map(item => item.challengeName)
  .filter((value, index, self) => self.indexOf(value) === index && value !==this.currentchallengename);
  }

  //Edit challenge
  editChallenge(challengeId:number,challengeName:String){
      if(this.challengenames.includes(challengeName)){
          this.challengenametaken = true;
          this.isSuccessful=false;
      }
      else{
        this.challengenametaken = false;
        this.enableEdit=true;
        const data = {
          challengeName: challengeName,
          userId:this.tokenStorage.getUser().id
        };
        this.ChallengeService.updatechallenge(challengeId,data).subscribe(
          data => {
            console.log(data);     
            this.sucessmessage=data;
            this.isLoggedIn = true;
            this.isSuccessful=true;
            this.getChallenge();
            this.enableEdit=false;
          },
          err => {
            this.errorMessage = err.error.message;
            this.isSuccessful=false;
          });
        }
  }

  //Show delete challenge model
  showdelchallengeModal(selectedItem:any):void {
    $("#mydelesolutionModal").modal('show');
    this.challengeId =selectedItem;// You get the Id of the selected item here
  }

  //Delete Challenge
  deleteChallenge() {
    this.ChallengeService.deleteChallenge(this.challengeId)
      .subscribe(
        data => {
          console.log("277",data);
          this.isdelete=true;
          this.reloadData();
        },
        error =>{
          console.log(error);
        });
        // this.hideModal();
  }

  //Hide modal
  hide(){
      this.showKeyModal = false;
  }

  //Reload
  reloadData() {
    this.challengelist = this.getChallenge();
    this.solutionlist = this.getChallengeSolution();
    this.approachlist = this.getChallengeApproach();
  }



  /**
   * 
   * Solution Rel Functions
   * 
   * 
   */

  //Add solution
  AddSolution(fl:NgForm){
    if(this.challengeNameselected=="Add solution") {  
        this.errorMessage="*Please Select Challenge";
      }
    else{
        const datalink = {
        solutionName: this.solution.solutionName,
        challengeId:this.challengeNameselectedId,
        userId:this.tokenStorage.getUser().id
        };
        this.ChallengeService.addSolution(datalink).subscribe(
          data => {  
            this.sucessmessage=data;
            this.isLoggedIn = true;
            this.isSuccessful=true;
            fl.resetForm();
            this.getChallengeSolution();
            },
          err => {
            this.errorMessage = '';
            this.challengeerrorMessage =err.error.message;
            this.challengeisSuccessful=false;
          });
        }  
  } 

  //Change value on click
  changeValue(id: number, property: string, event: any) {
    this.editField = event.target.textContent;
  }

  //solutionId
  getSolutionId(solution,i){
    this.solutionNameselectedId = solution.solutionId;
    this.solutionNameSelected = solution.solutionName;
  }

  //enable solution edit
  enablesolutionEditMethod(solution,id){
    this.currentsolutionid = solution.solutionId;
    this.currentsolutionname = solution.solutionName;
    this.soledit = true;
    this.enablesolutionEdit = true;
    this.enablesolutionEditIndex = id;
  }

  //edit solution
  editSolution(solutionid,solutionname){
    if(solutionname == ''){
      this.isSuccessful = false;
      this.soledit = false;
    }
    else{
      const data = {
        solutionName : solutionname,
        userId : this.tokenStorage.getUser().id
      };
      this.ChallengeService.updateSolution(solutionid,data)
          .subscribe(
            data =>{
              console.log(data);
              this.sucessmessage = data;
              this.isSuccessful = true;
              this.enablesolutionEdit = false;
              this.getChallengeSolution();
            },
            err => {
              this.errorMessage = err.error.message;
              this.isSuccessful = false;
            });
    }
  }

  //Get solutionlist
  getChallengeSolution(){
    this.solutionlist = [];
    this.ChallengeService
    .getSolution(this.challengeNameselectedId)
    .subscribe(
          data => {
            this.solutionlist= data;
            this.solutionlistdata = Object.keys(this.solutionlist).length===0;
            if(Object.keys(this.solutionlist).length!==0){
                    this.opentesttoggle(data[0],0);
              }
            this.solutionlistVisible=true;  
            },
          err => console.error(err), 
          () => console.log('getsolution completed') 
        );           
  }

  //show delete solution modal
  showdelSolutionModal(solution){
    $("mydelSolutionModal").modal('show');
    this.solutionId = solution.solutionId;
    console.log("454",solution);
  }

  //delete solution
  deletesolutions(){
    this.ChallengeService.deleteSolution(this.solutionId)
        .subscribe(
          data =>{
            console.log(data);
            this.isSuccessful = true;
            this.sucessmessage = data;
            document.getElementById('solution-modal').click();
            this.getChallengeSolution();
          },
          err =>{
            this.errorMessage = err.error.message;
            this.isSuccessful = false;
          }
        )
  }

  //Add Solution Link
  AddSolutionLink(solutionId,solutionlink,i,challengeId,ff:NgForm){
    console.log("406");
    const datalink = {
      solutionlink: solutionlink,
      solutionId:solutionId,
      challengeId:this.challengeNameselectedId,
      userId:this.tokenStorage.getUser().id
    };
    console.log("413");
    this.ChallengeService.addsolutionlink(datalink).subscribe(
        data => {
          this.solutionId =solutionId;
          ff.resetForm();
          this. getsolutionlink();
          this.getChallengeSolution();
          this.sucessmessage=data;
          this.isLoggedIn = true;
          this.solutionlinkisSuccessful=true;
        },
        err => {
          this.errorMessage='';
          this.solutionlinkerrorMessage =err.error.message.split(':')[1];
          this.solutionlinkisSuccessful=false;
        });
  }

  //Get Solution link
  getsolutionlink(){
    if(this.solutionId == undefined){}
    else{
      this.ChallengeService
      .getSolutionlink(this.solutionId)
      .subscribe(
            data => { 
              this.solutionlinklist= data;
              console.log("595",data); 
               localStorage.setItem("solutionlinkCount",this.solutionlinklist.length)
              // this.opencommentstoggle(data[0],0);
              this.solutionlinklistVisible=true;   
            },
            err => console.error(err), 
            () => console.log('getSolutionlink completed') 
            );
    }
  }

  //Select Solution keypoint
  onSelectSolutionKeypoint(selectedItem: any){
    this.iskeySuccessful=false;
    $("#mysolutionModal").modal('show');
    this.solutionkeypoints=selectedItem.solutionkeypoints;
    this.solutionlinkId =selectedItem.solutionlinkId;// You get the Id of the selected item here
    this.solutionId=selectedItem.solutionId;
    this.showKeyModal=true;
  }

  //Add Solution keypoints
  Addsolutionkeypoints(solutionlinkId,solutionId,fAddKey:NgForm){
    const dataKey = {
     keypoints: this.solutionkeypoint.solutionkeypoint,
     solutionlinkId:solutionlinkId,
     solutionId:solutionId,
     userId:this.tokenStorage.getUser().id,
     challengeId: this.challengeNameselectedId
   };
     this.ChallengeService.addsolutionkeypoints(dataKey).subscribe(
       data => {
         console.log(data);     
         this.sucessmessage="Comments Added";
         this.isLoggedIn = true;
         this.iskeySuccessful=true;
         fAddKey.resetForm();
         document.getElementById('keyclose').click();
         this.getsolutionkeypoint(this.solutionlinkId);
       },
       err => {
         this.errorMessage = err.error.message;
         this.iskeySuccessful=false;
       });
  }

  //Show Solution Model
  showSolutionShareModal(selectedItem:any):void {
    $("#solutionshareModal").modal('show');
    this.solutionlink =selectedItem.solutionlink;  
    this.solutionlinkId =selectedItem.solutionlinkId;// You get the Id of the selected item here
    this.solutionId=selectedItem.solutionId;
    this.isSuccessful = false;
  }

  //OnChange Access Type
  onchangeAccessType(val: any) {
    this.sharesolutionlink.accesstype=val;
  }

  //Share Solution link
  shareSolution(solutionlinkId,fshare: NgForm){
    if(!this.sharesolutionlink.accesstype){
      this.isSuccessful = false;
    }
    else{
      this.solutionlinkId=solutionlinkId;
      const datalink = {
        solutionId:this.solutionId,
        solutionlinkId:solutionlinkId,
        solutionlink:this.solutionlink,
        userId:this.tokenStorage.getUser().id,
        accesstype:this.sharesolutionlink.accesstype, 
        sharedTime:new Date().toDateString()
      };
      this.ChallengeService.addsharesolution(datalink).subscribe(
      (data: any) => {
          console.log(data);
          document.getElementById('solclose').click();
          fshare.resetForm()
          this.sharesolutionlink.accesstype='';
          this.isSuccessful = true;
          this.sucessmessage = data;
        },
      (err: any) => {
        this.isSuccessful = false;
        });
    }
  }

  //Show Solution link delete modal
  showsolutionModal(selectedItem:any):void {
    $("#mydelsolutionModal").modal('show');
    this.solutionlink =selectedItem.solutionlink;    
    this.solutionlinkId =selectedItem.solutionlinkId;// You get the Id of the selected item here
    console.log("Selected item Id: ", this.solutionlinkId); 
  }

  //Delete Solution link
  deleteSolutionlink() {
    this.ChallengeService
    .deleteSolutionLink(this.solutionlinkId)
    .subscribe(
        data => {
          console.log(data);
          this.isdelete=true;
          this.reloadData();
          this.sucessmessage="Link is deleted";
          this.solutionlinklist= this.getsolutionlink();
        },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful=false;
      });
  }

  //Open comments toggle
  opencommentstoggle(selectedItem:any,i){    
     this.showsolutionKeypointIndex=i;
     this.solutionlinkId =selectedItem.solutionlinkId;
     this.getsolutionkeypoint(this.solutionlinkId);
  }

  //Close comments toggle
  closecommentstoggle(selectedItem:any,i){    
    this.showsolutionKeypointIndex=null;
    this.solutionlinkId =selectedItem.solutionlinkId;
    // this.getsolutionkeypoint(this.solutionlinkId);
  }

  //Get Solution keypoints
  getsolutionkeypoint(solutionlinkId:number){
    this.SolutionKeypointList= null;
    this.limitedSolutionKeypointList =null;
    this.ChallengeService.getsolutionkeypoint(solutionlinkId)
    .subscribe(
          data => { 
            this.SolutionKeypointList= data;
            this.limitedSolutionKeypointList = this.SolutionKeypointList.slice(0,3);
           },
          err => {
            return console.error(err.error.message);
          });
  }

  //Show keypoints Modal
  showEditKeyModal(selectedItem:any):void{
    $("#myEditSolutionKeyPointModal").modal('show');
    this.solutionlinkId =selectedItem.solutionlinkId;
    this.keypoint.solutionkeypoint =selectedItem.keypoints; 
    this.keypointId=selectedItem.keypointId;
    this.solutionId=selectedItem.solutionId;
    this.showKeyModal=true;
    console.log("556",selectedItem);
  }

  //Edit keypoints
  EditKeypoint(keypoints:String){
    console.log("581", keypoints);
    const dataKey = {
          keypoints: keypoints,
          solutionlinkId:this.solutionlinkId,
          solutionId:this.solutionId,
          userId:this.tokenStorage.getUser().id 
        };
    this.ChallengeService.updateKeypoint(this.keypointId,dataKey).subscribe(
    data => {
        console.log(data);     
        this.sucessmessage="Comments  Edited";
        this.iskeySuccessful=true;
        this.hideModal();
        this.getsolutionkeypoint(this.solutionlinkId);
      },
    err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful=false;
      });   
  }

  //Prevent Keypoints
  prevent(keypoints:string){
   event.preventDefault();
   this.EditKeypoint(keypoints);
  }

  //Show del keypoint modal
  showdelKeyModal(selectedItem:any):void {
    $("#mydelKeyModal").modal('show');
    console.log("613", selectedItem);
    this.keypoint.solutionkeypoint =selectedItem.keypoint;  
    this.keypointId=selectedItem.keypointId;
    this.solutionlinkId =selectedItem.solutionlinkId;// You get the Id of the selected item here
  }

  //Delete keypoint
  deleteKeypoint(){
    this.ChallengeService.deleteKeypoint(this.keypointId)
      .subscribe(
        data => {
          console.log(data);
          this.getsolutionkeypoint(this.solutionlinkId);
        },
        error => console.log(error));
  }

  //View more solution keypoints
  onSolKeyPointsViewMore():void{
    this.limitedSolutionKeypointList1 = this.SolutionKeypointList.slice(3, this.SolutionKeypointList.length);
    this.limitedSolutionKeypointList = this.limitedSolutionKeypointList.concat(this.limitedSolutionKeypointList1);
    $('#view-more').hide();
    $('#view-less').show();
  }

  //View less solution keypoints
  onSolkeyPointsviewless(): void {
    this.limitedSolutionKeypointList = this.SolutionKeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }

  /**
   * 
   * Approach rel Functions
   * 
   * 
   */

  //Add Approach
  AddApproach(f2:NgForm) {
    if(this.challengeNameselected=="Add approach") {   
        this.errorMessage="*Please Select Challenge";
    }
    else{
      const datalink = {
          approachName: this.approach.approachName,
          challengeId:this.challengeNameselectedId,
          userId:this.tokenStorage.getUser().id
          };
      this.ChallengeService.addApproach(datalink).subscribe(
      data => {
        console.log("296",data);     
        this.sucessmessage=data;
        this.isLoggedIn = true;
        this.isSuccessful=true;
        f2.resetForm();
        this.approachisSuccessful=true;
        this.getChallengeApproach();
        console.log("added");
        },
      err => {
        this.errorMessage = '';
        this.challengeerrorMessage =err.error.message;
        this.challengeisSuccessful=false;
        });
      }
  }
         
  
  //Get Approach list
  getChallengeApproach(){
    this.ChallengeService
    .getApproach(this.challengeNameselectedId)
    .subscribe(
        data => { 
          this.approachlist= data;
          this.approachlistdata = Object.keys(this.approachlist).length===0;
          console.log(data);
          if(Object.keys(this.approachlist).length!==0){
              this.opentesttoggle(data[0],0);
            }
          this.approachlistVisible=true;  
          },
        err => console.error(err), 
        () => console.log('getapproach completed') 
      );           
  }


  //get approach id
  getApproachId(approach,i){
    this.approachNameselectedId = approach.approachId;
    this.approachNameSelected = approach.approachName;
  }

  //Enable approach edit
  enableapproachEditMethod(approach,id){
    this.currentapproachid = approach.approachId;
    this.currentapproachname = approach.approachName;
    this.appedit = true;
    this.enableapproachEdit = true;
    this.enableapproachEditIndex = id;
  }

  //Edit approach
  editApproach(approachid,approachname){
    if(approachname == ''){
      this.isSuccessful = false;
      this.appedit = false;
    }
    else{
      const data = {
        approachName : approachname,
        userId : this.tokenStorage.getUser().id
      };
      this.ChallengeService.updateApproach(approachid,data)
          .subscribe(
            data =>{
              console.log(data);
              this.sucessmessage = data;
              this.isSuccessful = true;
              this.enableapproachEdit = false;
              this.getChallengeApproach();
            },
            err =>{
              this.errorMessage = err.error.message;
              this.isSuccessful = false;
            });
    }
  }

  //show delete approach modal
  showdelapproachModal(approach){
    $("mydelapproachModal").modal('show');
    this.approachId = approach.approachId;
    console.log("844",approach);
  }

  //delete approach
  deleteapproach(){
    this.ChallengeService.deleteApproach(this.approachId)
        .subscribe(
          data =>{
            console.log(data);
            this.isSuccessful = true;
            this.sucessmessage = data;
            document.getElementById('approach-modal').click();
            this.getChallengeApproach();
          },
          err =>{
            this.errorMessage = err.error.message;
            this.isSuccessful = false;
          });
  }


  //Add Approach link
  AddApproachLink(approachId,approachlink,i,challengeId,ff1:NgForm){
      const datalink = {
        approachlink:approachlink,
        approachId:approachId,
        challengeId: this.challengeNameselectedId,
        userId:this.tokenStorage.getUser().id
      };
    this.ChallengeService.addapproachlink(datalink).subscribe(
      data => {
       this.showCollasalIndex=i;
       this.ApproachId=approachId;
       this.getChallengeApproach();
       console.log(data);     
       this.sucessmessage=data;
       this.isLoggedIn = true;
       this.approachlinkisSuccessful=true;
       this.getApproachlink();
      },
      err => {
       this.errorMessage='';
       this.approachlinkerrorMessage =err.error.message;
       this.approachlinkisSuccessful=false;
      });
  }

  //On select keypoint modal
  onSelectedKeypoint(selectedItem: any) {
    this.iskeySuccessful=false;
    $("#myapproachModal").modal('show');
    this.keyComments =selectedItem.keypoints; 
    console.log("Selected item Id: ", selectedItem.Id); 
    this.approachlinkId =selectedItem.approachlinkId;// You get the Id of the selected item here
    this.approachId=selectedItem.approachId
    this.showKeyModal=true;
    console.log("703",selectedItem);
  }

  //Add Approach keypoints
  Addapproachkeypoints (fAddkey1:NgForm){
        const dataKey = {
          keypoints: this.approachkeypoint.keypoints,
          approachlinkId:this.approachlinkId,
          approachId:this.approachId,
          userId:this.tokenStorage.getUser().id,
          challengeId: this.challengeNameselectedId
        };
        console.log("714", dataKey);
        this.ChallengeService.addapproachkeypoints(dataKey).subscribe(
        data => {
          console.log(data);     
          this.sucessmessage="Comments  Added";
          this.isLoggedIn = true;
          this.iskeySuccessful=true;
          fAddkey1.resetForm();
          this.getapproachkeypoint(this.approachlinkId);
        },
        err => {
          this.errorMessage = err.error.message;
          this.iskeySuccessful=false;
        });
  }

  //Get Approach Keypoint
  getapproachkeypoint(approachlinkId:number){
    this.KeypointList=null;
    this.limitedKeypointList=null;
    this.ChallengeService
    .getapproachkeypoint(approachlinkId)
    .subscribe(
          data => { 
            this.KeypointList= data;
            console.log("keypoints",this.KeypointList);
            this.limitedKeypointList = this.KeypointList.slice(0,3)
            console.log("sliced data", this.limitedKeypointList);       
          },
          err => {
            return console.error(err.error.message);
          });
  }

  //Open Share modal
  showApproachSharedModal(selectedItem:any):void {
    this.isSuccessful = false;
    $("#approachshareModal").modal('show');
    this.approachlinks =selectedItem.approachlink;  
    // console.log("Selected item Id: ", selectedItem.Id); 
    this.approachlinkId =selectedItem.approachlinkId;// You get the Id of the selected item here
    this.ApproachId=selectedItem.approachId;
  }

  //Share Approach
  shareApproach(approachlinkId,fshare: NgForm){
    console.log("810", this.shareapproachlink.accesstype);
    if(!this.shareapproachlink.accesstype){
      console.log("812", this.shareapproachlink.accesstype);
      this.isSuccessful = false;
    }else{
      console.log("814", this.shareapproachlink.accesstype);
      const datalink = {
        approachId:this.ApproachId,
        approachlinkId:approachlinkId,
        approachlink:this.approachlinks,
        userId:this.tokenStorage.getUser().id,
        accesstype:this.shareapproachlink.accesstype,
        sharedTime:new Date().toDateString()
      };
      this.ChallengeService.addshareapproach(datalink).subscribe(
      (data: any) => {
        console.log(data);
        document.getElementById('appclose').click();
        fshare.resetForm();
        this.shareapproachlink.accesstype='';
        this.sucessmessage = data;
        this.isSuccessful = true;
      },
      (err: any) => {
        this.isSuccessful = false;
      });
    }
  }

  //on change
  onchangeAccessTypes(val: any) {
    console.log("841", this.shareapproachlink.accesstype);
    this.shareapproachlink.accesstype=val;
    console.log("843", this.shareapproachlink.accesstype);
  }

  //Show del modal
  showedModal(selectedItem:any):void {
    $("#myapproachdelModal").modal('show');
    this.approachlink =selectedItem.approachlink;   
    console.log("Selected item Id: ", selectedItem.Id); 
    this.approachlinkId =selectedItem.approachlinkId;// You get the Id of the selected item here
  }

  //Delete Approach link
  deleteApproachlink(){
    this.ChallengeService.deleteApproachlink(this.approachlinkId)
      .subscribe(
        data => {
          console.log(data);
         this. isdelete=true;
          this.sucessmessage="Link is deleted";
          this.getApproachlink();
          this.reloadData();
        },
     
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful=false;
      });
  }

  //Get Approach link
  getApproachlink(){
    if(this.approachId == undefined){}
    else{
      this.ChallengeService.getApproachlink(this.approachId)
      .subscribe(
          data => { 
            this.approachlinklist= data 
            console.log(data); 
            // this.opentesttoggle(data[0],0); 
            this.approachlinklistVisible=true;     
          }, 
          err => console.error(err), 
          () => console.log('getApproachlink completed') 
        );
    } 
  }

  //Open keypoints
  openedcommentstoggle(selectedItem:any,i){
   this.showKeypointIndex=i;
   this.approachlinkId =selectedItem.approachlinkId;
   this.getapproachkeypoint(this.approachlinkId);
  }

  //Close Keypoints toggle
  closedcommentstoggle(selectedItem:any,i){
    this.showKeypointIndex=null;
    this.approachlinkId =selectedItem.approachlinkId;
    // this.getapproachkeypoint(this.approachlinkId );
  }

  //Showed edit keypoint 
  showedEditKeyModal(selectedItem:any):void{
    $("#myEditKeyPointModal").modal('show');
    this.approachlinkId =selectedItem.approachlinkId;
    this.approachkeypoints.keypoints =selectedItem.keypoints; 
    this.keypointId=selectedItem.keypointId;
    this.ApproachId=selectedItem.approachId;
    this.showKeyModal=true;
    console.log("869", selectedItem, this.approachkeypoint);
  }

  //Prevent
  prevents(keypoints:string){
    event.preventDefault();
    this.EditedKeypoint(keypoints);
   }

  //Edit Keypoint
  EditedKeypoint(keypoints:String){
    const dataKey = {
      keypoints: keypoints,   
      approachlinkId:this.approachlinkId,
      approachId:this.ApproachId,
      userId:this.tokenStorage.getUser().id     
     };
    this.ChallengeService.updateKeypoints(this.keypointId,dataKey).subscribe(
    data => {
      console.log(data);     
      this.sucessmessage="Comments  Edited";     
      this.iskeySuccessful=true;
      this.hideModal();
      this.getapproachkeypoint(this.approachlinkId);     
    },
    err => {
      console.log("861",err);
      this.errorMessage = err.error.message;
      this.iskeySuccessful=false;
    });
  }
  
  //Show del key model
  showeddelKeyModal(selectedItem:any):void {   
    $("#mydelKeyModal").modal('show');
    this.keypoint =selectedItem.keypoint;  
    this.keypointId=selectedItem.keypointId;
    this.approachlinkId =selectedItem.approachlinkId;// You get the Id of the selected item here
    console.log("889", selectedItem);
  }

  //Delete keypoint 
  deletedKeypoint(){
    this.ChallengeService.deleteKeypoints(this.keypointId)
      .subscribe(
      data => {
        console.log(data);
        this.getapproachkeypoint(this.approachlinkId);
      },
      error => console.log(error));
  }

  //On Approach keypoints view more
  onAppKeyPointsViewMore(){
    this.limitedKeypointList1 = this.KeypointList.slice(3, this.KeypointList.length);
    this.limitedKeypointList = this.limitedKeypointList.concat(this.limitedKeypointList1);
    $('#view-more').hide();
    $('#view-less').show();
  }

  //On Approach keypoint view less
  onAppkeyPointsviewless(){
    this.limitedKeypointList = this.KeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }







  buttonCol(selectedItem: any){
    this.SpinnerService.show();
    this.SolutionLinkId =selectedItem.solutionLinkId;// You get the Id of the selected item here
    this. getsolutionkeypoint(this.solutionlinkId);
  }
  openNav(selectedItem: any) {
      //  document.getElementById("collapseExample").style.width = "250px";
      this.SolutionLinkId =selectedItem.solutionLinkId;// You get the Id of the selected item here
      this. getsolutionkeypoint(this.solutionlinkId);
  }
  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }  
  opentoggle(id: number, property: string, event: any){
    $("#slideme").slideToggle();
  }            
  findByChallengeName(key)
            {
              this.isSuccessful=true; 
              this.enableEdit = false;
              
              this.ChallengeService.findByChallengeName(key,
                this.tokenStorage.getUser().id)
             
              .subscribe(
                    data => { 
                      this.challengelist= data  
                      console.log("list",data);
            
                     },
                     err => {
                      this.challengelist = JSON.parse(err.error).message;
                    }       
                    );
  }
  showChallengeShareModal(selectedItem:any):void {
      $("#shareModal").modal('show');
      this.challengeName =selectedItem.challengeName;   
      console.log("Selected item Id: ", selectedItem.challengeId); 
      this.challengeId =selectedItem.challengeId;// You get the Id of the selected item here       
  }
  onchangeShareAccessType(val: any) {          
      this.shareChallenge.accessType=val;          
  }          
  shareChallengeName(challengeId:number){
      this.challengeId=challengeId;       
      const datalink = {
          challengeId:challengeId,
          challengeName:this.challengeName,
          userId:this.tokenStorage.getUser().id,
          accesstype:this.shareChallenge.accessType,
          sharedTime:new Date().toDateString()  
        };
      this.ChallengeService.addshareChallenge(datalink).subscribe(
      (data: any) => {
          console.log(data);
          this.getChallenge();
          },
      (err: any) => {      
          }); 
  } 
  deletesolution(){
      $("#mydelsolutionModal").modal('show');
      // this.ChallengeService.deleteChallenge(this.challengeId)
      this.ChallengeService.deleteSolutionLink(this.challengeId)
      .subscribe(
          data => {
            console.log(data);
            //this.isdelete=true;
            this.reloadData(); 
            this.hideModal();
            //   $("#mydelsolutionModal").modal("hide");
            $("#mydelsolutionModal").modal('hide');
            //this.getChallenge();
          },
          error => {
            console.log(error)
            //this.hideModal();
            this.deleteSolutionlinkByCategory(this.challengeId);
            this.getChallenge();
          });
          this.hideModal();
  }
  showdelChallengeModal(selectedItem:any):void {   
    $("#mydelChallengeModal").modal('show');
    console.log("Selected item Id: ", selectedItem.Id); 
    this.challengeId =selectedItem.challengeId;// You get the Id of the selected item here
  }
  deleteSolutionlinkByCategory(challengeId){
    this.ChallengeService.deleteSolutionlinkByCategory(challengeId)
    .subscribe(
      data => {
        console.log(data);
        this. isdelete=true;
        this.sucessmessage="Link is deleted";
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful=false;
      });
  }
  hideModal():void {
    document.getElementById('close-modal').click();
  }
  linkisSuccessful=false;linkerrorMessage='';
  opentesttoggle(selectedItem:any,i){
      this.showCollasalIndex=i;
      this.solutionId =selectedItem.solutionId;
      this.getsolutionlink();
  }
  onKeyPointsViewMore(): void{
    this.limitedKeypointList2 = this.SolutionKeypointList.slice(3, this.SolutionKeypointList.length);
    this.limitedSolutionKeypointList = this.limitedSolutionKeypointList.concat(this.limitedKeypointList2);
    $('#view-more').hide();
    $('#view-less').show();     
  }
  onkeyPointsviewless(): void {
      this.limitedSolutionKeypointList = this.SolutionKeypointList.slice(0,3);
      $('#view-more').show();
      $('#view-less').hide();  
  }
  
  
  
  
  
  
  findBySolutionLink(){
   this.router.navigate(['searchviewpage'], { queryParams: { link: this.searchlink } });
  }
  editSolutionLink(solutionLinkId:number,Link:String){
      const datalink = {
        link: this.solutionlinks.solutionlink,
        solutionCategoryId:this.challengeNameselectedId,
        userId:this.tokenStorage.getUser().id
      };
      this.ChallengeService.updatesolutionLink(solutionLinkId,datalink).subscribe(
          data => {
            console.log(data);     
            this.sucessmessage=data;
            this.isLoggedIn = true;
            this.isSuccessful=true;
            //this.getsolutionlink();
            this.enableEdit=false;
          },
          err => {
            this.errorMessage = err.error.message;
            this.isSuccessful=false;
          });
  }
  updateList(id: number, property: string, event: any) {
      const editField = event.target.textContent;
      this.solutionlinklist[id][property] = editField;
      const datalink = {
        link: this. editField,
        solutionCategoryId:this.solutionlinklist[id].solutionCategoryId,
        userId:this.tokenStorage.getUser().id
      };
      this.ChallengeService.updatesolutionLink(this.solutionlinklist[id].solutionLinkId,datalink).subscribe(
      data => {
        console.log(data);
      },
      err => {
      });
  }
}



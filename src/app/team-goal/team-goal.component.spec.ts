import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamGoalComponent } from './team-goal.component';

describe('TeamGoalComponent', () => {
  let component: TeamGoalComponent;
  let fixture: ComponentFixture<TeamGoalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamGoalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamGoalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});


import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
import { TeamService } from '../_services/team.service';
import { NewteamComponent } from '../newteam/newteam.component';
import { FormBuilder, NgForm, Validators } from "@angular/forms";
import { TeamGoalService } from '../_services/teamGoal.service';

declare var $: any;

@Component({
  selector: 'app-team-goal',
  templateUrl: './team-goal.component.html',
  styleUrls: ['./team-goal.component.css']
})
export class TeamGoalComponent implements OnInit {
  skillerrorMessage: any;
  limitskillkeypoint: any;
  limitskillkeypoint1: any;
  limitstepkeypoint: any;
  limitstepkeypoint1: any;

  constructor(private teamService: TeamService,
    private teamGoalService: TeamGoalService,
    private router: Router, public fb: FormBuilder,
    private tokenStorage: TokenStorageService) { }

  teamBookmarkForm = this.fb.group({
    teamName: ['', [Validators.required]]
  })

  team = {

    teamName: ''
  }
  goallist: any = [];
  teamlist: any = [];
  goalNameselectedId: number;
  goalNameselected: String = 'Goal Name';
  isSubmitted = false;
  editField: string;
  enableEdit = false;
  enableEditIndex = null;
  isSuccessful = false;
  public selectedteamGoalid;
  public selectedteamGoalname;
  errorMessage = ''; teamgoalName: string;
  sucessmessage: any;
  isLoggedIn = false;
  hrefvisible = true;
  teamgoalId: number;
  Isdelete = false;
  p: number = 1;
  p1: number = 1;
  goalisSuccessful = false;
  goalerrorMessage = '';
  steperrorMessage = '';
  stepisSuccessful = false; skillisSuccessful = false;
  steplistVisible = true; skilllistVisible = true;
  steplist: any = []; skilllist: any = [];
  showCollasalIndex = null;
  teamstepId: number; teamskillId: number;
  skillId: number;
  linklist: any = []; skilllinklist: any = [];
  steplinklistVisible = true; skilllinklistVisible = true;
  linkisSuccessful = false;
  showKeyModal: boolean;
  teamsteplinkId: number; teamskilllinkId: number;
  teamId: number;
  iskeySuccessful = false;
  KeypointList: any = []; skillKeypointList: any = [];
  limitedKeypointList: any = [];
  teamstepkeypointId: number; teamskillkeypointId: number;
  isdelete = false;
  teamsteplink: ''; teamskilllink: '';
  showKeypointIndex = null;
  teamstepKeypoints: ''; teamskillKeypoints: '';
  isgetGoal = false;
  enablestepEdit = false;
  enablestepEditIndex = null;
  currentstepid;
  currentstepname;
  currentstepgoalId;
  stepkeytoggle: boolean = false;
  skillkeytoggle: boolean = false;

  enableskillEdit = false;
  enableskillEditIndex = null;
  public currentskillid;
  public currentskillname;

  teamgoal = {
    teamId: 0,
    teamgoalName: '',
    description: '',
    userId: ''
  };

  step = {
    teamId: 0,
    teamgoalId: 0,
    userId: 0,
    teamstepName: ''
  };

  steplink = {
    teamsteplink: '',
    teamId: 0,
    userId: 0,
    teamstepId: 0
  };

  stepkeypoint = {
    teamId: 0,
    teamstepKeypoints: '',
    teamsteplinkId: 0,
    teamstepId: 0,
    userId: 0
  }

  sharesteplink = {
    teamstepId: 0,
    teamsteplinkId: 0,
    teamsteplink: '',
    userId: 0,
    accesstype: null,
    sharedTime: new Date(),
    teamgoalId: 0
  }

  skill = {
    teamId: 0,
    teamgoalId: 0,
    userId: 0,
    teamskillName: ''
  };

  skilllink = {
    teamskilllink: '',
    teamId: 0,
    userId: 0,
    teamskillId: 0
  }

  skillkeypoint = {
    teamId: 0,
    teamskillKeypoints: '',
    teamskilllinkId: 0,
    teamskillId: 0,
    userId: 0
  }
  skillkeypoints = {
    teamId: 0,
    teamskillKeypoints: '',
    teamskilllinkId: 0,
    teamskillId: 0,
    userId: 0
  }

  shareskilllink = {
    teamskillId: 0,
    teamskilllinkId: 0,
    teamskilllink: '',
    userId: 0,
    accesstype: '',
    sharedTime: new Date(),
    teamgoalId: 0
  }

  shareGoal = {
    goalName: '',
    goalId: 0,
    shareId: 0,
    accessType: '',
    userId: 0,
    sharedTime: Date()
  };

  ngOnInit(): void {
    this.getTeam();
    this.steplistVisible = false;
    if (this.goallist == []) {
      this.isgetGoal = true;
      this.errorMessage = "No Goals"
    }
  }

  /**
   * 
   * Common Functions
   * 
   */

  hide() {
    this.showKeyModal = false;
    this.sucessmessage = "";
    this.iskeySuccessful = false;
  }


  /**
   * 
   * Team Related Functions
   * 
   */
  //Get Teams
  getTeam() {
    this.teamService
      .getTeamList(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.teamlist = data;
        },
        err => {
          this.teamlist = JSON.parse(err.error).message;
        });
  }

  //Team selection
  onchange(val: any) {
    this.team.teamName = val;
    this.getGoal();
  }

  //Invite members
  AddInviteToTeam() {
    this.isSubmitted = true;
    if (!this.team.teamName) {
      return false;
    } else {
      this.router.navigate(['newteam'], { queryParams: { teamId: this.team.teamName } });
    }
  }

  /**
   * 
   * Goal Related Functions 
   * 
   **/

  //Goal Adding
  AddGoal(f: NgForm) {
    const data = {
      teamgoalName: this.teamgoal.teamgoalName,
      userId: this.tokenStorage.getUser().id,
      teamId: this.team.teamName,
      description: this.teamgoal.description
    };
    this.teamGoalService.addGoal(data).subscribe(
      data => {
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        f.resetForm();
        this.getGoal();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      });
  }

  //Get all the goals
  getGoal() {
    this.teamGoalService
      .getGoal(this.tokenStorage.getUser().id, this.team.teamName)
      .subscribe(
        data => {
          this.goallist = data;
          if (this.goallist.length > 0) {
            this.getGoalid(this.goallist[0], 0);
          }
        },
        err => {
          this.goallist = err.error.message;
        });
  }

  //Get steps and skills
  getGoalid(topiclist: any, i: any) {
    this.goalNameselectedId = topiclist.teamgoalId;
    this.goalNameselected = topiclist.teamgoalName;
    this.getGoalStep();
    this.getskill();
    // this.getstepLink(this.teamstepId);
  }

  //Search goal
  changetestValue(event: any) {
    this.editField = event.target.value;
    if (this.editField === "") {
      this.getGoal();
    }
    else {
      this.findByGoalName(this.editField);
    }
  }

  //Find Goal by name
  findByGoalName(key) {
    this.isSuccessful = true;
    this.enableEdit = false;
    this.teamGoalService
      .findByGoalName(key, this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.goallist = data;
        },
        err => {
          this.goallist = err.error.message;
        });
  }

  //Enable goal edit
  enableEditMethod(e, i) {
    this.selectedteamGoalid = e.teamTopicId;
    this.selectedteamGoalname = e.teamTopicName;
    this.isSuccessful = true;
    this.enableEdit = true;
    this.enableEditIndex = i;
    this.hrefvisible = false;
    this.hrefvisible = i;
  }

  //Edit Goal Name
  editGoal(teamgoalId: number, teamgoalName: String) {
    if (teamgoalName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableEdit = true;
      const data = {
        teamgoalName: teamgoalName, teamId: this.team.teamName,
        userId: this.tokenStorage.getUser().id
      };
      this.teamGoalService
        .updateGoal(teamgoalId, data)
        .subscribe(
          data => {
            this.sucessmessage = data;
            this.isLoggedIn = true;
            this.isSuccessful = true;
            this.getGoal();
            this.enableEdit = false;
          },
          err => {
            console.log("348", err);
            this.errorMessage = err.error.message;
            this.isSuccessful = false;
          });
    }
  }

  //Get Goal Id to share
  showGoalShareModal(selectedItem: any): void {
    $("#shareModal").modal('show');
    this.teamgoalName = selectedItem.teamgoalName;
    this.teamgoalId = selectedItem.teamgoalId;// You get the Id of the selected item here
  }

  //Share the Goal
  shareGoalName(teamgoalId: number,) {
    this.teamgoalId = teamgoalId;
    const datalink = {
      teamgoalId: teamgoalId,
      teamgoalName: this.teamgoalName,
      userId: this.tokenStorage.getUser().id,
      accesstype: this.shareGoal.accessType,
      teamId: this.teamId,
      sharedTime: new Date().toDateString()
    };
    this.teamGoalService
      .addshareGoal(datalink)
      .subscribe(
        (data: any) => {
          this.getGoal();
        },
        (err: any) => {
          console.log(err);
        });
  }

  //Get Goal Id to Delete
  showdelGoalModal(selectedItem: any): void {
    $("#mydelGoalModal").modal('show');
    this.teamgoalId = selectedItem.teamgoalId;// You get the Id of the selected item here
  }
  showdelSkillModal(skill){
    $("mydelSkillModal").modal('show');
    this.teamskillId = skill.teamskillId;
  }
  deleteskill(){
    this.teamGoalService.deleteSkill(this.teamskillId)
        .subscribe(
          data =>{
            console.log(data);
            this.isSuccessful = true;
            this.sucessmessage = "skill is deleted";
            document.getElementById('skill-modal').click();
            this.getskill();
          },
          err =>{
            this.errorMessage = err.error.message;
            this.isSuccessful = false;
          }
        )
  }


  //Delete the Goal
  deleteGoal(teamgoalId) {
    this.teamGoalService.deleteGoal(this.teamgoalId)
      .subscribe(
        data => {
          this.goalNameselected = 'Goal Name';
          this.reloadData();
        },
        error => console.log(error));
    this.hideModal();
    //  this.getbookmarklink();
  }
  editSkill(skillId: number, skillName: String) {
    if (skillName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableskillEdit = true;
      const data = {
        teamskillName: skillName,
        userId: this.tokenStorage.getUser().id
      };

      this.teamGoalService.updateskill(skillId, data).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getskill();
          this.enableskillEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );

    }
  }
  enableskillEditMethod(s, id) {

    this.currentskillid = s.teamskillId;
    this.currentskillname = s.teamskillName;
    this.isSuccessful = true;
    this.enableskillEdit = true;
    this.enableskillEditIndex = s.teamskillId;
   

  }

  /**
   * 
   * Common functions for steps and skills
   * 
   */

  //Get data of step/skill to display
  changeValue(id: number, property: string, event: any) {
    this.editField = event.target.textContent;
  }


  /**
   * 
   * Step Related Functions 
   * 
   **/

  //Add the Step
  AddStep(fl: NgForm) {
    if (this.goalNameselected == "Goal Name") {
      this.errorMessage = "*Please Select Goal";
    }
    else {
      const datastep = {
        teamstepName: this.step.teamstepName,
        teamgoalId: this.goalNameselectedId,
        teamId: this.team.teamName,
        userId: this.tokenStorage.getUser().id
      };
      this.teamGoalService
        .addgoalStep(datastep)
        .subscribe(
          data => {
            this.sucessmessage = data;
            this.isLoggedIn = true;
            this.stepisSuccessful = true;
            fl.resetForm();
            this.getGoalStep();
            this.errorMessage = '';
          },
          err => {
            this.errorMessage = '';
            this.goalerrorMessage = err.error.message;
            this.stepisSuccessful = false;
          });
    }
  }

  //Get Steps for goal
  getGoalStep() {
    this.KeypointList = [];
    this.teamGoalService
      .getgoalStep(this.goalNameselectedId)
      .subscribe(
        data => {
          this.steplist = data;
          // this.opentesttoggle(data[0],0); 
          this.steplistVisible = true;
          this.stepkeytoggle = !this.stepkeytoggle;
        },
        err => console.error(err),
        () => console.log('getstep completed'))
  }

  //Delete step using goal id
  deleteStepByGoal(teamgoalId) {
    this.teamGoalService
      .deleteStepByGoal(teamgoalId)
      .subscribe(
        data => {
          this.Isdelete = true;
          this.sucessmessage = "Link is deleted";
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        });
  }

  //Add Step link
  AddstepLink(teamstepId, teamsteplink, id, teamId, ff: NgForm) {
    const data = {
      teamsteplink: teamsteplink,
      teamstepId: teamstepId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id,
      teamgoalId: this.goalNameselectedId
    };
    this.teamGoalService
      .addstepLink(data)
      .subscribe(
        data => {
          this.showCollasalIndex = id;
          this.teamstepId = teamstepId;
          ff.resetForm();
          this.getGoalStep();
          this.getstepLink(teamstepId);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.linkisSuccessful = true;
        },
        err => {
          this.steperrorMessage = err.error.message;
          this.linkisSuccessful = false;
        });
  }

  //Get step link using teamstepid
  getstepLink(teamstepId: number) {
    this.teamGoalService
      .getstepLink(teamstepId)
      .subscribe(
        data => {
          this.linklist = data
          this.steplinklistVisible = true;
        },
        err => console.error(err),
        () => console.log('getsteplink completed'));
  }

  //Get step link Id for keypoints
  onSelectKeypoint(selectedItem: any) {
    $("#myModal").modal('show');
    // this.keypoints =selectedItem.keypoints; 
    this.teamsteplinkId = selectedItem.teamsteplinkId;// You get the Id of the selected item here
    this.teamstepId = selectedItem.teamstepId;
    this.teamId = selectedItem.teamId;
    this.showKeyModal = true;
    this.iskeySuccessful = false;
  }

  //Add keypoints for step link
  AddstepKeypoint(fAddKey: NgForm) {
    const dataKey = {
      teamstepKeypoints: this.stepkeypoint.teamstepKeypoints,
      teamsteplinkId: this.teamsteplinkId,
      teamstepId: this.teamstepId,
      teamId: this.teamId,
      userId: this.tokenStorage.getUser().id,
      teamgoalId: this.goalNameselectedId
    };
    this.teamGoalService
      .addstepKeypoint(dataKey)
      .subscribe(
        data => {
          this.sucessmessage = "Comments Added";
          this.isLoggedIn = true;
          this.iskeySuccessful = true;
          fAddKey.resetForm();
          document.getElementById("closekeymodal").click();
          this.getstepkeypoint(this.teamsteplinkId);
        },
        err => {
          this.errorMessage = err.error.message;
          this.iskeySuccessful = false;
        });
  }

  //Get step link  for share
  showstepShareModal(selectedItem: any): void {
    this.isSuccessful = false;
    $("#sharestepModal").modal('show');
    //this.stepLink =selectedItem.stepLink;   
    this.teamsteplink = selectedItem.teamsteplink;
    this.teamsteplinkId = selectedItem.teamsteplinkId;// You get the Id of the selected item here
    this.teamstepId = selectedItem.teamstepId;
    this.teamId = selectedItem.teamId;
    this.sharesteplink.teamgoalId = selectedItem.teamgoalId;
    console.log("565", selectedItem);
    console.log("567", this.sharesteplink)
  }

  //Change share val
  onchangeAccessType(val: any) {
    this.sharesteplink.accesstype = val;
  }

  //Share step link
  sharestep(teamsteplinkId: number, fshare: NgForm) {
    if (!this.sharesteplink.accesstype) {
      this.isSuccessful = false;
    }
    else {
      this.teamsteplinkId = teamsteplinkId;
      const datalink = {
        teamstepId: this.teamstepId,
        teamId: this.teamId,
        teamsteplinkId: teamsteplinkId,
        teamsteplink: this.teamsteplink,
        userId: this.tokenStorage.getUser().id,
        accesstype: this.sharesteplink.accesstype,
        //username:this.username,
        sharedTime: new Date().toDateString(),
        teamgoalId: this.sharesteplink.teamgoalId
      };
      console.log("593", datalink);
      this.teamGoalService
        .addsharestep(datalink)
        .subscribe(
          (data: any) => {
            this.isSuccessful = true;
            document.getElementById("shareclose").click();
            this.sharesteplink.accesstype='';
            this.isSuccessful = true;
            fshare.resetForm();
          },
          (err: any) => {
            this.isSuccessful = false;
            console.log(err.error.message);
          });
    }
  }

  //Get step link id to delete
  showModal(selectedItem: any): void {
    // $("#mydelModal").modal('show');
    this.teamsteplink = selectedItem.teamsteplink;
    this.teamsteplinkId = selectedItem.teamsteplinkId;// You get the Id of the selected item here
    this.teamstepId = selectedItem.teamstepId;
  }

  //Delete step link
  deletesteplink() {
    this.teamGoalService
      .deletestepLink(this.teamsteplinkId)
      .subscribe(
        data => {
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";
          this.getstepLink(this.teamstepId);
          this.getGoalStep();
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        });
  }

  //Get step link id
  opencommentstoggle(selectedItem: any, i) {
    this.showKeypointIndex = i;
    this.teamsteplinkId = selectedItem.teamsteplinkId;
    this.getstepkeypoint(this.teamsteplinkId);
    // this.stepkeytoggle = !this.stepkeytoggle;
  }
  //Close Key toggle
  closecommentstoggle(selectedItem: any, i) {
    this.showKeypointIndex = null;
    this.teamsteplinkId = selectedItem.teamsteplinkId;
  }

  //Get keypoints for step links
  getstepkeypoint(teamsteplinkId: number) {
    this.KeypointList = [];
    this.limitstepkeypoint = [];
    this.limitstepkeypoint1 = [];
    this.teamGoalService
      .getstepKeypoint(teamsteplinkId)
      .subscribe(
        data => {
          this.KeypointList = data;
          this.limitstepkeypoint = this.KeypointList.slice(0, 3);
        },
        err => {
          return console.error(err);
        });
  }

  //Get Details to edit Key point
  showEditKeyModal(selectedItem: any): void {
    this.isSuccessful = false;
    $("#myEditKeyPointModal").modal('show');
    this.teamsteplinkId = selectedItem.teamsteplinkId;
    this.stepkeypoint.teamstepKeypoints = selectedItem.teamstepKeypoints;
    this.teamstepkeypointId = selectedItem.teamstepkeypointId;
    this.teamstepId = selectedItem.teamstepId;
    this.teamId = selectedItem.teamId;
    this.showKeyModal = true;
  }

  //Edited keypoint
  prevent(teamstepKeypoints: string) {
    event.preventDefault();
    this.EditstepKeypoint(teamstepKeypoints);
  }

  //Update Edited Keypoints
  EditstepKeypoint(teamstepKeypoints: String) {
    console.log("668", teamstepKeypoints);
    if (teamstepKeypoints == "") {
      console.log("68", teamstepKeypoints);
      this.iskeySuccessful = false;
    }
    else {
      const dataKey = {
        teamstepKeypoints: teamstepKeypoints,
        teamsteplinkId: this.teamsteplinkId,
        teamstepId: this.teamstepId,
        teamId: this.teamId,
        userId: this.tokenStorage.getUser().id
      };
      this.teamGoalService
        .updatestepKeypoint(this.teamstepkeypointId, dataKey)
        .subscribe(
          data => {
            this.sucessmessage = "Comments  Edited";
            this.iskeySuccessful = true;
            this.hideModal();
            this.getstepkeypoint(this.teamsteplinkId);
          },
          err => {
            this.errorMessage = err.error.message;
            this.iskeySuccessful = false;
          });
    }
  }

  //Hide Modal
  hideModal(): void {
    // document.getElementById('close-modal').click();
    document.getElementById('stepkeymodal').click();
  }

  //Get Details for keypoint to delete
  showdelKeyModal(selectedItem: any): void {
    $("#mydelKeyModal").modal('show');
    this.teamsteplinkId = selectedItem.teamsteplinkId;// You get the Id of the selected item here
    this.teamstepkeypointId = selectedItem.teamstepkeypointId;
  }

  //Delete Keypoint
  deletestepKeypoint() {
    this.teamGoalService.deletestepKeypoint(this.teamstepkeypointId)
      .subscribe(
        data => {
          this.getstepkeypoint(this.teamsteplinkId);
        },
        error => console.log(error));
    this.getstepkeypoint(this.teamsteplinkId);
  }


  //view more keypoints
  onstepKeyPointsViewMore() {
    this.limitstepkeypoint1 = this.KeypointList.slice(3, this.KeypointList.length);
    this.limitstepkeypoint = this.limitstepkeypoint.concat(this.limitstepkeypoint1);
    $('#view-more').hide();
    $('#view-less').show();
  }

  //view less keypoints
  onstepkeyPointsviewless() {
    this.limitstepkeypoint = this.KeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }

  /**
   * 
   * 
   * Skill Related Functions 
   * 
   * */

  //Add Skill
  AddSkill() {
    if (this.goalNameselected == "Goal Name") {
      this.errorMessage = "*Please Select Goal";
    }
    else {
      const datastep = {
        teamskillName: this.skill.teamskillName,
        teamgoalId: this.goalNameselectedId,
        teamId: this.team.teamName,
        userId: this.tokenStorage.getUser().id
      };
      this.teamGoalService
        .addSkill(datastep)
        .subscribe(
          data => {
            this.sucessmessage = data;
            this.isLoggedIn = true;
            this.isSuccessful = true;
            this.getskill();
          },
          err => {
            this.errorMessage = '';
            this.goalerrorMessage = err.error.message;
            this.skillisSuccessful = false;
          });
    }
  }

  //Get skills for goals
  getskill() {
    this.skillKeypointList = [];
    this.teamGoalService
      .getSkill(this.goalNameselectedId)
      .subscribe(
        data => {
          this.skilllist = data;
          this.skilllistVisible = true;
        },
        err => console.error(err),
        () => console.log('getskill completed'));
  }

  //Add skill link
  AddskillLink(teamskillId, teamskilllink, id, teamId, ffl: NgForm) {
    const data = {
      teamskilllink: teamskilllink,
      teamskillId: teamskillId,
      teamId: teamId,
      teamgoalId: this.goalNameselectedId,
      userId: this.tokenStorage.getUser().id
    };

    this.teamGoalService
      .addskillLink(data)
      .subscribe(
        data => {
          this.showCollasalIndex = id;
          this.teamskillId = teamskillId;
          ffl.resetForm();
          this.getskill();
          //this.getstepLink(this.stepId); 
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.linkisSuccessful = true;
        },
        err => {
          this.skillerrorMessage = err.error.message.split(':')[1];
          this.linkisSuccessful = false;
        });
    this.getSkillLink(teamskillId);
  }

  //Get data to Add keypoint for skill
  onskillSelectKeypoint(selectedItem: any) {
    this.isSuccessful = false;
    $("#myskillModal").modal('show');
    this.teamskilllinkId = selectedItem.teamskilllinkId;// You get the Id of the selected item here
    this.teamskillId = selectedItem.teamskillId;
    this.teamId = selectedItem.teamId;
    this.showKeyModal = true;
  }

  //Add keypoint for skill link
  AddskillKeypoint(flAddKey: NgForm) {
    const dataKey = {
      teamskillKeypoints: this.skillkeypoint.teamskillKeypoints,
      teamskilllinkId: this.teamskilllinkId,
      teamskillId: this.teamskillId,
      teamId: this.teamId,
      teamgoalId: this.goalNameselectedId,
      userId: this.tokenStorage.getUser().id,
    };
    this.teamGoalService
      .addskillKeypoint(dataKey)
      .subscribe(
        data => {
          this.sucessmessage = "Comments  Added";
          this.isLoggedIn = true;
          this.iskeySuccessful = true;
          flAddKey.resetForm();
          document.getElementById("closeskillkeymodal").click();
          this.getskillkeypoint(this.teamskilllinkId);
        },
        err => {
          this.errorMessage = err.error.message;
          this.iskeySuccessful = false;
        });
  }

  //Get data to share skill link
  showskillShareModal(selectedItem: any): void {
    this.isSuccessful = false;
    $("#shareskillModal").modal('show');
    //this.stepLink =selectedItem.stepLink;   
    this.teamskilllink = selectedItem.teamskilllink;
    this.teamskilllinkId = selectedItem.teamskilllinkId;// You get the Id of the selected item here
    this.teamId = selectedItem.teamId;
    this.teamskillId = selectedItem.teamskillId;
    this.shareskilllink.teamgoalId = selectedItem.teamgoalId;
    console.log("881", selectedItem)
    console.log("882", this.shareskilllink)
  }

  //On change skill access type
  onchangeskillAccessType(val: any) {
    this.shareskilllink.accesstype = val;
  }

  //Share skill link
  shareskill(teamskilllinkId: number, flshare: NgForm) {
    if (!this.shareskilllink.accesstype) {
      this.isSuccessful = false;
    }
    else {
      this.teamskilllinkId = teamskilllinkId;
      const datalink = {
        teamskillId: this.teamskillId,
        teamId: this.teamId,
        teamskilllinkId: teamskilllinkId,
        teamskilllink: this.teamskilllink,
        userId: this.tokenStorage.getUser().id,
        accesstype: this.shareskilllink.accesstype,
        //username:this.username,
        sharedTime: new Date().toDateString(),
        teamgoalId: this.shareskilllink.teamgoalId
      };

      this.teamGoalService
        .addshareskill(datalink)
        .subscribe(
          (data: any) => {
            document.getElementById("skillshareclose").click();
            this.shareskilllink.accesstype='';
            this.isSuccessful = true;
            flshare.resetForm();
          },
          (err: any) => {
            this.isSuccessful = false;
            console.log(err.error.message);
          });
    }
  }

  //Get data to delete skill link
  showskillModal(selectedItem: any): void {
    $("#myskilldelModal").modal('show');
    this.teamskilllink = selectedItem.teamskilllink;
    this.teamskilllinkId = selectedItem.teamskilllinkId;// You get the Id of the selected item here
    this.teamskillId = selectedItem.teamskillId;
  }


  //Delete skill link
  deleteskilllink() {
    this.teamGoalService
      .deleteskillLink(this.teamskilllinkId)
      .subscribe(
        data => {
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";
          this.getskill();
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        });
    this.getskill();
    this.hideModal();
  }

  //Get data to display skill link keypoints
  openskillcommentstoggle(selectedItem: any, i) {
    this.showKeypointIndex = i;
    this.teamskilllinkId = selectedItem.teamskilllinkId;
    this.getskillkeypoint(this.teamskilllinkId);
  }
  //Close key toggle
  closeskillcommentstoggle(selectedItem: any, i) {
    this.showKeypointIndex = null;
    this.teamskilllinkId = selectedItem.teamskilllinkId;
  }

  //get skill keyppoint
  getskillkeypoint(teamskilllinkId: number) {
    this.skillKeypointList = [];
    this.limitskillkeypoint = [];
    this.limitskillkeypoint1 = [];
    this.teamGoalService
      .getskillKeypoint(teamskilllinkId)
      .subscribe(
        data => {
          this.skillKeypointList = data;
          this.limitskillkeypoint = this.skillKeypointList.slice(0, 3);
        },
        err => {
          console.error(err);
          return console.error(err);
        });
  }

  //view more keypoints
  onskillKeyPointsViewMore() {
    this.limitskillkeypoint1 = this.skillKeypointList.slice(3, this.skillKeypointList.length);
    this.limitskillkeypoint = this.limitskillkeypoint.concat(this.limitskillkeypoint1);
    $('#view-more').hide();
    $('#view-less').show();
  }

  //view less keypoints
  onskillkeyPointsviewless() {
    this.limitskillkeypoint = this.skillKeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }

  //Get data to edit skill link keypoint 
  showskillEditKeyModal(selectedItem: any): void {
    $("#myskillEditKeyPointModal").modal('show');
    this.teamskilllinkId = selectedItem.teamskilllinkId;
    this.skillkeypoints.teamskillKeypoints = selectedItem.teamskillKeypoints;
    this.teamskillkeypointId = selectedItem.teamskillkeypointId;
    this.teamskillId = selectedItem.teamskillId;
    this.teamId = selectedItem.teamId;
    this.showKeyModal = true;
  }

  //Edit skill link keypoint
  skillprevent(teamskillKeypoints: string) {
    event.preventDefault();
    this.EditskillKeypoint(teamskillKeypoints);
  }

  //Get data to delete skill link keypoint
  showskilldelKeyModal(selectedItem: any): void {
    $("#showskilldelKeyModal").modal('show');
    this.teamskilllinkId = selectedItem.teamskilllinkId;// You get the Id of the selected item here
    this.teamskillkeypointId = selectedItem.teamskillkeypointId;
  }

  //Delete skill link keypoint
  deleteskillKeypoint() {
    this.teamGoalService
      .deleteskillKeypoint(this.teamskillkeypointId)
      .subscribe(
        data => {
          this.getskillkeypoint(this.teamskilllinkId);
        },
        error => console.log(error));
    this.getskillkeypoint(this.teamskilllinkId);
  }







  onchangeGoalAccessType(val: any) {
    this.shareGoal.accessType = val;
  }

  reloadData() {
    this.goallist = this.getGoal();
    this.getGoalStep();
    this.getskill();
  }



  // opentesttoggle(selectedItem:any,i)
  //   {
  //      this.showCollasalIndex=i;
  //      this.teamstepId =selectedItem.stepId;
  //      this.getstepLink(this.teamstepId);
  //     //  this.skillId=selectedItem.skillId;
  //   }


  deletesteplinkBystep(teamstepId) {
    this.teamGoalService
      .deletesteplinkBystep(teamstepId)
      .subscribe(
        data => {
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";

        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        });
  }








  getSkillLink(teamskillId: number) {
    this.teamGoalService
      .getskillLink(teamskillId)
      .subscribe(
        data => {
          this.skilllinklist = data
          this.skilllinklistVisible = true;
        },
        err => console.error(err),
        () => console.log('getskilllink completed')
      );
  }





  EditskillKeypoint(teamskillKeypoints: String) {
    const dataKey = {
      teamskillKeypoints: teamskillKeypoints,
      teamskilllinkId: this.teamskilllinkId,
      teamskillId: this.teamskillId,
      teamId: this.teamId,
      userId: this.tokenStorage.getUser().id
    };
    this.teamGoalService.updateskillKeypoint(this.teamskillkeypointId, dataKey).subscribe(
      data => {
        this.sucessmessage = "Comments  Edited";
        this.iskeySuccessful = true;
        this.getskillkeypoint(this.teamskilllinkId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
    this.getskillkeypoint(this.teamskilllinkId);

  }

  showdelStepModal(selectedItem: any): void {
      $("mydelStepModal").modal('show');
    this.teamstepId = selectedItem.teamstepId;
  }

  deleteStep() {
    this.teamGoalService
      .deleteteamstep(this.teamstepId)
      .subscribe(
        data => {
          this.reloadData();
        },
        error => console.log(error));
    this.hideModal();
    this.getGoal;
  }

  enablestepEditMethod(steps, id) {
    this.currentstepid = steps.teamstepId;
    this.currentstepname = steps.teamstepName;
    // this.currentstepgoalId = steps.stepgoalid;
    this.enablestepEdit = true;
    this.enablestepEditIndex = id;
    this.hrefvisible = false;
    this.hrefvisible = id;
  }

  editStep(stepId: number, stepName: String) {
    if (stepName === "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enablestepEdit = true;
      const data = {
        teamstepName: stepName,
        userId: this.tokenStorage.getUser().id,
        teamgoalId: this.goalNameselectedId
      };
      this.teamGoalService.updateStep(stepId, data)
        .subscribe(
          data => {
            this.isSuccessful = true;
            this.sucessmessage = data;
            this.isLoggedIn = true;
            this.getGoalStep();
            this.enablestepEdit = true;
            // location.reload();
            this.currentstepid=null;
          },
          err => {
            this.isSuccessful = false;
          });
    }
  }
}

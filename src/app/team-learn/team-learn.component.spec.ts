import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamLearnComponent } from './team-learn.component';

describe('TeamLearnComponent', () => {
  let component: TeamLearnComponent;
  let fixture: ComponentFixture<TeamLearnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamLearnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamLearnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

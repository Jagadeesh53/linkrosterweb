import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
import { TeamService } from '../_services/team.service';
import { NewteamComponent } from '../newteam/newteam.component';
import { FormBuilder, NgForm, Validators } from "@angular/forms";
import { TeamTopicService } from '../_services/teamTopic.service';

declare var $: any;

@Component({
  selector: 'app-team-learn',
  templateUrl: './team-learn.component.html',
  styleUrls: ['./team-learn.component.css']
})
export class TeamLearnComponent implements OnInit {
  p: number = 1;
  p1: number = 1;
  teamlist: any = []; isSubmitted = false; teamName: any;
  searchlink: '';
  topicNameselected: any = {};
  topicNameselectedId: any = {};
  bookmarkLinkselected: any = {};
  bookmarkLinkselectedId: any = {};
  public selectedteamTopicid;
  public selectedteamTopicname;

  teamTopicId: number;
  bookmarkLinkId: number;
  topiclist: any = [];
  bookmarklinklist: any = [];
  KeypointList: any = [];
  limitedKeypointList: any = [];
  limitedKeypointList2: any = [];
  message: String;
  editComment: string;
  isdelete = false;
  enableEdit = false;
  enableEditIndex = null;
  enableconceptEdit = false;
  enableconceptEditIndex = null;
  public currentconceptid;
  public currentconceptname;
  hrefvisible = true;
  topicisSuccessful = false; topicerrorMessage = '';
  conceptisSuccessful = false; concepterrorMessage = '';
  conceptlist: any = [];
  conceptlistVisible = true;
  conceptlinklist: any = [];
  isgetTopic = false;
  teamConceptKeypoints: String;
  teamConceptId: number;
  teamConceptLinkId: number;
  teamConceptlink: string;
  showKeypointIndex = null;
  teamConceptKeypointId: number;
  form: any;
  errorMessage = '';
  sucessmessage: any;
  isLoggedIn = false;
  contenteditable = false;
  editField: string;
  teamtopic = {

    teamTopicName: '',
    userId: 0,
    teamId: 0,
    description: ''
  };
  keypoint = {

    teamConceptKeypoints: '',
    teamConceptLinkId: 0,
    teamConceptId: 0,
    teamId: 0,
    userId: 0
  };
  concept = {

    teamConceptName: '',
    teamTopicId: 0,
    userId: 0, teamId: 0
  };
  shareconceptlink = {

    teamConceptLinkId: 0,
    userId: 0, teamId: 0,
    accessType: null,
    teamConceptlink: '',
    teamConceptId: 0,
    sharedTime: new Date().toDateString(),
    teamTopicId: 0

  };

  teamTopicShare = {
    learnName: '',
    learnId: 0,
    shareId: 0,
    accessType: 'nulls',
    userId: 0,
    sharedTime: Date()
  };
  teamTopicName: string;


  content = '';
  isSuccessful = false;
  showCollasal = true;
  showCollasalIndex = null;
  isgetBookmark = false;
  isgetKeyPoints = false;
  NoRecorderrorMessage = "";
  constructor(private teamService: TeamService,
    private teamTopicService: TeamTopicService,
    private router: Router, public fb: FormBuilder,
    private tokenStorage: TokenStorageService

  ) { }
  teamBookmarkForm = this.fb.group({
    teamName: ['', [Validators.required]]
  })
  showKeyModal: boolean;
  teambookmarkLinkId: number;
  Link: string;
  teamkeyComments: string;
  teamkeypointId: number;
  teambookmarkCategoryId: number; teamId: any;
  team = {

    teamName: ''
  }

  onchange(val: any) {
    console.log("128", val);
    this.team.teamName = val;
    this.getTopic();


    // this.bookmarklinkId=selectedlink.teambookmarkLinkId;
    //  this.showCollasalIndex=id;
    //  this.teambookmarkLinkId =selectedlink.teambookmarkLinkId;

    //  this.getkeypoint(this.bookmarklinkId);
    // $('#collapseExample').collapse('show');
  }
  buttonCol(selectedItem: any) {
    this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;// You get the Id of the selected item here

    // this.getkeypoint(this.teambookmarkLinkId );
  }
  bookmarklinkId: number;
  opentesttoggle(selectedItem: any, i) {

    //this.getBookmarkid(this.bookmarklist[0],0); 
    //this.getbookmarklink(this.data);

    console.log(selectedItem, "<--->", i);
    //   console.log(selectedItem.teambookmarkLinkId[0]);
    this.bookmarklinkId = selectedItem.teambookmarkLinkId;

    // this.showCollasalIndex = i;
    this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;

    //  this.getkeypoint(this.bookmarklinkId);

    //   $('#collapseExample').collapse('show');
    //addLimitedKeyPoints(start: number, end: number): void {
    // this.limitedKeypointList = this.limitedKeypointList.concat(this.KeypointList.slice(start, end))
    // }
  }
  openNav(selectedItem: any) {

    //  document.getElementById("collapseExample").style.width = "250px";
    this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;// You get the Id of the selected item here

    // this.getkeypoint(this.teambookmarkLinkId );
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
  enableconceptEditMethod(s, id) {
    this.currentconceptid = s.teamConceptId;
    this.currentconceptname = s.teamConceptName;
    //this.isSuccessful = true;
    this.enableconceptEdit = true;
    this.enableconceptEditIndex = id;
    console.log(id, s);
   

  }
  editConcept(conceptId: number, conceptName: String) {

    if (conceptName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableconceptEdit = true;
     
      const datastep = {
        teamConceptName: conceptName,
        teamConceptId: conceptId,
        userId: this.tokenStorage.getUser().id
      };
      this.teamTopicService.updateConcept(datastep).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getconcept();
          this.enableconceptEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        });
    }
  }
  deleteConcept() {
    this.teamTopicService.deleteConcept(this.teamConceptId)
      .subscribe(
        data => {
          console.log(data);
          document.getElementById('close-modal').click();
          // this.reloadData();
          this.getconcept();
        },
        error => console.log(error));
    // this.hideModal();
  }
  showdelConceptModal(selectedItem: any): void {

    $("#mydelConceptModal").modal('show');

    console.log("Selected item Id: ", selectedItem.teamConceptId);

    this.teamConceptId = selectedItem.teamConceptId;
  }


  hide() {
    this.showKeyModal = false;
  }


  opentoggle(id: number, property: string, event: any) {


    $("#slideme").slideToggle();
  }
  keybookmarklinkId: number;
  onSelectKeypoint(selectedItem: any) {
    $("#myModal").modal('show');
    // this.teamConceptKeypoints =selectedItem.keypoints; 
    console.log("Selected item Id: ", selectedItem.Id);
    this.teamConceptLinkId = selectedItem.teamConceptLinkId;// You get the Id of the selected item here
    this.showKeyModal = true;
    this.teamConceptId = selectedItem.teamConceptId;
    this.teamId = selectedItem.teamId;
    //this.getkeypoint(this.BookmarkLinkId);
  }

  showdelTopicModal(selectedItem: any): void {

    $("#mydelTopicModal").modal('show');

    console.log("Selected item Id: ", selectedItem.teamTopicId);

    this.teamTopicId = selectedItem.teamTopicId;// You get the Id of the selected item here

  }

  showModal(selectedItem: any): void {

    $("#mydelModal").modal('show');
    this.teamConceptlink = selectedItem.teamConceptlink;
    console.log("Selected item Id: ", selectedItem.Id);
    this.teamConceptLinkId = selectedItem.teamConceptLinkId;// You get the Id of the selected item here

  }
  showEditKeyModal(selectedItem: any): void {

    $("#myEditKeyPointModal").modal('show');
    this.teamConceptKeypointId = selectedItem.teamConceptKeypointId;
    this.editComment = selectedItem.teamConceptKeypoints;
    this.teamConceptKeypoints = selectedItem.teamConceptKeypoints;
    this.teamConceptLinkId = selectedItem.teamConceptLinkId;
    this.teamConceptId = selectedItem.teamConceptId;
    this.teamId = selectedItem.teamId;
    this.showKeyModal = true;


  }
  keyid: number;
  showdelKeyModal(selectedItem: any): void {

    this.teamConceptKeypointId = selectedItem.teamConceptKeypointId;
    this.teamConceptKeypoints = selectedItem.teamConceptKeypoints;

    this.teamConceptLinkId = selectedItem.teamConceptLinkId;// You get the Id of the selected item here
    $("#mydelKeyModal").modal('show');
  }
  sendModal(): void {
    //do something here
    this.hideModal();

  }
  hideModal(): void {

    document.getElementById('close-modal').click();
  }
  ngOnInit() {
    this.conceptlistVisible = false;
    this.getTeam();
    this.topicNameselected = "Add Category Link";
    // this.getTopic();
    // this.getconcept();
    // this.bookmarklinklistVisible=false;
    // this.getkeypoint(this.teambookmarkLinkId);
    if (this.topiclist == []) {

      this.isgetTopic = true;
      this.errorMessage = "No Topics"
    }


  }

  getTeam() {
    this.teamService.getTeamList(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.teamlist = data
        },
        err => {
          this.teamlist = JSON.parse(err.error).message;
        }
      );
  }
  getTopicid(topiclist: any, i: any) {

    this.topicNameselectedId = topiclist.teamTopicId;
    this.topicNameselected = topiclist.teamTopicName;

    this.getconcept();

  }

  deleteTopic() {
    console.log("293");
    this.teamTopicService.deleteTopic(this.teamTopicId)
      .subscribe(
        data => {
          console.log("297", data);
          this.reloadData();
        },
        error => console.log("300", error));
    // this.hideModal();
    // this.deleteConceptlinkByCategory(this.teamTopicId);
    //  this.getbookmarklink();
  }
  reloadData() {
    this.topicNameselected="Add Category Link"
    this.topiclist = this.getTopic();
  }
  deleteConceptlinkByCategory(teamTopicId) {

    this.teamTopicService.deleteConceptlinkByCategory(teamTopicId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";

        },

        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
  }

  showLearnShareModal(selectedItem: any): void {

    $("#shareModal").modal('show');
    this.teamTopicName = selectedItem.teamTopicName;
    console.log("Selected item Id: ", selectedItem.Id);
    this.teamTopicId = selectedItem.teamTopicId;// You get the Id of the selected item here

  }

  onchangeLearnAccessType(val: any) {

    this.teamTopicShare.accessType = val;


  }

  shareTopicName(teamTopicId: number,) {


    this.teamTopicId = teamTopicId;

    const datalink = {
      teamTopicId: this.teamTopicId,
      teamTopicName: this.teamTopicName,
      userId: this.tokenStorage.getUser().id,
      accesstype: this.teamTopicShare.accessType,
      teamId: this.teamId,
      sharedTime: new Date().toDateString()

    };
    this.teamTopicService.addshareTopic(datalink).subscribe(
      (data: any) => {
        console.log(data);
        this.getTopic();
      },
      (err: any) => {

      }
    );

  }

  getTopic() {
    console.log("372", this.team.teamName);
    this.topiclist = [];
    this.teamTopicService
      .getTopic(this.tokenStorage.getUser().id, this.team.teamName)
      .subscribe(
        data => {
          this.topiclist = data;
          console.log(data, "bookmark category");
          if (this.topiclist.length > 0) {
            this.getTopicid(this.topiclist[0], 0);
          }
          //this.getbookmarklink(this.data);
        },
        err => {
          console.log("383", err);
          this.topiclist = JSON.parse(err.error).message;
        }
      );
  }

  onKeyPointsViewMore(): void {
    this.limitedKeypointList2 = this.KeypointList.slice(3, this.KeypointList.length);
    this.limitedKeypointList = this.limitedKeypointList.concat(this.limitedKeypointList2);
    console.log(this.limitedKeypointList);
    console.log(this.KeypointList.length, this.limitedKeypointList.length);
    $('#view-more').hide();
    $('#view-less').show();
  }

  onkeyPointsviewless(): void {
    this.limitedKeypointList = this.KeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }

  AddTopics(f: NgForm) {

    const data = {
      teamTopicName: this.teamtopic.teamTopicName,
      userId: this.tokenStorage.getUser().id,
      teamId: this.team.teamName,
      description: this.teamtopic.description
    };

    this.teamTopicService.addTopic(data).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        f.resetForm();
        this.getTopic();
      },
      err => {
        console.log("423", err);
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      }
    );
  }

  iskeySuccessful = false;

  changeValue(id: number, property: string, event: any) {

    this.editField = event.target.textContent;

  }
  changetestValue(event: any) {

    this.editField = event.target.value;
    this.findByTopicName(this.editField);
    //(this.editField );

    if (this.editField.length == 0) {
      this.getTopic();
    }
    // else if(this.editField != this.bookmark.bookmarkName){

    // }
  }
  findByTopicName(key) {
    this.isSuccessful = true;
    this.enableEdit = false;

    this.teamTopicService.findByTopicName(key,
      this.tokenStorage.getUser().id)

      .subscribe(
        data => {
          this.topiclist = data
          console.log("list", data);
        },
        err => {
          this.topiclist = JSON.parse(err.error).message;
        }
      );
  }

  editTopic(teamTopicId: number, teamTopicName: String) {
    if (teamTopicName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableEdit = true;
      const data = {
        teamTopicName: teamTopicName, teamId: this.team.teamName,
        userId: this.tokenStorage.getUser().id
      };

      this.teamTopicService.updateTopic(teamTopicId, data).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getTopic();
          this.enableEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );

    }
  }


  enableEditMethod(e, i) {
    this.selectedteamTopicid = e.teamTopicId;
    this.selectedteamTopicname = e.teamTopicName;
    this.isSuccessful = true;
    this.enableEdit = true;
    this.enableEditIndex = i;
    console.log(i, e);
    this.hrefvisible = false;
    this.hrefvisible = i;

  }


  AddInviteToTeam() {

    this.isSubmitted = true;

    if (!this.team.teamName) {

      return false;
    } else {

      this.router.navigate(['newteam'], { queryParams: { teamId: this.team.teamName } });
    }
  }
  AddConcept(fl: NgForm) {
    if (this.topicNameselected == "Add concept") {

      this.errorMessage = "*Please Select Topic";
    }
    else {
      const datastep = {
        teamConceptName: this.concept.teamConceptName,
        teamTopicId: this.topicNameselectedId,
        teamId: this.team.teamName,
        userId: this.tokenStorage.getUser().id
      };
      this.teamTopicService.addConcept(datastep).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          fl.resetForm();
          this.conceptisSuccessful = true;
          this.getconcept();

          console.log("added");
        },
        err => {
          this.errorMessage = '';
          this.concepterrorMessage = err.error.message;
          this.conceptisSuccessful = false;
        }
      );
    }
    //this.getconcept
  }
  getconcept() {
    this.conceptlist = [];
    this.teamTopicService.getconcept(this.topicNameselectedId)

      .subscribe(
        data => {
          this.conceptlist = data;
          console.log(data);
          if (this.conceptlist.length > 0) {
            this.opentesttoggle(data[0], 0);
          }
          this.conceptlistVisible = true;
        },
        err => console.error(err),
        () => console.log('concept completed')
      );
  }
  AddconceptLink(teamConceptId, conceptLink, i, teamId) {

    const data = {
      teamConceptlink: conceptLink,
      teamConceptId: teamConceptId,
      teamId: teamId,
      userId: this.tokenStorage.getUser().id,
      teamTopicId: this.topicNameselectedId
    };
    this.teamTopicService.addconceptLink(data).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        //this.showCollasalIndex = i;
        //  this.conceptLinkCategoryId =selectedItem.conceptId;
        // this.conceptLinkCategoryId =conceptId; 
        this.getconcept();
        this.getconceptLink(teamConceptId);

        console.log("added");
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      }
    );
  }

  getconceptLink(teamConceptId: number) {
    this.conceptlinklist = [];
    this.teamTopicService.getconceptLink(teamConceptId)
      .subscribe(
        data => {
          this.conceptlinklist = data
          console.log(data);
          // this.opentesttoggle(data[0],0);   
        },
        err => console.error(err),
        () => console.log('getconceptlink completed')
      );
  }
  Addconceptkeypoints(conceptlinkId: number, fAddKey: NgForm) {


    const dataKey = {
      teamConceptKeypoints: this.keypoint.teamConceptKeypoints,
      teamConceptLinkId: this.teamConceptLinkId,
      teamConceptId: this.teamConceptId,
      userId: this.tokenStorage.getUser().id,
      teamId: this.teamId,
      teamTopicId: this.topicNameselectedId
    };

    this.teamTopicService.addconceptkeypoints(dataKey).subscribe(
      data => {
        console.log(data);
        fAddKey.resetForm();
        this.sucessmessage = "Comments  Added";
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        var $parent = this;
        setTimeout(function () {

          $parent.sucessmessage = "";
          $parent.isLoggedIn = false;
          $parent.iskeySuccessful = false;
          $parent.showKeyModal = false;

          document.getElementById('close-modal').click();
       

        }, 1000);
        // if(this.teamConceptLinkId>0){
        //   this.getConceptkeypoint(this.teamConceptLinkId);
        // }
        this.closecommentstoggle(dataKey,null);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );

    //  this.getsolutionkeypoint(this.solutionlinkId);
    //this.hideModal();
  }
  showconceptShareModal(selectedItem: any): void {

    $("#shareconceptModal").modal('show');
    this.teamConceptlink = selectedItem.teamConceptlink;
    console.log("Selected item Id: ", selectedItem.Id);
    this.teamConceptLinkId = selectedItem.teamConceptLinkId;// You get the Id of the selected item here
    this.teamConceptId = selectedItem.teamConceptId;
    this.teamId = selectedItem.teamId;
    this.shareconceptlink.teamTopicId = selectedItem.teamTopicId;
    console.log("663", selectedItem);
  }
  shareteamConcept(teamConceptLinkId: number, fshare: NgForm) {
    if (!this.shareconceptlink.accessType) {
      this.isSuccessful = false;
    }
    else {
    this.teamConceptLinkId = teamConceptLinkId;
    const datalink = {
      teamConceptId: this.teamConceptId,
      teamconceptlink: this.teamConceptlink,
      teamId: this.teamId,
      teamConceptLinkId: teamConceptLinkId,
      userId: this.tokenStorage.getUser().id,
      accesstype: this.shareconceptlink.accessType,
      sharedTime: new Date().toDateString(),
      teamTopicId: this.shareconceptlink.teamTopicId

    };
    console.log("681", datalink);
    this.teamTopicService.addshareconcept(datalink).subscribe(
      (data: any) => {
        console.log(data);
        document.getElementById('topicclose').click();
        fshare.resetForm();
        this.isSuccessful = true;
        this.shareconceptlink.accessType='';
      },
      (err: any) => {
        this.isSuccessful = false;
      }
    );
    }
  }
  onchangeAccessType(val: any) {
    this.shareconceptlink.accessType = val;
  }
  deleteconceptlink() {

    this.teamTopicService.deleteconceptlink(this.teamConceptLinkId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";
          this.getconceptLink(this.teamConceptId);
          this.reloadData();
        },

        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
    this.getconceptLink(this.teamConceptId);
    this.hideModal();
  }
  opencommentstoggle(selectedItem: any, i) {

    //alert("hi")
    this.showCollasalIndex = i;
    // this.showKeypointIndex = i;
    this.teamConceptLinkId = selectedItem.teamConceptLinkId;

    this.getConceptkeypoint(this.teamConceptLinkId);



  }
  closecommentstoggle(selectedItem: any, i) {

    this.showCollasalIndex = null;
    //  this.showKeypointIndex = null;
    this.teamConceptLinkId = selectedItem.teamConceptLinkId;

    // this.getConceptkeypoint(this.teamConceptLinkId);



  }
  getConceptkeypoint(teamConceptLinkId: number) {
    this.KeypointList = null;
    this.teamTopicService
      .getConceptkeypoint(teamConceptLinkId)
      .subscribe(
        data => {
          console.log(data)
          this.KeypointList = data;

        },
        err => {
          return console.error(err);
        }
        // () => console.log('getKeypointList completed') 
      );
  }
  EditKeypoint(teamConceptKeypoints: String) {



    const dataKey = {
      teamConceptKeypoints: teamConceptKeypoints,
      teamConceptLinkId: this.teamConceptLinkId,
      teamConceptId: this.teamConceptId,
      teamId: this.teamId,
      userId: this.tokenStorage.getUser().id
    };


    this.teamTopicService.updateKeypoint(this.teamConceptKeypointId, dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Edited";

        this.iskeySuccessful = true;
        this.getConceptkeypoint(this.teamConceptLinkId);

      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );

    this.getConceptkeypoint(this.teamConceptLinkId);


  }
  prevent(teamConceptKeypoints: string) {
    event.preventDefault();
    this.EditKeypoint(teamConceptKeypoints);


  }
  deleteKeypoint() {


    this.teamTopicService.deleteKeypoint(this.teamConceptKeypointId)

      .subscribe(
        data => {
          console.log(data);
          this.getConceptkeypoint(this.teamConceptLinkId);

        },
        error => console.log(error));

    this.getConceptkeypoint(this.teamConceptLinkId);


  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeambookmarkComponent } from './teambookmark.component';

describe('TeambookmarkComponent', () => {
  let component: TeambookmarkComponent;
  let fixture: ComponentFixture<TeambookmarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeambookmarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeambookmarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

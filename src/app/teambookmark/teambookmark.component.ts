import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
import { TeamService } from '../_services/team.service';
import { NewteamComponent } from '../newteam/newteam.component';
import { FormBuilder, NgForm, Validators } from "@angular/forms";

declare var $: any;

@Component({
  selector: 'app-teambookmark',
  templateUrl: './teambookmark.component.html',
  styleUrls: ['./teambookmark.component.css']
})
export class TeambookmarkComponent implements OnInit {
  p: number = 1;
  p1: number = 1;
  teamlist: any = []; isSubmitted = false; teamName: any;
  searchlink: '';
  bookmarkNameselected: any = {};
  bookmarkNameselectedId: any = {};
  bookmarkLinkselected: any = {};
  bookmarkLinkselectedId: any = {};
  public selectedbookmarkid;
  public selectedbookmarkname;
  teambookmarkLinkId: number;
  teambookmarkId: number;
  bookmarkLinkId: number;
  bookmarklist: any = [];
  bookmarklinklist: any = [];
  KeypointList: any = [];
  limitedKeypointList: any = [];
  limitedKeypointList2: any = [];
  message: String;
  isdelete = false;
  enableEdit = false;
  enableEditIndex = null;
  hrefvisible = true;
  bookmarklinklistVisible = true;
  bookmarkinkeditMode = false;
  form: any;
  errorMessage = '';
  sucessmessage: any;
  isLoggedIn = false;
  contenteditable = false;
  editField: string;
  teambookmark = {

    teambookmarkName: '',
    userId: 0,
    teamId: 0
  };
  keypoint = {

    teamkeyComments: '',
    teambookmarkLinkId: 0,
    teambookmarkCategoryId: 0
  };
  keypoints = {

    teamkeyComments: '',
    teambookmarkLinkId: 0,
    teambookmarkCategoryId: 0
  };
  bookmarklink = {

    teambookmarkLink: '',
    teambookmarkCategoryId: 0,
    userId: 0, teamId: 0
  };
  sharebookmarklink = {
    teambookmarkCategoryId: 0,
    teambookmarkLinkId: 0,
    userId: 0, teamId: 0,
    accessType: '',
    sharedTime: new Date().toDateString()

  };
  content = '';
  isSuccessful = false;
  showCollasal = true;
  showCollasalIndex = null;
  isgetBookmark = false;
  isgetKeyPoints = false;
  NoRecorderrorMessage = "";
  iskeySuccessful: boolean;
  constructor(private teamService: TeamService,
    private router: Router, public fb: FormBuilder,
    private tokenStorage: TokenStorageService

  ) { }
  teamBookmarkForm = this.fb.group({
    teamName: ['', [Validators.required]]
  })
  showKeyModal: boolean;

  Link: string;
  teamkeyComments: string;
  teamkeypointId: number;
  teambookmarkCategoryId: number; teamId: any;
  team = {

    teamName: ''
  }


  ngOnInit() {
    this.getTeam();
    this.bookmarkNameselected = "Add CategoryLink";
    // this.getbookmark();
    this.bookmarklinklistVisible = false;
    // this.getkeypoint(this.teambookmarkLinkId);
    if (this.bookmarklist == []) {
      this.isgetBookmark = true;
      this.errorMessage = "No Bookmarks"
    }
  }

  //get team
  getTeam() {
    this.teamService.getTeamList(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.teamlist = data;
        },
        err => {
          this.teamlist = JSON.parse(err.error).message;
        });
  }

  //Team Name on change
  onchange(val: any) {
    this.team.teamName = val;
    this.getbookmark();
    // this.bookmarklinkId=selectedlink.teambookmarkLinkId;
    //  this.showCollasalIndex=id;
    //  this.teambookmarkLinkId =selectedlink.teambookmarkLinkId;
    //  this.getkeypoint(this.bookmarklinkId);
    // $('#collapseExample').collapse('show');
  }

  //Add Invite
  AddInviteToTeam() {
    this.isSubmitted = true;
    if (!this.team.teamName) {
      return false;
    } else {
      this.router.navigate(['newteam'], { queryParams: { teamId: this.team.teamName } });
    }
  }

  //Add Bookmark
  AddBookmark(f) {
    console.log(this.team.teamName);
    const data = {
      teambookmarkName: this.teambookmark.teambookmarkName,
      userId: this.tokenStorage.getUser().id,
      teamId: this.team.teamName
    };
    this.teamService.addBookmark(data).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        f.resetForm();
        this.getbookmark();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      });
  }

  //Get Bookmark category
  getbookmark() {
    this.bookmarklinklist = [];
    this.KeypointList = [];
    console.log(this.team);
    this.teamService
      .getBookmark(this.tokenStorage.getUser().id, this.team.teamName)
      .subscribe(
        data => {
          this.bookmarklist = data;
          console.log(data, "bookmark category");
          if (this.bookmarklist.length > 0) {
            this.getBookmarkid(this.bookmarklist[0], 0);
          }
          else {
            this.bookmarkNameselectedId = null;
            this.isSuccessful = false;
            this.limitedKeypointList = null;
            this.bookmarkNameselected = "Add CategoryLink";
          }
          // this.getBookmarkid(this.bookmarklist[0], 0);
          //this.getbookmarklink(this.data);
        },
        err => {
          this.bookmarklist = err.error.message;
        });
  }


  //Search Bookmark category
  changetestValue(event: any) {
    this.editField = event.target.value;
    if (this.editField.length == 0) {
      this.getbookmark();
    } else {
      this.findByBookmarkName(this.editField);
    }
  }

  //get Bookmark category id
  getBookmarkid(bookmarklist: any, i: any) {
    this.bookmarkNameselectedId = bookmarklist.teambookmarkId;
    this.bookmarkNameselected = bookmarklist.teambookmarkName;
    this.getbookmarklink();
  }

  //get bookmark link
  getbookmarklink() {
    this.bookmarklinklist = [];
    this.teamService.getBookmarklink(this.bookmarkNameselectedId)
      .subscribe(
        data => {
          this.bookmarklinklist = data;
          console.log(data, "bookmark link");
          if (this.bookmarklinklist.length > 0) {
            this.opentesttoggle(data[0], 0);
          }
          this.bookmarklinklistVisible = true;
        },
        err => console.error(err),
        () => console.log('getBookarklink completed')
      );
  }


  //edit bookmark category
  enableEditMethod(e, i) {
    this.selectedbookmarkid = e.teambookmarkId;
    this.selectedbookmarkname = e.teambookmarkName;
    this.isSuccessful = true;
    this.enableEdit = true;
    this.enableEditIndex = i;
    console.log(i, e);
    this.hrefvisible = false;
    this.hrefvisible = i;
  }


  //Update bookmark category
  editBookmark(teambookmarkId: number, teambookmarkName: String) {
    if (teambookmarkName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableEdit = true;
      const data = {
        teambookmarkName: teambookmarkName, teamId: this.team.teamName,
        userId: this.tokenStorage.getUser().id
      };
      this.teamService.updatebookmark(teambookmarkId, data).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getbookmark();
          this.enableEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        });
    }
  }

  //show bookmark category link model
  showdelBookmarkModal(selectedItem: any): void {
    $("#mydelBookmarkModal").modal('show');
    console.log("Selected item Id: ", selectedItem);
    this.teambookmarkId = selectedItem.teambookmarkId;// You get the Id of the selected item here
  }

  //delete bookmark category
  deleteBookmark() {
    console.log("290", this.teambookmarkId);
    this.teamService.deleteBookmark(this.teambookmarkId)
      .subscribe(
        data => {
          console.log(data);
          this.hideModal();
          this.reloadData();
        },
        error => console.log(error));
    // this.deleteBookmarklinkByCategory(this.teambookmarkId);
    // this.getbookmarklink();
  }
  reloadData() {
    this.bookmarklist = this.getbookmark();
  }


  //Add bookmark
  AddBookmarkLink(fl: NgForm) {
    if (this.bookmarkNameselected == "Add CategoryLink") {
      this.errorMessage = "Please Select Bookmark";
    }
    else {
      const datalink = {
        teamId: this.team.teamName,
        teambookmarkLink: this.bookmarklink.teambookmarkLink,
        teambookmarkCategoryId: this.bookmarkNameselectedId,
        userId: this.tokenStorage.getUser().id
      };
      this.teamService.addBookmarklink(datalink).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          fl.resetForm();
          this.getbookmarklink();
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        });
    }
  }

  //Bookmark link 
  changeValue(id: number, property: string, event: any) {
    this.editField = event.target.textContent;
  }


  //on keypoint select
  onSelectKeypoint(selectedItem: any): void {
    this.iskeySuccessful = false;
    $("#myModal").modal('show');
    this.teamkeyComments = selectedItem.teamkeyComments;
    console.log("Selected item Id: ", selectedItem.Id);
    this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;// You get the Id of the selected item here
    this.keybookmarklinkId = selectedItem.teambookmarkLinkId;
    this.teamId = selectedItem.teamId;
    this.showKeyModal = true;
    this.getkeypoint(selectedItem.teambookmarkLinkId);
  }


  //Add keypoint
  AddKeypoint(teambookmarkLinkId: number, fAddKey: NgForm) {
    const dataKey = {
      teamkeyComments: this.keypoint.teamkeyComments,
      teambookmarkLinkId: this.keybookmarklinkId,
      teambookmarkCategoryId: this.bookmarkNameselectedId,
      teamId: this.teamId,
      userId: this.tokenStorage.getUser().id
    };

    this.teamService.addKeypoint(dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Added";
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        fAddKey.resetForm();
        document.getElementById("closekeymodal").click();
        this.getkeypoint(teambookmarkLinkId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      }
    );
  }

  //Get keypoint
  getkeypoint(BookmarkLinkId: number) {
    this.KeypointList = [];
    this.limitedKeypointList = [];
    this.teamService
      .getKeypoint(BookmarkLinkId)
      .subscribe(
        data => {
          this.KeypointList = data;
          // this.addLimitedKeyPoints(0, 3) // initially render 3 items    
          this.limitedKeypointList = this.KeypointList.slice(0, 3);
          console.log("sliced data", this.limitedKeypointList);
        },
        err => console.error(err),
        () => console.log('getKeypointList completed')
      );
  }


  //Key share model
  showShareModal(selectedItem: any): void {
    $("#shareModal").modal('show');
    this.Link = selectedItem.link;
    console.log("Selected item Id: ", selectedItem);
    this.teamId = selectedItem.teamId;
    this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;// You get the Id of the selected item here
    this.sharebookmarklink.teambookmarkCategoryId = selectedItem.teambookmarkCategoryId;
  
    console.log("408", selectedItem);
  }


  //Share access type
  onchangeAccessType(val: any) {
    this.sharebookmarklink.accessType = val;
  }

  //share bookmark
  shareBookmark(teambookmarkLinkId, fshare: NgForm) {
    console.log("this.sharebookmarklink.accessType", this.sharebookmarklink.accessType);
    if(!this.sharebookmarklink.accessType){
      this.isSuccessful=false;
    }
    else {
      const datalink = {
        teambookmarklinkId: teambookmarkLinkId,
        userId: this.tokenStorage.getUser().id,
        teamId: this.teamId,
        accesstype: this.sharebookmarklink.accessType,
        sharedTime: new Date().toDateString(),
        teambookmarkcategoryId: this.sharebookmarklink.teambookmarkCategoryId
      };
      this.teamService.addsharebookmark(datalink).subscribe(
          (data: any) => {
            console.log(data);
            document.getElementById("shareclose").click();
            this.isSuccessful=true;
            this.sharebookmarklink.accessType = '';
            fshare.resetForm();
            this.getbookmarklink();
          },
          err => {
            console.log(err);
            this.isSuccessful=false;
          });
    }
  }

  //Show delete model
  showModal(selectedItem: any): void {
    $("#mydelModal").modal('show');
    this.Link = selectedItem.link;
    console.log("Selected item Id: ", selectedItem);
    this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;// You get the Id of the selected item here
  }


  //Delete Bookmark link
  deleteBookmarklink(teambookmarkLinkId: number) {
    console.log("442", teambookmarkLinkId);
    this.teamService.deleteBookmarklink(teambookmarkLinkId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";
          document.getElementById("closedelmodal").click();
          this.bookmarklinklist = this.getbookmarklink();
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
  }

  //open comments toggle
  opentesttoggle(selectedItem: any, i) {
    console.log(selectedItem, "<--->", i);
    this.bookmarklinkId = selectedItem.teambookmarkLinkId;
    this.showCollasalIndex = i;
    // this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;
    this.getkeypoint(this.bookmarklinkId);
  }

  //close comments toggle
  closetesttoggle(selectedItem: any, i): void {
    this.showCollasalIndex = null;
    this.bookmarklinkId = selectedItem.teambookmarkLinkId;
  }

  //edit keypoint
  showEditKeyModal(selectedItem: any): void {
    this.isSuccessful = false;
    $("#myEditKeyPointModal").modal('show');
    this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;
    this.keypoints.teamkeyComments = selectedItem.teamkeyComments;
    this.teamkeypointId = selectedItem.teamkeypointId;
    this.teambookmarkCategoryId = selectedItem.teambookmarkCategoryId;
    this.showKeyModal = true;
    console.log("484", selectedItem);
  }

  //prevent keypoint edit
  prevent(teamkeyComments: string) {
    console.log("496");
    event.preventDefault();
    this.EditKeypoint(teamkeyComments);
  }

  //Update keypoint
  EditKeypoint(teamkeyComments: String) {
    console.log("503");
    const dataKey = {
      teamkeyComments: teamkeyComments,
      teambookmarkLinkId: this.teambookmarkLinkId,
      teambookmarkCategoryId: this.bookmarkNameselectedId
    };
    this.teamService.updateKeypoint(this.teamkeypointId, dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Edited";
        this.iskeySuccessful = true;
        document.getElementById("stepkeymodal").click();
        this.getkeypoint(this.teambookmarkLinkId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      });
  }

  //show del keypoint
  showdelKeyModal(selectedItem: any): void {
    $("#mydelKeyModal").modal('show');
    this.keyid = selectedItem.teamkeypointId;
    this.teamkeyComments = selectedItem.teamkeyComments;
    this.teamkeypointId = selectedItem.keypointId;
    this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;// You get the Id of the selected item here
  }

  //delete keypoint
  deleteKeypoint() {
    this.teamService.deleteKeypoint(this.keyid)
      .subscribe(
        data => {
          console.log(data);
          this.getkeypoint(this.teambookmarkLinkId);
        },
        error => console.log(error));
  }

  //view comments
  onKeyPointsViewMore(): void {
    this.limitedKeypointList2 = this.KeypointList.slice(3, this.KeypointList.length);
    this.limitedKeypointList = this.limitedKeypointList.concat(this.limitedKeypointList2);
    console.log(this.limitedKeypointList);
    console.log(this.KeypointList.length, this.limitedKeypointList.length);
    $('#view-more').hide();
    $('#view-less').show();
  }

  //close
  onkeyPointsviewless(): void {
    this.limitedKeypointList = this.KeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }


  buttonCol(selectedItem: any) {
    this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;// You get the Id of the selected item here

    this.getkeypoint(this.teambookmarkLinkId);
  }
  bookmarklinkId: number;



  openNav(selectedItem: any) {

    //  document.getElementById("collapseExample").style.width = "250px";
    this.teambookmarkLinkId = selectedItem.teambookmarkLinkId;// You get the Id of the selected item here

    this.getkeypoint(this.teambookmarkLinkId);
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }


  hide() {
    this.showKeyModal = false;
  }


  opentoggle(id: number, property: string, event: any) {


    $("#slideme").slideToggle();
  }
  keybookmarklinkId: number;






  keyid: number;

  sendModal(): void {
    //do something here
    this.hideModal();

  }
  hideModal(): void {

    document.getElementById('close-modal').click();
  }









  deleteBookmarklinkByCategory(bookmarkId) {

    this.teamService.deleteBookmarklinkByCategory(bookmarkId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";

        },

        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
  }










  //   onKeyUpEvent(event: any){

  //     //console.log(event.target.value);
  //     if(event.target.value.substr(0, 6)!='http://' || event.target.value.substr(0, 8)!='https://'){
  //      //alert("Invalid URL");
  //      this.errorMessage ="URL should be enter with Http";
  // //     this.bookmarklink.teambookmarkLink="";
  //      }

  //  }






  findByBookmarkName(key) {
    this.isSuccessful = true;
    this.enableEdit = false;

    this.teamService.findByBookmarkName(key,
      this.tokenStorage.getUser().id)

      .subscribe(
        data => {
          this.bookmarklist = data
          console.log("list", data);
        },
        err => {
          this.bookmarklist = JSON.parse(err.error).message;
        }
      );
  }
  findByBookmarkLink() {
    this.router.navigate(['searchviewpage'], { queryParams: { link: this.searchlink } });
  }

  editBookmarkLink(teambookmarkLinkId: number, teambookmarkLink: String) {

    const datalink = {

      teambookmarkLink: this.bookmarklink.teambookmarkLink,
      teambookmarkCategoryId: this.bookmarkNameselectedId,

      userId: this.tokenStorage.getUser().id
    };
    this.teamService.updatebookmarkLink(teambookmarkLinkId, datalink).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        this.getbookmarklink();
        this.enableEdit = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      }
    );
  }

  // changeValue(id: number, property: string, event: any) {

  //   this.editField = event.target.textContent;

  // }


  updateList(id: number, property: string, event: any) {

    const editField = event.target.textContent;
    this.bookmarklinklist[id][property] = editField;
    const datalink = {

      teambookmarkLink: this.editField,
      teambookmarkCategoryId: this.bookmarklinklist[id].teambookmarkCategoryId,
      userId: this.tokenStorage.getUser().id
    };
    this.teamService.updatebookmarkLink(this.bookmarklinklist[id].teambookmarkLinkId, datalink).subscribe(
      data => {
        console.log(data);

        this.getbookmarklink();

      },
      err => {

      }
    );


  }



  update(id: number, event: any) {


    const datalink = {

      link: this.editField,
      bookmark_id: this.bookmarklinklist[id].bookmarkCategoryId,

      userId: this.tokenStorage.getUser().id
    };
    this.teamService.updatebookmarkLink(this.bookmarklinklist[id].bookmarkLinkId, datalink).subscribe(
      data => {
        console.log(data);
        this.getbookmarklink();


      },
      err => {

      }
    );


  }


}


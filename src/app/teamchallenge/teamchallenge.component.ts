import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { TeamService } from '../_services/team.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { TeamChallengeService } from '../_services/teamchallenge.service';
import { NgForm } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-teamchallenge',
  templateUrl: './teamchallenge.component.html',
  styleUrls: ['./teamchallenge.component.css']
})
export class TeamchallengeComponent implements OnInit {
  limitedKeypointList1: any;
  ApproachKeypointList: any;
  ApplimitedKeypointList: any;
  ApplimitedKeypointList1: any;
  teamsolutionNameSelected: any;
  currentteamsolutionid: any;
  currentsolutionname: any;
  soledit: boolean;
  enablesolutionEdit: boolean;
  enablesolutionEditIndex: any;
  teamapproachNameSelected: any;
  currentapproachid: any;
  currentapproachname: any;
  enableapproachEdit: boolean;
  enableapproachEditIndex: any;
  appedit: boolean;


  constructor(private TeamChallengeService: TeamChallengeService, private teamService: TeamService,
    private router: Router,
    private tokenStorage: TokenStorageService,
    private SpinnerService: NgxSpinnerService
  ) { }


  team = {

    teamName: ''
  }

  showKeyModal: boolean;
  TeamSolutionLinkId: number;
  Link: string;
  keyComments: string;
  teamkeypointId: number;
  // solutionCategoryId:number;
  teamshareId: any;
  teamchallenge = {

    teamchallengeName: '',
    description: '',
    userId: 0
  };
  teamsolution = {
    teamsolutionName: '',
    teamchallengeId: 0,
    userId: 0
  };
  teamapproach = {
    teamapproachName: '',
    teamchallengeId: 0,
    userId: 0
  };
  teamsolutionkeypoint = {

    teamsolutionkeypoints: '',
    teamsolutionLinkId: 0,
    teamsolutionId: 0,
    userId: 0
  };
  solutionkeypoint = {

    teamsolutionkeypoints: '',
    teamsolutionLinkId: 0,
    teamsolutionId: 0,
    userId: 0
  };
  teamapproachkeypoint = {

    teamapproachkeypoints: '',
    teamapproachLinkId: 0,
    teamapproachId: 0,
    userId: 0
  };
  approachkeypoint = {

    teamapproachkeypoints: '',
    teamapproachLinkId: 0,
    teamapproachId: 0,
    userId: 0
  };
  teamsolutionlinks = {

    teamsolutionlink: '',
    teamsolution_id: 0,
    userId: 0
  };

  teamapproachlinks = {

    teamapproachlink: '',
    teamapproach_id: 0,
    userId: 0
  };

  teamsharesolutionlink = {
    teamsolutionId: 0,
    teamsolutionlink: '',
    teamsolutionLinkId: 0,
    teamshareId: 0,
    accesstype: '',
    sharedTime: Date(),
    userId: 0,
    teamchallengeId: 0
  };

  shareteamapproachlink = {
    teamapproachId: 0,
    teamapproachlink: '',
    teamapproachLinkId: 0,
    teamapproachName: '',
    teamshareId: 0,
    accesstype: '',
    sharedTime: Date(),
    userId: 0,
    teamchallengeId: 0
  };

  p: number = 1;
  username: string;
  errorMessage = '';
  searchlink: '';
  isdelete = false;
  sharedTime: number = Date.now();
  sucessmessage: any;
  isLoggedIn = false;
  isSuccessful = false;
  isgetGoal = false;
  teamchallengelist: any = [];
  teamchallengeNameselected: any = {};
  teamchallengeNameselectedId: any = {};
  isgetChallenge = false;
  editField: string;
  enableEdit = false; teamId: number;
  public currentteamchallengeid;
  public currentteamchallengename;
  enableEditIndex = null;
  hrefvisible = true;
  teamchallengeId: number;
  teamsolutionlist: any = [];
  approachlist: any = [];
  teamapproacherrorMessage = ''; teamapproachlistVisible = true; TeamApproachId: number;
  teamapproachNameselectedId: any = {}; teamapproachisSuccessful = false;
  teamchallengeisSuccessful = false; teamsolutionerrorMessage = '';
  teamsolutionisSuccessful = false; teamchallengeerrorMessage = '';
  teamsolutionlistVisible = true; showCollasalIndex = null;
  TeamKeypointId: number;
  teamsolutionlink: string;
  teamsolutionId: number;
  teamsolutionNameselectedId: any = {};
  teamsolutionlinkShareId: number;
  teamsolutionlinkId: number;
  teamapproachlink: string;
  teamapproachId: number;
  approachlinklistVisible = true;
  teamapproachlinkShareId: number;
  teamapproachlinkId: number;
  approachlistVisible = true;
  teamsolutionLinkselected: any = {};
  teamsolutionLinkselectedId: any = {};
  teamsolutionlinkisSuccessful = true;
  teamsolutionlinklist: any = []; KeypointList: any = [];
  teamsolutionlinklistVisible = true; teamsolutionlinkerrorMessage = '';
  teamsolutionlinkeditMode = false;
  limitedKeypointList: any = [];
  limitedKeypointList2: any = [];
  teamapproachLinkId: number;
  teamapproachLinkselected: any = {};
  teamapproachLinkselectedId: any = {};
  teamapproachlinkisSuccessful = true;
  teamapproachlinklist: any = [];
  teamapproachlinklistVisible = true; teamapproachlinkerrorMessage = '';
  teamapproachlinkeditMode = false;
  iskeySuccessful = false;
  teamKeypointId: number;
  showKeypointIndex: null;
  teamlist: any = [];
  isSubmitted = false;
  teamsolutionkeypoints: String;

  shareChallenge = {
    teamchallengeName: '',
    teamchallengeId: 0,
    teamId: 0,
    shareId: 0,
    accessType: '',
    userId: 0,
    sharedTime: Date()
  };
  teamchallengeName: string;


  ngOnInit(): void {
    this.getTeam();
    this.teamchallengeNameselected = "Add CategoryLink";
    // this.getTeamChallenge();
    this.teamsolutionlistVisible = false;
    this.approachlistVisible = false;
    this.teamsolutionlinklistVisible = false;
    this.approachlinklistVisible = false;
    if (this.teamchallengelist == []) {
      this.isgetChallenge = true;
      this.errorMessage = "No Topics"
    }
  }

  /**
   * 
   * Team Rel Functions
   */

  //Get Team
  getTeam() {
    this.teamService
      .getTeamList(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.teamlist = data;
          console.log("202", this.teamlist);
          // this.getTeamChallenge()
        },
        err => {
          this.teamlist = JSON.parse(err.error).message;
        });
  }


  //On Team name select
  onchange(val: any) {
    this.team.teamName = val;
    this.getTeamChallenge();
  }

  //Invite
  AddInviteToTeam() {
    this.isSubmitted = true;
    if (!this.team.teamName) {
      return false;
    } else {
      this.router.navigate(['newteam'], { queryParams: { teamId: this.team.teamName } });
    }
  }

  /**
   * 
   * Challenge Rel Functions 
   * 
   */

  //Add Challenge
  AddTopic(f: NgForm) {
    const data = {
      teamchallengeName: this.teamchallenge.teamchallengeName,
      description: this.teamchallenge.description,
      userId: this.tokenStorage.getUser().id,
      teamId: this.team.teamName
    };
    this.TeamChallengeService.addTopic(data).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        f.resetForm();
        this.getTeamChallenge();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      });
  }

  //Get Challenge
  getTeamChallenge() {
    this.TeamChallengeService
      .getChallenge(this.tokenStorage.getUser().id, this.team.teamName)
      .subscribe(
        data => {
          this.teamchallengelist = data;
          console.log("teamchallengelist", this.teamchallengelist);
          if(this.teamchallengelist.length<=0){
            this.teamchallengeNameselected = "Add CategoryLink";
          }
          else{
            this.getTeamChallengeid(this.teamchallengelist[0], 0);
          }
        },
        err => {
          console.log("274", err.error);
          this.errorMessage = err.error.message;
        });
  }

  //Challenge Search
  changetestValue(event: any) {
    this.editField = event.target.value;
    if (this.editField == "") {
      this.getTeamChallenge();
    }
    else {
      this.findByTeamChallengeName(this.editField);
    }
  }

  //find the challenge
  findByTeamChallengeName(key) {
    this.isSuccessful = true;
    this.enableEdit = false;
    this.TeamChallengeService.findByTeamChallengeName(key,
      this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.teamchallengelist = data;
          console.log("list", data);
        },
        err => {
          this.teamchallengelist = err.error.message;
        });
  }

  //edit challenge name 
  enableEditMethod(e, i) {
    this.currentteamchallengeid = e.teamchallengeId;
    this.currentteamchallengename = e.teamchallengeName;
    this.isSuccessful = true;
    this.enableEdit = true;
    this.enableEditIndex = i;
    console.log(i, e);
    this.hrefvisible = false;
    this.hrefvisible = i;
  }

  //save edited challenge name
  editTeamChallenge(teamchallengeId: number, teamchallengeName: String) {
    if (teamchallengeName == "") {
      this.isSuccessful = false;
    }
    else {
      this.isSuccessful = true;
      this.enableEdit = true;
      const data = {
        teamchallengeName: teamchallengeName,
        userId: this.tokenStorage.getUser().id
      };

      this.TeamChallengeService.updateteamchallenge(teamchallengeId, data).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          this.getTeamChallenge();
          this.enableEdit = false;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );

    }
  }

  //Show del challenge 
  showdelTeamChallengeModal(selectedItem: any): void {
    $("#mydelTeamChallengeModal").modal('show');
    this.teamchallengeId = selectedItem.teamchallengeId;
  }

  //Delete challenge
  deleteTeamChallenge() {
    this.teamchallengelist = [];
    this.TeamChallengeService.deleteTeamChallenge(this.teamchallengeId)
      .subscribe(
        data => {
          this.reloadData();
        },
        error => console.log("573", error));
  }

  //Reload challenge list
  reloadData() {
    this.teamchallengelist = this.getTeamChallenge();
    this.getTeamChallengeSolution();
    this.getTeamChallengeApproach();
  }


  /**
   * 
   * Solution rel functions 
   * 
   */

  //Add solution
  AddTeamSolution(fl:NgForm) {
    if (this.teamchallengeNameselected == "Add teamsolution") {
      this.errorMessage = "*Please Select TeamChallenge";
    }
    else {
      const datalink = {
        teamsolutionName: this.teamsolution.teamsolutionName,
        teamchallengeId: this.teamchallengeNameselectedId,
        teamId: this.team.teamName,
        userId: this.tokenStorage.getUser().id
      };
      this.TeamChallengeService.AddTeamSolution(datalink).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = data;
          this.isLoggedIn = true;
          this.isSuccessful = true;
          fl.resetForm();
          this.getTeamChallengeSolution();
          console.log("added");
        },
        err => {
          this.errorMessage = '';
          this.teamchallengeerrorMessage = err.error.message;
          this.teamchallengeisSuccessful = false;
        });
    }
  }

  //Get Solutions
  getTeamChallengeSolution() {
    this.TeamChallengeService.getTeamChallengeSolution(this.teamchallengeNameselectedId)
      .subscribe(
        data => {
          this.teamsolutionlist = data;
          console.log(data);
          // this.opentesttoggle(data[0],0); 
          this.teamsolutionlistVisible = true;
        },
        err => console.error(err),
        () => console.log('getsolution completed')
      );
  }


  //change value solution link
  changeValue(id: number, property: string, event: any) {
    this.editField = event.target.textContent;
  }

  //solution team solutionid
  getTeamSolutionId(solution,i){
    this.teamsolutionNameselectedId = solution.teamsolutionId;
    this.teamsolutionNameSelected = solution.teamsolutionName;
  }

  //enable solution edit
  enablesolutionEditMethod(solution,id){
    this.currentteamsolutionid = solution.teamsolutionId;
    this.currentsolutionname = solution.teamsolutionName;
    this.soledit = true;
    this.enablesolutionEdit = true;
    this.enablesolutionEditIndex = id;
    console.log("468", solution);
  }

  //edit solution
  editSolution(solutionid,solutionname){
    if(solutionname == ''){
      this.isSuccessful = false;
      this.soledit = false;
    }
    else{
      const data = {
        teamsolutionName : solutionname,
        userId : this.tokenStorage.getUser().id
      };
      console.log("482", data);
      this.TeamChallengeService.updateSolution(solutionid,data)
          .subscribe(
            data =>{
              console.log(data);
              this.isSuccessful = true;
              this.sucessmessage = data;
              this.enablesolutionEdit = false;
              this.getTeamChallengeSolution();
            },
            err =>{
              this.errorMessage = err.error.message;
              this.isSuccessful = false;
            });
    }
  }

  //show delete solution modal
  showdelSolutionModal(solution){
    $("mydelSolutionModal").modal('show');
    this.teamsolutionId = solution.teamsolutionId;
    console.log("502",solution);
  }

  //delete solution
  deletesolution(){
    this.TeamChallengeService.deleteSolution(this.teamsolutionId)
        .subscribe(
          data =>{
            console.log(data);
            this.isSuccessful = true;
            this.sucessmessage = data;
            document.getElementById('solution-modal').click();
            this.getTeamChallengeSolution();
          }
        )
  }

  //Add Solution link
  AddTeamSolutionLink(teamsolutionId, teamsolutionlink, i,ff:NgForm) {
    const datalink = {
      teamsolutionlink: teamsolutionlink,
      teamsolutionId: teamsolutionId,
      teamchallengeId: this.teamchallengeNameselectedId,
      teamId: this.team.teamName,
      userId: this.tokenStorage.getUser().id
    };
    this.TeamChallengeService.AddTeamSolutionLink(datalink).subscribe(
      data => {
        this.showCollasalIndex = i;
        this.teamsolutionId = teamsolutionId;
        ff.resetForm();
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.teamsolutionlinkisSuccessful = true;
        this.getTeamChallengeSolution();
        this.getteamsolutionlink(teamsolutionId);
        //  this.getsolutionlink(this.solutionId);
      },
      err => {
        this.errorMessage = '';
        this.teamsolutionlinkerrorMessage = err.error.message;
        this.teamsolutionlinkisSuccessful = false;
      }
    );
  }

  //Get Solution links
  getteamsolutionlink(teamsolutionId: number) {
    this.TeamChallengeService
        .getteamsolutionlink(teamsolutionId)
        .subscribe(
        data => {
          this.teamsolutionlinklist = data;
          console.log(data);
          // this.opentesttoggle(data[0],0); 
          this.teamsolutionlinklistVisible = true;
        },
        err => console.error(err),
        () => console.log('getTeamSolutionlink completed')
      );
  }

  //On select solution key
  onSelectKeypoint(selectedItem: any) {
    this.iskeySuccessful= false;
    $("#myModal").modal('show');
    this.keyComments = selectedItem.teamsolutionkeypoints;
    console.log("Selected item Id: ", selectedItem);
    this.teamsolutionlinkId = selectedItem.teamsolutionlinkId;// You get the Id of the selected item here
    this.showKeyModal = true;
    //this.getkeypoint(this.BookmarkLinkId);
  }

  //Add solution keyppoints
  Addteamsolutionkeypoints(teamsolutionlinkId, teamsolutionId,fAddKey:NgForm) {
    const dataKey = {
      teamsolutionkeypoints: this.teamsolutionkeypoint.teamsolutionkeypoints,
      teamsolutionlinkId: this.teamsolutionlinkId,
      teamsolutionId: teamsolutionId,
      teamId: this.team.teamName,
      userId: this.tokenStorage.getUser().id,
      teamchallengeId: this.teamchallengeNameselectedId
    };
    this.TeamChallengeService.addteamsolutionkeypoints(dataKey).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = "Comments  Added";
        this.isLoggedIn = true;
        this.iskeySuccessful = true;
        fAddKey.resetForm();
        document.getElementById("closesolkey").click();
        this.getteamsolutionkeypoint(this.teamsolutionlinkId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      });
  }

  //Get solution key
  getteamsolutionkeypoint(teamsolutionlinkId: number) {
    this.KeypointList = [];
    this.limitedKeypointList=[];
    this.TeamChallengeService.getteamsolutionkeypoint(teamsolutionlinkId)
      .subscribe(
        data => {
          this.KeypointList = data;
          console.log("teamsolutionkeypoints", this.KeypointList);
          if(this.KeypointList.length>0){
            this.limitedKeypointList = this.KeypointList.slice(0, 3)
            console.log("sliced data", this.limitedKeypointList);
          }
        },
        err => {
          return console.error(err.error.message);
        }
        // () => console.log('getKeypointList completed') 
      );
  }

  //Get solution share modal
  showsolutionShareModal(selectedItem: any): void {
    this.isSuccessful = false;
    $("#solutionshareModal").modal('show');
    this.teamsolutionlink = selectedItem.teamsolutionlink;
    // console.log("Selected item Id: ", selectedItem.Id); 
    this.teamsolutionlinkId = selectedItem.teamsolutionlinkId;// You get the Id of the selected item here
    this.teamsolutionId = selectedItem.teamsolutionId;
    this.teamsharesolutionlink.teamchallengeId = selectedItem.teamchallengeId;
    console.log("561", selectedItem);
  }

  //Change value
  onchangeAccessType(val: any) {
    this.teamsharesolutionlink.accesstype = val;
  }

  //Share Solution link
  shareTeamSolution(teamsolutionlinkId,fshare:NgForm) {
    if(!this.teamsharesolutionlink.accesstype){
      this.isSuccessful= false;
    }
    else{
      this.teamsolutionlinkId = teamsolutionlinkId;
      const datalink = {
        teamsolutionId: this.teamsolutionId,
        teamsolutionlinkId: teamsolutionlinkId,
        teamsolutionlink: this.teamsolutionlink,
        teamId: this.team.teamName,
        userId: this.tokenStorage.getUser().id,
        accesstype: this.teamsharesolutionlink.accesstype,
        sharedTime: new Date().toDateString(),
        teamchallengeId : this.teamsharesolutionlink.teamchallengeId
      };
      console.log("solution",datalink);
      this.TeamChallengeService.addteamsharesolution(datalink).subscribe(
        (data: any) => {
          console.log(data);
          document.getElementById("shareclose").click();
          this.teamsharesolutionlink.accesstype='';
          fshare.resetForm();
          this.isSuccessful = true;
          // this.getsolutionlink()
        },
        (err: any) => {
          this.isSuccessful = false;
          console.log(err.error.message);
        });
    }
  }

  //Show solution del model
  showsolutionModal(selectedItem: any): void {
    $("#mydelsolutionModal").modal('show');
    this.teamsolutionlink = selectedItem.teamsolutionlink;
    console.log("Selected item Id: ", selectedItem);
    this.teamsolutionId = selectedItem.teamsolutionId;
    this.teamsolutionlinkId = selectedItem.teamsolutionlinkId;// You get the Id of the selected item here
  }

  //Delete solution link
  deleteTeamSolutionlink() {
    this.TeamChallengeService.deleteTeamSolutionlink(this.teamsolutionlinkId)
      .subscribe(
        data => {
          console.log(data);
          this.isdelete = true;
          this.sucessmessage = "Link is deleted";
          this.getTeamChallengeSolution();
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSuccessful = false;
        }
      );
  }
  hideModal(): void {
    document.getElementById('close-modal').click();
  }

  //open comments toggle
  opencommentstoggle(selectedItem: any, i) {
    this.showKeypointIndex = i;
    this.teamsolutionlinkId = selectedItem.teamsolutionlinkId;
    this.getteamsolutionkeypoint(this.teamsolutionlinkId);
  }

  //close comments toggle
  closecommentstoggle(selectedItem: any, i){
    this.showKeypointIndex = null;
    this.teamsolutionlinkId = selectedItem.teamsolutionlinkId;
  }

  //show edit key model
  showEditKeyModal(selectedItem: any): void {
    $("#myEditKeyPointModal").modal('show');
    this.teamsolutionlinkId = selectedItem.teamsolutionlinkId;
    this.solutionkeypoint.teamsolutionkeypoints = selectedItem.teamsolutionkeypoints;
    this.teamkeypointId = selectedItem.teamkeypointId;
    this.teamsolutionId = selectedItem.teamsolutionId;
    this.showKeyModal = true;
    console.log("652", selectedItem);
  }

  //Prevent
  prevent(teamsolutionkeypoints: string) {
    event.preventDefault();
    this.EditKeypoint(teamsolutionkeypoints);
  }
  
  //edit key
  EditKeypoint(teamsolutionkeypoints: String) {
    if(teamsolutionkeypoints ===""){
      this.iskeySuccessful = false;
    }
    else{
      const dataKey = {
        teamsolutionkeypoints: teamsolutionkeypoints,
        teamsolutionlinkId: this.teamsolutionlinkId,
        teamsolutionId: this.teamsolutionId,
        userId: this.tokenStorage.getUser().id
      };
      this.TeamChallengeService.updateKeypoint(this.teamkeypointId, dataKey).subscribe(
        data => {
          console.log(data);
          this.sucessmessage = "Comments  Edited";
          this.iskeySuccessful = true;
          document.getElementById("solkeymodal").click();
          this.getteamsolutionkeypoint(this.teamsolutionlinkId);
        },
        err => {
          this.errorMessage = err.error.message;
          this.iskeySuccessful = false;
        });
    }
  }


  //show del keymodal
  showdelKeyModal(selectedItem: any): void {
    $("#mydelKeyModal").modal('show');
    this.solutionkeypoint.teamsolutionkeypoints = selectedItem.teamsolutionkeypoint;
    this.teamkeypointId = selectedItem.teamkeypointId;
    this.teamsolutionlinkId = selectedItem.teamsolutionlinkId;// You get the Id of the selected item here
  }

  //Delete keypoint
  deleteKeypoint() {
    this.TeamChallengeService.deleteKeypoint(this.teamkeypointId)
      .subscribe(
        data => {
          console.log(data);
          this.getteamsolutionkeypoint(this.teamsolutionlinkId);
          //  this.getapproachkeypoint(this.approachlinkId);
        },
        error => console.log(error));
    // this.getteamsolutionkeypoint(this.teamsolutionlinkId);
    // this.getapproachkeypoint(this.approachlinkId);
  }


  //view more keypoints
  onsolKeyPointsViewMore() {
    this.limitedKeypointList1 = this.KeypointList.slice(3, this.KeypointList.length);
    this.limitedKeypointList = this.limitedKeypointList.concat(this.limitedKeypointList1);
    $('#view-more').hide();
    $('#view-less').show();
  }

   //view less keypoints
   onsolkeyPointsviewless() {
    this.limitedKeypointList = this.KeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }


/**
 * 
 * Approach rel functions 
 * 
 */

//Add Approach
AddTeamApproach(f2:NgForm) {
  if (this.teamchallengeNameselected == "Add teamapproach") {
    this.errorMessage = "*Please Select teamChallenge";
  }
  else {
    const datalink = {
      teamapproachName: this.teamapproach.teamapproachName,
      teamchallengeId: this.teamchallengeNameselectedId,
      teamId: this.team.teamName,
      userId: this.tokenStorage.getUser().id
    };
    this.TeamChallengeService.addApproach(datalink).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        f2.resetForm();
        this.getTeamChallengeApproach();
        console.log("added");
      },
      err => {
        this.errorMessage = '';
        this.teamchallengeerrorMessage = err.error.message;
        this.teamchallengeisSuccessful = false;
      });
  }
}

//Get approach
getTeamChallengeApproach() {
  this.TeamChallengeService.getApproach(this.teamchallengeNameselectedId)
    .subscribe(
      data => {
        this.approachlist = data;
        console.log(data);
        // this.opentesttoggle(data[0],0); 
        this.teamapproachlistVisible = true;
      },
      err => console.error(err),
      () => console.log('getteamapproach completed')
    );
}

//get approach id
getApproachId(approach,id){
  this.teamapproachNameselectedId = approach.teamapproachId;
  this.teamapproachNameSelected = approach.approachName;
}

//Enable approach edit
enableapproachEditMethod(approach,id){
  this.currentapproachid = approach.teamapproachId;
  this.currentapproachname = approach.teamapproachName;
  this.appedit = true;
  this.enableapproachEdit = true;
  this.enableapproachEditIndex = id;
}

//Edit approach
editApproach(approachid,approachname){
  if(approachname == ''){
    this.isSuccessful = false;
    this.appedit = false;
  }
  else{
    const data = {
      teamapproachName : approachname,
      userId : this.tokenStorage.getUser().id
    };
    this.TeamChallengeService.updateApproach(approachid,data)
        .subscribe(
          data =>{
            console.log(data);
            this.sucessmessage = data;
            this.isSuccessful = true;
            this.enableapproachEdit = false;
            this.getTeamChallengeApproach();
          },
          err =>{
            this.errorMessage = err.error.message;
            this.isSuccessful = false;
          });
  }
}

//show delete approach modal
showdelapproachModal(approach){
  $("mydelapproachModal").modal('show');
  this.teamapproachId = approach.teamapproachId;
  console.log("844",approach);
}

  //delete approach
  deleteapproach(){
    this.TeamChallengeService.deleteApproach(this.teamapproachId)
        .subscribe(
          data =>{
            console.log(data);
            this.isSuccessful = true;
            this.sucessmessage = data;
            document.getElementById('approach-modal').click();
            this.getTeamChallengeApproach();
          },
          err =>{
            this.errorMessage = err.error.message;
            this.isSuccessful = false;
          });
  }

//Add Team Appraoch link
AddTeamApproachLink(teamapproachId, teamapproachlink, i, ffl:NgForm) {
  const datalink = {
    teamapproachlink: teamapproachlink,
    teamapproachId: teamapproachId,
    teamchallengeId: this.teamchallengeNameselectedId,
    teamId: this.team.teamName,
    userId: this.tokenStorage.getUser().id
  };
  this.TeamChallengeService.addapproachlink(datalink).subscribe(
    data => {
      this.showCollasalIndex = i;
      this.TeamApproachId = teamapproachId;
      ffl.resetForm();
      console.log(data);
      this.sucessmessage = data;
      this.isLoggedIn = true;
      this.teamapproachlinkisSuccessful = true;
      this.getTeamChallengeApproach();
      this.getTeamApproachlink(teamapproachId);
      //this. getapproachlink();
    },
    err => {
      this.errorMessage = '';
      this.teamapproachlinkerrorMessage = err.error.message;
      this.teamapproachlinkisSuccessful = false;
    });
}


//Get approach link
getTeamApproachlink(teamapproachId) {
  this.TeamChallengeService.getApproachlink(teamapproachId)
    .subscribe(
      data => {
        this.teamapproachlinklist = data;
        console.log(data);
        // this.opentesttoggle(data[0],0); 
        this.teamapproachlinklistVisible = true;
      },
      err => console.error(err),
      () => console.log('getTeamApproachlink completed')
    );
}

//On select approach keypoint
onSelectedKeypoint(selectedItem: any) {
  this.iskeySuccessful= false;
  $("#myApproachModal").modal('show');
  this.keyComments = selectedItem.teamapproachkeypoints;
  this.teamapproachlinkId = selectedItem.teamapproachlinkId;// You get the Id of the selected item here
  this.showKeyModal = true;
}

//Add approach keypoint
Addteamapproachkeypoints(teamapproachlinkId, teamapproachId,flAddKey:NgForm) {
  const dataKey = {
    teamapproachkeypoints: this.teamapproachkeypoint.teamapproachkeypoints,
    teamapproachlinkId: this.teamapproachlinkId,
    teamapproachId: teamapproachId,
    teamId: this.team.teamName,
    userId: this.tokenStorage.getUser().id,
    teamchallengeId: this.teamchallengeNameselectedId
  };
  this.TeamChallengeService.addapproachkeypoints(dataKey).subscribe(
    data => {
      console.log(data);
      this.sucessmessage = "Comments  Added";
      this.isLoggedIn = true;
      this.iskeySuccessful = true;
      flAddKey.resetForm();
      document.getElementById("closeappkey").click();
      this.getteamapproachkeypoint(this.teamapproachlinkId);
    },
    err => {
      this.errorMessage = err.error.message;
      this.iskeySuccessful = false;
    }
  );
}

//Get Approach keypoint
getteamapproachkeypoint(teamapproachlinkId: number) {
  this.ApproachKeypointList = [];
  this.ApplimitedKeypointList = [];
  this.TeamChallengeService.getapproachkeypoint(teamapproachlinkId)
    .subscribe(
      data => {
        this.ApproachKeypointList = data;
        if(this.ApproachKeypointList.length>0){
          this.ApplimitedKeypointList = this.ApproachKeypointList.slice(0, 3)
        }
      },
      err => {
        return console.error(err);
      }
      // () => console.log('getKeypointList completed') 
    );
}

//show approach share model
showSharedModal(selectedItem: any): void {
  this.isSuccessful = false;
  $("#shareApproachModal").modal('show');
  this.teamapproachlinks = selectedItem.teamapproachlink;
  this.teamapproachlinkId = selectedItem.teamapproachlinkId;// You get the Id of the selected item here
  this.TeamApproachId = selectedItem.teamapproachId;
  this.teamId = selectedItem.teamId;
  this.shareteamapproachlink.teamchallengeId = selectedItem.teamchallengeId;
  console.log("887", selectedItem);
}

//on change
onchangeAccessTypes(val: any) {
  this.shareteamapproachlink.accesstype = val;
}

//share Approach link
shareApproach(teamapproachlinkId,flshare:NgForm) {
  if(!this.shareteamapproachlink.accesstype){
    this.isSuccessful= false;
  }
  else{
    this.teamapproachlinkId = teamapproachlinkId;
    const datalink = {
      teamapproachId: this.TeamApproachId,
      teamapproachlinkId: teamapproachlinkId,
      teamapproachlink: this.teamapproachlinks,
      userId: this.tokenStorage.getUser().id,
      accesstype: this.shareteamapproachlink.accesstype,
      teamId: this.team.teamName,
      sharedTime: new Date().toDateString(),
      teamchallengeId :this.shareteamapproachlink.teamchallengeId
    };

    console.log("913", datalink);
    this.TeamChallengeService.addshareapproach(datalink).subscribe(
      (data: any) => {
        // this.getsolutionlink()
        document.getElementById("sharedclose").click();
        this.shareteamapproachlink.accesstype='';
        flshare.resetForm();
        this.isSuccessful = true;
      },
      (err: any) => {
        this.isSuccessful = false;
      });
  }
}

//Show del approach model
showedModal(selectedItem: any): void {
  $("#mydelapproachModal").modal('show');
  this.teamapproachlink = selectedItem.teamapproachlink;
  console.log("Selected item Id: ", selectedItem);
  this.teamapproachId = selectedItem.teamapproachId;
  this.teamapproachlinkId = selectedItem.teamapproachlinkId;// You get the Id of the selected item here
}

//Delete approach link
deleteApproachlink() {
  this.TeamChallengeService.deleteApproachlink(this.teamapproachlinkId)
    .subscribe(
      data => {
        console.log(data);
        this.isdelete = true;
        this.sucessmessage = "Link is deleted";
        this.getTeamChallengeApproach();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      });
}

//open approach keypoints
openedcommentstoggle(selectedItem: any, i) {
  this.showKeypointIndex = i;
  this.teamapproachlinkId = selectedItem.teamapproachlinkId;
  this.getteamapproachkeypoint(this.teamapproachlinkId);
}

//close approach keypoints
closedcommentstoggle(selectedItem: any, i) {
  this.showKeypointIndex = null;
  this.teamapproachlinkId = selectedItem.teamapproachlinkId;
  // this.getteamapproachkeypoint(this.teamapproachlinkId);
}


//show edit approach key model
showedEditKeyModal(selectedItem: any): void {
  $("#myEditKeyPointModal").modal('show');
  this.teamapproachlinkId = selectedItem.teamapproachlinkId;
  this.approachkeypoint.teamapproachkeypoints = selectedItem.teamapproachkeypoints;
  this.teamkeypointId = selectedItem.teamkeypointId;
  this.TeamApproachId = selectedItem.teamapproachId;
  this.showKeyModal = true;
  console.log("976", selectedItem);
}

//prevent 
prevented(teamapproachkeypoints: string) {
  event.preventDefault();
  this.EditedKeypoint(teamapproachkeypoints);
}

//Edit keypoint
EditedKeypoint(teamapproachkeypoints: String) {
  if(teamapproachkeypoints === ""){
    this.iskeySuccessful = false;
  }
  else{
    const dataKey = {
      teamapproachkeypoints: teamapproachkeypoints,
      teamapproachlinkId: this.teamapproachlinkId,
      teamapproachId: this.teamapproachId,
      userId: this.tokenStorage.getUser().id,
      teamId: this.team.teamName,
      teamchallengeId: this.teamchallengeNameselectedId
    };

    this.TeamChallengeService.updateKeypoints(this.teamkeypointId, dataKey).subscribe(
      data => {
        console.log("655",data);
        this.sucessmessage = "Comments  Edited";
        this.iskeySuccessful = true;
        document.getElementById("appkeymodal").click();
        this.getteamapproachkeypoint(this.teamapproachlinkId);
      },
      err => {
        this.errorMessage = err.error.message;
        this.iskeySuccessful = false;
      });
    }
  }


//show approach del model
showeddelKeyModal(selectedItem: any): void {
  $("#mydelKeyModal").modal('show');
  this.approachkeypoint.teamapproachkeypoints = selectedItem.teamapproachkeypoints;
  this.teamkeypointId = selectedItem.teamkeypointId;
  this.teamapproachlinkId = selectedItem.teamapproachlinkId;// You get the Id of the selected item here
  console.log("1006", selectedItem);
}


//Delete appraoch keypoint
deletedKeypoint() {
  console.log("1012",this.teamkeypointId);
  this.TeamChallengeService.deleteKeypoints(this.teamkeypointId)
    .subscribe(
      data => {
        console.log(data);
        this.getteamapproachkeypoint(this.teamapproachlinkId);
      },
      error => console.log(error));
}


 //view more keypoints
 onappKeyPointsViewMore() {
  this.ApplimitedKeypointList1 = this.ApproachKeypointList.slice(3, this.ApproachKeypointList.length);
  this.ApplimitedKeypointList = this.ApplimitedKeypointList.concat(this.ApplimitedKeypointList1);
  $('#view-more').hide();
  $('#view-less').show();
}

 //view less keypoints
 onappkeyPointsviewless() {
  this.ApplimitedKeypointList = this.ApproachKeypointList.slice(0, 3);
  $('#view-more').show();
  $('#view-less').hide();
}





  buttonCol(selectedItem: any) {
    this.SpinnerService.show();
    this.TeamSolutionLinkId = selectedItem.teamsolutionLinkId;// You get the Id of the selected item here
  }
  openNav(selectedItem: any) {
    //  document.getElementById("collapseExample").style.width = "250px";
    this.TeamSolutionLinkId = selectedItem.teamsolutionLinkId;// You get the Id of the selected item here
  }
  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
  hide() {
    this.showKeyModal = false;
  }
  opentoggle(id: number, property: string, event: any) {
    $("#slideme").slideToggle();
  }
  showChallengeShareModal(selectedItem: any): void {
    this.isSuccessful = false;
    $("#shareModal").modal('show');
    this.teamchallengeName = selectedItem.teamchallengeName;
    console.log("Selected item Id: ", selectedItem.challengeId);
    this.teamchallengeId = selectedItem.teamchallengeId;// You get the Id of the selected item here
  }

  onchangeShareAccessType(val: any) {
    this.shareChallenge.accessType = val;
  }

  shareChallengeName(teamchallengeId: number,) {
    this.teamchallengeId = teamchallengeId;
    const datalink = {
      teamchallengeId: this.teamchallengeId,
      teamchallengeName: this.teamchallengeName,
      userId: this.tokenStorage.getUser().id,
      accesstype: this.shareChallenge.accessType,
      teamId: this.teamId,
      sharedTime: new Date().toDateString()
    };
    this.TeamChallengeService.addteamChallengeshare(datalink).subscribe(
      (data: any) => {
        console.log(data);
        this.getTeamChallenge();
      },
      (err: any) => {
      }
    );

  }
  getTeamChallengeid(teamchallengelist: any, i: any) {
    this.teamchallengeNameselectedId = teamchallengelist.teamchallengeId;
    this.teamchallengeNameselected = teamchallengelist.teamchallengeName;
    this.getTeamChallengeSolution();
    this.getTeamChallengeApproach();
    // this.getsolutionlink(); 
  }  
  // deleteSolutionlinkByCategory(challengeId)
  // {

  // this.ChallengeService.deleteSolutionlinkByCategory(challengeId)
  // .subscribe(
  //   data => {
  //     console.log(data);
  //    this. isdelete=true;
  //     this.sucessmessage="Link is deleted";

  //   },
  //   err => {
  //     this.errorMessage = err.error.message;
  //     this.isSuccessful=false;
  //   }
  // );
  // }
  linkisSuccessful = false; linkerrorMessage = '';
  // opentesttoggle(selectedItem:any,i)
  //   {


  //      this.showCollasalIndex=i;
  //      this.solutionId =selectedItem.solutionId;

  //      this.getsolutionlink(this.solutionId );



  //   }
  onKeyPointsViewMore(): void {
    this.limitedKeypointList2 = this.KeypointList.slice(3, this.KeypointList.length);

    this.limitedKeypointList = this.limitedKeypointList.concat(this.limitedKeypointList2);
    console.log(this.limitedKeypointList);
    console.log(this.KeypointList.length, this.limitedKeypointList.length);
    $('#view-more').hide();
    $('#view-less').show();
  }

  onkeyPointsviewless(): void {
    this.limitedKeypointList = this.KeypointList.slice(0, 3);
    $('#view-more').show();
    $('#view-less').hide();
  }
  findBySolutionLink() {
    this.router.navigate(['searchviewpage'], { queryParams: { link: this.searchlink } });
  }
  findByApproachLink() {
    this.router.navigate(['searchviewpage'], { queryParams: { link: this.searchlink } });
  }
  editSolutionLink(teamsolutionLinkId: number, Link: String) {
    const datalink = {
      link: this.teamsolutionlinks.teamsolutionlink,
      teamsolutionId: this.teamchallengeNameselectedId,
      userId: this.tokenStorage.getUser().id
    };
    this.TeamChallengeService.updatesolutionLink(teamsolutionLinkId, datalink).subscribe(
      data => {
        console.log(data);
        this.sucessmessage = data;
        this.isLoggedIn = true;
        this.isSuccessful = true;
        //this.getsolutionlink();
        this.enableEdit = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessful = false;
      }
    );
  }
  // updateList(id: number, property: string, event: any) {

  // const editField = event.target.textContent;
  // this.solutionlinklist[id][property] = editField;
  // const datalink = {

  //   link: this. editField,
  //   solutionCategoryId:this.solutionlinklist[id].solutionCategoryId,
  //   userId:this.tokenStorage.getUser().id
  // };
  //   this.ChallengeService.updatesolutionLink(this.solutionlinklist[id].solutionLinkId,datalink).subscribe(
  //     data => {
  //       console.log(data);

  //  //this.getsolutionlink();

  // },
  // err => {

  // }
  // );
  // alert("updated");

  // }


}

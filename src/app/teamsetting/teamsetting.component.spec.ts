import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsettingComponent } from './teamsetting.component';

describe('TeamsettingComponent', () => {
  let component: TeamsettingComponent;
  let fixture: ComponentFixture<TeamsettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamsettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

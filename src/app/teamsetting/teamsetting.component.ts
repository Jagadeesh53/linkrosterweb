import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import {TeamService} from '../_services/team.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';declare var $:any;
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-teamsetting',
  templateUrl: './teamsetting.component.html',
  styleUrls: ['./teamsetting.component.css']
})
export class TeamsettingComponent implements OnInit {
  form: any = {};teamMemberlist:any[];Statusmessage:String;teamList:any[];InviteList:any[];
  invitationlistempty=false;sucessmessage="";
  constructor(private teamService: TeamService,  private router: Router,
    private route: ActivatedRoute,
    private tokenStorage:TokenStorageService,private authservice:AuthService,private SpinnerService: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.SpinnerService.show();
    this.form.teamId = this.route.snapshot.queryParamMap.get('teamId');
    if(this.form.teamId){
      this.getteamMemberlist(this.form.teamId);
       this.getteamname(this.form.teamId);
      }
      this.showInviteList();
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.SpinnerService.hide();
    },2000);

  }

  getteamMemberlist(teamId:number)
  {
    this.teamService.getallTeamInviteMember(this.tokenStorage.getUser().id,teamId)
     .subscribe(data=>{
       this.teamMemberlist=data
       
       for(var i = 0; i < this.teamMemberlist.length; i++)
  { 
    if(this.teamMemberlist[i].status==0){
     
      this. Statusmessage="Pending"}
    
      console.log(this.teamMemberlist[i].status); 
      

  }
  
    
  });
  }
getteamname(teamId)
 {
   
   this.teamService.getTeamdetail(this.tokenStorage.getUser().id,teamId)
   .subscribe(data=>{this.teamList=data
  console.log("team",this.teamList)});
}

 
 showInviteList()
 {
  this.InviteList = [];
   this.teamService.showPendingList(this.tokenStorage.getUser().email)
   
   .subscribe(data=>{this.InviteList=data
  
  console.log("invitelist",this.InviteList)
  console.log("len",this.InviteList.length.valueOf());
  if(this.InviteList.length.valueOf()>0)
  {
    
  this.invitationlistempty=true;
  }
  else{
   
  this.invitationlistempty=false;
   }
   
 }
 
 );
}

 AcceptInvite(inviteId:Number,teamId:Number,status:boolean,teamAccessType:String)

 {
  
  this.AddTeamAcceptedMember(teamId,status,teamAccessType);
 
   this.teamService.showAcceptInvitePage(inviteId)
   .subscribe(() => {
  
     console.log("success 95");
     this.router.navigate(['teamsetting'], { queryParams: { teamId: teamId } });
    //  setTimeout(() => {
    //   window.location.reload();
    // },2000);
     //window.location.reload();
     this.showInviteList();
   }, error => {
    
     console.log(error);
    
   })
  
 

 }

  DeclineInvite(inviteId:Number,teamId:Number,status:boolean,teamAccessType:String)
  {
   this.teamService.declineInvite(inviteId,teamId)
   .subscribe((data) => {
     console.log("success 95", data);
     this.router.navigate(['teamsetting'], { queryParams: { teamId: teamId } });

     this.showInviteList();
   }, error => {
    
     console.log(error);
    
   })
  
 

 }

 AddTeamAcceptedMember(teamId:Number,status:boolean,teamAccessType:String)
 {
  
   const TeamMember_data = {
    
     userId:this.tokenStorage.getUser().id,
    
     accessrole:teamAccessType,
   
  
     status:1, 
 
     teamId:  teamId
    
     
   };
  
     this.teamService.addTeamAcceptedMember(TeamMember_data).subscribe(
       
       (data) => {
         console.log(data);     
         
        
      
         
       
        
       },
       err => {
    
       
       }
     );
   
 }
}
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../_model/user';
import { User1Service } from '../_services/user1.service';
import { Router } from '@angular/router';
import { TokenStorageService } from '../_services/token-storage.service';
import {ProfileService} from '../_services/profile.service';
import { NgxSpinnerService } from "ngx-spinner";
// import { EventEmitter } from '@angular/core';
// import { $ } from 'protractor';
@Component({
  selector: 'app-usermodule',
  templateUrl: './usermodule.component.html',
  styleUrls: ['./usermodule.component.css']
})
export class UsermoduleComponent implements OnInit {
 
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = false;
 
  paramSubscription:any;
  form: any = {};
  // users: Observable<User[]>;
  username: any;
  users:any;
    sucessmessage: any;
  isLoggedIn: boolean;
  // isSuccessful: boolean;
  chainrequest: any;
 id:'';
  userid:any;
  currentUser: any;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message:any;
  selecteduserId: any;
  statusverified: any;
  // successmessage="Invite Already Sent"; 
 
    status= null;


  constructor(private userService: User1Service,
    private router: Router,private tokenStorage:TokenStorageService,private profileService:ProfileService,
   private spinner: NgxSpinnerService,
    ) {}

  ngOnInit() {
    
    this.currentUser = this.tokenStorage.getUser();
    this.reloadData() ;
    // this.getList(this);
   
  }
  
  reloadData() {
   this.userService.getList(this.tokenStorage.getUser().id)
    .subscribe(
      data => {
        this.users = data;
        console.log("userlist",data);
        console.log('data',this.users);
        // setTimeout(() => {
        //   /** spinner ends after 5 seconds */
        //   this.spinner.hide();
        // }, 2000);  
      },
      error => {
        console.log(error);
      });




    
  }

 
  UserDetails(id: number){
    this.router.navigate(['details', id]);
  }

  searchName() {
    if(this.username==''){
      this.reloadData();
    } else {
      this.userService.getUsers(this.username,this.tokenStorage.getUser().id)
        .subscribe(
          data => {
            this.users = data;
            console.log(data);
          },
          error => {
            console.log(error);
          });
    }
  }

  
  AddChain(chainuserid) {
    
    const data = {
      // userId:userId,
       chainuserid:chainuserid,
    status:status,
      userid:this.tokenStorage.getUser().id,
      
  

   };
   
     this.userService.addChain(data).subscribe(
       data => {
         console.log(data),"message";  
        //  this.sucessmessage=data;
        this.selecteduserId = chainuserid;
      
        // this.statusverified = status;  alert(this.statusverified);
       
        
         this.isLoggedIn = true;
         this.isSuccessful=true; 
        //  location.reload();
  
          
      },
      err => {
        this.errorMessage = err.error.message;
        location.reload();
      }

     );
     
   
 }

//  getList(id) {

// this.userService.getChainlist(this.tokenStorage.getUser().id)
//   .subscribe(
//     data => { 
//       this.users = data;
//       console.log(data);
    
//     },
//     error => {
//       console.log(error);
//     });
// }

// getList(id)
//  {
//    this.userService.getChainlist(this.tokenStorage.getUser().id)
//    .subscribe(
//      data => { 
//        this.users= data ;  
//        console.log(data);     
//       },
//       err => {
//        this.users = JSON.parse(err.error).message;
//      }       
//      );
//  }

// refreshPage() {
//   this._document.defaultView.location.reload();
// }


}
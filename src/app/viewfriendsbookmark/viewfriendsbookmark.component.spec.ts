import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewfriendsbookmarkComponent } from './viewfriendsbookmark.component';

describe('ViewfriendsbookmarkComponent', () => {
  let component: ViewfriendsbookmarkComponent;
  let fixture: ComponentFixture<ViewfriendsbookmarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewfriendsbookmarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewfriendsbookmarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

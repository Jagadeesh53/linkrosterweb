import { Component, OnInit, Input } from '@angular/core';
import { BookmarkService } from '../_services/bookmark.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { bookmarkuser } from '../_model/bookmarkuser';
import { forkJoin } from 'rxjs';
import { AuthService } from '../_services/auth.service';
import { Observable } from 'rxjs';
import * as Rx from 'rxjs'; import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-viewfriendsbookmark',
  templateUrl: './viewfriendsbookmark.component.html',
  styleUrls: ['./viewfriendsbookmark.component.css']
})
export class ViewfriendsbookmarkComponent implements OnInit {

  bookmarkNameselected: any = {};
  bookmarkNameselectedId: any = {};
  bookmarkLinkselected: any = {};
  bookmarkLinkselectedId: any = {};
  bookmarklinklistVisible = true;
  bookmarkUserList: any = [];
  bookmarklist: any = [];
  friendslist: any = [];
  friendsListRequestModel: any = {};
  bookmarklinklist: any = [];
  bookmarkUserlist: any = [];
  KeypointList: any = [];
  userlist: any = [];
  isSuccessful = false;
  showCollasal = true;
  errorMessage = false;
  showCollasalIndex = null;
  showKeyModal: boolean;
  BookmarkLinkId: number;
  Link: string;
  keyComments: string;
  keypointId: number;
  bookmarkCategoryId: number;
  chainuseridparam: string;
  isShow: boolean = false;
  friend = {

    chainuserid: 0

  };

  constructor(private bookmarkService: BookmarkService, private route: ActivatedRoute,
    private tokenStorage: TokenStorageService, private authservice: AuthService
  ) { }


  opentesttoggle(selectedItem: any, i) {
    this.KeypointList = [];
    console.log("i ", i);
    this.showCollasalIndex = i;
    this.BookmarkLinkId = selectedItem.bookmarkLinkId;
    console.log("bookmarklinkid ", this.BookmarkLinkId);
    this.getkeypoint(this.BookmarkLinkId);
  }

  buttonCol(selectedItem: any) {
    this.BookmarkLinkId = selectedItem.bookmarkLinkId;// You get the Id of the selected item here
    this.getkeypoint(this.BookmarkLinkId);
  }

  ngOnInit(): void {
    this.chainuseridparam =
      this.route.snapshot.queryParamMap.get('chainuserid').toString();
    this.getbookmarkbyUser(this.chainuseridparam);
    this.bookmarkNameselected = "Add CategoryLink";
    // this.getbookmark();
    this.getfriendlist();
    let friendlist = this.bookmarkService
      .getfriendlist(this.tokenStorage.getUser().id);
    const friendlist$ = this.bookmarkService
      .getfriendlist(this.tokenStorage.getUser().id);
    this.friendsListRequestModel = this.friendslist;
    console.log("userid", this.friendsListRequestModel)
    this.getAlluser();
  }


  getAlluser() {
    console.log('--- delete ---- ' + JSON.stringify(this.friendslist));
    this.authservice.getAllUserlist(this.friendslist)
      .subscribe(
        data => {
          this.userlist = data
          console.log("userlsit", this.friendslist);
        },
        err => console.error(err),
        () => console.log('getBookarklink completed')
      );
  }

  getfriendlist() {
    this.bookmarkService
      .getfriendlist(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.friendslist = data
          console.log("friends", this.friendslist);
          this.authservice.getAllUserlist(this.friendslist)
            .subscribe(
              userdata => {
                this.userlist = userdata
                console.log("userlsit", userdata);
                console.log("testing", data);

              },
              err => console.error(err),
              () => console.log('getBookarklink completed')
            );

        },
        err => console.error(err),
        () => console.log('getBookarklink completed')
      );

  }

  getbookmarkbyUser(chainuseridparam) {
    this.bookmarkService
      .getBookmark(chainuseridparam)
      .subscribe(
        data => {
          this.bookmarklist = data
          console.log("testing", data);
        },
        err => {
          this.bookmarklist = JSON.parse(err.error).message;
        }
      );
  }

  getbookmark() {
    this.bookmarkService
      .getBookmark(this.tokenStorage.getUser().id)
      .subscribe(
        data => {
          this.bookmarklist = data
        },
        err => {
          this.bookmarklist = JSON.parse(err.error).message;
        }
      );
  }

  getBookmarkid(bookmarklist: any) {
    this.isShow = true;
    this.bookmarkNameselectedId = bookmarklist.bookmarkId;
    this.bookmarkNameselected = bookmarklist.bookmarkName;
    this.getbookmarklink();
  }

  getbookmarklink() {
    this.bookmarkService.getBookmarklink(this.bookmarkNameselectedId)
      .subscribe(
        data => {
          this.bookmarklinklist = data
          console.log("bookmark ", data);
          this.bookmarklinklistVisible = true;
        },
        err => console.error(err),
        () => console.log('getBookarklink completed')
      );
  }

  getkeypoint(BookmarkLinkId: number) {
    this.bookmarkService
      .getKeypoint(BookmarkLinkId)
      .subscribe(
        data => {
          this.KeypointList = data;
          console.log("data ", data);
        },
        err => console.error(err),
        () => console.log('getKeypointList completed')
      );
  }
}
